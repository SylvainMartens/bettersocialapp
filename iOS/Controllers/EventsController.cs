﻿using System;

namespace SocialAPP
{
    public static class EventsController
    {
        public static EventHandler Log;//Implemented

        public static EventHandler OnReturnToHome;//Implemented

        //Connection
        public static EventHandler OnConnected;
        public static EventHandler OnDisconnected;

        //Lobby
        public static EventHandler OnGetVersion;//Implemented

        public static EventHandler OnGetLoginResult;//Implemented
        public static EventHandler OnGetRegisterResult;//Implemented
        public static EventHandler OnGetRecoverResult;//Implemented
        public static EventHandler OnGetAccountInfos;//Implemented

        //Edits
        public static EventHandler OnGetEditAccount;//Implemented
        public static EventHandler OnGetEditAccountResult;//Implemented
        public static EventHandler OnProfileEdited;//Implemented
        public static EventHandler OnGetEditProfileData;//Implemented
        public static EventHandler OnGetUploadProfilePictureToken;//Implemented

        //Profile
        public static EventHandler OnGetProfileData;//Implemented
        public static EventHandler OnGetProfilePictures;//Implemented
        public static EventHandler OnGetProfileFollowers;//Implemented
        public static EventHandler OnGetProfileFollowings;//Implemented

        //Messages
        public static EventHandler OnLoadConversationsResult;//Implemented
        public static EventHandler OnLoadMessagesResult;//Implemented
        public static EventHandler OnReceiveMessage;//Implemented
        public static EventHandler OnMessageSeen;//Implemented
        public static EventHandler OnConversationStarted;//Implemented

        //Search
        public static EventHandler OnGetSearchResults;//Implemented

        //Notifications
        public static EventHandler OnGetNotifications;//Implemented
        public static EventHandler OnGetNotification;//Implemented

        //Posts
        public static EventHandler OnGetProfilePosts;//Implemented
        public static EventHandler OnGetPosts;//Implemented
        public static EventHandler OnGetPost;//Implemented
        public static EventHandler OnGetPostDeleted;//Implemented
        public static EventHandler OnGetComments;//Implemented
        public static EventHandler OnGetComment;//Implemented
        public static EventHandler OnGetCommentDeleted;//Implemented

        //Payments
        public static EventHandler OnGetTransactions;//Implemented
        public static EventHandler OnGetPaymentResult;//Implemented
        public static EventHandler OnGetVerifyMembership;//Implemented
        public static EventHandler OnPremiumRequired;//Implemented

        //Blocks
        public static EventHandler OnGetBlockedUsers;//Implemented

        //Calls
        public static EventHandler OnGetRequest;
        public static EventHandler OnGetRequests;
        public static EventHandler OnStartCalling;
        public static EventHandler OnReceiveCall;
        public static EventHandler OnCallRingTone;
        public static EventHandler OnStartCall;
        public static EventHandler OnCallEnded;
        public static EventHandler OnCallVideoChanged;
        //Temp until we fix udp server
        public static EventHandler OnSyncAudio;
        public static EventHandler OnSyncVideo;
        //Mapping
        public static EventHandler OnGetPosition;
    }
}