﻿using NetworkCommsDotNet;
using NetworkCommsDotNet.Connections;
using NetworkCommsDotNet.Connections.TCP;
using NetworkCommsDotNet.DPSBase;
using System;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using System.Reflection;
using SocialAPP.Shared;

namespace Server.Shared
{
    public class NetworkClient
    {
        string _hostname = "127.0.0.1";
        int _port = 4141;

        /// <summary>
        /// A suitable certificate to use for the example
        /// </summary>
        static X509Certificate certificate;

        /// <summary>
        /// SSLOptions which will be used for outgoing connections
        /// </summary>
        static SSLOptions connectionSSLOptions;

        /// <summary>
        /// The SendReceiveOptions used for sending
        /// </summary>
        static SendReceiveOptions sendingSendReceiveOptions;

        static ConnectionInfo connectionInfo;
        static TCPConnection connection;

        public Action<byte, byte, byte[]> OnMessageReceived;
        public Action OnConnected;
        public Action OnDisconnected;

        public NetworkClient(string hostname, int port)
        {
            _hostname = hostname;
            _port = port;

            connectionInfo = new ConnectionInfo(new IPEndPoint(IPAddress.Parse(_hostname), _port), ApplicationLayerProtocolStatus.Enabled);

            SetDebugTimeouts();

            //Ensure we have a certificate to use
            //Maximum security is achieved by using a trusted certificate
            //To keep things simple here we just create a self signed certificate for testing
            var assembly = Assembly.GetExecutingAssembly();
            var assemblyName = assembly.FullName.Split(',')[0];

            byte[] bytes;
            using (System.IO.Stream s = assembly.GetManifestResourceStream(assemblyName + ".Resources.curssor_com.pfx"))
            {
                long length = s.Length;
                bytes = new byte[length];
                s.Read(bytes, 0, (int) length);
            }

            certificate = new X509Certificate2(bytes, "Security1QW!");
            //Add a global incoming packet handler for packets of type "Message"
            //This handler will convert the incoming raw bytes into a string (this is what 
            //the <string> bit means) and then write that string to the local console window.
            NetworkComms.AppendGlobalIncomingPacketHandler<NetworkMessage>("SecureNetworkMessage", (packetHeader, connection, networkMessage) =>
            {
                if (networkMessage == null) return;
                OnMessageReceived?.Invoke(networkMessage.Id, networkMessage.Tag, networkMessage.Data);
                //Console.WriteLine("\n  ... Incoming message from " + connection.ToString() + " saying '" + incomingString + "'.");
            });
            
            //Create suitable SSLOptions to use with TCP
            SelectSSLOptions();
        }
        
        private static void SetDebugTimeouts()
        {
            NetworkComms.ConnectionEstablishTimeoutMS = 10000;
            NetworkComms.PacketConfirmationTimeoutMS = 5000;
            NetworkComms.ConnectionAliveTestTimeoutMS = 1000;
        }

        /// <summary>
        /// Select the SSL options
        /// </summary>
        private void SelectSSLOptions()
        {
            //Configure the connection options
            //These will be used when establishing outgoing connections
            connectionSSLOptions = new SSLOptions(certificate, true, true);//Provide certificate for outgoing connection
            //connectionSSLOptions = new SSLOptions("hesabot.com", true);

            //Select if the dataPadder will be enabled
            sendingSendReceiveOptions = new SendReceiveOptions<ProtobufSerializer, DataPadder>();
            DataPadder.AddPaddingOptions(sendingSendReceiveOptions.Options, 4096, DataPadder.DataPaddingType.Random, true);
        }

        public bool Connect()
        {
            try
            {
                // Get a connection to the target server using the connection SSL options we configured earlier
                //If there is a problem with the SSL handshake this will throw a CommsSetupShutdownException
                connection = TCPConnection.GetConnection(connectionInfo, sendingSendReceiveOptions, connectionSSLOptions);
                connection.AppendShutdownHandler(ClientDisconnected);
                OnConnected?.Invoke();
                return true;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
            }
            return false;
        }
        
        public void Disconnect()
        {
            connection?.Dispose();
        }

        void ClientDisconnected(Connection connection)
        {
            OnDisconnected?.Invoke();
        }

        public bool Send(byte id, byte tag, NetworkWriter data)
        {
            if (connection == null) return false;
            try
            {
                //Send our message on the encrypted connection
                connection.SendObject("SecureNetworkMessage", new NetworkMessage()
                {
                    Id = id,
                    Tag = tag,
                    Data = data.ToArray()
                });
                return true;
            }catch(Exception ex)
            {
                Console.WriteLine(ex);
            }
            return false;
        }

        public bool Send(byte id, byte tag, byte[] data)
        {
            if (connection == null) return false;
            try
            {
                //Send our message on the encrypted connection
                connection.SendObject("SecureNetworkMessage", new NetworkMessage()
                {
                    Id = id,
                    Tag = tag,
                    Data = data
                });
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return false;
        }

    }
}