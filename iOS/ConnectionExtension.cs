﻿using NetworkCommsDotNet.Connections;
using System;
using SocialAPP.Shared;

public static class ConnectionExtension
{
    public static bool SendReply(this Connection connection, byte id, byte tag, NetworkWriter writer, bool isTcp = true)
    {
        if (connection == null || writer == null) return false;
        try
        {
			var messageName = isTcp ? "SecureNetworkMessage" : "NetworkMessage";
            connection.SendObject(messageName, new NetworkMessage()
            {
                Id = id,
                Tag = tag,
                Data = writer.ToArray()
            });
            return true;
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex);
        }
        return false;
    }

    public static bool SendReply(this Connection connection, byte id, byte tag, byte[] data, bool isTcp = true)
    {
        if (connection == null || data == null) return false;
        try
        {
			var messageName = isTcp ? "SecureNetworkMessage" : "NetworkMessage";
            connection.SendObject(messageName, new NetworkMessage()
            {
                Id = id,
                Tag = tag,
                Data = data
            });
            return true;
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex);
        }
        return false;
    }

    public static void Close(this Connection connection)
    {
		try
		{
        	connection?.Dispose();
		}catch(Exception)
		{

		}
    }
}