﻿using System;
using AVFoundation;
using Foundation;
using UIKit;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using SocialAPP.Shared;
namespace SocialAPP.iOS
{
	public static class AudioPlayer
	{
		static AVAudioPlayer _ringTone;
		static AVAudioPlayer _player;

		static List<byte[]> Bytes = new List<byte[]>();
		static bool running;
		static bool soundMuted;

		public static void ActivateAudioSession()
		{
			var session = AVAudioSession.SharedInstance();
			session.SetCategory(AVAudioSessionCategory.Ambient);
			session.SetActive(true);
		}

		public static void DeactivateAudioSession()
		{
			var session = AVAudioSession.SharedInstance();
			session.SetActive(false);
		}

		public static void ReactivateAudioSession()
		{
			var session = AVAudioSession.SharedInstance();
			session.SetActive(true);
		}

		public static void PlayRingTone(bool inSpeakers = false)
		{
			ActivateAudioSession();
			if (_ringTone == null)
			{
				NSError err = null;
				try
				{
					var songURL = "Assets/ringtone.mp3";
					songURL = System.IO.Path.Combine(NSBundle.MainBundle.BundlePath, songURL);
					_ringTone = AVAudioPlayer.FromUrl(new NSUrl(songURL, false), out err);
					_ringTone.Volume = 1;
				}
				catch (Exception ex)
				{
					Console.WriteLine(ex.StackTrace);
					if(err != null) Console.WriteLine(err);
				}
			}
			_ringTone.NumberOfLoops = 0;
			_ringTone.Play();
		}

		public static Task PlayByteArrayAsync(byte[] bArray)
		{
			var audioPacket = ProtoSerializer.Deserialize<AudioPacket>(bArray);
			if (audioPacket == null) return null;
			Bytes.Add(audioPacket.Data);
			if (!running)
			{
				running = true;
				EndedPlay(null);
			}
			return null;
		}

		public static void ToggleMuteSound()
		{
			soundMuted = !soundMuted;
		}

		private static void EndedPlay(NSNotification obj)
		{
			if (!running) return;
			try
			{

				if (Bytes.Count != 0)
				{
					var bytes = Bytes[0];
					Bytes.Remove(bytes);

					if (_player != null)
					{
						_player.Stop();
						_player.Dispose();
					}

					if (soundMuted)
					{
						running = false;
						Bytes.Clear();
						return;
					}
					/*var documents = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
					var filename = "tmp_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".mp3";
					filename = System.IO.Path.Combine(documents, filename);
					System.IO.File.Create(filename);
					using (var file = System.IO.File.OpenWrite(filename))
					{
						file.Write(bytes, 0, bytes.Length);
						file.Close();
					}
					var source = new NSUrl(filename);*/

					NSData source = NSData.FromArray(bytes);
					//ObjCRuntime.Class.ThrowOnInitFailure = false;
					NSNotificationCenter.DefaultCenter.AddObserver(AVPlayerItem.DidPlayToEndTimeNotification, EndedPlay);
					NSError _error;
					_player = new AVAudioPlayer(source, "wav", out _error);

					if (_error != null)
					{

						Console.WriteLine(_error.Description);
						_error = null;
					}
					_player.PrepareToPlay();
					_player.Volume = (float)1.0;
					_player.Play();
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.StackTrace);
				running = false;
			}
		}

		public static void Stop()
		{
			running = false;
			_player.Stop();
			_player.Dispose();
			_player = null;
			Bytes.Clear();
		}

		public static void StopRingTone()
		{
			if (_ringTone != null)
			{
				_ringTone.Stop();
			}
		}
	}
}
