﻿using Network;

namespace Server.Master
{
    public class MasterServer : Plugin
    {
        public override string name { get { return "Master Server"; } }
        public override string version { get { return "1.0"; } }
        public override Command[] commands { get { return new Command[0]; } }
        public override string author { get { return "Sylvain William Martens"; } }
        public override string supportEmail { get { return "thehesa@gmail.com"; } }

        public MasterServer()
        {
            ConnectionService.onData += OnDataReceived;
        }

        public void OnDataReceived(ConnectionService con, ref NetworkMessage msg)
        {
            switch (msg.tag)
            {
                default:

                break;
            }
        }
    }
}