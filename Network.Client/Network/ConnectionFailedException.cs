using System;

namespace Network
{
    public class ConnectionFailedException : Exception
    {
        public ConnectionFailedException(string message) : base(message)
        {
        }

        public ConnectionFailedException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}