using System;

namespace Network
{
    public class NotConnectedException : Exception
    {
        public NotConnectedException(string message) : base(message)
        {
        }
    }
}