namespace Network
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            NetworkServer.Bootstrap(0, new System.Action<string>(Program.OnLog), new System.Action<string>(Program.OnWarning), new System.Action<string>(Program.OnError), new System.Action<string>(Program.OnFatal), null, null);
            System.AppDomain.CurrentDomain.ProcessExit += delegate (object sender, System.EventArgs e)
            {
                NetworkServer.Close(false);
            };
            while (true)
            {
                string text = System.Console.ReadLine();
                if (text != null && text != "")
                {
                    Interface.ExecuteCommand(text);
                }
            }
        }

        private static void OnLog(string message)
        {
            System.Console.WriteLine(message);
        }

        private static void OnWarning(string message)
        {
            System.Console.ForegroundColor = System.ConsoleColor.Yellow;
            System.Console.WriteLine("[Warning] " + message);
            System.Console.ResetColor();
        }

        private static void OnError(string message)
        {
            System.Console.ForegroundColor = System.ConsoleColor.Red;
            System.Console.WriteLine("[Error] " + message);
            System.Console.ResetColor();
        }

        private static void OnFatal(string message)
        {
            System.Console.ForegroundColor = System.ConsoleColor.DarkRed;
            System.Console.BackgroundColor = System.ConsoleColor.Gray;
            System.Console.WriteLine("[Fatal] " + message);
            System.Console.ResetColor();
        }
    }
}