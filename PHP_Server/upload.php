<?php
ob_start("ob_gzHandler");

if(!isset($_GET['token'])) exit();
if(empty($_FILES["file"]["name"])) exit();

try{
	$hostname = "localhost";
    $dbname = "social";
    $username = "root";
    $pw = "";
    $db = new PDO ("mysql:host=$hostname;dbname=$dbname","$username","$pw");
	$token = (int) $_GET["token"];

	$sql = "SELECT accountId FROM uploadTokens WHERE id=? LIMIT 1";
	$query = $db->prepare($sql);
	$params = [ $token ];
	$query->execute($params);

	while($row = $query->fetch()) {
		$accountId = $row['accountId'];
		$uploads_dir = 'uploads/profile'; //Directory to save the file that comes from client application.
		if ($_FILES["file"]["error"] == UPLOAD_ERR_OK) {
			$name = $accountId.".png";
			$outputname = "$uploads_dir/$name";
			$tmp_name = $_FILES["file"]["tmp_name"];

			if(file_exists($outputname))
			{
			  chmod($outputname, 0755);
			  unlink($outputname);
			}else echo "";

			move_uploaded_file($tmp_name, $outputname);

			echo "success";

			$db->exec("DELETE FROM uploadtokens WHERE accountId='".$accountId."';");
		}else
		{
			echo $_FILES["file"]["error"];
			error_log("error uploading: "+$_FILES["file"]["error"]);
		}
    }
	unset($query);
	unset($db);
}catch(PDOException  $e ){
	echo "Error: ".$e;
	error_log($e);
}catch(Exception $e)
{
	echo "Error: ".$e;
	error_log($e);
}
?>