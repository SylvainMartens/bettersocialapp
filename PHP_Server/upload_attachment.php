<?php
ob_start("ob_gzHandler");

if(empty($_FILES["file"]["name"])) exit();

try{
	$hostname = "localhost";
    $dbname = "social";
    $username = "root";
    $pw = "";
    $db = new PDO ("mysql:host=$hostname;dbname=$dbname","$username","$pw");

	$uploads_dir = 'uploads/attachment'; //Directory to save the file that comes from client application.
	if ($_FILES["file"]["error"] == UPLOAD_ERR_OK) {
        $hash = md5_file($_FILES['file']['tmp_name']);

        $query1 = $db->prepare("SELECT * FROM attachments WHERE hash = ? LIMIT 1");
        $params = [ $hash ];
	    $query1->execute($params);

        while($row = $query1->fetch()) {
		    echo $row['id'];
            exit();
        }

        $name = $hash.".png";
		$outputname = "$uploads_dir/$name";

        $query = $db->prepare("INSERT INTO attachments (hash, path) VALUES(?, ?)");
        $params = [ $hash, $outputname ];
	    $query->execute($params);
        $id = $db->lastInsertId();

		$tmp_name = $_FILES["file"]["tmp_name"];

		if(!file_exists($outputname))
		    move_uploaded_file($tmp_name, $outputname);

		echo $id;
	}else
	{
		//echo $_FILES["file"]["error"];
		//error_log("error uploading: "+$_FILES["file"]["error"]);
        echo "0";
	}
	unset($query);
	unset($db);
}catch(PDOException  $e ){
	//echo "Error: ".$e;
	//error_log($e);
    echo "0";
}catch(Exception $e)
{
	//echo "Error: ".$e;
	//error_log($e);
    echo "0";
}
?>