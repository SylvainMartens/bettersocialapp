<?php
ob_start("ob_gzHandler");
if(!isset($_GET['attachment'])) exit();

try{
	$hostname = "localhost";
    $dbname = "social";
    $username = "root";
    $pw = "";
    $db = new PDO ("mysql:host=$hostname;dbname=$dbname","$username","$pw");

    $id = (int) $_GET['attachment'];

    $query = $db->prepare("SELECT * FROM attachments WHERE id = ? LIMIT 1");
    $params = [ $id ];
	$query->execute($params);

    while($row = $query->fetch()) {
		$path = $row['path'];
        $type = 'image/jpeg';
        header('Content-Type:'.$type);
        header('Content-Length: ' . filesize($path));
        readfile($path);
    }

	unset($query);
	unset($db);
}catch(PDOException  $e )
{
}catch(Exception $e)
{
}
?>