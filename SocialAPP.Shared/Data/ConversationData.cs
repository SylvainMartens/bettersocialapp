﻿using ProtoBuf;
using System;

namespace SocialAPP.Shared.Data
{
    [ProtoContract, Serializable]
    public class ConversationData
    {
        [ProtoMember(1)]
        public string Id { get; set; }

        [ProtoMember(2)]
        public string User1Id { get; set; }

        [ProtoMember(3)]
        public string User2Id { get; set; }

        [ProtoMember(4)]
        public string User1Name { get; set; }

        [ProtoMember(5)]
        public string User2Name { get; set; }

        [ProtoMember(6)]
        public string User1Avatar { get; set; }

        [ProtoMember(7)]
        public string User2Avatar { get; set; }

        [ProtoMember(8)]
        public string LastMessage { get; set; }

        [ProtoMember(9)]
        public bool LastMessageFromMe { get; set; }

        [ProtoMember(10)]
        public bool LastMessageSeen { get; set; }

        [ProtoMember(11)]
        public int User1Gender { get; set; }

        [ProtoMember(12)]
        public int User2Gender { get; set; }
    }
}