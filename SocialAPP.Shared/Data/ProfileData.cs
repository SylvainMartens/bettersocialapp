﻿namespace SocialAPP.Shared.Data
{
    public class ProfileData
    {
        public string Id
        {
            get;
            set;
        }

        public string Username
        {
            get;
            set;
        }

        public string Picture
        {
            get;
            set;
        }

        public string City
        {
            get;
            set;
        }

        public string Job
        {
            get;
            set;
        }

        public string Activities
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public string LookingFor
        {
            get;
            set;
        }

        public string LookingForDescription
        {
            get;
            set;
        }

        public int Age
        {
            get;
            set;
        }

        public int Gender
        {
            get;
            set;
        }

        public bool Followed { get; set; }

        public FollowData[] Followers { get; set; }

        public FollowData[] Following { get; set; }

        public int PhotosCount
        {
            get;
            set;
        }

        public bool IsBlocked
        {
            get;
            set;
        }
    }
}