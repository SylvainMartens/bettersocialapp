﻿using ProtoBuf;
using System;

namespace SocialAPP.Shared.Data
{
    [ProtoContract, Serializable]
    public class GroupData
    {
        [ProtoMember(1)]
        public string Id
        {
            get;
            set;
        }

        [ProtoMember(2)]
        public string OwnerId
        {
            get;
            set;
        }

        [ProtoMember(3)]
        public string Name
        {
            get;
            set;
        }

        [ProtoMember(4)]
        public bool IsPublic
        {
            get;
            set;
        }

        [ProtoMember(5)]
        public DateTime CreatedAt
        {
            get;
            set;
        }
    }
}