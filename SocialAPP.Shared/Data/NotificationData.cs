﻿using ProtoBuf;
using System;

namespace SocialAPP.Shared.Data
{
    [ProtoContract, Serializable]
    public class NotificationData
    {
        [ProtoMember(1)]
        public string Id { get; set; }

        [ProtoMember(2)]
        public NotificationTypes Type { get; set; }

        [ProtoMember(3)]
        public DateTime When { get; set; }

        [ProtoMember(4)]
        public string FromId { get; set; }

        [ProtoMember(5)]
        public string FromName { get; set; }

        [ProtoMember(6)]
        public string FromAvatar { get; set; }

        [ProtoMember(7)]
        public int FromGender { get; set; }

        [ProtoMember(8)]
        public string ProfileId { get; set; }
    }
}