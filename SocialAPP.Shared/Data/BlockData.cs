﻿using ProtoBuf;
using System;

namespace SocialAPP.Shared.Data
{
    [ProtoContract, Serializable]
    public class BlockData
    {
        [ProtoMember(1)]
        public string Id { get; set; }
        [ProtoMember(2)]
        public string UserId { get; set; }
        [ProtoMember(3)]
        public string ProfileId { get; set; }
        [ProtoMember(4)]
        public DateTime DateAt { get; set; }
        [ProtoMember(5)]
        public string ProfileUserName { get; set; }
        [ProtoMember(6)]
        public string ProfileAvatar { get; set; }
        [ProtoMember(7)]
        public int ProfileGender { get; set; }
    }
}