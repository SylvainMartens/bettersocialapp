﻿using ProtoBuf;
using System;

namespace SocialAPP.Shared.Data
{
    [ProtoContract, Serializable]
    public class CallData
    {
        [ProtoMember(1)]
        public string Id { get; set; }
        [ProtoMember(2)]
        public string UserId { get; set; }
        [ProtoMember(3)]
        public string UserName { get; set; }
        [ProtoMember(4)]
        public string UserAvatar { get; set; }
        [ProtoMember(5)]
        public int UserGender { get; set; }
        [ProtoMember(6)]
        public string ProfileId { get; set; }
        [ProtoMember(7)]
        public string ProfileName { get; set; }
        [ProtoMember(8)]
        public string ProfileAvatar { get; set; }
        [ProtoMember(9)]
        public int ProfileGender { get; set; }
        [ProtoMember(10)]
        public bool Answered { get; set; }
    }
}