﻿using ProtoBuf;
using System;

namespace SocialAPP.Shared.Data
{
    [ProtoContract, Serializable]
    public class GroupCommentData
    {
        [ProtoMember(1)]
        public string Id
        {
            get;
            set;
        }

        [ProtoMember(2)]
        public string PostId
        {
            get;
            set;
        }

        [ProtoMember(3)]
        public string UserId
        {
            get;
            set;
        }

        [ProtoMember(4)]
        public string Message
        {
            get;
            set;
        }

        [ProtoMember(5)]
        public string AttachmentId
        {
            get;
            set;
        }

        [ProtoMember(6)]
        public DateTime DateAt
        {
            get;
            set;
        }

        [ProtoMember(7)]
        public string UserName
        {
            get;
            set;
        }

        [ProtoMember(8)]
        public string UserAvatar
        {
            get;
            set;
        }

        [ProtoMember(9)]
        public int Gender
        {
            get;
            set;
        }
    }
}