﻿namespace SocialAPP.Shared
{
    public static class ClientTags
    {
        //Lobby
        public const byte GET_VERSION = 1;

        public const byte DO_LOGIN = 2;
        public const byte DO_LOGOUT = 3;
        public const byte DO_REGISTER = 4;
        public const byte DO_RECOVER = 39;
        public const byte DISCONNECTED_BY_SERVER = 5;
        public const byte GET_ACCOUNT_DATA = 6;
        public const byte GETEDIT_ACCOUNT = 33;

        //Edits
        public const byte UPDATE_POS = 7;

        public const byte GET_PROFILE_DATA = 8;
        public const byte EDIT_PROFILE_DATA = 9;
        public const byte EDIT_PROFILE_PICTURE = 10;
        public const byte UPDATE_PROFILE_PICTURE = 11;
        public const byte DO_SAVEACCOUNT = 34;

        //Profile
        public const byte FOLLOW = 19;

        public const byte UNFOLLOW = 20;

        public const byte GETPROFILE_PHOTOS = 32;

        //Messages
        public const byte SEND_MESSAGE = 13;

        public const byte GET_CONVERSATIONS = 14;
        public const byte GET_MESSAGES = 15;
        public const byte RECEIVED_MESSAGE = 16;
        public const byte MESSAGE_SEEN = 17;
        public const byte START_CONVERSATION = 18;

        //Search
        public const byte SEARCH_PROFILES = 12;

        //Notifications
        public const byte GET_NOTIFICATIONS = 21;

        public const byte RECEIVED_NOTIFICATION = 22;
        public const byte POKE = 23;
        public const byte DELETE_NOTIFICATION = 24;

        //Post
        public const byte CREATE_POST = 25;

        public const byte DELETE_POST = 26;

        public const byte COMMENT_POST = 27;
        public const byte UNCOMMENT_POST = 28;

        public const byte GETUSER_POSTS = 29;
        public const byte GETRECENT_POSTS = 30;

        public const byte GETCOMMENTS_POST = 31;

        //Payments
        public const byte GET_TRANSACTIONS = 35;

        public const byte SEND_TRANSACTION = 36;
        public const byte VERIFY_MEMBERSHIP = 37;
        public const byte PREMIUM_REQUIRED = 38;

        //Blocks
        public const byte GET_BLOCKEDUSERS = 40;
        public const byte BLOCK_USER = 41;
        public const byte UNBLOCK_USER = 42;

        //Calls
        public const byte REQUEST = 43;//When user try to call ( send request or call ) | Client & Server side
        public const byte CALL_REQUEST_RESULT = 44;//When user accept or decline a call request. | Server Side

        public const byte DO_CALL = 45;//When user is calling ( user1 ) | Client Side
        public const byte SET_CALL_RESULT = 46;//When user accept or decline a call ( user2 ) | Server side.

        public const byte DO_CALL_RING = 50;//The ring tone | Client Side
        
        public const byte RECEIVE_CALL = 47;//When the user 2 receive the call | Client Side
        public const byte END_CALL = 48;//When one of the users end the call | Client & Server Side
        //Video Call
        public const byte VIDEO_ENABLED = 49;//When one of the users toggle the camera on/off | Client & Server Side
        public const byte START_CALL = 51;//When user2 accepted call, both users receive the informations required to connect to the call server and speak to each other. | Client Side

        //Temp Until UDP Server works correctly
        public const byte SET_CALL_ID = 52;
        public const byte SYNC_AUDIO = 53;
        public const byte SYNC_VIDEO = 54;
        //Mapping
        public const byte REQUEST_POS = 55;
        public const byte SET_REQUEST_POS_RESULT = 56;
        public const byte SEND_REQUEST_POS = 57;
        //Msgs Request
        public const byte SET_MSG_REQUEST_RESULT = 58;
        //Groups///////////////////////////////////////////////////////////////////////////////////////////
        public const byte CREATE_GROUP = 59;
        public const byte DELETE_GROUP = 60;

        public const byte INVITE_GROUP = 61;
        public const byte SET_GROUP_INVITE_RESULT = 62;

        public const byte REQUEST_JOIN_GROUP = 63;
        public const byte SET_GROUP_REQUEST_RESULT = 64;

        public const byte SET_MEMBER_ADMINISTRATOR = 65;

        public const byte DELETE_GROUP_MEMBER = 66;

        public const byte CREATE_GROUP_POST = 67;
        public const byte GET_GROUP_POSTS = 68;

        public const byte COMMENT_GROUP_POST = 69;
        public const byte GET_GROUP_POST_COMMENTS = 70;

        public const byte SET_GROUP_PUBLIC = 71;

        public const byte GET_GROUP_MEMBERS = 72;

        public const byte SEARCH_GROUP = 73;

        public const byte GET_PENDING_GROUP_INVITATIONS = 74;
        public const byte GET_MY_GROUP_INVITATIONS = 75;

        public const byte GET_MY_PENDING_GROUP_REQUESTS = 76;
        public const byte GET_GROUP_REQUESTS = 77;

        public const byte QUIT_GROUP = 78;
        public const byte DELETE_GROUP_POST = 79;
        public const byte DELETE_GROUP_COMMENT = 80;

        public const byte CANCEL_REQUEST_GROUP = 81;

        public const byte GET_MY_GROUPS = 82;
        public const byte GET_USER_GROUPS = 83;
    }
}