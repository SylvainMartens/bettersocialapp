﻿using System.IO;

namespace SocialAPP.Shared
{
    public static class Serializer
    {
        /*public static byte[] Serialize<T>(T obj)
        {
            if (obj == null) return null;
            byte[] returnBytes = new byte[0];
            
            using (MemoryStream ms = new MemoryStream())
            {
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(ms, obj);
                returnBytes = ms.ToArray();
            }
            return returnBytes;
            //return Newtonsoft.Json.JsonConvert.SerializeObject(obj);
        }*/

        public static byte[] Serialize<T>(T serializableObject)
        {
            T obj = serializableObject;

            using (MemoryStream stream = new MemoryStream())
            {
                System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(typeof(T));

                x.Serialize(stream, obj);

                return stream.ToArray();
            }
        }

        /*public static  T Deserialize<T>(byte[] arrBytes)//byte[] arrBytes)
        {
            T obj = default(T);
            using (MemoryStream memStream = new MemoryStream())
            {
                BinaryFormatter binForm = new BinaryFormatter();
                memStream.Write(arrBytes, 0, arrBytes.Length);
                memStream.Seek(0, SeekOrigin.Begin);
                obj = (T)binForm.Deserialize(memStream);
            }
            return obj;
        }*/
        public static T Deserialize<T>(byte[] serilizedBytes)
        {
            System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(typeof(T));

            using (MemoryStream stream = new MemoryStream(serilizedBytes))
            {
                return (T)x.Deserialize(stream);
            }
        }
    }
}