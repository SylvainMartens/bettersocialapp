﻿using ProtoBuf;
using System;

namespace SocialAPP.Shared
{
    [ProtoContract, Serializable]
    public class CallMessage
    {
        [ProtoMember(1)]
        public byte Tag { get; set; }
        [ProtoMember(2)]
        public byte[] Data { get; set; }

        public string ReadString()
        {
            if (Data == null) return null;
            return ProtoSerializer.Deserialize<string>(Data);
        }

        public bool ReadBoolean()
        {
            if (Data == null) return false;
            return ProtoSerializer.Deserialize<bool>(Data);
        }

        public int ReadInt32()
        {
            if (Data == null) return 0;
            return ProtoSerializer.Deserialize<int>(Data);
        }

        public T ReadObject<T>()
        {
            if (Data == null) return default(T);
            return ProtoSerializer.Deserialize<T>(Data);
        }
    }
}