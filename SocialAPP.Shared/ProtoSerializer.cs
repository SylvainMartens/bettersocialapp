using ProtoBuf;
using System.IO;

namespace SocialAPP.Shared
{
    public class ProtoSerializer
    {
        public static byte[] Serialize<T>(T obj)
        {
            byte[] result;
            using (MemoryStream memoryStream = new MemoryStream())
            {
                ProtoBuf.Serializer.Serialize(memoryStream, obj);
                result = memoryStream.ToArray();
            }
            return result;
        }

        public static void Serialize<T>(Stream stream, T obj)
        {
            ProtoBuf.Serializer.Serialize(stream, obj);
        }

        public static T Deserialize<T>(byte[] bytes)
        {
            T result = default(T);
            try
            {
                using (MemoryStream memoryStream = new MemoryStream(bytes, 0, bytes.Length))
                {
                    result = ProtoBuf.Serializer.Deserialize<T>(memoryStream);
                }
            }
            catch (System.Exception ex)
            {
                System.Console.WriteLine(ex.StackTrace);
            }
            return result;
        }

        public static T Deserialize<T>(Stream stream)
        {
            return ProtoBuf.Serializer.Deserialize<T>(stream);
        }
    }
}