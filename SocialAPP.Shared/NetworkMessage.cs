﻿using ProtoBuf;

namespace SocialAPP.Shared
{
	[ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
	public class NetworkMessage
	{
		public byte Id { get; set; }
		public byte Tag { get; set; }
		public byte[] Data { get; set; }
	}
}