using HashLib;
using System.Text;

namespace SocialAPP.Shared
{
    public class Encryption
    {
        public static string SHA1Of(string text)
        {
            IHash hash = HashFactory.Crypto.CreateSHA1();
            HashResult hashResult = hash.ComputeString(text, Encoding.UTF8);
            return hashResult.ToString().Replace("-", "").ToLower();
        }
    }
}