﻿using Hazel;
using Hazel.Udp;
using System;

namespace SocialAPP.Shared
{
    public class CallClient
    {
        private string _hostName = "192.168.1.4";
        private int _port = 4298;
        public string CallId = "";
        public bool RemoteVideoEnabled;
        public bool MyVideoEnabled;

        private UdpClientConnection _clientConnection;

        public Action<byte[]> OnGetAudio;
        public Action<byte[]> OnGetVideo;
        public Action<bool> OnVideoEnabled;
        public Action OnDisconnected;
        bool disconnectedTriggered;

        public int Ping
        {
            get
            {
                if (_clientConnection == null) return 0;
                return (int) _clientConnection.AveragePing;
            }
        }

        public CallClient(string hostName, int port, string callId)
        {
            _hostName = hostName;
            _port = port;
            CallId = callId;
            _clientConnection = new UdpClientConnection(new NetworkEndPoint(_hostName, port, IPMode.IPv4));
            _clientConnection.DataReceived += OnDataReceived;
            _clientConnection.Disconnected += (object sender, DisconnectedEventArgs a) => {
                if (!disconnectedTriggered)
                {
                    disconnectedTriggered = true;
                    OnDisconnected?.Invoke();
                }
            };
        }

        public bool Connect()
        {
            try
            {
                disconnectedTriggered = false;
                _clientConnection.Connect();
                
                var callMessage = new CallMessage()
                {
                    Tag = CallTags.SET_CALL,
                    Data = ProtoSerializer.Serialize(CallId)
                };
                var bytes = ProtoSerializer.Serialize(callMessage);
                _clientConnection.SendBytes(bytes, SendOption.Reliable);
                return true;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
            return false;
        }

        private void OnDataReceived(object sender, DataReceivedEventArgs args)
        {
            //Connection connection = (Connection)sender;
            var callMessage = ProtoSerializer.Deserialize<CallMessage>(args.Bytes);
            if (callMessage == null) return;
            switch(callMessage.Tag)
            {
                case CallTags.SYNC_AUDIO:
                {
                    OnGetAudio?.Invoke(callMessage.Data);
                }
                break;
                case CallTags.SYNC_VIDEO:
                {
                    OnGetVideo?.Invoke(callMessage.Data);
                }
                break;
                case CallTags.DISCONNECTED:
                {
                    if (!disconnectedTriggered)
                    {
                        disconnectedTriggered = true;
                        OnDisconnected?.Invoke();
                    }
                }
                break;
                case CallTags.VIDEO_ENABLED:
                {
                    if (callMessage.Data.Length != 1) return;
                    var result = (int)callMessage.Data[0];
                    OnVideoEnabled?.Invoke(result == 1);
                }
                break;
            }
            args.Recycle();
        }

        public void SyncAudio(byte[] audio)
        {
            var callMessage = new CallMessage()
            {
                Tag = CallTags.SYNC_AUDIO,
                Data = audio
            };
            var bytes = ProtoSerializer.Serialize(callMessage);
            _clientConnection.SendBytes(bytes, SendOption.Fragmented);
        }

        public void SyncVideo(byte[] video)
        {
            var callMessage = new CallMessage()
            {
                Tag = CallTags.SYNC_VIDEO,
                Data = video
            };
            var bytes = ProtoSerializer.Serialize(callMessage);
            _clientConnection.SendBytes(bytes, SendOption.Fragmented);
        }

        public void SetVideoEnabled()
        {
            var mbyte = MyVideoEnabled ? 1 : 0;
            var callMessage = new CallMessage()
            {
                Tag = CallTags.VIDEO_ENABLED,
                Data = new byte[] { (byte) mbyte }
            };
            var bytes = ProtoSerializer.Serialize(callMessage);
            _clientConnection.SendBytes(bytes, SendOption.Fragmented);
        }

        public void Destroy()
        {
            disconnectedTriggered = true;
            _clientConnection.Close();
            _clientConnection.Dispose();
            OnGetAudio = null;
            OnGetVideo = null;
            OnDisconnected = null;
        }
    }
}