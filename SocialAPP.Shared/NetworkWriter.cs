﻿using System.IO;
using System.Text;

namespace SocialAPP.Shared
{
    public class NetworkWriter : BinaryWriter
    {
        private long Length
        {
            get
            {
                return BaseStream.Length;
            }
        }

        public NetworkWriter() : base(new MemoryStream(12288))
        {
        }

        public NetworkWriter(Encoding encoding) : base(new MemoryStream(12288), encoding)
        {
        }

        internal NetworkWriter(Stream stream) : base(stream)
        {
        }

        internal NetworkWriter(Stream stream, Encoding encoding) : base(stream, encoding)
        {
        }

        public byte[] ToArray()
        {
            return ((MemoryStream)BaseStream).ToArray();
        }

        public override void Write(byte[] value)
        {
            base.Write(value.Length);
            for (int i = 0; i < value.Length; i++)
            {
                byte value2 = value[i];
                base.Write(value2);
            }
        }

        public override void Write(char[] value)
        {
            base.Write(value.Length);
            for (int i = 0; i < value.Length; i++)
            {
                char ch = value[i];
                base.Write(ch);
            }
        }

        public virtual void Write(bool[] value)
        {
            base.Write(value.Length);
            for (int i = 0; i < value.Length; i++)
            {
                bool value2 = value[i];
                base.Write(value2);
            }
        }

        public virtual void Write(decimal[] value)
        {
            base.Write(value.Length);
            for (int i = 0; i < value.Length; i++)
            {
                decimal value2 = value[i];
                base.Write(value2);
            }
        }

        public virtual void Write(double[] value)
        {
            base.Write(value.Length);
            for (int i = 0; i < value.Length; i++)
            {
                double value2 = value[i];
                base.Write(value2);
            }
        }

        public virtual void Write(short[] value)
        {
            base.Write(value.Length);
            for (int i = 0; i < value.Length; i++)
            {
                short value2 = value[i];
                base.Write(value2);
            }
        }

        public virtual void Write(int[] value)
        {
            base.Write(value.Length);
            for (int i = 0; i < value.Length; i++)
            {
                int value2 = value[i];
                base.Write(value2);
            }
        }

        public virtual void Write(long[] value)
        {
            base.Write(value.Length);
            for (int i = 0; i < value.Length; i++)
            {
                long value2 = value[i];
                base.Write(value2);
            }
        }

        public virtual void Write(sbyte[] value)
        {
            base.Write(value.Length);
            for (int i = 0; i < value.Length; i++)
            {
                sbyte value2 = value[i];
                base.Write(value2);
            }
        }

        public virtual void Write(float[] value)
        {
            base.Write(value.Length);
            for (int i = 0; i < value.Length; i++)
            {
                float value2 = value[i];
                base.Write(value2);
            }
        }

        public virtual void Write(string[] value)
        {
            base.Write(value.Length);
            for (int i = 0; i < value.Length; i++)
            {
                string value2 = value[i];
                base.Write(value2);
            }
        }

        public virtual void Write(ushort[] value)
        {
            base.Write(value.Length);
            for (int i = 0; i < value.Length; i++)
            {
                ushort value2 = value[i];
                base.Write(value2);
            }
        }

        public virtual void Write(uint[] value)
        {
            base.Write(value.Length);
            for (int i = 0; i < value.Length; i++)
            {
                uint value2 = value[i];
                base.Write(value2);
            }
        }

        public virtual void Write(ulong[] value)
        {
            base.Write(value.Length);
            for (int i = 0; i < value.Length; i++)
            {
                ulong value2 = value[i];
                base.Write(value2);
            }
        }
    }
}