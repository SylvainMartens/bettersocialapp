namespace HashLib
{
    public interface ICryptoBuildIn : ICrypto, IHash, IBlockHash
    {
    }
}