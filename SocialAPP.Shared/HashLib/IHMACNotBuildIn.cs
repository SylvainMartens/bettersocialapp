namespace HashLib
{
    public interface IHMACNotBuildIn : IHMAC, IWithKey, IHash, ICrypto, IBlockHash, ICryptoNotBuildIn
    {
    }
}