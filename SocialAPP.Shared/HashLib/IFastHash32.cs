namespace HashLib
{
    public interface IFastHash32
    {
        int ComputeByteFast(byte a_data);

        int ComputeCharFast(char a_data);

        int ComputeShortFast(short a_data);

        int ComputeUShortFast(ushort a_data);

        int ComputeIntFast(int a_data);

        int ComputeUIntFast(uint a_data);

        int ComputeLongFast(long a_data);

        int ComputeULongFast(ulong a_data);

        int ComputeFloatFast(float a_data);

        int ComputeDoubleFast(double a_data);

        int ComputeStringFast(string a_data);

        int ComputeBytesFast(byte[] a_data);

        int ComputeCharsFast(char[] a_data);

        int ComputeShortsFast(short[] a_data);

        int ComputeUShortsFast(ushort[] a_data);

        int ComputeIntsFast(int[] a_data);

        int ComputeUIntsFast(uint[] a_data);

        int ComputeLongsFast(long[] a_data);

        int ComputeULongsFast(ulong[] a_data);

        int ComputeDoublesFast(double[] a_data);

        int ComputeFloatsFast(float[] a_data);
    }
}