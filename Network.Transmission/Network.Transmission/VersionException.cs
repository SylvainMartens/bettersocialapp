using System;

namespace Network.Transmission
{
    public class VersionException : Exception
    {
        private byte specifiedEncodingVersion;

        public VersionException(byte specifiedEncodingVersion)
        {
            this.specifiedEncodingVersion = specifiedEncodingVersion;
        }

        public override string ToString()
        {
            return "You specified an Encoding Version of " + this.specifiedEncodingVersion.ToString() + "but that doesn't exit!";
        }
    }
}