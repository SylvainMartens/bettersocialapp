using System;
using System.Collections.Generic;

namespace Network.Transmission
{
    public class HandshakeProtocol
    {
        public const byte latestHandshakeVersion = 0;

        public static int GetNoBytesOfHandshake(byte version)
        {
            int result;
            switch (version)
            {
                case 0:
                result = 2;
                break;

                case 1:
                result = 4;
                break;

                default:
                throw new VersionException(version);
            }
            return result;
        }

        public static Handshake DecodeHandshake(byte version, byte[] bytes)
        {
            Handshake result = default(Handshake);
            switch (version)
            {
                case 0:
                result.ID = (ushort)bytes[0];
                break;

                case 1:
                result.connectionCode = (ConnectionCode)bytes[0];
                result.ID = BitConverter.ToUInt16(bytes, 1);
                break;

                default:
                throw new VersionException(version);
            }
            return result;
        }

        public static byte[] EncodeHandshake(Handshake data, byte version)
        {
            List<byte> list = new List<byte>();
            list.Add(version);
            switch (version)
            {
                case 0:
                list.Add((byte)data.ID);
                break;

                case 1:
                list.Add((byte)data.connectionCode);
                list.AddRange(BitConverter.GetBytes(data.ID));
                break;

                default:
                throw new VersionException(version);
            }
            return list.ToArray();
        }

        public static byte[] EncodeHandshake(Handshake data)
        {
            return HandshakeProtocol.EncodeHandshake(data, 0);
        }
    }
}