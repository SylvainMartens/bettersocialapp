namespace Network.Transmission
{
    public class DataBufferItem
    {
        public byte[] header;

        public byte[] body;

        public DataBufferItem()
        {
        }

        public DataBufferItem(byte[] header, byte[] body)
        {
            this.header = header;
            this.body = body;
        }
    }
}