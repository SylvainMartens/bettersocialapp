using System;

namespace Network.Transmission
{
    public struct Handshake
    {
        public ushort ID;

        public ConnectionCode connectionCode;

        [Obsolete("You should now specify a ConnectionCode")]
        public Handshake(ushort ID)
        {
            this.ID = ID;
            this.connectionCode = ConnectionCode.Accept;
        }

        public Handshake(ushort ID, ConnectionCode connectionCode)
        {
            this.ID = ID;
            this.connectionCode = connectionCode;
        }
    }
}