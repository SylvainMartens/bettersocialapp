using Network.Transmission;

namespace Network
{
    public struct NetworkMessage
    {
        public ushort senderID;

        public byte distributionMode;

        public ushort destinationID;

        public byte tag;

        public ushort subject;

        public object data;

        internal DataBufferItem dataBytes;

        public DistributionType distributionType
        {
            get
            {
                DistributionType result;
                try
                {
                    result = (DistributionType)this.distributionMode;
                }
                catch
                {
                    result = DistributionType.Custom;
                }
                return result;
            }
            set
            {
                this.distributionMode = (byte)value;
            }
        }

        internal NetworkMessage(ushort senderID, byte distributionMode, ushort destinationID, byte tag, ushort subject)
        {
            this.senderID = senderID;
            this.distributionMode = distributionMode;
            this.destinationID = destinationID;
            this.tag = tag;
            this.subject = subject;
            this.data = null;
            this.dataBytes = null;
        }

        internal NetworkMessage(ushort senderID, byte distributionMode, ushort destinationID, byte tag, ushort subject, DataBufferItem dataBytes)
        {
            this.senderID = senderID;
            this.distributionMode = distributionMode;
            this.destinationID = destinationID;
            this.tag = tag;
            this.subject = subject;
            this.data = null;
            this.dataBytes = dataBytes;
        }

        public NetworkMessage(ushort senderID, DistributionType distributionType, ushort destinationID, byte tag, ushort subject, object data)
        {
            this.senderID = senderID;
            this.distributionMode = (byte)distributionType;
            this.destinationID = destinationID;
            this.tag = tag;
            this.subject = subject;
            this.data = data;
            this.dataBytes = null;
        }

        public NetworkMessage(ushort senderID, byte distributionMode, ushort destinationID, byte tag, ushort subject, object data)
        {
            this.senderID = senderID;
            this.distributionMode = distributionMode;
            this.destinationID = destinationID;
            this.tag = tag;
            this.subject = subject;
            this.data = data;
            this.dataBytes = null;
        }

        public NetworkMessage(ushort senderID, DistributionType distributionType, ushort destinationID, byte tag, ushort subject, NetworkWriter data)
        {
            this.senderID = senderID;
            this.distributionMode = (byte)distributionType;
            this.destinationID = destinationID;
            this.tag = tag;
            this.subject = subject;
            this.data = data;
            this.dataBytes = null;
        }

        public void DecodeData()
        {
            if (this.data == null)
            {
                this.data = TransmissionProtocol.DecodeMessageData(this.dataBytes);
            }
        }
    }
}