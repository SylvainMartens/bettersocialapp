namespace Network
{
    public enum DistributionType
    {
        All,
        Server,
        Others,
        ID,
        Reply,
        Custom
    }
}