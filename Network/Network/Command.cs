using System;

namespace Network
{
    public struct Command
    {
        public string name;

        public string description;

        public Action<string[]> callback;

        public Command(string name, string description, Action<string[]> callback)
        {
            this.name = name;
            this.description = description;
            this.callback = callback;
        }
    }
}