namespace Network
{
    internal interface IPlugin
    {
        string name
        {
            get;
        }

        string version
        {
            get;
        }

        Command[] commands
        {
            get;
        }

        string author
        {
            get;
        }

        string supportEmail
        {
            get;
        }
    }
}