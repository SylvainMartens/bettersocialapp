using System;

namespace Network
{
    internal class SystemCommands
    {
        internal static void ListCommands(string[] parts)
        {
            string text = "";
            foreach (Command current in Interface.commands.Values)
            {
                string text2 = text;
                text = string.Concat(new string[]
                {
                    text2,
                    current.name,
                    "\n\t",
                    current.description,
                    "\n"
                });
            }
            Interface.Log(text);
        }

        internal static void Close(string[] parts)
        {
            NetworkServer.Close(true);
        }

        internal static void Kick(string[] parts)
        {
            try
            {
                NetworkServer.GetConnectionServiceByID(ushort.Parse(parts[0])).Close(true, null);
            }
            catch (IndexOutOfRangeException)
            {
                Interface.LogError("Invalid Connection ID");
            }
            catch (FormatException)
            {
                Interface.LogError("Not a valid number!");
            }
        }

        internal static void KickAll(string[] parts)
        {
            while (NetworkServer.connections.Count > 0)
            {
                if (NetworkServer.connections[0] != null)
                {
                    NetworkServer.connections[0].Close(true, null);
                }
                else
                {
                    NetworkServer.connections.RemoveAt(0);
                }
            }
        }
    }
}