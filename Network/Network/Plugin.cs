using System.Collections.Generic;
using System.IO;

namespace Network
{
    public abstract class Plugin : IPlugin
    {
        public abstract string name
        {
            get;
        }

        public abstract string version
        {
            get;
        }

        public abstract Command[] commands
        {
            get;
        }

        public abstract string author
        {
            get;
        }

        public abstract string supportEmail
        {
            get;
        }

        public virtual void Update()
        {
        }

        protected bool IsInstalled()
        {
            return Directory.Exists(this.GetSubdirectory());
        }

        protected void InstallSubdirectory(Dictionary<string, byte[]> fileContents)
        {
            if (!this.IsInstalled())
            {
                Directory.CreateDirectory(this.GetSubdirectory());
                foreach (KeyValuePair<string, byte[]> current in fileContents)
                {
                    File.WriteAllBytes(this.GetSubdirectory() + Path.DirectorySeparatorChar + current.Key, current.Value);
                }
            }
        }

        public string GetSubdirectory()
        {
            return string.Concat(new object[]
            {
                Directory.GetCurrentDirectory(),
                Path.DirectorySeparatorChar,
                "Plugins/",
                Path.DirectorySeparatorChar,
                this.name
            });
        }
    }
}