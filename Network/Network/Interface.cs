using System;
using System.Collections.Generic;
using System.IO;

namespace Network
{
    public class Interface
    {
        public delegate void CommandEvent(string[] messageParts);

        internal static Dictionary<string, Command> commands = new Dictionary<string, Command>();

        private static string logFilePath = "";

        private static object consoleLock = new object();

        private static object logfileLock = new object();

        private static Action<string> onLog;

        private static Action<string> onWarning;

        private static Action<string> onError;

        private static Action<string> onFatal;

        public static void LogTrace(string message)
        {
            LogTrace(message, "Trace");
        }

        internal static void LogTrace(string message, string prefix)
        {
            message.Replace("\n", "\n\t\t\t");
            lock (logfileLock)
            {
                using (StreamWriter streamWriter = new StreamWriter(logFilePath, true))
                {
                    streamWriter.WriteLine(string.Concat(new string[]
                    {
                        DateTime.Now.ToString("d/M/yyyy HH:mm:ss tt"),
                        "\t[",
                        prefix,
                        "]\t",
                        message
                    }));
                }
            }
        }

        public static void Log(string message)
        {
            lock (consoleLock)
            {
                onLog?.Invoke(message);
            }
            LogTrace(message, "Log");
        }

        public static void LogWarning(string message)
        {
            lock (consoleLock)
            {
                onWarning?.Invoke(message);
            }
            LogTrace(message, "Warning");
        }

        public static void LogError(string message)
        {
            lock (consoleLock)
            {
                onError?.Invoke(message);
            }
            LogTrace(message, "Error");
        }

        public static void LogFatal(string message)
        {
            lock (consoleLock)
            {
                onFatal?.Invoke(message);
            }
            LogTrace(message, "Fatal");
        }

        internal static void Setup(Action<string> onLog, Action<string> onWarning, Action<string> onError, Action<string> onFatal)
        {
            if (!Directory.Exists(string.Concat(new object[]
            {
                Directory.GetCurrentDirectory(),
                Path.DirectorySeparatorChar,
                "Logs",
                Path.DirectorySeparatorChar,
                DateTime.Now.ToString("d-M-yyyy")
            })))
            {
                Directory.CreateDirectory(string.Concat(new object[]
                {
                    Directory.GetCurrentDirectory(),
                    Path.DirectorySeparatorChar,
                    "Logs",
                    Path.DirectorySeparatorChar,
                    DateTime.Now.ToString("d-M-yyyy")
                }));
            }
            logFilePath = string.Concat(new object[]
            {
                Directory.GetCurrentDirectory(),
                Path.DirectorySeparatorChar,
                "Logs",
                Path.DirectorySeparatorChar,
                DateTime.Now.ToString("d-M-yyyy"),
                Path.DirectorySeparatorChar,
                DateTime.Now.ToString("HH-mm-ss tt"),
                ".txt"
            });
            Interface.onLog = onLog;
            Interface.onWarning = onWarning;
            Interface.onError = onError;
            Interface.onFatal = onFatal;
        }

        internal static void SetupServerCommands()
        {
            Command[] array = new Command[5];
            array[0] = new Command("ListCommands", "Lists all the commands available.", new Action<string[]>(SystemCommands.ListCommands));
            array[1] = new Command("Stop", "Stops the server and closes it safely.", new Action<string[]>(SystemCommands.Close));
            array[2] = new Command("Kick", "Forces a client of id (parameter) to disconnect.", new Action<string[]>(SystemCommands.Kick));
            array[3] = new Command("KickAll", "Forces all clients to disconnect.", new Action<string[]>(SystemCommands.KickAll));
            array[4] = new Command("LogPerformance", "Outputs the current performance stats to screen.", delegate (string[] args)
            {
                PerformanceMonitor.LogPerformanceData();
            });
            RegisterCommands(array);
        }

        public static void ExecuteCommand(string input)
        {
            string[] array = input.ToLower().Split(new char[]
            {
                ' '
            });
            string[] array2 = new string[array.Length - 1];
            if (array.Length > 1)
            {
                Array.Copy(array, 1, array2, 0, array.Length - 1);
            }
            if (commands.ContainsKey(array[0]))
            {
                LogTrace("Executing command " + commands[array[0]].name);
                commands[array[0]].callback(array2);
                return;
            }
            LogError("No such command found!");
            LogTrace("Could not execute command " + array[0] + " as was not present in commands dictionary.");
        }

        internal static void RegisterCommands(Command[] commandsToAdd)
        {
            for (int i = 0; i < commandsToAdd.Length; i++)
            {
                commands.Add(commandsToAdd[i].name.ToLower(), commandsToAdd[i]);
            }
        }
    }
}