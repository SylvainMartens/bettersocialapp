namespace Network
{
    public class NetworkUtilities
    {
        public static NetworkWriter ConvertReaderToWriter(NetworkReader reader)
        {
            NetworkWriter darkRiftWriter = new NetworkWriter();
            byte[] array = reader.ReadBytes((int)reader.BaseStream.Length);
            darkRiftWriter.Write(array, 0, array.Length);
            return darkRiftWriter;
        }
    }
}