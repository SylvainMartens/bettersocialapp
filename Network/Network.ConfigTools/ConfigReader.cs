using System;
using System.Collections.Generic;
using System.IO;

namespace Network.ConfigTools
{
    public class ConfigReader
    {
        private Dictionary<string, string> nodes = new Dictionary<string, string>();

        private object loadLock = new object();

        public bool loaded
        {
            get;
            private set;
        }

        public string this[string key]
        {
            get
            {
                string result;
                try
                {
                    result = this.nodes[key.ToLower()];
                }
                catch (KeyNotFoundException)
                {
                    result = null;
                }
                return result;
            }
        }

        public ConfigReader(string filename)
        {
            this.Load(filename);
        }

        public void Load(string filename)
        {
            lock (this.loadLock)
            {
                this.nodes.Clear();
                try
                {
                    using (StreamReader streamReader = new StreamReader(filename))
                    {
                        string text;
                        while ((text = streamReader.ReadLine()) != null)
                        {
                            if (!string.IsNullOrEmpty(text.Trim()) && !text.TrimStart(new char[0]).StartsWith("//"))
                            {
                                string[] array = text.ToLower().Split(new char[]
                                {
                                    ':'
                                });
                                try
                                {
                                    this.nodes[array[0].Trim()] = array[1].Trim();
                                }
                                catch (IndexOutOfRangeException)
                                {
                                    Interface.LogWarning("Syntax error in " + filename + ". Make sure the line is in {Key} : {Value} format. Line was not loaded.");
                                }
                            }
                        }
                    }
                    this.loaded = true;
                }
                catch (FileNotFoundException ex)
                {
                    throw ex;
                }
            }
        }

        public bool IsTrue(string key)
        {
            bool result;
            try
            {
                if (this.nodes[key.ToLower()].ToLower() == "true")
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch (KeyNotFoundException)
            {
                result = false;
            }
            return result;
        }
    }
}