using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace Network.Storage
{
    /// <summary>
	/// 	Base class for Database connector plugins
	/// </summary>
	public abstract class Database : IPlugin, IDisposable
    {
        /// <summary>
        /// 	The name of the plugin.
        /// </summary>
        /// <value>The plugin's name.</value>
        public abstract string name
        {
            get;
        }

        /// <summary>
        /// 	The version of the plugin.
        /// </summary>
        /// <value>The plugin's verison</value>
        /// <remarks>Must be in dot format (eg: 1.2.10)</remarks>
        public abstract string version
        {
            get;
        }

        /// <summary>
        /// 	A list of commands this plugin uses.
        /// </summary>
        /// <value>The commands used by this plugin.</value>
        public abstract Command[] commands
        {
            get;
        }

        /// <summary>
        /// 	The author of the plugin.
        /// </summary>
        /// <value>The author.</value>
        public abstract string author
        {
            get;
        }

        /// <summary>
        /// 	An email at which users can get support at.
        /// </summary>
        /// <value>A support email.</value>
        public abstract string supportEmail
        {
            get;
        }

        /// <summary>
        /// 	Gets the name of the database this plugin operates with.
        /// </summary>
        /// <value>The name of the database.</value>
        public abstract string databaseName
        {
            get;
        }

        /// <summary>
        /// 	Executes a query on the database returning an array of rows.
        /// </summary>
        /// <returns>The rows of the database selected.</returns>
        /// <param name="query">The query.</param>
        /// <param name="paramaters">The paramaters to be added to this query.</param>
        public abstract DatabaseRow[] ExecuteQuery(string query, params QueryParameter[] paramaters);

        /// <summary>
        /// 	Executes a query on the database with a scalar return.
        /// </summary>
        /// <returns>The object returned from the database.</returns>
        /// <param name="query">The query.</param>
        /// <param name="paramaters">The paramaters to be added to this query.</param>
        public abstract object ExecuteScalar(string query, params QueryParameter[] paramaters);

        /// <summary>
        /// 	Execute the specified query on the database.
        /// </summary>
        /// <param name="query">The query.</param>
        ///             <param name="paramaters">The paramaters to be added to this query.</param>
        public abstract void ExecuteNonQuery(string query, params QueryParameter[] paramaters);

        /// <summary>
        /// 	Removes any characters that could allow SQL injection.
        /// </summary>
        /// <param name="c">Th string to escape</param>
        /// <param name="query">Query.</param>
        public virtual string EscapeString(string s)
        {
            return Regex.Escape(s);
        }

        /// <summary>
        /// 	Releases all resource used by the <see cref="T:DarkRift.Storage.Database" /> object.
        /// </summary>
        /// <remarks>Call <see cref="M:DarkRift.Storage.Database.Dispose" /> when you are finished using the <see cref="T:DarkRift.Storage.Database" />. The
        /// <see cref="M:DarkRift.Storage.Database.Dispose" /> method leaves the <see cref="T:DarkRift.Storage.Database" /> in an unusable state. After
        /// calling <see cref="M:DarkRift.Storage.Database.Dispose" />, you must release all references to the <see cref="T:DarkRift.Storage.Database" /> so
        /// the garbage collector can reclaim the memory that the <see cref="T:DarkRift.Storage.Database" /> was occupying.</remarks>
        public abstract void Dispose();

        /// <summary>
        /// 	Determines whether the plugin is installed.
        /// </summary>
        /// <returns><c>true</c> if the plugin is installed; otherwise, <c>false</c>.</returns>
        protected bool IsInstalled()
        {
            return Directory.Exists(this.GetSubdirectory());
        }

        /// <summary>
        /// 	Creates the plugin's subdirectory and writes the default files to it.
        /// </summary>
        /// <param name="fileContents">File contents.</param>
        protected void InstallSubdirectory(Dictionary<string, byte[]> fileContents)
        {
            if (!this.IsInstalled())
            {
                Directory.CreateDirectory(this.GetSubdirectory());
                foreach (KeyValuePair<string, byte[]> current in fileContents)
                {
                    File.WriteAllBytes(this.GetSubdirectory() + "/" + current.Key, current.Value);
                }
            }
        }

        /// <summary>
        /// 	Gets the address of the plugin's subdirectory.
        /// </summary>
        /// <returns>The subdirectory address.</returns>
        public string GetSubdirectory()
        {
            return Directory.GetCurrentDirectory() + "\\Plugins\\" + this.name;
        }
    }
}