﻿using Network;
using Network.Storage;
using Server.Shared.Controllers;
using SocialAPP.Shared.Data;
using System;
using System.Collections.Generic;
using DatabaseRow = Network.Storage.DatabaseRow;
using Interface = Network.Interface;
using NetworkWriter = Network.NetworkWriter;
using NetworkReader = Network.NetworkReader;

namespace Server.Main.Controllers
{
    public static class AccountsController
    {
        public static void SaveAccountData(AccountData accountData)
        {
            NetworkServer.database.ExecuteNonQuery("UPDATE accounts SET password = @password, email = @email, firstname = @firstname, lastname = @lastname, lastIp = @lastIp, isSuspended = @isSuspended, suspendedReason = @suspendedReason, isBanned = @isBanned, bannedReason = @bannedReason, picture = @picture, premiumMode = @premiumMode, isPremium = @isPremium, latitude = @latitude, longitude = @longitude, dob = @dob, gender = @gender, city = @city, job = @job, activities = @activities, description = @description, lookingFor = @lookingFor, lookingForDescription = @lookingForDescription WHERE id = @accountId LIMIT 1",
                new QueryParameter("accountId", accountData.Id),
                new QueryParameter("password", accountData.Password),
                new QueryParameter("email", accountData.Email),
                new QueryParameter("firstname", accountData.Firstname),
                new QueryParameter("lastname", accountData.Lastname),
                new QueryParameter("lastIp", accountData.LastIp),
                new QueryParameter("isSuspended", accountData.IsSuspended),
                new QueryParameter("suspendedReason", accountData.SuspendedReason),
                new QueryParameter("isBanned", accountData.IsBanned),
                new QueryParameter("bannedReason", accountData.BannedReason),
                new QueryParameter("picture", accountData.Avatar),
                new QueryParameter("premiumMode", accountData.PremiumMode),
                new QueryParameter("isPremium", accountData.IsPremium),
                new QueryParameter("latitude", accountData.Latitude),
                new QueryParameter("longitude", accountData.Longitude),
                new QueryParameter("dob", accountData.Dob_y + "-" + accountData.Dob_m + "-" + accountData.Dob_d + " 00:00"),
                new QueryParameter("gender", accountData.Gender),
                new QueryParameter("city", accountData.City),
                new QueryParameter("job", accountData.Job),
                new QueryParameter("activities", accountData.Activities),
                new QueryParameter("description", accountData.Description),
                new QueryParameter("lookingFor", accountData.LookingFor),
                new QueryParameter("lookingForDescription", accountData.LookingForDescription)
            );
        }

        public static void CacheAccount(AccountData accountData)
        {
            if (accountData == null) return;
            CacheController.Set("acc_id_" + accountData.Id, accountData, 3600);
            CacheController.Set("acc_usr_" + accountData.Username, accountData, 120);
            CacheController.Set("acc_email_" + accountData.Email, accountData, 120);
        }

        private static AccountData AccountDataFromDatabaseRow(DatabaseRow row)
        {
            if (row.Count == 25)
            {
                try
                {
                    var dob = row["dob"].ToString().Split(' ')[0].Split('/');
                    return new AccountData()
                    {
                        Id = row["id"].ToString(),
                        Username = row["username"].ToString(),
                        Password = row["password"].ToString(),
                        Email = row["email"].ToString(),
                        Firstname = row["firstname"].ToString(),
                        Lastname = row["lastname"].ToString(),
                        CreationIp = row["creationIp"].ToString(),
                        LastIp = row["lastIp"].ToString(),
                        IsSuspended = row["isSuspended"].ToString() == "1",
                        SuspendedReason = row["suspendedReason"].ToString(),
                        IsBanned = row["isBanned"].ToString() == "1",
                        BannedReason = row["bannedReason"].ToString(),
                        Avatar = row["picture"].ToString(),
                        PremiumMode = Convert.ToInt32(row["premiumMode"].ToString()),
                        IsPremium = PaymentsController.HasMembership(row["id"].ToString()),
                        Latitude = Convert.ToDouble(row["latitude"].ToString()),
                        Longitude = Convert.ToDouble(row["longitude"].ToString()),
                        Dob_d = dob[1],
                        Dob_m = dob[0],
                        Dob_y = dob[2],
                        Gender = Convert.ToInt32(row["gender"].ToString()),
                        City = row["city"].ToString(),
                        Job = row["job"].ToString(),
                        Activities = row["activities"].ToString(),
                        Description = row["description"].ToString(),
                        LookingFor = row["lookingFor"].ToString(),
                        LookingForDescription = row["lookingForDescription"].ToString(),
                    };
                }
                catch (Exception ex) { Interface.LogError(ex.StackTrace); }
            }
            return null;
        }

        public static AccountData GetAccountById(string id, bool alsoSearchMysql = true)
        {
            var cachedAccount = CacheController.Get<AccountData>("acc_id_" + id);
            if (cachedAccount != null) return cachedAccount;
            if (!alsoSearchMysql) return null;
            //Get Account from MySQL
            var rows = NetworkServer.database.ExecuteQuery("SELECT * FROM accounts WHERE id=@accountId LIMIT 1", new QueryParameter("accountId", id));
            if (rows.Length > 0)
            {
                var accountData = AccountDataFromDatabaseRow(rows[0]);
                CacheAccount(accountData);
                return accountData;
            }
            return null;
        }

        public static AccountData GetAccountByUsername(string username)
        {
            var cachedAccount = CacheController.Get<AccountData>("acc_usr_" + username);
            if (cachedAccount != null) return cachedAccount;
            //Get Account from MySQL
            var rows = NetworkServer.database.ExecuteQuery("SELECT * FROM accounts WHERE username=@accountUsername LIMIT 1", new QueryParameter("accountUsername", username));
            if (rows.Length > 0)
            {
                var accountData = AccountDataFromDatabaseRow(rows[0]);
                CacheAccount(accountData);
                return accountData;
            }
            return null;
        }

        public static AccountData GetAccountByEmail(string email)
        {
            var cachedAccount = CacheController.Get<AccountData>("acc_email_" + email);
            if (cachedAccount != null) return cachedAccount;
            //Get Account from MySQL
            var rows = NetworkServer.database.ExecuteQuery("SELECT * FROM accounts WHERE email=@accountEmail LIMIT 1", new QueryParameter("accountEmail", email));
            if (rows.Length > 0)
            {
                var accountData = AccountDataFromDatabaseRow(rows[0]);
                CacheAccount(accountData);
                return accountData;
            }
            return null;
        }

        public static void CreateAccount(AccountData accountData)
        {
            lock (NetworkServer.database)
            {
                NetworkServer.database.ExecuteNonQuery("INSERT INTO accounts (username, password, email, firstname, lastname, creationIp, lastIp, dob, gender) VALUES (@username, @password, @email, @firstname, @lastname, @creationIp, @lastIp, @dob, @gender)",
                    new QueryParameter("username", accountData.Username),
                    new QueryParameter("password", accountData.Password),
                    new QueryParameter("email", accountData.Email),
                    new QueryParameter("firstname", accountData.Firstname),
                    new QueryParameter("lastname", accountData.Lastname),
                    new QueryParameter("creationIp", accountData.CreationIp),
                    new QueryParameter("lastIp", accountData.LastIp),
                    new QueryParameter("dob", accountData.Dob_y + "-" + accountData.Dob_m + "-" + accountData.Dob_d + " 00:00"),
                    new QueryParameter("gender", accountData.Gender)
                );
            }
        }
        
        public static Dictionary<List<AccountData>, int> SearchAccounts(string accountId, double latitude, double longitude, int rangeInKM, string username, string gender, string minAge, string maxAge, int limit, int page = 1, bool isPremium = false)
        {
            List<AccountData> returnList = new List<AccountData>();
            int totalCount = 0;
            //Find profiles here...
            var query = "";

            if (rangeInKM != 0)
                query = "SELECT SQL_CALC_FOUND_ROWS * FROM accounts AS account WHERE (((acos(sin((@orig_lat*pi()/180)) * sin((account.latitude*pi()/180))+cos((@orig_lat*pi()/180))*cos((account.latitude*pi()/180))*cos(((@orig_lon-account.longitude)*pi()/180))))*180/pi())*60*1.1515*1609.344) <= @dist AND account.id <> @accountId ";
            else
                query = "SELECT SQL_CALC_FOUND_ROWS * FROM accounts AS account WHERE account.id <> @accountId ";
            //Add Filters here
            if (!string.IsNullOrEmpty(username))
            {
                query += "AND account.username LIKE @username ";
            }
            if (!string.IsNullOrEmpty(gender))
            {
                query += "AND account.gender = @gender ";
            }
            if (!string.IsNullOrEmpty(minAge))
            {
                query += "AND TIMESTAMPDIFF(YEAR, dob, CURDATE()) >= @minAge ";
            }
            if (!string.IsNullOrEmpty(maxAge))
            {
                query += "AND TIMESTAMPDIFF(YEAR, dob, CURDATE()) <= @maxAge ";
            }

            if (!isPremium)
            {
                //SHOW ONLY PREMIUM MEMBERS...
                query += "AND EXISTS (SELECT * FROM transactions AS transaction WHERE transaction.userId = account.id AND DATE(NOW()) < transaction.expireAt) ";
            }
            //Make sure we do not show user if blocked.
            query += "AND NOT EXISTS (SELECT * FROM blocks AS block WHERE block.userId = @accountId AND block.profileId = account.id LIMIT 1) ";
            query += "AND NOT EXISTS (SELECT * FROM blocks AS block WHERE block.userId = account.id AND block.profileId = @accountId LIMIT 1) ";

            //
            var count_query = query;
            if (rangeInKM != 0)
            {
                query += " ORDER BY (((acos(sin((@orig_lat*pi()/180)) * sin((account.latitude*pi()/180))+cos((@orig_lat*pi()/180))*cos((account.latitude*pi()/180))*cos(((@orig_lon-account.longitude)*pi()/180))))*180/pi())*60*1.1515*1609.344) ASC LIMIT @limit OFFSET @page;";
            }
            else
            {
                query += " ORDER BY id DESC LIMIT @limit OFFSET @page;";
            }
            DatabaseRow[] rows;
            lock (NetworkServer.database)
            {
                rows = NetworkServer.database.ExecuteQuery(query,
                    new QueryParameter("accountId", accountId),
                    new QueryParameter("orig_lat", latitude),
                    new QueryParameter("orig_lon", longitude),
                    new QueryParameter("dist", 1000 * rangeInKM),
                    new QueryParameter("username", "%"+ username+"%"),
                    new QueryParameter("gender", gender),
                    new QueryParameter("minAge", minAge),
                    new QueryParameter("maxAge", maxAge),
                    new QueryParameter("limit", limit),
                    new QueryParameter("page", limit * (page - 1)));
                //if(rows.Length > 0)
                totalCount = Convert.ToInt32(NetworkServer.database.ExecuteScalar(count_query.Replace("SQL_CALC_FOUND_ROWS *", "COUNT(*)"),
                    new QueryParameter("accountId", accountId),
                    new QueryParameter("orig_lat", latitude),
                    new QueryParameter("orig_lon", longitude),
                    new QueryParameter("dist", 1000 * rangeInKM),
                    new QueryParameter("username", "%" + username + "%"),
                    new QueryParameter("gender", gender),
                    new QueryParameter("minAge", minAge),
                    new QueryParameter("maxAge", maxAge)));
            }

            foreach (var row in rows)
            {
                var account = AccountDataFromDatabaseRow(row);
                account.Password = "";
                account.Email = "";
                account.LastIp = "";
                account.CreationIp = "";

                returnList.Add(account);
            }

            //Return Data
            var returnDict = new Dictionary<List<AccountData>, int>();
            returnDict.Add(returnList, totalCount);
            return returnDict;
        }
    }
}