﻿using Network;
using Network.Storage;
using Server.Shared.Controllers;
using SocialAPP.Shared.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using DatabaseRow = Network.Storage.DatabaseRow;
using Interface = Network.Interface;
using NetworkWriter = Network.NetworkWriter;
using NetworkReader = Network.NetworkReader;

namespace Server.Main.Controllers
{
    public static class BlockController
    {
        public static bool HasBlocked(string user1Id, string user2Id)
        {
            try
            {
                var blockData = CacheController.Get<BlockData>("block_u_" + user1Id + "_" + user2Id);
                if (blockData != null)
                {
                    CacheBlockData(blockData);
                    return true;
                }
                else
                {
                    var rows = NetworkServer.database.ExecuteQuery("SELECT * FROM blocks WHERE userId = @user1Id AND profileId = @user2Id LIMIT 1", new QueryParameter("user1Id", user1Id), new QueryParameter("user2Id", user2Id));
                    if (rows.Length == 1)
                    {
                        blockData = RowToBlockData(rows[0]);
                        if (blockData != null)
                        {
                            CacheBlockData(blockData);
                            return true;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                Interface.LogError(ex.StackTrace);
            }
            return false;
        }

        public static bool BlockUser(string myAccountId, string otherAccountId)
        {
            if (HasBlocked(myAccountId, otherAccountId)) return true;//This user is already blocked!
            try
            {
                var blockedUser = AccountsController.GetAccountById(otherAccountId);
                if (blockedUser == null) return false;

                var insertedId = "";
                lock (NetworkServer.database)
                {
                    insertedId = Convert.ToInt32(NetworkServer.database.ExecuteScalar("INSERT INTO blocks (userId, profileId, dateAt) VALUES (@myAccountId, @otherAccountId, NOW()); SELECT LAST_INSERT_ID();", new QueryParameter("myAccountId", myAccountId), new QueryParameter("otherAccountId", otherAccountId))).ToString();
                }
                if (!string.IsNullOrEmpty(insertedId))
                {
                    var blockData = new BlockData()
                    {
                        Id = insertedId,
                        UserId = myAccountId,
                        ProfileId = otherAccountId,
                        ProfileUserName = blockedUser.Username,
                        ProfileAvatar = blockedUser.Avatar,
                        ProfileGender = blockedUser.Gender,
                        DateAt = DateTime.Now
                    };
                    CacheBlockData(blockData);

                    FollowsController.UnFollow(myAccountId, otherAccountId, false);
                    FollowsController.UnFollow(otherAccountId, myAccountId, false);

                    return true;
                }
            }
            catch(Exception ex)
            {
                Interface.LogError(ex.StackTrace);
            }
            return false;
        }

        public static bool UnBlockUser(string myAccountId, string otherAccountId)
        {
            if (!HasBlocked(myAccountId, otherAccountId)) return true;//This user is not even blocked!
            try
            {
                var rows = NetworkServer.database.ExecuteQuery("SELECT * FROM blocks WHERE userId = @user1Id AND profileId = @user2Id LIMIT 1", new QueryParameter("user1Id", myAccountId), new QueryParameter("user2Id", otherAccountId));
                if (rows.Length == 1)
                {
                    var blockData = RowToBlockData(rows[0]);
                    if (blockData != null)
                    {
                        NetworkServer.database.ExecuteNonQuery("DELETE FROM blocks WHERE id = @blockId", new QueryParameter("blockId", blockData.Id));
                        UnCacheBlockData(blockData);
                        return true;
                    }
                }
                else return true;
            }
            catch(Exception ex)
            {
                Interface.LogError(ex.StackTrace);
            }
            return false;
        }

        public static List<BlockData> GetBlockedUsers(string myAccountId)
        {
            try
            {
                var blockedArray = CacheController.Get<BlockData[]>("blocks_" + myAccountId);
                if (blockedArray != null)
                {
                    CacheController.Set("blocks_" + myAccountId, blockedArray, 3600);
                    return blockedArray.ToList();
                }
                else
                {
                    var returnList = new List<BlockData>();
                    var rows = NetworkServer.database.ExecuteQuery("SELECT * FROM blocks WHERE userId = @userId", new QueryParameter("userId", myAccountId));
                    foreach (var row in rows)
                    {
                        var blockData = RowToBlockData(row);
                        if (blockData != null)
                        {
                            CacheBlockData(blockData);
                            returnList.Add(blockData);
                        }
                    }
                    CacheController.Set("blocks_" + myAccountId, returnList.ToArray(), 3600);
                    return returnList;
                }
            }
            catch(Exception ex)
            {
                Interface.LogError(ex.StackTrace);
            }
            return new List<BlockData>();
        }

        public static void CacheBlockData(BlockData blockData)
        {
            if (blockData == null) return;
            CacheController.Set("block_" + blockData.Id, blockData, 3600);
            CacheController.Set("block_u_" + blockData.UserId + "_" + blockData.ProfileId, blockData, 3600);

            var blocksArray = CacheController.Get<BlockData[]>("blocks_" + blockData.UserId);
            if (blocksArray != null)
            {
                var list = blocksArray.ToList();
                var blockFound = list.FirstOrDefault(x => x.Id == blockData.Id);
                if (blockFound == null)
                {
                    list.Add(blockData);
                    CacheController.Set("blocks_" + blockData.UserId, list.ToArray(), 3600);
                }
            }
        }

        public static void UnCacheBlockData(BlockData blockData)
        {
            if (blockData == null) return;
            CacheController.Remove("block_" + blockData.Id);
            CacheController.Remove("block_u_" + blockData.UserId + "_" + blockData.ProfileId);

            var blocksArray = CacheController.Get<BlockData[]>("blocks_" + blockData.UserId);
            if (blocksArray != null)
            {
                var list = blocksArray.ToList();
                var blockFound = list.FirstOrDefault(x => x.Id == blockData.Id);
                if (blockFound != null)
                {
                    list.Remove(blockFound);
                    CacheController.Set("blocks_" + blockData.UserId, list.ToArray(), 3600);
                }
            }
        }

        public static void UnCacheBlockData(string blockId)
        {
            if (string.IsNullOrEmpty(blockId)) return;
            var blockData = CacheController.Get<BlockData>("block_" + blockId);
            if (blockData != null)
            {
                UnCacheBlockData(blockData);
            }
        }

        public static BlockData RowToBlockData(DatabaseRow row)
        {
            if(row != null && row.Count == 4)
            {
                try
                {
                    var otherAccountId = row["profileId"].ToString();
                    var blockedUser = AccountsController.GetAccountById(otherAccountId);
                    if (blockedUser == null) return null;

                    DateTime dateAt = DateTime.Parse(row["dateAt"].ToString());
                    return new BlockData()
                    {
                        Id = row["id"].ToString(),
                        UserId = row["userId"].ToString(),
                        ProfileId = otherAccountId,
                        ProfileUserName = blockedUser.Username,
                        ProfileAvatar = blockedUser.Avatar,
                        ProfileGender = blockedUser.Gender,
                        DateAt = dateAt
                    };
                }catch(Exception ex)
                {
                    Interface.LogError(ex.StackTrace);
                }
            }
            return null;
        }
    }
}