﻿using Network;
using Network.Storage;
using System;
using DatabaseRow = Network.Storage.DatabaseRow;
using Interface = Network.Interface;
using NetworkWriter = Network.NetworkWriter;
using NetworkReader = Network.NetworkReader;

namespace Server.Main.Controllers
{
    public static class UploadsController
    {
        public static string CreateProfileUploadToken(string accountId, string Ip)
        {
            string returnId = "";
            lock (NetworkServer.database)
            {
                returnId = Convert.ToInt32(NetworkServer.database.ExecuteScalar("INSERT INTO uploadTokens (accountId, ip) VALUES (@accountId, @Ip);SELECT LAST_INSERT_ID();", new QueryParameter("accountId", accountId), new QueryParameter("Ip", Ip))).ToString();
                //NetworkServer.database.ExecuteScalar("")
            }
            Interface.Log("Last uploadTokens Id = " + returnId);
            return returnId;
        }
    }
}