﻿using Network;
using Network.Storage;
using Server.Shared;
using Server.Shared.Controllers;
using SocialAPP.Shared;
using SocialAPP.Shared.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DatabaseRow = Network.Storage.DatabaseRow;
using QueryParameter = Network.Storage.QueryParameter;
using Interface = Network.Interface;
using NetworkWriter = Network.NetworkWriter;
using NetworkReader = Network.NetworkReader;

namespace Server.Main.Controllers
{
    public static class GroupsController
    {
        #region Group
        //Get group by name
        public static GroupData GetGroupByName(string groupName)
        {
            GroupData group = CacheController.Get<GroupData>("group_n_" + groupName);
            if(group != null)
            {
                CacheGroup(group);
                return group;
            }else
            {
                //Get group from MySQL
                var rows = NetworkServer.database.ExecuteQuery("SELECT * FROM groups WHERE groupName = @groupName LIMIT 1", new QueryParameter("groupName", groupName));
                if (rows.Length == 1)
                {
                    group = DatabaseRowToGroupData(rows[0]);
                    CacheGroup(group);
                    return group;
                }
            }
            return null;
        }

        //Get group by Id
        public static GroupData GetGroupById(string groupId)
        {
            GroupData group = CacheController.Get<GroupData>("group_i_" + groupId);
            if (group != null)
            {
                CacheGroup(group);
                return group;
            }
            else
            {
                //Get group from MySQL
                var rows = NetworkServer.database.ExecuteQuery("SELECT * FROM groups WHERE id = @groupId LIMIT 1", new QueryParameter("groupId", groupId));
                if (rows.Length == 1)
                {
                    group = DatabaseRowToGroupData(rows[0]);
                    CacheGroup(group);
                    return group;
                }
            }
            return null;
        }

        //Create group
        public static GroupData CreateGroup(string userId, string groupName)
        {
            if(GetGroupByName(groupName) != null)
            {
                return null;
            }else
            {
                try
                {
                    var groupId = "";
                    lock (NetworkServer.database)
                    {
                        groupId = Convert.ToInt32(NetworkServer.database.ExecuteScalar("INSERT INTO groups (ownerId, groupName, isPublic, createdAt) VALUES(@ownerId, @groupName, @isPublic, NOW()); SELECT LAST_INSERT_ID();",
                            new QueryParameter("ownerId", userId),
                            new QueryParameter("groupName", groupName),
                            new QueryParameter("isPublic", "0"))).ToString();
                    }
                    var group = new GroupData()
                    {
                        Id = groupId,
                        Name = groupName,
                        IsPublic = false,
                        OwnerId = userId,
                        CreatedAt = DateTime.Now
                    };
                    CacheGroup(group);
                    
                    return group;
                }catch(Exception e)
                {
                    Interface.LogError(e.StackTrace);
                }
            }
            return null;
        }

        public static GroupData DatabaseRowToGroupData(DatabaseRow row)
        {
            if (row != null && row.Count == 5)
            {
                try
                {
                    DateTime createdAt = DateTime.Parse(row["createdAt"].ToString());
                    return new GroupData()
                    {
                        Id = row["id"].ToString(),
                        OwnerId = row["ownerId"].ToString(),
                        Name = row["groupName"].ToString(),
                        IsPublic = row["isPublic"].ToString() == "1",
                        CreatedAt = createdAt
                    };
                }
                catch (Exception ex) { Interface.LogError(ex.StackTrace); }
            }
            return null;
        }

        //Delete group
        public static void DeleteGroup(string userId, string groupId)
        {
            var group = GetGroupById(groupId);
            if (group == null || group.OwnerId != userId) return;
            var groupMembers = GetGroupMembers(userId, groupId);
            //Delete group members
            foreach(var groupMember in groupMembers)
            {
                DeleteGroupMember(userId, groupMember.UserId, groupId);
            }
            //Delete group
            NetworkServer.database.ExecuteNonQuery("DELETE FROM groups WHERE id = @groupId", new QueryParameter("groupId", groupId));
            DeleteCacheGroup(group);
        }

        //Cache group data
        public static void CacheGroup(GroupData group)
        {
            if (group == null) return;
            CacheController.Set("group_i_" + group.Id, group, 3600);
            CacheController.Set("group_n_" + group.Name, group, 3600);
        }
        public static void DeleteCacheGroup(GroupData group)
        {
            if (group == null) return;
            //TODO
        }

        //Set group public
        public static void SetGroupPublic(string userId, string groupId, bool isPublic)
        {
            var group = GetGroupById(groupId);
            if (group == null) return;
            if (!IsGroupMemberAdmin(userId, groupId)) return;
            NetworkServer.database.ExecuteNonQuery("UPDATE groups SET isPublic = @isPublic WHERE id = @groupId", new QueryParameter("groupId", groupId), new QueryParameter("isPublic", isPublic ? 1 : 0));
            group.IsPublic = isPublic;
            CacheGroup(group);
        }

        //Search group
        public static List<GroupData> SearchGroup(string userId, string groupName)
        {
            var returnList = new List<GroupData>();
            var rows = NetworkServer.database.ExecuteQuery("SELECT * FROM groups AS group WHERE group.groupName LIKE @groupName AND (group.isPublic = 0 OR EXISTS(SELECT * FROM groupmembers as member WHERE member.groupId = group.id AND member.userId = @userId))", new QueryParameter("groupName", "%" + groupName + "%"), new QueryParameter("userId", userId));
            foreach (var row in rows)
            {
                var group = DatabaseRowToGroupData(row);
                returnList.Add(group);
            }
            return returnList;
        }

        //Is Group Member
        public static bool IsGroupMember(string userId, string groupId)
        {
            var group = GetGroupById(groupId);
            if (group != null)
            {
                if (group.OwnerId == userId)
                    return true;
                else if (GetGroupMemberById(userId, groupId) != null)
                    return true;
            }
            return false;
        }
        public static bool IsGroupMemberAdmin(string userId, string groupId)
        {
            var groupMember = GetGroupMemberById(userId, groupId);
            if (groupMember != null)
            {
                if (groupMember.IsAdmin)
                    return true;
                else
                {
                    var group = GetGroupById(groupId);
                    if (group.OwnerId == userId) return true;
                }
            }
            return false;
        }
        #endregion

        #region Invitation
        //Invite user to group
        public static GroupInvitationData InviteUserToGroup(string myAccountId, string otherAccountId, string groupId)
        {
            var group = GetGroupById(groupId);
            if (group == null) return null;

            if (!IsGroupMemberAdmin(myAccountId, groupId)) ;

            try
            {
                var invitationId = "";
                lock (NetworkServer.database)
                {
                    invitationId = Convert.ToInt32(NetworkServer.database.ExecuteScalar("INSERT INTO groupinvitations (groupId, userId, fromId, dateAt) VALUES(@groupId, @userId, @fromId, NOW()); SELECT LAST_INSERT_ID();",
                        new QueryParameter("groupId", groupId),
                        new QueryParameter("userId", otherAccountId),
                        new QueryParameter("fromId", myAccountId))).ToString();
                }
                var invitation = new GroupInvitationData()
                {
                    Id = groupId,
                    GroupId = groupId,
                    UserId = otherAccountId,
                    FromId = myAccountId,
                    DateAt = DateTime.Now
                };
                if (invitation == null) return null;

                CacheGroupInvitation(invitation);

                NotificationsController.TriggerNotification(invitation.FromId, invitation.UserId, NotificationTypes.GroupInvite);
                TriggerInviteSent(invitation);

                return invitation;
            }
            catch (Exception e)
            {
                Interface.LogError(e.StackTrace);
            }
            //
            return null;
        }
        
        //Set invitation decision
        public static void SetGroupInvitationResult(string userId, string invitationId, bool result)
        {
            //TODO
            var invitation = GetGroupInvitationById(invitationId);
            if (invitation == null) return;

            if (invitation.UserId != userId) return;
            //
            if(result)
            {
                NotificationsController.TriggerNotification(userId, invitation.FromId, NotificationTypes.GroupInviteAccepted);
                AddGroupMember(userId, invitation.GroupId);
                DeleteInvitation(invitation);
            }
            else
            {
                NotificationsController.TriggerNotification(userId, invitation.FromId, NotificationTypes.GroupInviteDeclined);
                DeleteInvitation(invitation);
            }
        }

        //Get invitation by id
        public static GroupInvitationData GetGroupInvitationById(string invitationId)
        {
            GroupInvitationData invitation = CacheController.Get<GroupInvitationData>("ginvite_i_" + invitationId);
            if (invitation != null)
            {
                CacheGroupInvitation(invitation);
                return invitation;
            }
            else
            {
                //Get group from MySQL
                var rows = NetworkServer.database.ExecuteQuery("SELECT * FROM groupinvitations WHERE id = @invitationId LIMIT 1", new QueryParameter("invitationId", invitationId));
                if (rows.Length == 1)
                {
                    invitation = DatabaseRowToGroupInvitation(rows[0]);
                    CacheGroupInvitation(invitation);
                    return invitation;
                }
            }
            return null;
        }

        //Get invitation by account id
        public static GroupInvitationData GetGroupInvitationByAccountId(string userId, string groupId)
        {
            GroupInvitationData invitation = CacheController.Get<GroupInvitationData>("ginvite_ig_" + userId+"_"+ groupId);
            if (invitation != null)
            {
                CacheGroupInvitation(invitation);
                return invitation;
            }
            else
            {
                //Get group from MySQL
                var rows = NetworkServer.database.ExecuteQuery("SELECT * FROM groupinvitations WHERE userId = @userId AND groupId = @groupId LIMIT 1", new QueryParameter("userId", userId), new QueryParameter("groupId", groupId));
                if (rows.Length == 1)
                {
                    invitation = DatabaseRowToGroupInvitation(rows[0]);
                    CacheGroupInvitation(invitation);
                    return invitation;
                }
            }
            return null;
        }

        //Delete invitation
        public static void DeleteInvitation(GroupInvitationData groupInvitation)
        {
            try
            {
                NetworkServer.database.ExecuteNonQuery("DELETE FROM groupinvitations WHERE id = @invitationId", new QueryParameter("invitationId", groupInvitation.Id));
                DeleteCacheGroupInvitation(groupInvitation);
            }
            catch (Exception ex)
            {
                Interface.LogError(ex.StackTrace);
            }
        }

        public static GroupInvitationData DatabaseRowToGroupInvitation(DatabaseRow row)
        {
            if (row != null && row.Count == 5)
            {
                try
                {
                    DateTime dateAt = DateTime.Parse(row["dateAt"].ToString());
                    return new GroupInvitationData()
                    {
                        Id = row["id"].ToString(),
                        GroupId = row["groupId"].ToString(),
                        UserId = row["userId"].ToString(),
                        FromId = row["fromIs"].ToString(),
                        DateAt = dateAt
                    };
                }
                catch (Exception ex) { Interface.LogError(ex.StackTrace); }
            }
            return null;
        }

        //Cache invitation
        public static void CacheGroupInvitation(GroupInvitationData groupInvitation)
        {
            if (groupInvitation == null) return;
            CacheController.Set("ginvite_i_" + groupInvitation.Id, groupInvitation, 3600);
            CacheController.Set("ginvite_ig_" + groupInvitation.UserId+"_"+groupInvitation.GroupId, groupInvitation, 3600);
            var userInvitations = CacheController.Get<GroupInvitationData[]>("guinv_" + groupInvitation.UserId);
            if (userInvitations == null)
            {
                CacheController.Set("guinv_" + groupInvitation.UserId, new[] { groupInvitation }, 3600);
            }else
            {
                if(userInvitations.Count(x => x.UserId == groupInvitation.UserId) == 0)
                {
                    var list = userInvitations.ToList();
                    list.Add(groupInvitation);
                    CacheController.Set("guinv_" + groupInvitation.UserId, list.ToArray(), 3600);
                }
            }
            var groupInvitations = CacheController.Get<GroupInvitationData[]>("guinv_" + groupInvitation.GroupId);
            if (groupInvitations == null)
            {
                CacheController.Set("guinv_" + groupInvitation.GroupId, new[] { groupInvitation }, 3600);
            }
            else
            {
                if (groupInvitations.Count(x => x.UserId == groupInvitation.UserId) == 0)
                {
                    var list = groupInvitations.ToList();
                    list.Add(groupInvitation);
                    CacheController.Set("guinv_" + groupInvitation.GroupId, list.ToArray(), 3600);
                }
            }
        }

        //Delete cache invitation
        public static void DeleteCacheGroupInvitation(GroupInvitationData groupInvitation)
        {
            if (groupInvitation == null) return;
            CacheController.Remove("ginvite_i_" + groupInvitation.Id);
            CacheController.Remove("ginvite_ig_" + groupInvitation.UserId + "_" + groupInvitation.GroupId);
            var userInvitations = CacheController.Get<GroupInvitationData[]>("guinv_" + groupInvitation.UserId);
            if (userInvitations != null)
            {
                var list = userInvitations.ToList();
                list.Remove(groupInvitation);
                CacheController.Set("guinv_" + groupInvitation.UserId, list.ToArray(), 3600);
            }
            var groupInvitations = CacheController.Get<GroupInvitationData[]>("ginvs_" + groupInvitation.GroupId);
            if (groupInvitations != null)
            {
                var list = groupInvitations.ToList();
                list.Remove(groupInvitation);
                CacheController.Set("ginvs_" + groupInvitation.GroupId, list.ToArray(), 3600);
            }
        }
        
        //Get Pending group invitations
        public static List<GroupInvitationData> GetPendingGroupInvitations(string userId, string groupId)
        {
            var returnList = new List<GroupInvitationData>();
            var group = GetGroupById(groupId);
            if (group == null) return returnList;
            if (!IsGroupMemberAdmin(userId, groupId)) return returnList;
            try
            {

                var rows = NetworkServer.database.ExecuteQuery("SELECT * FROM groupinvitations WHERE groupId = @groupId", new QueryParameter("groupId", groupId));
                foreach(var row in rows)
                {
                    var invitation = DatabaseRowToGroupInvitation(row);
                    returnList.Add(invitation);
                }

                CacheController.Set("ginvs_" + groupId, returnList.ToArray(), 3600);

                return returnList;
            }
            catch(Exception e)
            {
                Interface.LogError(e.StackTrace);
            }
            return returnList;
        }

        //Get My group invitations
        public static List<GroupInvitationData> GetMyGroupInvitations(string userId)
        {
            var returnList = new List<GroupInvitationData>();
            var invitations = CacheController.Get<GroupInvitationData[]>("guinv_" + userId);
            if(invitations != null)
            {
                CacheController.Set("guinv_" + userId, invitations, 3600);
                return invitations.ToList();
            }

            try
            {
                var rows = NetworkServer.database.ExecuteQuery("SELECT * FROM groupinvitations WHERE userId = @userId", new QueryParameter("userId", userId));
                foreach (var row in rows)
                {
                    var invitation = DatabaseRowToGroupInvitation(row);
                    returnList.Add(invitation);
                }
                CacheController.Set("guinv_" + userId, returnList.ToArray(), 3600);
                return returnList;
            }
            catch (Exception e)
            {
                Interface.LogError(e.StackTrace);
            }
            return returnList;
        }
        public static void CancelGroupInvitation(string myAccountId, string otherAcountId, string groupId)
        {
            var group = GetGroupById(groupId);
            if (group == null) return;
            if (!IsGroupMemberAdmin(myAccountId, groupId)) return;
            var invitation = GetGroupInvitationByAccountId(otherAcountId, groupId);
            if (invitation == null) return;
            DeleteCacheGroupInvitation(invitation);
            TriggerInviteCancelled(invitation);
        }
        #endregion
        
        #region Requests
        //Get Request by id
        public static GroupRequestData GetGroupRequestById(string requestId)
        {
            try
            {
                var request = CacheController.Get<GroupRequestData>("greq_" + requestId);
                if(request != null)
                {
                    CacheGroupRequest(request);
                    return request;
                }else
                {
                    var rows = NetworkServer.database.ExecuteQuery("SELECT * FROM grouprequests WHERE id = @requestId LIMIT 1", new QueryParameter("requestId", requestId));
                    if(rows.Length == 1)
                    {
                        request = DatabaseRowToRequestData(rows[0]);
                        if(request != null)
                        {
                            CacheGroupRequest(request);
                            return request;
                        }
                    }
                }
            }catch(Exception e)
            {
                Interface.LogError(e.StackTrace);
            }
            return null;
        }
        //DatabaseRowToRequest
        public static GroupRequestData DatabaseRowToRequestData(DatabaseRow row)
        {
            if(row != null && row.Count == 4)
            {
                try
                {
                    DateTime dateAt = DateTime.Parse(row["dateAt"].ToString());
                    return new GroupRequestData()
                    {
                        Id = row["id"].ToString(),
                        GroupId = row["groupId"].ToString(),
                        UserId = row["userId"].ToString(),
                        DateAt = dateAt
                    };
                }
                catch (Exception ex) { Interface.LogError(ex.StackTrace); }
            }
            return null;
        }

        //Cache Request
        public static void CacheGroupRequest(GroupRequestData request)
        {
            if (request == null) return;
            CacheController.Set("greq_" + request.Id, request, 3600);
            //Group Requests
            {
                var requests = CacheController.Get<GroupRequestData[]>("greqs_" + request.GroupId);
                if (requests == null)
                {
                    CacheController.Set("greqs_" + request.GroupId, new[] { request }, 3600);
                }
                else
                {
                    if (requests.Count(x => x.Id == request.Id) == 0)
                    {
                        var list = requests.ToList();
                        list.Add(request);
                        CacheController.Set("greqs_" + request.GroupId, list.ToArray(), 3600);
                    }
                }
            }
            //User Requests
            {
                var requests = CacheController.Get<GroupRequestData[]>("ugreq_" + request.UserId);
                if (requests == null)
                {
                    CacheController.Set("ugreq_" + request.UserId, new[] { request }, 3600);
                }
                else
                {
                    if (requests.Count(x => x.Id == request.Id) == 0)
                    {
                        var list = requests.ToList();
                        list.Add(request);
                        CacheController.Set("ugreq_" + request.UserId, list.ToArray(), 3600);
                    }
                }
            }
        }
        //Delete Cache Request
        public static void DeleteCacheGroupRequest(GroupRequestData request)
        {
            if (request == null) return;
            CacheController.Remove("greq_" + request.Id);
            //Group Requests
            {
                var requests = CacheController.Get<GroupRequestData[]>("greqs_" + request.GroupId);
                if (requests != null)
                {
                    var list = requests.ToList();
                    list.Remove(request);
                    CacheController.Set("greqs_" + request.GroupId, list.ToArray(), 3600);
                }
            }
            //User Requests
            {
                var requests = CacheController.Get<GroupRequestData[]>("ugreq_" + request.UserId);
                if (requests != null)
                {
                    var list = requests.ToList();
                    list.Remove(request);
                    CacheController.Set("ugreq_" + request.UserId, list.ToArray(), 3600);
                }
            }
        }

        //Request group join
        public static GroupRequestData RequestGroupJoin(string userId, string groupId)
        {
            var group = GetGroupById(groupId);
            if (group == null) return null;
            if (!group.IsPublic) return null;
            try
            {
                var insertedId = "";
                lock (NetworkServer.database)
                {
                    groupId = Convert.ToInt32(NetworkServer.database.ExecuteScalar("INSERT INTO grouprequests (groupId, userId, dateAt) VALUES(@groupId, @userId, NOW()); SELECT LAST_INSERT_ID();",
                        new QueryParameter("userId", userId),
                        new QueryParameter("groupId", groupId))).ToString();
                }
                var request = new GroupRequestData()
                {
                    Id = insertedId,
                    GroupId = groupId,
                    UserId = userId,
                    DateAt = DateTime.Now
                };
                CacheGroupRequest(request);
                TriggerRequestSent(request);

                return request;
            }
            catch(Exception e)
            {
                Interface.LogError(e.StackTrace);
            }
            return null;
        }

        //Set request decision
        public static void SetRequestResult(string myAccountId, string requestId, bool result)
        {
            var request = GetGroupRequestById(requestId);
            if (request == null) return;
            if (!IsGroupMemberAdmin(myAccountId, request.GroupId)) return;
            if(!result)
            {
                DeleteGroupRequest(request);
                NotificationsController.TriggerNotification(request.UserId, myAccountId, NotificationTypes.GroupRequestDeclined);
            }else
            {
                AddGroupMember(request.UserId, request.GroupId);
                DeleteGroupRequest(request);
                NotificationsController.TriggerNotification(request.UserId, myAccountId, NotificationTypes.GroupRequestAccepted);
            }
        }
        
        //Get My Pending group requests
        public static List<GroupRequestData> GetMyPendingGroupRequests(string userId)
        {
            var requests = CacheController.Get<GroupRequestData[]>("ugreq_" + userId);
            if(requests != null)
            {
                CacheController.Set("ugreq_" + userId, requests, 3600);
                return requests.ToList();
            }else
            {
                try
                {
                    var rows = NetworkServer.database.ExecuteQuery("SELECT * FROM grouprequests WHERE userId = @userId", new QueryParameter("userId", userId));
                    var returnList = new List<GroupRequestData>();

                    foreach(var row in rows)
                    {
                        var request = DatabaseRowToRequestData(row);
                        if(request != null)
                        {
                            returnList.Add(request);
                        }
                    }

                    if(returnList.Count > 0) CacheController.Set("ugreq_" + userId, returnList.ToArray(), 3600);

                    return returnList;
                }catch(Exception e)
                {
                    Interface.LogError(e.StackTrace);
                }
            }
            return null;
        }

        //Get Group Requests
        public static List<GroupRequestData> GetGroupRequests(string userId, string groupId)
        {
            var returnList = new List<GroupRequestData>();

            if(IsGroupMemberAdmin(userId, groupId))
            {
                var requests = CacheController.Get<GroupRequestData[]>("greqs_" + groupId);
                if (requests != null)
                {
                    CacheController.Set("greqs_" + groupId, requests, 3600);
                    return requests.ToList();
                }
                else
                {
                    try
                    {
                        var rows = NetworkServer.database.ExecuteQuery("SELECT * FROM grouprequests WHERE groupId = @groupId", new QueryParameter("groupId", groupId));
                        
                        foreach (var row in rows)
                        {
                            var request = DatabaseRowToRequestData(row);
                            if (request != null)
                            {
                                returnList.Add(request);
                            }
                        }

                        if (returnList.Count > 0) CacheController.Set("greqs_" + groupId, returnList.ToArray(), 3600);

                        return returnList;
                    }
                    catch (Exception e)
                    {
                        Interface.LogError(e.StackTrace);
                    }
                }
            }

            return returnList;
        }

        public static void CancelGroupRequest(string userId, string requestId)
        {
            var request = GetGroupRequestById(requestId);
            if(request == null || request.UserId == userId) return;
            TriggerRequestCancelled(request);
            DeleteGroupRequest(request);
        }

        private static void DeleteGroupRequest(GroupRequestData request)
        {
            if (request == null) return;
            NetworkServer.database.ExecuteNonQuery("DELETE FROM grouprequests WHERE id = @requestId", new QueryParameter("requestId", request.Id));
            DeleteCacheGroupRequest(request);
        }

        #endregion

        #region Member

        //Add group member
        public static GroupMemberData GetGroupMemberById(string userId, string groupId)
        {
            var groupMember = CacheController.Get<GroupMemberData>("groupm_" + userId + "_" + groupId);
            if(groupMember != null)
            {
                CacheGroupMember(groupMember);
                return groupMember;
            }else
            {
                var rows = NetworkServer.database.ExecuteQuery("SELECT * FROM groupmembers WHERE userId = @memberId LIMIT 1", new QueryParameter("memberId", userId));
                if (rows.Length == 1)
                {
                    groupMember = DatabaseRowToGroupMember(rows[0]);
                    CacheGroupMember(groupMember);
                    return groupMember;
                }
            }
            return null;
        }

        private static GroupMemberData AddGroupMember(string accountId, string groupId)
        {
            if (GetGroupById(groupId) == null || AccountsController.GetAccountById(accountId) == null || IsGroupMember(accountId, groupId)) return null;
            try
            {
                var memberId = "";
                lock (NetworkServer.database)
                {
                    memberId = Convert.ToInt32(NetworkServer.database.ExecuteScalar("INSERT INTO groupmembers (groupId, userId, isAdmin, createdAt) VALUES(@groupId, @userId, 0, NOW()); SELECT LAST_INSERT_ID();",
                        new QueryParameter("groupId", groupId),
                        new QueryParameter("userId", accountId)
                    )).ToString();
                }
                var groupMember = new GroupMemberData()
                {
                    Id = groupId,
                    GroupId = groupId,
                    UserId = accountId,
                    IsAdmin = false,
                    CreatedAt = DateTime.Now
                };

                CacheGroupMember(groupMember);

                var groupMembers = CacheController.Get<GroupMemberData[]>("group_mbrs_" + groupId);
                if(groupMember == null)
                {
                    CacheController.Set("group_mbrs_" + groupId, new[] { groupMember }, 3600);
                }
                else
                {
                    if(groupMembers.Count(x => x.UserId == accountId) == 0)
                    {
                        var list = groupMembers.ToList();
                        list.Add(groupMember);
                        CacheController.Set("group_mbrs_" + groupId, list.ToArray(), 3600);
                    }
                }
                return groupMember;
            }
            catch(Exception ex)
            {
                Interface.LogError(ex.StackTrace);
            }
            return null;
        }

        public static GroupMemberData DatabaseRowToGroupMember(DatabaseRow row)
        {
            if (row != null && row.Count == 5)
            {
                try
                {
                    DateTime createdAt = DateTime.Parse(row["createdAt"].ToString());
                    return new GroupMemberData()
                    {
                        Id = row["id"].ToString(),
                        UserId = row["userId"].ToString(),
                        GroupId = row["groupId"].ToString(),
                        IsAdmin = row["isAdmin"].ToString() == "1",
                        CreatedAt = createdAt
                    };
                }
                catch (Exception ex) { Interface.LogError(ex.StackTrace); }
            }
            return null;
        }

        public static void CacheGroupMember(GroupMemberData groupMember)
        {
            if (groupMember == null) return;
            CacheController.Set("groupm_" + groupMember.UserId + "_" + groupMember.GroupId, groupMember, 3600);
        }

        public static void DeleteCacheGroupMember(GroupMemberData groupMember)
        {
            if (groupMember == null) return;
            CacheController.Remove("groupm_" + groupMember.UserId + "_" + groupMember.GroupId);
        }

        //Remove group member
        public static void DeleteGroupMember(string myAccountId, string otherAccountId, string groupId)
        {
            if (myAccountId == otherAccountId || IsGroupMemberAdmin(myAccountId, groupId))
            {
                var groupMember = GetGroupMemberById(otherAccountId, groupId);
                if (groupMember == null) return;
                try
                {
                    NetworkServer.database.ExecuteNonQuery("DELETE FROM groupmembers WHERE id = @id", new QueryParameter("id", groupMember.Id));
                    DeleteCacheGroupMember(groupMember);
                    var groupMembers = CacheController.Get<GroupMemberData[]>("group_mbrs_" + groupId);
                    if (groupMembers == null) return;

                    var list = groupMembers.ToList();
                    list.Remove(groupMember);
                    CacheController.Set("group_mbrs_" + groupId, list.ToArray(), 3600);
                }
                catch (Exception ex)
                {
                    Interface.LogError(ex.StackTrace);
                }
            }
        }

        //Get group members
        public static List<GroupMemberData> GetGroupMembers(string myAccountId, string groupId)
        {
            var group = GetGroupById(groupId);
            if (group == null) return default(List<GroupMemberData>);
            if(!group.IsPublic)
            {
                if(!IsGroupMember(myAccountId, groupId)) return default(List<GroupMemberData>);
            }
            var groupMembers = CacheController.Get<GroupMemberData[]>("group_mbrs_"+groupId);
            if(groupMembers != null)
            {
                CacheController.Set("group_mbrs_" + groupId, groupMembers, 3600);
                return groupMembers.ToList();
            }else
            {
                var rows = NetworkServer.database.ExecuteQuery("SELECT * FROM groupmembers WHERE groupId = @groupId LIMIT 1", new QueryParameter("groupId", groupId));
                var returnList = new List<GroupMemberData>();
                foreach(var row in rows)
                {
                    var groupMember = DatabaseRowToGroupMember(row);
                    CacheGroupMember(groupMember);
                    returnList.Add(groupMember);
                }

                CacheController.Set<GroupMemberData[]>("group_mbrs_" + groupId, returnList.ToArray(), 3600);

                return returnList;
            }
            return null;
        }

        //Set group administrators
        public static void SetGroupAdministrator(string myAccountId, string otherAccountId, string groupId, bool isAdmin)
        {
            //TODO
            var group = GetGroupById(groupId);
            if (group == null) return;

            if (group.OwnerId != myAccountId) return;

            var groupMember = GetGroupMemberById(otherAccountId, groupId);
            if (groupMember == null || groupMember.IsAdmin == isAdmin) return;
            try
            {
                NetworkServer.database.ExecuteNonQuery("UPDATE groupmembers SET isAdmin = @isAdmin WHERE userId = @userId LIMIT 1", new QueryParameter("userId", otherAccountId), new QueryParameter("isAdmin", isAdmin ? 1 : 0));
                groupMember.IsAdmin = isAdmin;
                CacheGroupMember(groupMember);
            }
            catch (Exception ex)
            {
                Interface.LogError(ex.StackTrace);
            }
        }

        //Get user groups
        public static List<GroupData> GetUserGroups(string myAccountId, string otherAccountId)
        {
            var groups = CacheController.Get<GroupData[]>("groups_u_"+ otherAccountId);
            if(groups != null)
            {
                CacheController.Set("groups_u_" + otherAccountId, groups, 3600);
                return groups.ToList();
            }else{
                try
                {
                    var rows = NetworkServer.database.ExecuteQuery("SELECT * FROM groups AS group WHERE EXISTS (SELECT * FROM groupmembers AS member WHERE member.userId = @userId AND member.groupId = group.id)", new QueryParameter("userId", otherAccountId));
                    var returnList = new List<GroupData>();
                    foreach(var row in rows)
                    {
                        var group = DatabaseRowToGroupData(row);

                        if(myAccountId != otherAccountId)
                        {
                            if(!group.IsPublic)
                            {
                                if(IsGroupMember(myAccountId, group.Id))
                                {
                                    returnList.Add(group);
                                }
                            }
                            else returnList.Add(group);
                        }
                        else returnList.Add(group);

                        CacheGroup(group);
                    }
                    CacheController.Set("groups_u_" + otherAccountId, returnList.ToArray(), 3600);
                }
                catch(Exception e)
                {
                    Interface.LogError(e.StackTrace);
                }
            }
            return null;
        }

        //Quit group
        public static void QuitGroup(string myAccountId, string groupId)
        {
            var groupMember = GetGroupMemberById(myAccountId, groupId);
            if (groupMember == null) return;
            DeleteGroupMember(myAccountId, myAccountId, groupId);
        }
        #endregion
        
        #region Post & Comments
        //Get group post by id
        public static GroupPostData GetGroupPostById(string postId)
        {
            GroupPostData groupPost = CacheController.Get<GroupPostData>("gpost_i_" + postId);
            if (groupPost != null)
            {
                CacheGroupPost(groupPost);
                return groupPost;
            }
            else
            {
                //Get group from MySQL
                var rows = NetworkServer.database.ExecuteQuery("SELECT * FROM groupposts WHERE id = @postId LIMIT 1", new QueryParameter("postId", postId));
                if (rows.Length == 1)
                {
                    groupPost = DatabaseRowToGroupPostData(rows[0]);
                    CacheGroupPost(groupPost);
                    return groupPost;
                }
            }
            return null;
        }

        public static void CacheGroupPost(GroupPostData groupPost)
        {
            if (groupPost == null) return;
            CacheController.Set("gpost_i_" + groupPost.Id, groupPost, 3600);

            var groupPosts = CacheController.Get<GroupPostData[]>("gposts_" + groupPost.Id);
            if (groupPosts == null)
            {
                CacheController.Set("gposts_" + groupPost.Id, new[] { groupPost }, 3600);
            }
            else
            {
                if (groupPosts.Count(x => x.Id == groupPost.Id) == 0)
                {
                    var list = groupPosts.ToList();
                    list.Add(groupPost);
                    CacheController.Set("gposts_" + groupPost.Id, list.ToArray(), 3600);
                }
            }
        }

        public static void DeleteCacheGroupPost(GroupPostData groupPost)
        {
            if (groupPost == null) return;
            CacheController.Remove("gpost_i_" + groupPost.Id);

            var groupPosts = CacheController.Get<GroupPostData[]>("gposts_" + groupPost.Id);
            if (groupPosts != null)
            {
                var list = groupPosts.ToList();
                list.Remove(groupPost);
                CacheController.Set("gposts_" + groupPost.Id, list.ToArray(), 3600);
            }
        }

        public static GroupPostData DatabaseRowToGroupPostData(DatabaseRow row)
        {
            if (row != null && row.Count == 6)
            {
                try
                {
                    DateTime dateAt = DateTime.Parse(row["dateAt"].ToString());
                    var userId = row["userId"].ToString();
                    var userAccount = AccountsController.GetAccountById(userId);

                    string message = "";
                    Base64.TryParseBase64(Encoding.UTF8, row["message"].ToString(), out message);
                    if (string.IsNullOrEmpty(message)) message = row["message"].ToString();

                    return new GroupPostData()
                    {
                        Id = row["id"].ToString(),
                        GroupId = row["groupId"].ToString(),
                        UserId = userId,
                        Message = message,
                        AttachmentId = row ["attachmentId"].ToString(),
                        DateAt = dateAt,
                        UserAvatar = userAccount.Avatar,
                        UserName = userAccount.Username,
                        Gender = userAccount.Gender
                    };
                }
                catch (Exception ex) { Interface.LogError(ex.StackTrace); }
            }
            return null;
        }

        //Get group post
        public static List<GroupPostData> GetGroupPosts(string myAccountId, string groupId, int page = 1)
        {
            var returnList = new List<GroupPostData>();
            try
            {
                var group = GetGroupById(groupId);
                if (!group.IsPublic && !IsGroupMember(myAccountId, groupId)) return returnList;

                var posts = CacheController.Get<GroupPostData[]>("gposts_" + groupId);
                if (posts != null)
                {
                    CacheController.Set("gposts_" + groupId, posts, 3600);
                    return posts.ToList();
                }
                else
                {
                    var rows = NetworkServer.database.ExecuteQuery("SELECT * FROM groupposts WHERE groupId = @groupId", new QueryParameter("groupId", groupId));
                    foreach (var row in rows)
                    {
                        var groupPost = DatabaseRowToGroupPostData(row);
                        if (groupPost != null)
                        {
                            returnList.Add(groupPost);
                        }
                    }
                    CacheController.Set("gposts_" + groupId, returnList.ToArray(), 3600);
                }
            }
            catch(Exception e)
            {
                Interface.LogError(e.StackTrace);
            }
            return returnList;
        }

        //Create group post
        public static GroupPostData CreateGroupPost(string userId, string groupId, string message, string attachmentId)
        {
            if (!IsGroupMember(userId, groupId)) return null;
            try
            {
                var insertedId = "";
                var contentBase64 = Base64.ToBase64(Encoding.UTF8, message);
                lock (NetworkServer.database)
                {
                    insertedId = Convert.ToInt32(NetworkServer.database.ExecuteScalar("INSERT INTO groupposts (groupId, userId, message, attachmentId, dateAt) VALUES (@groupId, @userId, @message, @attachmentId, NOW()); SELECT LAST_INSERT_ID();",
                        new QueryParameter("groupId", groupId),
                        new QueryParameter("userId", userId),
                        new QueryParameter("message", contentBase64),
                        new QueryParameter("attachmentId", attachmentId)
                    )).ToString();
                }
                var userAccount = AccountsController.GetAccountById(userId);
                var post = new GroupPostData()
                {
                    Id = insertedId,
                    UserId = userId,
                    GroupId = groupId,
                    UserName = userAccount.Username,
                    UserAvatar = userAccount.Avatar,
                    Gender = userAccount.Gender,
                    Message = message,
                    AttachmentId = attachmentId,
                    DateAt = DateTime.Now
                };
                CacheGroupPost(post);
                TriggerPostCreated(post);
                return post;
            }
            catch (Exception ex)
            {
                Interface.LogError(ex.StackTrace);
            }
            return null;
        }

        public static void RemoveGroupPost(string userId, string postId)
        {
            try
            {
                var groupPost = GetGroupPostById(postId);
                if (groupPost == null) return;
                if (groupPost.UserId != userId && !IsGroupMemberAdmin(userId, groupPost.GroupId)) return;
                
                NetworkServer.database.ExecuteNonQuery("DELETE FROM groupposts WHERE id = @postId", new QueryParameter("postId", postId));

                DeleteCacheGroupPost(groupPost);
                TriggerPostDeleted(groupPost);
            }catch(Exception e)
            {
                Interface.LogError(e.StackTrace);
            }
        }

        //Get Group Comment by Id
        public static GroupCommentData GetGroupCommentById(string commentId)
        {
            GroupCommentData comment = CacheController.Get<GroupCommentData>("gcom_i_" + commentId);
            if (comment != null)
            {
                CacheGroupComment(comment);
                return comment;
            }
            else
            {
                //Get group from MySQL
                var rows = NetworkServer.database.ExecuteQuery("SELECT * FROM groupcomments WHERE id = @commentId LIMIT 1", new QueryParameter("commentId", commentId));
                if (rows.Length == 1)
                {
                    comment = DatabaseRowToGroupComment(rows[0]);
                    CacheGroupComment(comment);
                    return comment;
                }
            }
            return null;
        }

        //DatabaseRow to GroupComment
        public static GroupCommentData DatabaseRowToGroupComment(DatabaseRow row)
        {
            if (row != null && row.Count == 6)
            {
                try
                {
                    DateTime dateAt = DateTime.Parse(row["dateAt"].ToString());
                    var userId = row["userId"].ToString();
                    var userAccount = AccountsController.GetAccountById(userId);

                    string message = "";
                    Base64.TryParseBase64(Encoding.UTF8, row["message"].ToString(), out message);
                    if (string.IsNullOrEmpty(message)) message = row["message"].ToString();

                    return new GroupCommentData()
                    {
                        Id = row["id"].ToString(),
                        PostId = row["postId"].ToString(),
                        UserId = row["userId"].ToString(),
                        Message = message,
                        AttachmentId = row["attachmentId"].ToString(),
                        DateAt = dateAt,
                        UserAvatar = userAccount.Avatar,
                        UserName = userAccount.Username,
                        Gender = userAccount.Gender
                    };
                }
                catch (Exception ex) { Interface.LogError(ex.StackTrace); }
            }
            return null;
        }

        //Cache Group Comment
        public static void CacheGroupComment(GroupCommentData groupComment)
        {
            if (groupComment == null) return;
            CacheController.Set("gcom_i_" + groupComment.Id, groupComment, 3600);

            var comments = CacheController.Get<GroupCommentData[]>("gcoms_" + groupComment.PostId);
            if (comments == null)
            {
                CacheController.Set("gcoms_" + groupComment.PostId, new[] { groupComment }, 3600);
            }
            else
            {
                if (comments.Count(x => x.Id == groupComment.Id) == 0)
                {
                    var list = comments.ToList();
                    list.Add(groupComment);
                    CacheController.Set("gcoms_" + groupComment.PostId, list.ToArray(), 3600);
                }
            }
        }

        //Delete Cache Group Comment
        public static void DeleteCacheGroupComment(GroupCommentData groupComment)
        {
            if (groupComment == null) return;
            CacheController.Remove("gcom_i_" + groupComment.Id);

            var comments = CacheController.Get<GroupCommentData[]>("gcoms_" + groupComment.PostId);
            if (comments != null)
            {
                var list = comments.ToList();
                list.Remove(groupComment);
                CacheController.Set("gcoms_" + groupComment.PostId, list.ToArray(), 3600);
            }
        }

        //Get post comments
        public static List<GroupCommentData> GetGroupPostComments(string myAccountId, string postId)
        {
            var post = GetGroupPostById(postId);
            if (post == null) return default(List<GroupCommentData>);
            if(!IsGroupMember(myAccountId, post.GroupId)) return default(List<GroupCommentData>);
            try
            {
                var comments = CacheController.Get<GroupCommentData[]>("gcoms_" + postId);
                if(comments != null)
                {
                    CacheController.Set("gcoms_" + postId, comments, 3600);
                    return comments.ToList();
                }else
                {
                    var rows = NetworkServer.database.ExecuteQuery("SELECT * FROM groupcomments WHERE postId = @postId", new QueryParameter("postId", postId));
                    List<GroupCommentData> returnList = new List<GroupCommentData>();
                    foreach(var row in rows)
                    {
                        var comment = DatabaseRowToGroupComment(row);
                        if(comment != null)
                        {
                            returnList.Add(comment);
                        }
                    }
                    CacheController.Set("gcoms_" + postId, returnList.ToArray(), 3600);
                    return returnList;
                }
            }catch(Exception e)
            {
                Interface.LogError(e.StackTrace);
            }
            return default(List<GroupCommentData>);
        }

        //Comment post
        public static GroupCommentData CreateGroupComment(string userId, string postId, string message, string attachmentId = "")
        {
            var post = GetGroupPostById(postId);
            if (post == null) return null;
            var group = GetGroupById(post.GroupId);
            if (group == null) return null;
            if (!IsGroupMember(userId, group.Id)) return null;

            try
            {
                var insertedId = "";
                var contentBase64 = Base64.ToBase64(Encoding.UTF8, message);
                lock (NetworkServer.database)
                {
                    insertedId = Convert.ToInt32(NetworkServer.database.ExecuteScalar("INSERT INTO groupcomments (postId, userId, message, attachmentId, dateAt) VALUES (@postId, @userId, @message, @attachmentId, NOW()); SELECT LAST_INSERT_ID();",
                        new QueryParameter("postId", postId),
                        new QueryParameter("userId", userId),
                        new QueryParameter("message", contentBase64),
                        new QueryParameter("attachmentId", attachmentId)
                    )).ToString();
                }
                var userAccount = AccountsController.GetAccountById(userId);
                var comment = new GroupCommentData()
                {
                    Id = insertedId,
                    UserId = userId,
                    PostId = postId,
                    UserName = userAccount.Username,
                    UserAvatar = userAccount.Avatar,
                    Gender = userAccount.Gender,
                    Message = message,
                    AttachmentId = attachmentId,
                    DateAt = DateTime.Now
                };
                CacheGroupComment(comment);
                TriggerCommentCreated(comment);
                return comment;
            }
            catch(Exception e)
            {
                Interface.LogError(e.StackTrace);
            }
            return null;
        }

        public static void RemoveGroupComment(string userId, string commentId)
        {
            var comment = GetGroupCommentById(commentId);
            if (comment == null) return;
            var post = GetGroupPostById(comment.Id);
            if (post == null) return;
            if (comment.UserId != userId && !IsGroupMemberAdmin(userId, post.GroupId)) return;//We are not an admin or the user who created the comment.
            try
            {
                NetworkServer.database.ExecuteNonQuery("DELETE FROM groupcomments WHERE id = @commentId", new QueryParameter("commentId", commentId));
                DeleteCacheGroupComment(comment);
                TriggerCommentDeleted(comment);
            }catch(Exception e)
            {
                Interface.LogError(e.StackTrace);
            }
        }
        #endregion

        #region Triggers
        private static void TriggerRequestSent(GroupRequestData request)
        {
            //TODO
        }
        private static void TriggerRequestResult(GroupRequestData request)
        {
            //TODO
        }
        private static void TriggerRequestCancelled(GroupRequestData request)
        {
            //TODO
        }
        private static void TriggerInviteSent(GroupInvitationData invitation)
        {
            //TODO
        }
        private static void TriggerInviteCancelled(GroupInvitationData invitation)
        {
            //TODO
        }
        private static void TriggerPostCreated(GroupPostData groupPost)
        {
            //TODO
        }
        private static void TriggerPostDeleted(GroupPostData groupPost)
        {
            //TODO
        }
        private static void TriggerCommentCreated(GroupCommentData groupComment)
        {
            //TODO
        }
        private static void TriggerCommentDeleted(GroupCommentData groupComment)
        {
            //TODO
        }
        #endregion
    }
}