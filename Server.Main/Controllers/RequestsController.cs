﻿using Network;
using Network.Storage;
using Server.Shared.Controllers;
using SocialAPP.Shared.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using DatabaseRow = Network.Storage.DatabaseRow;
using Interface = Network.Interface;
using NetworkWriter = Network.NetworkWriter;
using NetworkReader = Network.NetworkReader;

namespace Server.Main.Controllers
{
    public static class RequestsController
    {
        public static List<RequestData> GetRequests(string myAccountId)
        {
            var requests = CacheController.Get<RequestData[]>("requests_" + myAccountId);
            if (requests != null)
            {
                CacheController.Set("requests_" + myAccountId, requests, 3600);
                return requests.ToList();
            }
            else
            {
                var rows = NetworkServer.database.ExecuteQuery("SELECT * FROM requests WHERE (userId = @accountId OR profileId = @accountId) ORDER BY id DESC", new QueryParameter("accountId", myAccountId));
                List<RequestData> returnList = new List<RequestData>();
                foreach (var row in rows)
                {
                    var request = RowToRequest(row);
                    if (request != null)
                    {
                        returnList.Add(request);
                    }
                }
                return returnList;
            }
            return null;
        }

        public static RequestData RowToRequest(DatabaseRow row)
        {
            if (row == null || row.Count != 6) return null;
            try
            {
                var user = AccountsController.GetAccountById(row["userId"].ToString());
                var otherAccount = AccountsController.GetAccountById(row["profileId"].ToString());
                if (user == null || otherAccount == null) return null;
                DateTime dateAt = DateTime.Parse(row["dateAt"].ToString());

                return new RequestData()
                {
                    Id = row["id"].ToString(),
                    UserId = user.Id,
                    UserName = user.Username,
                    UserAvatar = user.Avatar,
                    UserGender = user.Gender,
                    ProfileId = otherAccount.Id,
                    ProfileName = otherAccount.Username,
                    ProfileGender = otherAccount.Gender,
                    DateAt = dateAt,
                    Accepted = Convert.ToInt32(row["accepted"].ToString()) == 1,
                    RequestType = Convert.ToInt32(row["requestType"].ToString())
                };
            }
            catch (Exception ex)
            {
                Interface.LogError(ex.StackTrace);
            }
            return null;
        }

        public static void CacheRequest(RequestData request)
        {
            //User 1
            var user1Requests = CacheController.Get<RequestData[]>("requests_" + request.UserId);
            if (user1Requests == null)
            {
                user1Requests = new RequestData[] { request };
                CacheController.Set("requests_" + request.UserId, user1Requests, 3600);
            }
            else
            {
                var temp = user1Requests.ToList();
                var foundRequest = temp.FirstOrDefault(x => x.Id == request.Id);
                if (foundRequest != null)
                {
                    foundRequest = request;
                }
                else
                {
                    temp.Add(request);
                }
                CacheController.Set("requests_" + request.UserId, temp.ToArray(), 3600);
            }
            //User 2
            var user2Requests = CacheController.Get<RequestData[]>("requests_" + request.ProfileId);
            if (user2Requests == null)
            {
                user2Requests = new RequestData[] { request };
                CacheController.Set("requests_" + request.UserId, user2Requests, 3600);
            }
            else
            {
                var temp = user2Requests.ToList();
                var foundRequest = temp.FirstOrDefault(x => x.Id == request.Id);
                if (foundRequest != null)
                {
                    foundRequest = request;
                }
                else
                {
                    temp.Add(request);
                }
                CacheController.Set("requests_" + request.ProfileId, temp.ToArray(), 3600);
            }
        }
        public static void DeleteCacheRequest(RequestData request)
        {
            //User 1
            var user1Requests = CacheController.Get<RequestData[]>("requests_" + request.UserId);
            if (user1Requests != null)
            {
                var temp = user1Requests.ToList();
                var foundRequest = temp.FirstOrDefault(x => x.Id == request.Id);
                if (foundRequest != null)
                {
                    temp.Remove(foundRequest);
                }
                CacheController.Set("requests_" + request.UserId, temp.ToArray(), 3600);
            }
            //User 2
            var user2Requests = CacheController.Get<RequestData[]>("requests_" + request.ProfileId);
            if (user2Requests != null)
            {
                var temp = user2Requests.ToList();
                var foundRequest = temp.FirstOrDefault(x => x.Id == request.Id);
                if (foundRequest != null)
                {
                    temp.Remove(foundRequest);
                }
                CacheController.Set("requests_" + request.ProfileId, temp.ToArray(), 3600);
            }
        }
    }
}