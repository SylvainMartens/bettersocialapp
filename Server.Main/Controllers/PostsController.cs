﻿using Network;
using Network.Storage;
using Server.Shared;
using Server.Shared.Controllers;
using SocialAPP.Shared;
using SocialAPP.Shared.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DatabaseRow = Network.Storage.DatabaseRow;
using QueryParameter = Network.Storage.QueryParameter;
using Interface = Network.Interface;
using NetworkWriter = Network.NetworkWriter;
using NetworkReader = Network.NetworkReader;

namespace Server.Main.Controllers
{
    public static class PostsController
    {
        //Cache
        public static void CachePost(PostData post)
        {
            if (post == null) return;
            CacheController.Set("post_id_" + post.Id, post, 3600);
        }

        public static void CacheComment(CommentData comment)
        {
            if (comment == null) return;
            CacheController.Set("comment_id_" + comment.Id, comment, 3600);
        }

        public static void UnCachePost(PostData post)
        {
            if (post == null) return;
            CacheController.Remove("post_id_" + post.Id);
            try
            {
                var commentsArray = CacheController.Get<PostData[]>("posts_uid_" + post.UserId);
                if (commentsArray != null)
                {
                    var list = commentsArray.ToList();
                    var commentFound = list.FirstOrDefault(x => x.Id == post.Id);
                    if (commentFound != null)
                    {
                        list.Remove(commentFound);
                        if (list.Count > 0)
                            CacheController.Set("posts_uid_" + post.UserId, list.ToArray(), 3600);
                        else
                            CacheController.Remove("posts_uid_" + post.UserId);
                    }
                }
            }
            catch (Exception ex)
            {
                Interface.LogError(ex.StackTrace);
            }

            if (!string.IsNullOrEmpty(post.AttachmentId))
            {
                try
                {
                    var cachedCount = CacheController.Get<int?>("posts_uid_pc_" + post.UserId);
                    if (cachedCount != null)
                    {
                        cachedCount--;
                        if (cachedCount < 0) cachedCount = 0;
                        CacheController.Set("posts_uid_pc_" + post.UserId, (int?)cachedCount, 3600);
                    }
                }
                catch (Exception ex)
                {
                    Interface.LogError(ex.StackTrace);
                }
            }
        }

        public static void UnCacheComment(CommentData comment)
        {
            if (comment == null) return;
            CacheController.Remove("comment_id_" + comment.Id);

            try
            {
                var commentsArray = CacheController.Get<CommentData[]>("comments_" + comment.PostId);
                if (commentsArray != null)
                {
                    var list = commentsArray.ToList();
                    var commentFound = list.FirstOrDefault(x => x.Id == comment.Id);
                    if (commentFound != null)
                    {
                        list.Remove(commentFound);
                        if (list.Count > 0)
                            CacheController.Set("comments_" + comment.PostId, list.ToArray(), 3600);
                        else
                            CacheController.Remove("comments_" + comment.PostId);
                    }
                }
            }
            catch (Exception ex)
            {
                Interface.LogError(ex.StackTrace);
            }
        }

        //Get Post
        public static PostData GetPostById(string postId)
        {
            try
            {
                var post = CacheController.Get<PostData>("post_id_" + postId);
                if (post != null)
                {
                    CachePost(post);//Refresh Cache
                    return post;
                }
                else
                {
                    var rows = NetworkServer.database.ExecuteQuery("SELECT * FROM posts WHERE id = @postId LIMIT 1", new QueryParameter("postId", postId));
                    if (rows.Length == 1)
                    {
                        post = PostRowToData(rows[0]);
                        if (post != null)
                        {
                            CachePost(post);
                            return post;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Interface.LogError(ex.StackTrace);
            }
            return null;
        }

        private static PostData PostRowToData(DatabaseRow row)
        {
            if (row.Count == 5)
            {
                try
                {
                    DateTime dateAt = DateTime.Parse(row["dateAt"].ToString());
                    var userId = row["userId"].ToString();
                    var userAccount = AccountsController.GetAccountById(userId);
                    string message = "";
                    Base64.TryParseBase64(Encoding.UTF8, row["message"].ToString(), out message);
                    if (string.IsNullOrEmpty(message)) message = row["message"].ToString();
                    return new PostData()
                    {
                        Id = row["id"].ToString(),
                        UserId = userId,
                        Content = message,
                        AttachmentId = row["attachmentId"].ToString(),
                        DateAt = dateAt,
                        UserAvatar = userAccount.Avatar,
                        UserName = userAccount.Username,
                        Gender = userAccount.Gender
                    };
                }
                catch (Exception ex) { Interface.LogError(ex.StackTrace); }
            }
            return null;
        }

        public static CommentData CommentRowToData(DatabaseRow row)
        {
            if (row.Count == 6)
            {
                try
                {
                    DateTime dateAt = DateTime.Parse(row["dateAt"].ToString());
                    var userId = row["userId"].ToString();
                    var userAccount = AccountsController.GetAccountById(userId);
                    var post = GetPostById(row["postId"].ToString());
                    string message = "";
                    Base64.TryParseBase64(Encoding.UTF8, row["message"].ToString(), out message);
                    if (string.IsNullOrEmpty(message)) message = row["message"].ToString();
                    return new CommentData()
                    {
                        Id = row["id"].ToString(),
                        UserId = userId,
                        PostId = post.Id,
                        PostUserId = post.UserId,
                        Content = message,
                        AttachmentId = row["attachmentId"].ToString(),
                        DateAt = dateAt,
                        UserAvatar = userAccount.Avatar,
                        UserName = userAccount.Username,
                        Gender = userAccount.Gender
                    };
                }
                catch (Exception ex) { Interface.LogError(ex.StackTrace); }
            }
            return null;
        }

        //Get Posts On a profile
        public static List<PostData> GetPostByUserId(string userId, int page = 1)
        {
            var returnList = new List<PostData>();
            try
            {
                var postsArray = CacheController.Get<PostData[]>("posts_uid_" + userId);
                if (postsArray != null)
                {
                    CacheController.Set("posts_uid_" + userId, postsArray, 3600);//Refresh cache!
                    return postsArray.ToList();
                }
                else
                {
                    var rows = NetworkServer.database.ExecuteQuery("SELECT * FROM ( SELECT * FROM posts WHERE userId = @userId ORDER BY id DESC) sub ORDER BY id ASC", new QueryParameter("userId", userId));

                    foreach (var row in rows)
                    {
                        var post = PostRowToData(row);
                        if (post != null)
                        {
                            returnList.Add(post);
                            CachePost(post);
                        }
                    }
                    if (rows.Length > 0)
                    {
                        CacheController.Set("posts_uid_" + userId, returnList.ToArray(), 3600);
                    }
                    return returnList;
                }
            }
            catch (Exception ex)
            {
                Interface.LogError(ex.StackTrace);
            }
            return returnList;
        }

        public static int GetPostWithPicturesCountByUserId(string userId)
        {
            try
            {
                var cachedCount = CacheController.Get<int?>("posts_uid_pc_" + userId);
                if (cachedCount != null)
                {
                    CacheController.Set("posts_uid_pc_" + userId, (int?)cachedCount, 3600);
                    return (int)cachedCount;
                }
                else
                {
                    var count = GetPostWithPicturesByUserId(userId).Count;
                    CacheController.Set("posts_uid_pc_" + userId, (int?)cachedCount, 3600);
                    return count;
                }
            }
            catch (Exception ex)
            {
                Interface.LogError(ex.StackTrace);
            }
            return 0;
        }

        public static List<PostData> GetPostWithPicturesByUserId(string userId, int page = 1)
        {
            var returnList = new List<PostData>();
            try
            {
                var rows = NetworkServer.database.ExecuteQuery("SELECT * FROM ( SELECT * FROM posts WHERE userId = @userId AND attachmentId is not null AND attachmentId <> ''  ORDER BY id DESC LIMIT 25 OFFSET @page) sub ORDER BY id ASC", new QueryParameter("userId", userId), new QueryParameter("page", page - 1));
                foreach (var row in rows)
                {
                    var post = PostRowToData(row);
                    if (post != null)
                    {
                        returnList.Add(post);
                        CachePost(post);
                    }
                }
                return returnList;
            }
            catch (Exception ex)
            {
                Interface.LogError(ex.StackTrace);
            }
            return returnList;
        }

        //Get Recent Posts
        public static List<PostData> GetRecentPosts(string userId)
        {
            var returnList = new List<PostData>();
            try
            {
                var rows = NetworkServer.database.ExecuteQuery("SELECT * FROM ( SELECT post.id FROM posts as post INNER JOIN accounts as account ON post.userId = account.id LEFT JOIN follows as follow ON account.id = follow.profileId WHERE post.userId = @userId OR follow.userId = @userId ORDER BY post.dateAt DESC) sub ORDER BY id ASC", new QueryParameter("userId", userId));

                foreach (var row in rows)
                {
                    var postId = row["id"].ToString();
                    if (!string.IsNullOrEmpty(postId))
                    {
                        var post = GetPostById(postId);
                        if (post != null)
                        {
                            returnList.Add(post);
                        }
                    }
                }
                return returnList;
            }
            catch (Exception ex)
            {
                Interface.LogError(ex.StackTrace);
            }
            return returnList;
        }

        //Get Comment
        public static CommentData GetCommentById(string commentId)
        {
            try
            {
                var comment = CacheController.Get<CommentData>("comment_id_" + commentId);
                if (comment != null)
                {
                    CacheComment(comment);//Refresh Cache
                    return comment;
                }
                else
                {
                    var rows = NetworkServer.database.ExecuteQuery("SELECT * FROM comments WHERE id = @commentId LIMIT 1", new QueryParameter("commentId", commentId));
                    if (rows.Length == 1)
                    {
                        comment = CommentRowToData(rows[0]);
                        if (comment != null)
                        {
                            CacheComment(comment);
                            return comment;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Interface.LogError(ex.StackTrace);
            }
            return null;
        }

        //Get Comments
        public static List<CommentData> GetCommentsByPostId(string userId, string postId)
        {
            var returnList = new List<CommentData>();
            try
            {
                var post = GetPostById(postId);
                if (post != null)
                {
                    if (post.UserId == userId || FollowsController.IsFollowing(userId, post.UserId))
                    {
                        var commentsArray = CacheController.Get<CommentData[]>("comments_" + postId);
                        if (commentsArray != null)
                        {
                            CacheController.Set("comments_" + postId, commentsArray, 3600);//Refresh cache!
                            return commentsArray.ToList();
                        }
                        else
                        {
                            //Get comments from MySQL
                            var rows = NetworkServer.database.ExecuteQuery("SELECT * FROM ( SELECT * FROM comments WHERE postId = @postId ORDER BY id DESC) sub ORDER BY id ASC", new QueryParameter("postId", postId));

                            foreach (var row in rows)
                            {
                                var comment = CommentRowToData(row);
                                if (comment != null)
                                {
                                    returnList.Add(comment);
                                    CacheComment(comment);
                                }
                            }
                            if (rows.Length > 0)
                            {
                                CacheController.Set("comments_" + postId, returnList.ToArray(), 3600);
                            }
                            return returnList;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Interface.LogError(ex.StackTrace);
            }
            return returnList;
        }

        //Create Post
        public static PostData Post(string userId, string content, string attachmentId = null)
        {
            try
            {
                var insertedId = "";
                var contentBase64 = Base64.ToBase64(Encoding.UTF8, content);
                lock (NetworkServer.database)
                {
                    insertedId = Convert.ToInt32(NetworkServer.database.ExecuteScalar("INSERT INTO posts (userId, message, attachmentId, dateAt) VALUES (@userId, @message, @attachmentId, NOW()); SELECT LAST_INSERT_ID();",
                        new QueryParameter("userId", userId),
                        new QueryParameter("message", contentBase64),
                        new QueryParameter("attachmentId", attachmentId)
                    )).ToString();
                }
                var userAccount = AccountsController.GetAccountById(userId);
                var post = new PostData()
                {
                    Id = insertedId,
                    UserId = userId,
                    UserName = userAccount.Username,
                    UserAvatar = userAccount.Avatar,
                    Gender = userAccount.Gender,
                    Content = content,
                    AttachmentId = attachmentId,
                    DateAt = DateTime.Now
                };
                CachePost(post);
                TriggerPostCreated(post);

                if (!string.IsNullOrEmpty(attachmentId))
                {
                    var cachedCount = CacheController.Get<int?>("posts_uid_pc_" + userId);
                    if (cachedCount != null)
                    {
                        cachedCount++;
                        CacheController.Set("posts_uid_pc_" + userId, cachedCount, 3600);
                    }
                    else
                    {
                        var count = GetPostWithPicturesByUserId(userId).Count;
                        CacheController.Set("posts_uid_pc_" + userId, (int?)cachedCount, 3600);
                    }
                }

                return post;
            }
            catch (Exception ex)
            {
                Interface.LogError(ex.StackTrace);
            }
            return null;
        }

        public static void TriggerPostCreated(PostData post)
        {
            if (post == null) return;
            var followers = FollowsController.GetFollowers(post.UserId);
            foreach (var follower in followers)
            {
                try
                {
                    var session = SessionsController.GetSessionByAccountId(follower.UserId);
                    if (session != null)
                    {
                        using (NetworkWriter writer = new NetworkWriter())
                        {
                            writer.Write(ProtoSerializer.Serialize(post));
                            NetworkServer.GetConnectionServiceByID(session.ConnectionId).SendReply(ClientTags.CREATE_POST, 0, writer);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Interface.LogError(ex.StackTrace);
                }
            }
            MainServer.NotifyPost(post);
        }

        //Trigger Post
        public static void TriggerPostDeleted(PostData post)
        {
            if (post == null) return;
            var followers = FollowsController.GetFollowers(post.UserId);
            foreach (var follower in followers)
            {
                try
                {
                    var session = SessionsController.GetSessionByAccountId(follower.UserId);
                    if (session != null)
                    {
                        using (NetworkWriter writer = new NetworkWriter())
                        {
                            writer.Write(post.Id);
                            writer.Write(true);
                            NetworkServer.GetConnectionServiceByID(session.ConnectionId).SendReply(ClientTags.DELETE_POST, 0, writer);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Interface.LogError(ex.StackTrace);
                }
            }
            MainServer.NotifyPostRemoved(post);
        }

        //Delete Post
        public static bool DeletePost(string userId, string postId)
        {
            try
            {
                var post = GetPostById(postId);
                if (postId != null && post.UserId == userId)//If the post isnt null && user is the owner of the post!
                {
                    lock (NetworkServer.database)
                    {
                        NetworkServer.database.ExecuteNonQuery("DELETE FROM posts WHERE id = @postId", new QueryParameter("postId", postId));
                    }
                    UnCachePost(post);
                    TriggerPostDeleted(post);
                    return true;
                }
            }
            catch (Exception ex)
            {
                Interface.LogError(ex.StackTrace);
            }
            return false;
        }

        //Comment Post
        public static CommentData AddComment(string userId, string postId, string content, string attachmentId = null)
        {
            try
            {
                var post = GetPostById(postId);
                if (post != null)
                {
                    //Check if user is allowed to comment this post!
                    if (post.UserId == userId || (FollowsController.IsFollowing(post.UserId, userId) || FollowsController.IsFollowing(userId, post.UserId)))
                    {
                        var insertedId = "";
                        var contentBase64 = Base64.ToBase64(Encoding.UTF8, content);
                        lock (NetworkServer.database)
                        {
                            insertedId = Convert.ToInt32(NetworkServer.database.ExecuteScalar("INSERT INTO comments (userId, postId, message, attachmentId, dateAt) VALUES (@userId, @postId, @message, @attachmentId, NOW()); SELECT LAST_INSERT_ID();",
                                new QueryParameter("userId", userId),
                                new QueryParameter("postId", postId),
                                new QueryParameter("message", contentBase64),
                                new QueryParameter("attachmentId", attachmentId)
                            )).ToString();
                        }
                        var userAccount = AccountsController.GetAccountById(userId);
                        var comment = new CommentData()
                        {
                            Id = insertedId,
                            UserId = userId,
                            UserName = userAccount.Username,
                            UserAvatar = userAccount.Avatar,
                            Gender = userAccount.Gender,
                            PostId = postId,
                            PostUserId = post.UserId,
                            Content = content,
                            AttachmentId = attachmentId,
                            DateAt = DateTime.Now
                        };
                        CacheComment(comment);

                        var commentsArray = CacheController.Get<CommentData[]>("comments_" + postId);
                        if (commentsArray != null)
                        {
                            var list = commentsArray.ToList();
                            list.Add(comment);
                            CacheController.Set("comments_" + postId, list.ToArray(), 3600);
                        }
                        TriggerCommentCreated(comment);
                        return comment;
                    }
                }
            }
            catch (Exception ex)
            {
                Interface.LogError(ex.StackTrace);
            }
            return null;
        }

        //Delete Comment
        public static bool RemoveComment(string userId, string commentId)
        {
            try
            {
                var comment = GetCommentById(commentId);
                if (comment != null)
                {
                    var post = GetPostById(comment.PostId);
                    if (post != null && (comment.UserId == userId || post.UserId == userId))//If user created the comment or if the user is the owner of the post
                    {
                        lock (NetworkServer.database)
                        {
                            NetworkServer.database.ExecuteNonQuery("DELETE FROM comments WHERE id = @commentId", new QueryParameter("commentId", commentId));
                        }
                        UnCacheComment(comment);
                        TriggerCommentDeleted(comment);
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                Interface.LogError(ex.StackTrace);
            }
            return false;
        }

        //Trigger Comment
        public static void TriggerCommentCreated(CommentData comment)
        {
            if (comment == null) return;
            var post = GetPostById(comment.PostId);
            var followers = FollowsController.GetFollowers(post.UserId);
            foreach (var follower in followers)
            {
                var session = SessionsController.GetSessionByAccountId(follower.UserId);
                if (session != null)
                {
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        writer.Write(true);
                        writer.Write(ProtoSerializer.Serialize(comment));
                        NetworkServer.GetConnectionServiceByID(session.ConnectionId).SendReply(ClientTags.COMMENT_POST, 0, writer);
                    }
                }
            }
            if (comment.UserId != post.UserId)
            {
                var session = SessionsController.GetSessionByAccountId(post.UserId);
                if (session != null)
                {
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        writer.Write(true);
                        writer.Write(ProtoSerializer.Serialize(comment));
                        NetworkServer.GetConnectionServiceByID(session.ConnectionId).SendReply(ClientTags.COMMENT_POST, 0, writer);
                    }
                }
            }
            MainServer.NotifyComment(comment);
        }

        public static void TriggerCommentDeleted(CommentData comment)
        {
            if (comment == null) return;
            var post = GetPostById(comment.PostId);
            var followers = FollowsController.GetFollowers(post.UserId);
            foreach (var follower in followers)
            {
                try
                {
                    var session = SessionsController.GetSessionByAccountId(follower.UserId);
                    if (session != null)
                    {
                        using (NetworkWriter writer = new NetworkWriter())
                        {
                            writer.Write(comment.Id);
                            writer.Write(true);
                            NetworkServer.GetConnectionServiceByID(session.ConnectionId).SendReply(ClientTags.UNCOMMENT_POST, 0, writer);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Interface.LogError(ex.StackTrace);
                }
            }
            if (comment.UserId != post.UserId)
            {
                try
                {
                    var session = SessionsController.GetSessionByAccountId(post.UserId);
                    if (session != null)
                    {
                        using (NetworkWriter writer = new NetworkWriter())
                        {
                            writer.Write(comment.Id);
                            writer.Write(true);
                            NetworkServer.GetConnectionServiceByID(session.ConnectionId).SendReply(ClientTags.UNCOMMENT_POST, 0, writer);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Interface.LogError(ex.StackTrace);
                }
            }
            try
            {
                var session = SessionsController.GetSessionByAccountId(comment.UserId);
                if (session != null)
                {
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        writer.Write(comment.Id);
                        writer.Write(true);
                        NetworkServer.GetConnectionServiceByID(session.ConnectionId).SendReply(ClientTags.UNCOMMENT_POST, 0, writer);
                    }
                }
            }
            catch (Exception ex)
            {
                Interface.LogError(ex.StackTrace);
            }
            MainServer.NotifyCommentRemoved(comment);
        }
    }
}