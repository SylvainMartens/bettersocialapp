﻿using Network;
using Network.Storage;
using Server.Shared.Controllers;
using SocialAPP.Shared.Data;
using Stripe;
using System;
using System.Collections.Generic;
using DatabaseRow = Network.Storage.DatabaseRow;
using Interface = Network.Interface;
using NetworkWriter = Network.NetworkWriter;
using NetworkReader = Network.NetworkReader;

namespace Server.Main.Controllers
{
    public static class PaymentsController
    {
        public static void Initialize()
        {
            StripeConfiguration.SetApiKey("sk_test_9Slee6Da9yVqdWmjgjblPmIz");//TODO REPLACE THIS TO PRODUCTION KEY.
        }

        public static void CacheTransaction(TransactionData transaction, bool cacheUid = true)
        {
            if (transaction == null) return;
            CacheController.Set("transaction_" + transaction.Id, transaction, 3600);
            if (cacheUid) CacheController.Set("transaction_uid_" + transaction.UserId, transaction, 3600);
        }

        public static TransactionData TransactionRowToData(DatabaseRow row)
        {
            if (row != null && row.Count == 7)
            {
                try
                {
                    DateTime dateAt = DateTime.Parse(row["dateAt"].ToString());
                    DateTime expireAt = DateTime.Parse(row["expireAt"].ToString());
                    return new TransactionData()
                    {
                        Id = row["id"].ToString(),
                        UserId = row["userId"].ToString(),
                        TokenId = row["tokenId"].ToString(),
                        Amount = Convert.ToInt32(row["amount"].ToString()),
                        Currency = row["currency"].ToString(),
                        DateAt = dateAt,
                        ExpireAt = expireAt
                    };
                }
                catch (Exception ex) { Interface.LogError(ex.StackTrace); }
            }
            return null;
        }

        public static bool HasMembership(string userId)
        {
            var transactionCached = CacheController.Get<TransactionData>("transaction_uid_" + userId);
            if (transactionCached != null)
            {
                if (transactionCached.ExpireAt <= DateTime.Now)
                {
                    //Expired
                    CacheController.Remove("transaction_uid_" + transactionCached.UserId);
                }
                else
                {
                    CacheController.Set("transaction_uid_" + transactionCached.UserId, transactionCached, 3600);
                    return true;
                }
            }
            //Get from MySQL
            bool hasMembership = false;
            var rows = NetworkServer.database.ExecuteQuery("SELECT * FROM transactions WHERE userId = @userId AND DATE(NOW()) < expireAt", new QueryParameter("userId", userId));
            foreach (var row in rows)
            {
                var transaction = TransactionRowToData(row);
                if (transaction.ExpireAt <= DateTime.Now)
                {
                    //Expired
                }
                else
                {
                    CacheTransaction(transaction);
                    hasMembership = true;
                    break;
                }
            }
            return hasMembership;
        }

        public static TransactionData GetTransactionById(string transactionId)
        {
            try
            {
                var transaction = CacheController.Get<TransactionData>("transaction_" + transactionId);
                if (transaction != null)
                {
                    CacheTransaction(transaction, false);//Refresh Cache
                    return transaction;
                }
                else
                {
                    var rows = NetworkServer.database.ExecuteQuery("SELECT * FROM transactions WHERE id = @transactionId LIMIT 1", new QueryParameter("transactionId", transactionId));
                    if (rows.Length == 1)
                    {
                        transaction = TransactionRowToData(rows[0]);
                        if (transaction != null)
                        {
                            CacheTransaction(transaction, false);
                            return transaction;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Interface.LogError(ex.StackTrace);
            }
            return null;
        }

        public static bool CreateTransaction(string userId, string tokenId, int plan)
        {
            if (HasMembership(userId))
            {
                return true;//We wont charge this user since he already has a membership.
            }
            var tokenService = new StripeTokenService();
            StripeToken stripeToken = tokenService.Get(tokenId);

            if (stripeToken != null)
            {
                var myCharge = new StripeChargeCreateOptions();

                var amount = 999;
                var currency = "usd";
                var days = 31;

                switch (plan)
                {
                    case 0://Daily
                    amount = 99;
                    days = 1;
                    break;

                    case 1://Weekly
                    amount = 299;
                    days = 7;
                    break;

                    case 2://Monthly
                    amount = 999;
                    days = 31;
                    break;
                }

                // always set these properties
                myCharge.Amount = amount;
                myCharge.Currency = currency;
                // set this if you want to
                myCharge.Description = "Social App - Premium membership";

                myCharge.SourceTokenOrExistingSourceId = tokenId;

                // set this property if using a customer - this MUST be set if you are using an existing source!
                //myCharge.CustomerId = *customerId *;

                // set this if you have your own application fees (you must have your application configured first within Stripe)
                //myCharge.ApplicationFee = 25;

                // (not required) set this to false if you don't want to capture the charge yet - requires you call capture later
                myCharge.Capture = true;

                var chargeService = new StripeChargeService();
                StripeCharge stripeCharge = chargeService.Create(myCharge);
                var status = stripeCharge.Status;

                tokenId = stripeCharge.Id;

                if (stripeCharge.Paid && !stripeCharge.Refunded)
                {
                    var insertedId = "";
                    lock (NetworkServer.database)
                    {
                        insertedId = Convert.ToInt32(NetworkServer.database.ExecuteScalar("INSERT INTO transactions (userId, tokenId, amount, currency, dateAt, expireAt) VALUES (@userId, @tokenId, @amount, @currency, NOW(), NOW() + INTERVAL @days DAY); SELECT LAST_INSERT_ID();",
                            new QueryParameter("userId", userId),
                            new QueryParameter("tokenId", tokenId),
                            new QueryParameter("amount", amount),
                            new QueryParameter("currency", currency),
                            new QueryParameter("days", days)
                        )).ToString();
                    }

                    var transaction = new TransactionData()
                    {
                        Id = insertedId,
                        UserId = userId,
                        TokenId = tokenId,
                        Amount = amount,
                        Currency = currency,
                        DateAt = DateTime.Now,
                        ExpireAt = DateTime.Now.AddDays(days)
                    };

                    CacheTransaction(transaction);

                    return true;
                }
                else
                {
                }
            }

            return false;
        }

        public static bool VerifyTransaction(string transactionId)
        {
            var transaction = GetTransactionById(transactionId);
            if (transaction != null)
            {
                var chargeService = new StripeChargeService();
                StripeCharge stripeCharge = chargeService.Get(transaction.TokenId);

                if (stripeCharge.Paid && !stripeCharge.Refunded)
                    return true;
            }
            return false;
        }

        public static List<TransactionData> GetTransactions(string userId, int page = 1)
        {
            var returnList = new List<TransactionData>();
            try
            {
                var rows = NetworkServer.database.ExecuteQuery("SELECT * FROM ( SELECT * FROM transactions WHERE userId = @userId ORDER BY id DESC LIMIT 25 OFFSET @page) sub ORDER BY id ASC", new QueryParameter("userId", userId), new QueryParameter("page", page - 1));

                foreach (var row in rows)
                {
                    var transaction = TransactionRowToData(row);
                    if (transaction != null)
                    {
                        returnList.Add(transaction);
                        CacheTransaction(transaction);
                    }
                }
                return returnList;
            }
            catch (Exception ex)
            {
                Interface.LogError(ex.StackTrace);
            }
            return returnList;
        }
    }
}