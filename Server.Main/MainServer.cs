﻿using Network;
using Server.Main.Controllers;
using SocialAPP.Shared;
using SocialAPP.Shared.Data;
using System;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using NetworkMessage = Network.NetworkMessage;
using Interface = Network.Interface;
using NetworkServer = Network.NetworkServer;
using NetworkReader = Network.NetworkReader;
using NetworkWriter = Network.NetworkWriter;

namespace Server.Main
{
    public class MainServer : Plugin
    {
        public override string name { get { return "Main Server"; } }
        public override string version { get { return "1.0"; } }
        public override Command[] commands { get { return new Command[0]; } }
        public override string author { get { return "Sylvain William Martens"; } }
        public override string supportEmail { get { return "thehesa@gmail.com"; } }

        private static NetworkConnection MasterServerConnection = new NetworkConnection();

        public string MasterServerHostname = "127.0.0.1";
        public int MasterServerPort = 4296;

        public const string Version = "1.0.1";

        public MainServer()
        {
            ConnectionService.onData += OnDataReceivedFromClients;
            MasterServerConnection.onData += OnDataReceivedFromMaster;
            ConnectToMasterServer();
            Task.Factory.StartNew(() =>
            {
                while (true)
                {
                    if (MasterServerConnection.isConnected) MasterServerConnection.Receive();
                    Thread.Sleep(25);
                }
            });
            PaymentsController.Initialize();
        }

        public void ConnectToMasterServer()
        {
            bool _connected = false;
            while (!_connected)
            {
                try
                {
                    MasterServerConnection.Connect(MasterServerHostname, MasterServerPort);
                    _connected = true;
                    continue;
                }
                catch (Exception ex)
                {
                    Interface.LogError("Master Server unreachable: " + ex.StackTrace);
                }
                Thread.Sleep(1000);
            }
        }

        public void OnDataReceivedFromMaster(byte tag, ushort subject, object data)
        {
            switch (tag)
            {
                default:

                break;
            }
        }

        public void OnDataReceivedFromClients(ConnectionService con, ref NetworkMessage msg)
        {
            switch (msg.tag)
            {
                #region Authentication

                case ClientTags.GET_VERSION:
                {
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        writer.Write(Version);
                        con.SendReply(ClientTags.GET_VERSION, 0, writer);
                    }
                }
                break;

                case ClientTags.DO_LOGIN:
                {
                    SessionsController.DeleteByConnectionId(con.id);

                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        string version = reader.ReadString();
                        if (!Version.Equals(version))
                        {
                            writer.Write(1);//Application outdated
                            msg.data = writer;
                            con.SendReply(ClientTags.DO_LOGIN, 0, writer);
                        }
                        else
                        {
                            string username = reader.ReadString();
                            string password = reader.ReadString();
                            var account = AccountsController.GetAccountByUsername(username);
                            if (account == null)
                            {
                                writer.Write(2);//Wrong infos
                                msg.data = writer;
                                con.SendReply(ClientTags.DO_LOGIN, 0, writer);
                            }
                            else if (!account.Password.Equals(password))
                            {
                                writer.Write(2);//Wrong infos
                                msg.data = writer;
                                con.SendReply(ClientTags.DO_LOGIN, 0, writer);
                            }
                            else if (account.IsSuspended)
                            {
                                writer.Write(3);//Is suspended
                                msg.data = writer;
                                con.SendReply(ClientTags.DO_LOGIN, 0, writer);
                            }
                            else if (account.IsBanned)
                            {
                                writer.Write(4);//Is banned
                                msg.data = writer;
                                con.SendReply(ClientTags.DO_LOGIN, 0, writer);
                            }
                            else
                            {
                                var session = SessionsController.GetSessionByAccountId(account.Id);
                                if (session != null)
                                {
                                    SessionsController.DeleteByAccountId(account.Id);
                                    DisconnectAccountId(account.Id);
                                }

                                SessionsController.Create(account.Id, con.remoteEndPoint.ToString().Split(':')[0], con.id);

                                writer.Write(0);//Success
                                writer.Write(account.Id);
                                writer.Write(account.Username);
                                writer.Write(account.Avatar);
                                writer.Write(account.Gender);
                                writer.Write(PaymentsController.HasMembership(account.Id));

                                //var requests = CallsController.GetCallRequests(account.Id);
                                var requests = RequestsController.GetRequests(account.Id);
                                writer.Write(ProtoSerializer.Serialize(requests.ToArray()));

                                NotifyUserOnline(account.Id);

                                msg.data = writer;
                                con.SendReply(ClientTags.DO_LOGIN, 0, writer);
                            }
                        }
                    }
                }
                break;

                case ClientTags.DO_LOGOUT:
                {
                    var session = SessionsController.GetSessionByConnectionId(con.id);
                    if (session != null)
                    {
                        var account = AccountsController.GetAccountById(session.AccountId);
                        AccountsController.SaveAccountData(account);
                        SessionsController.DeleteByConnectionId(con.id);
                        NotifyUserOffline(account.Id);
                    }
                }
                break;

                case ClientTags.DO_REGISTER:
                {
                    SessionsController.DeleteByConnectionId(con.id);

                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var username = reader.ReadString();
                        var password = reader.ReadString();
                        var email = reader.ReadString();
                        var firstname = reader.ReadString();
                        var lastname = reader.ReadString();
                        var dob_d = reader.ReadInt32();
                        var dob_m = reader.ReadInt32();
                        var dob_y = reader.ReadInt32();
                        var gender = reader.ReadInt32();

                        if (string.IsNullOrEmpty(username) || username.Length < 4 || username.Length > 30)
                        {
                            writer.Write(1);//Username must be between 4 and 30 characters.
                            msg.data = writer;
                            con.SendReply(ClientTags.DO_REGISTER, 0, writer);
                        }
                        else if (!new Regex("^[A-Za-z]{1}[A-Za-z0-9]{4,31}$", RegexOptions.IgnoreCase).Match(username).Success)
                        {
                            writer.Write(2);//Username is invalid (A-z 0-9 only).
                            msg.data = writer;
                            con.SendReply(ClientTags.DO_REGISTER, 0, writer);
                        }
                        else if (!IsEmailValid(email))
                        {
                            writer.Write(4);//Email is invalid.
                            msg.data = writer;
                            con.SendReply(ClientTags.DO_REGISTER, 0, writer);
                        }
                        else if (string.IsNullOrEmpty(firstname) || firstname.Length < 4 || firstname.Length > 30)
                        {
                            writer.Write(5);//Firstname must be between 2 and 30 characters.
                            msg.data = writer;
                            con.SendReply(ClientTags.DO_REGISTER, 0, writer);
                        }
                        else if (string.IsNullOrEmpty(lastname) || lastname.Length < 4 || lastname.Length > 30)
                        {
                            writer.Write(6);//Lastname must be between 2 and 30 characters.
                            msg.data = writer;
                            con.SendReply(ClientTags.DO_REGISTER, 0, writer);
                        }
                        else if (dob_d < 1 || dob_d > 31)
                        {
                            writer.Write(7);//Please select your birth date.
                            msg.data = writer;
                            con.SendReply(ClientTags.DO_REGISTER, 0, writer);
                        }
                        else if (dob_m < 1 || dob_m > 12)
                        {
                            writer.Write(7);//Please select your birth date.
                            msg.data = writer;
                            con.SendReply(ClientTags.DO_REGISTER, 0, writer);
                        }
                        else if (dob_y < 1900 || dob_y > DateTime.Now.Year)
                        {
                            writer.Write(7);//Please select your birth date.
                            msg.data = writer;
                            con.SendReply(ClientTags.DO_REGISTER, 0, writer);
                        }
                        else if (!Is18OrOver(dob_d, dob_m, dob_y))
                        {
                            writer.Write(8);//18+ years old to use this app.
                            msg.data = writer;
                            con.SendReply(ClientTags.DO_REGISTER, 0, writer);
                        }
                        else if (AccountsController.GetAccountByUsername(username) != null)
                        {
                            writer.Write(9);//Username already used
                            msg.data = writer;
                            con.SendReply(ClientTags.DO_REGISTER, 0, writer);
                        }
                        else if (AccountsController.GetAccountByEmail(email) != null)
                        {
                            writer.Write(10);//Email already used
                            msg.data = writer;
                            con.SendReply(ClientTags.DO_REGISTER, 0, writer);
                        }
                        else
                        {
                            gender = (gender < 0 || gender > 1) ? 0 : gender;
                            //Everthing is fine!
                            AccountsController.CreateAccount(new AccountData()
                            {
                                Username = username,
                                Password = password,
                                Email = email,
                                Firstname = firstname,
                                Lastname = lastname,
                                CreationIp = con.remoteEndPoint.ToString().Split(':')[0],
                                LastIp = con.remoteEndPoint.ToString().Split(':')[0],
                                Gender = gender,
                                Dob_d = dob_d.ToString(),
                                Dob_m = dob_m.ToString(),
                                Dob_y = dob_y.ToString()
                            });
                            writer.Write(0);//Success
                            msg.data = writer;
                            con.SendReply(ClientTags.DO_REGISTER, 0, writer);
                        }
                    }
                }
                break;

                case ClientTags.DO_RECOVER:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var userText = reader.ReadString();
                        if (string.IsNullOrEmpty(userText))
                        {
                            writer.Write(1);//Account not found
                        }
                        else
                        {
                            AccountData account = null;
                            if (IsEmailValid(userText))
                            {
                                account = AccountsController.GetAccountByEmail(userText);
                            }
                            else
                            {
                                account = AccountsController.GetAccountByUsername(userText);
                            }

                            if (account == null)
                            {
                                writer.Write(1);
                            }
                            else
                            {
                                try
                                {
                                    var newPassword = System.Web.Security.Membership.GeneratePassword(8, 2);
                                    account.Password = Encryption.SHA1Of(newPassword);
                                    AccountsController.SaveAccountData(account);
                                    AccountsController.CacheAccount(account);
                                    writer.Write(0);

                                    //Send new password by email!
                                    using (var client = new SmtpClient("smtp.gmail.com", 587)
                                    {
                                        Host = "smtp.gmail.com",
                                        Port = 587,
                                        EnableSsl = true,
                                        DeliveryMethod = SmtpDeliveryMethod.Network,
                                        UseDefaultCredentials = false,
                                        Timeout = 30 * 1000,
                                        Credentials = new NetworkCredential("curssordev@gmail.com", "0212wisa")
                                    })
                                    {
                                        var fromAddr = new MailAddress("no-reply@socialapp.com", "Social APP");
                                        var toAddr = new MailAddress(account.Email, account.Firstname + " " + account.Lastname);
                                        using (var mail = new MailMessage(fromAddr, toAddr))
                                        {
                                            mail.Subject = "Recover account.";
                                            mail.IsBodyHtml = true;
                                            mail.Body = "<html><body>Hello " + account.Firstname + ", we reseted your account password.<br>Please use the following password to login <b><span>" + newPassword + "</span></b></body></html>";
                                            client.Send(mail);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Interface.LogError(ex.StackTrace);
                                }
                            }
                        }
                        msg.data = writer;
                        con.SendReply(ClientTags.DO_RECOVER, 0, writer);
                    }
                }
                break;

                #endregion Authentication

                #region Edit Profile

                case ClientTags.GET_ACCOUNT_DATA:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, null);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, null);
                                con.Close();
                            }
                            else
                            {
                                writer.Write(account.Avatar);
                                writer.Write(account.City);
                                writer.Write(account.Job);
                                writer.Write(account.Activities);
                                writer.Write(account.Description);
                                writer.Write(account.LookingFor);
                                writer.Write(account.LookingForDescription);
                                writer.Write(account.Gender);
                                msg.data = writer;
                                con.SendReply(ClientTags.GET_ACCOUNT_DATA, 0, writer);
                            }
                        }
                    }
                }
                break;

                case ClientTags.DO_SAVEACCOUNT:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, null);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, null);
                                con.Close();
                            }
                            else
                            {
                                var email = reader.ReadString();
                                var username = reader.ReadString();
                                var password = reader.ReadString();
                                var firstname = reader.ReadString();
                                var lastname = reader.ReadString();
                                var dob_d = reader.ReadInt32();
                                var dob_m = reader.ReadInt32();
                                var dob_y = reader.ReadInt32();

                                if (string.IsNullOrEmpty(email) || !IsEmailValid(email))
                                {
                                    writer.Write(1);//Email invalid.
                                }
                                else if (string.IsNullOrEmpty(username) || username.ToLower() != account.Username.ToLower())
                                {
                                    writer.Write(2);//Username invalid.
                                }
                                else if (string.IsNullOrEmpty(firstname) || firstname.Length < 2 || firstname.Length > 30)
                                {
                                    writer.Write(3);//Firstname invalid.
                                }
                                else if (string.IsNullOrEmpty(lastname) || lastname.Length < 2 || lastname.Length > 30)
                                {
                                    writer.Write(4);//Lastname invalid.
                                }
                                else if (dob_d < 1 || dob_d > 31)
                                {
                                    writer.Write(5);//Date of birth invalid.
                                }
                                else if (dob_m < 1 || dob_m > 12)
                                {
                                    writer.Write(5);//Date of birth invalid.
                                }
                                else if (dob_y < 1940 || dob_y > 2016)
                                {
                                    writer.Write(5);//Date of birth invalid.
                                }
                                else
                                {
                                    account.Email = email;
                                    account.Username = username;
                                    if (!string.IsNullOrEmpty(password))
                                        account.Password = password;
                                    account.Firstname = firstname;
                                    account.Lastname = lastname;
                                    account.Dob_d = dob_d.ToString();
                                    account.Dob_m = dob_m.ToString();
                                    account.Dob_y = dob_y.ToString();
                                    AccountsController.SaveAccountData(account);
                                    AccountsController.CacheAccount(account);
                                    writer.Write(0);
                                }

                                msg.data = writer;
                                con.SendReply(ClientTags.DO_SAVEACCOUNT, 0, writer);
                            }
                        }
                    }
                }
                break;

                case ClientTags.EDIT_PROFILE_DATA:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var city = reader.ReadString();
                                var job = reader.ReadString();
                                var activities = reader.ReadString();
                                var description = reader.ReadString();
                                var lookingFor = reader.ReadString();
                                var lookingForDescription = reader.ReadString();
                                var lookingForVerification = Convert.ToInt32(lookingFor);

                                if (lookingForVerification < 0 || lookingForVerification > 4) lookingForVerification = 0;

                                account.City = city;
                                account.Job = job;
                                account.Activities = activities;
                                account.Description = description;
                                account.LookingFor = lookingForVerification.ToString();
                                account.LookingForDescription = lookingForDescription;
                                AccountsController.CacheAccount(account);
                                AccountsController.SaveAccountData(account);

                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.EDIT_PROFILE_DATA, 0, writer);
                            }
                        }
                    }
                }
                break;

                case ClientTags.EDIT_PROFILE_PICTURE:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var uploadToken = UploadsController.CreateProfileUploadToken(account.Id, con.remoteEndPoint.ToString().Split(':')[0]);

                                writer.Write(uploadToken);
                                msg.data = writer;
                                con.SendReply(ClientTags.EDIT_PROFILE_PICTURE, 0, writer);
                            }
                        }
                    }
                }
                break;

                case ClientTags.UPDATE_PROFILE_PICTURE:
                {
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                account.Avatar = "assets/avatars/" + account.Id + ".png&t=" + DateTime.Now.Ticks;
                                AccountsController.CacheAccount(account);
                                AccountsController.SaveAccountData(account);
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.UPDATE_PROFILE_PICTURE, 0, writer);
                            }
                        }
                    }
                }
                break;

                #endregion Edit Profile

                #region Other

                case ClientTags.UPDATE_POS:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var latitude = reader.ReadDouble();
                                var longitude = reader.ReadDouble();
                                if (account.Latitude != latitude && account.Longitude != longitude)
                                {
                                    account.Latitude = latitude;
                                    account.Longitude = longitude;
                                    AccountsController.CacheAccount(account);
                                    AccountsController.SaveAccountData(account);
                                }
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.UPDATE_POS, 0, writer);
                            }
                        }
                    }
                }
                break;

                #endregion Other

                #region View Profile

                case ClientTags.GETEDIT_ACCOUNT:
                {
                    msg.DecodeData();
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                writer.Write(account.Email);
                                writer.Write(account.Username);
                                writer.Write(account.Firstname);
                                writer.Write(account.Lastname);
                                writer.Write(account.Dob_d);
                                writer.Write(account.Dob_m);
                                writer.Write(account.Dob_y);

                                msg.data = writer;
                                con.SendReply(ClientTags.GETEDIT_ACCOUNT, 0, writer);
                            }
                        }
                    }
                }
                break;

                case ClientTags.GET_PROFILE_DATA:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var accountId = reader.ReadString();
                                AccountData otherAccount;
                                if (accountId == "0" || accountId == account.Id) otherAccount = account;
                                else otherAccount = AccountsController.GetAccountById(accountId);

                                if (otherAccount == null)
                                {
                                    writer.Write(0);
                                    msg.data = writer;
                                    con.SendReply(ClientTags.GET_PROFILE_DATA, 0, writer);
                                }
                                else
                                {
                                    if (BlockController.HasBlocked(otherAccount.Id, account.Id))
                                    {
                                        //Cannot send message
                                        writer.Write(0);
                                        msg.data = writer;
                                        con.SendReply(ClientTags.GET_PROFILE_DATA, 0, writer);
                                    }
                                    else
                                    {
                                        writer.Write(1);
                                        writer.Write(otherAccount.Id);
                                        writer.Write(otherAccount.Username);
                                        writer.Write(otherAccount.Avatar);
                                        writer.Write(otherAccount.City);
                                        writer.Write(otherAccount.Job);
                                        writer.Write(otherAccount.Activities);
                                        writer.Write(otherAccount.Description);
                                        writer.Write(otherAccount.LookingFor);
                                        writer.Write(otherAccount.LookingForDescription);
                                        writer.Write(otherAccount.Gender);
                                        writer.Write(GetAge(Convert.ToInt32(otherAccount.Dob_d), Convert.ToInt32(otherAccount.Dob_m), Convert.ToInt32(otherAccount.Dob_y)));
                                        writer.Write(FollowsController.IsFollowing(account.Id, otherAccount.Id));

                                        var followers = FollowsController.GetFollowers(otherAccount.Id);
                                        writer.Write(ProtoSerializer.Serialize(followers));

                                        var followings = FollowsController.GetFollowings(otherAccount.Id);
                                        writer.Write(ProtoSerializer.Serialize(followings));

                                        var posts = PostsController.GetPostByUserId(otherAccount.Id, 1);
                                        writer.Write(ProtoSerializer.Serialize(posts));

                                        writer.Write(PostsController.GetPostWithPicturesCountByUserId(otherAccount.Id));

                                        writer.Write(BlockController.HasBlocked(account.Id, otherAccount.Id));

                                        if (otherAccount.Id != account.Id)
                                        {
                                            NotificationsController.TriggerNotification(account.Id, otherAccount.Id, NotificationTypes.View);
                                        }

                                        msg.data = writer;
                                        con.SendReply(ClientTags.GET_PROFILE_DATA, 0, writer);
                                    }
                                }
                            }
                        }
                    }
                }
                break;

                case ClientTags.GETPROFILE_PHOTOS:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var accountId = reader.ReadString();
                                AccountData otherAccount;
                                if (accountId == "0" || accountId == account.Id) otherAccount = account;
                                else otherAccount = AccountsController.GetAccountById(accountId);

                                if (otherAccount == null)
                                {
                                    writer.Write(0);
                                    msg.data = writer;
                                    con.SendReply(ClientTags.GET_PROFILE_DATA, 0, writer);
                                }
                                else
                                {
                                    var page = reader.ReadInt32();
                                    var photos = PostsController.GetPostWithPicturesByUserId(otherAccount.Id, page);
                                    writer.Write(ProtoSerializer.Serialize(photos.ToArray()));

                                    msg.data = writer;
                                    con.SendReply(ClientTags.GETPROFILE_PHOTOS, 0, writer);
                                }
                            }
                        }
                    }
                }
                break;

                #endregion View Profile

                #region Search

                case ClientTags.SEARCH_PROFILES:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var username = reader.ReadString();
                                var gender = reader.ReadString();
                                var minAge = reader.ReadString();
                                var maxAge = reader.ReadString();
                                var distance = reader.ReadString();
                                var page = reader.ReadInt32();

                                var range = 0;

                                if (!string.IsNullOrEmpty(distance))
                                {
                                    range = Convert.ToInt32(distance);
                                    if (range < 0) range = 0;
                                    if (range > 100) range = 100;
                                }

                                var profiles = AccountsController.SearchAccounts(account.Id, account.Latitude, account.Longitude, range, username, gender, minAge, maxAge, 50, page, PaymentsController.HasMembership(account.Id));

                                writer.Write(ProtoSerializer.Serialize(profiles.Keys.First()));
                                writer.Write(profiles.Values.First());
                                msg.data = writer;
                                con.SendReply(ClientTags.SEARCH_PROFILES, 0, writer);
                            }
                        }
                    }
                }
                break;

                #endregion Search

                #region Messages
                case ClientTags.GET_CONVERSATIONS:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var page = reader.ReadInt32();
                                if (page < 0) page = 0;

                                var conversation = MessagesController.GetConversations(session.AccountId, page);

                                writer.Write(ProtoSerializer.Serialize(conversation));
                                msg.data = writer;
                                con.SendReply(ClientTags.GET_CONVERSATIONS, 0, writer);
                            }
                        }
                    }
                }
                break;
                case ClientTags.GET_MESSAGES:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var conversationId = reader.ReadString();
                                var page = reader.ReadInt32();
                                if (page < 0) page = 0;

                                var messages = MessagesController.GetMessages(conversationId, session.AccountId, page);

                                writer.Write(ProtoSerializer.Serialize(messages));
                                msg.data = writer;
                                con.SendReply(ClientTags.GET_MESSAGES, 0, writer);
                            }
                        }
                    }
                }
                break;
                case ClientTags.SEND_MESSAGE:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var toId = reader.ReadString();
                                var message = reader.ReadString();
                                var attachmentId = reader.ReadString();

                                if (BlockController.HasBlocked(toId, session.AccountId) || BlockController.HasBlocked(session.AccountId, toId))
                                {
                                    //Cannot send message
                                    writer.Write(0);
                                    msg.data = writer;
                                    con.SendReply(ClientTags.RECEIVED_MESSAGE, 0, writer);
                                }
                                else
                                {
                                    var result = MessagesController.SendMessage(session.AccountId, toId, message, account.IsPremium, attachmentId);
                                    if (result != null)
                                    {
                                        writer.Write(1);
                                        writer.Write(ProtoSerializer.Serialize(result));

                                        var conversation = MessagesController.GetConversationById(result.ConversationId);
                                        if (conversation == null)
                                        {
                                        }
                                        else
                                        {
                                            var user1 = AccountsController.GetAccountById(conversation.User1Id);
                                            if (user1 != null)
                                            {
                                                conversation.User1Name = user1.Username;
                                                conversation.User1Avatar = user1.Avatar;
                                                conversation.User1Gender = user1.Gender;

                                                var user2 = AccountsController.GetAccountById(conversation.User2Id);

                                                conversation.User2Name = user2.Username;
                                                conversation.User2Avatar = user2.Avatar;
                                                conversation.User2Gender = user2.Gender;
                                            }
                                            MessagesController.CacheConversation(conversation);

                                            writer.Write(ProtoSerializer.Serialize(conversation));
                                        }
                                        msg.data = writer;
                                        con.SendReply(ClientTags.RECEIVED_MESSAGE, 0, writer);
                                    }
                                    else
                                    {
                                        writer.Write(0);
                                        msg.data = writer;
                                    }
                                }
                            }
                        }
                    }
                }
                break;
                case ClientTags.MESSAGE_SEEN:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var messageId = reader.ReadString();

                                MessagesController.SetMessageHasSeen(session.AccountId, messageId);

                                writer.Write(0);
                                msg.data = writer;
                            }
                        }
                    }
                }
                break;
                case ClientTags.START_CONVERSATION:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var toId = reader.ReadString();
                                if (!account.IsPremium)
                                {
                                    var otherAccount = AccountsController.GetAccountById(toId);
                                    if (otherAccount != null)
                                        MessagesController.GetOrCreateRequest(account, otherAccount);
                                    writer.Write(0);
                                    msg.data = writer;
                                }
                                else
                                {
                                    var conversationId = MessagesController.GetConversationId(account.Id, toId);
                                    if (string.IsNullOrEmpty(conversationId))
                                    {
                                        writer.Write(0);
                                    }
                                    else
                                    {
                                        var conversation = MessagesController.GetConversationById(conversationId);
                                        if (conversation == null)
                                        {
                                            writer.Write(0);
                                        }
                                        else
                                        {
                                            var user1 = AccountsController.GetAccountById(conversation.User1Id);
                                            if (user1 != null)
                                            {
                                                conversation.User1Name = user1.Username;
                                                conversation.User1Avatar = user1.Avatar;
                                                conversation.User1Gender = user1.Gender;
                                            }

                                            var user2 = AccountsController.GetAccountById(conversation.User2Id);
                                            if (user2 != null)
                                            {
                                                conversation.User2Name = user2.Username;
                                                conversation.User2Avatar = user2.Avatar;
                                                conversation.User2Gender = user2.Gender;
                                            }
                                            MessagesController.CacheConversation(conversation);

                                            writer.Write(1);
                                            writer.Write(ProtoSerializer.Serialize(conversation));
                                        }
                                    }
                                    msg.data = writer;
                                    con.SendReply(ClientTags.START_CONVERSATION, 0, writer);
                                }
                            }
                        }
                    }
                }
                break;
                case ClientTags.SET_MSG_REQUEST_RESULT:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var otherAccountId = reader.ReadString();
                                var result = reader.ReadBoolean();

                                MessagesController.SetRequestResult(account.Id, otherAccountId, result);

                                writer.Write(0);
                                msg.data = writer;
                            }
                        }
                    }
                }
                break;
                #endregion Messages

                #region Follows

                case ClientTags.FOLLOW:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var accountId = reader.ReadString();

                                FollowsController.Follow(account.Id, accountId);

                                writer.Write(0);
                                msg.data = writer;
                            }
                        }
                    }
                }
                break;

                case ClientTags.UNFOLLOW:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var accountId = reader.ReadString();

                                FollowsController.UnFollow(account.Id, accountId);

                                writer.Write(0);
                                msg.data = writer;
                            }
                        }
                    }
                }
                break;

                #endregion Follows

                #region Notifications

                case ClientTags.GET_NOTIFICATIONS:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var notifications = NotificationsController.GetNotifications(account.Id);
                                writer.Write(ProtoSerializer.Serialize(notifications));
                                msg.data = writer;
                                con.SendReply(ClientTags.GET_NOTIFICATIONS, 0, writer);
                            }
                        }
                    }
                }
                break;

                case ClientTags.POKE:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var toId = reader.ReadString();
                                if (!string.IsNullOrEmpty(toId)) NotificationsController.TriggerNotification(account.Id, toId, NotificationTypes.Poke);
                                writer.Write(0);
                                msg.data = writer;
                            }
                        }
                    }
                }
                break;

                case ClientTags.DELETE_NOTIFICATION:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var notificationId = reader.ReadString();
                                if (!string.IsNullOrEmpty(notificationId))
                                {
                                    NotificationsController.DeleteNotification(account.Id, notificationId);
                                }
                                writer.Write(0);
                                msg.data = writer;
                            }
                        }
                    }
                }
                break;

                #endregion Notifications

                #region Posts

                case ClientTags.CREATE_POST:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                /*if (!PaymentsController.HasMembership(account.Id))
                                {
                                    writer.Write(0);
                                    msg.data = writer;
                                    con.SendReply(ClientTags.PREMIUM_REQUIRED, 0, writer);
                                }
                                else*/
                                {
                                    var content = reader.ReadString();
                                    var attachmentId = reader.ReadString();

                                    var post = PostsController.Post(account.Id, content, attachmentId);

                                    writer.Write(ProtoSerializer.Serialize(post));

                                    msg.data = writer;
                                    con.SendReply(ClientTags.CREATE_POST, 0, writer);
                                }
                            }
                        }
                    }
                }
                break;

                case ClientTags.DELETE_POST:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var postId = reader.ReadString();

                                var result = PostsController.DeletePost(account.Id, postId);

                                writer.Write(postId);
                                writer.Write(result);

                                msg.data = writer;
                                con.SendReply(ClientTags.DELETE_POST, 0, writer);
                            }
                        }
                    }
                }
                break;

                case ClientTags.GETUSER_POSTS:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var userId = reader.ReadString();

                                var results = PostsController.GetPostByUserId(userId);

                                writer.Write(ProtoSerializer.Serialize(results.ToArray()));

                                msg.data = writer;
                                con.SendReply(ClientTags.GETUSER_POSTS, 0, writer);
                            }
                        }
                    }
                }
                break;

                case ClientTags.GETRECENT_POSTS:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var results = PostsController.GetRecentPosts(account.Id);

                                writer.Write(ProtoSerializer.Serialize(results.ToArray()));

                                msg.data = writer;
                                con.SendReply(ClientTags.GETRECENT_POSTS, 0, writer);
                            }
                        }
                    }
                }
                break;

                case ClientTags.COMMENT_POST:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                /*if (!PaymentsController.HasMembership(account.Id))
                                {
                                    writer.Write(0);
                                    msg.data = writer;
                                    con.SendReply(ClientTags.PREMIUM_REQUIRED, 0, writer);
                                }
                                else*/
                                var postId = reader.ReadString();
                                var content = reader.ReadString();
                                var attachmentId = reader.ReadString();
                                var post = PostsController.GetPostById(postId);

                                if (BlockController.HasBlocked(post.UserId, account.Id))
                                {
                                    //Cannot send message
                                    writer.Write(0);
                                    msg.data = writer;
                                    //con.SendReply(ClientTags.COMMENT_POST, 0, writer);
                                }
                                else
                                {
                                    var comment = PostsController.AddComment(account.Id, postId, content, attachmentId);

                                    if (postId != null && post.UserId == account.Id)
                                        writer.Write(true);
                                    else
                                        writer.Write(true);

                                    writer.Write(ProtoSerializer.Serialize(comment));

                                    msg.data = writer;
                                    con.SendReply(ClientTags.COMMENT_POST, 0, writer);
                                }
                            }
                        }
                    }
                }
                break;

                case ClientTags.UNCOMMENT_POST:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var commentId = reader.ReadString();

                                var result = PostsController.RemoveComment(account.Id, commentId);

                                writer.Write(result);

                                msg.data = writer;
                                con.SendReply(ClientTags.UNCOMMENT_POST, 0, writer);
                            }
                        }
                    }
                }
                break;

                case ClientTags.GETCOMMENTS_POST:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var postId = reader.ReadString();

                                var results = PostsController.GetCommentsByPostId(account.Id, postId);

                                writer.Write(ProtoSerializer.Serialize(results.ToArray()));

                                msg.data = writer;
                                con.SendReply(ClientTags.GETCOMMENTS_POST, 0, writer);
                            }
                        }
                    }
                }
                break;

                #endregion Posts

                #region Payments
                case ClientTags.GET_TRANSACTIONS:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var page = reader.ReadInt32();
                                if (page < 1) page = 1;

                                var transactions = PaymentsController.GetTransactions(account.Id, page);

                                writer.Write(ProtoSerializer.Serialize(transactions.ToArray()));

                                msg.data = writer;
                                con.SendReply(ClientTags.GET_TRANSACTIONS, 0, writer);
                            }
                        }
                    }
                }
                break;
                case ClientTags.SEND_TRANSACTION:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var tokenId = reader.ReadString();
                                var plan = reader.ReadInt32();

                                if (plan < 0) plan = 0;
                                if (plan > 2) plan = 2;

                                var result = PaymentsController.CreateTransaction(account.Id, tokenId, plan);

                                writer.Write(result);

                                msg.data = writer;
                                con.SendReply(ClientTags.SEND_TRANSACTION, 0, writer);
                            }
                        }
                    }
                }
                break;
                case ClientTags.VERIFY_MEMBERSHIP:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var result = PaymentsController.HasMembership(account.Id);
                                writer.Write(result);
                                msg.data = writer;
                                con.SendReply(ClientTags.VERIFY_MEMBERSHIP, 0, writer);
                            }
                        }
                    }
                }
                break;
                #endregion Payments

                #region Blocks
                case ClientTags.GET_BLOCKEDUSERS:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var result = BlockController.GetBlockedUsers(account.Id);

                                writer.Write(ProtoSerializer.Serialize(result));
                                msg.data = writer;
                                con.SendReply(ClientTags.GET_BLOCKEDUSERS, 0, writer);
                            }
                        }
                    }
                }
                break;
                case ClientTags.BLOCK_USER:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(false);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(false);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var accountId = reader.ReadString();

                                var result = BlockController.BlockUser(account.Id, accountId);

                                writer.Write(result);
                                msg.data = writer;
                            }
                        }
                    }
                }
                break;
                case ClientTags.UNBLOCK_USER:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(false);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(false);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var accountId = reader.ReadString();

                                var result = BlockController.UnBlockUser(account.Id, accountId);

                                writer.Write(result);
                                msg.data = writer;
                            }
                        }
                    }
                }
                break;
                #endregion

                #region Calls
                case ClientTags.REQUEST:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var otherAccountId = reader.ReadString();
                                var otherAccount = AccountsController.GetAccountById(otherAccountId);
                                if (otherAccount == null)
                                {
                                    writer.Write(0);
                                    msg.data = writer;
                                    //con.SendReply(ClientTags.CALL_REQUEST, 0, writer);
                                }
                                else
                                {
                                    var result = CallsController.SendCall(account, otherAccount);
                                    if (result)
                                    {
                                        //Premium Required to begin a call request.
                                        writer.Write(0);
                                        msg.data = writer;
                                        con.SendReply(ClientTags.PREMIUM_REQUIRED, 0, writer);
                                    }
                                    else
                                    {
                                        writer.Write(0);
                                        msg.data = writer;
                                    }
                                }
                            }
                        }
                    }
                }
                break;
                case ClientTags.CALL_REQUEST_RESULT:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var otherAccountId = reader.ReadString();
                                var result = reader.ReadBoolean();

                                CallsController.SetCallRequestResult(account.Id, otherAccountId, result);

                                writer.Write(0);
                                msg.data = writer;
                            }
                        }
                    }
                }
                break;
                case ClientTags.SET_CALL_RESULT:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var callId = reader.ReadString();
                                var result = reader.ReadBoolean();

                                CallsController.SetCallResult(callId, account.Id, result);

                                writer.Write(0);
                                msg.data = writer;
                            }
                        }
                    }
                }
                break;
                case ClientTags.END_CALL:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var callId = reader.ReadString();

                                CallsController.EndCall(callId, account.Id);

                                writer.Write(0);
                                msg.data = writer;
                            }
                        }
                    }
                }
                break;
                case ClientTags.VIDEO_ENABLED:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var callId = reader.ReadString();
                                var enabled = reader.ReadBoolean();

                                var call = CallsController.GetCallById(callId);
                                if (call != null && (account.Id == call.UserId || account.Id == call.ProfileId))
                                {
                                    if (account.Id == call.UserId)
                                    {
                                        var otherSession = SessionsController.GetSessionByAccountId(call.ProfileId);
                                        writer.Write(enabled);
                                        msg.data = writer;
                                        NetworkServer.GetConnectionServiceByID(otherSession.ConnectionId)?.SendReply(ClientTags.VIDEO_ENABLED, 0, writer);
                                    }
                                    else
                                    {
                                        var otherSession = SessionsController.GetSessionByAccountId(call.UserId);
                                        writer.Write(enabled);
                                        msg.data = writer;
                                        NetworkServer.GetConnectionServiceByID(otherSession.ConnectionId)?.SendReply(ClientTags.VIDEO_ENABLED, 0, writer);
                                    }
                                }
                                else
                                {
                                    writer.Write(0);
                                    msg.data = writer;
                                }
                            }
                        }
                    }
                }
                break;
                case ClientTags.SET_CALL_ID:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var callId = reader.ReadString();

                                CallsController.SetCallId(callId, con.id);

                                writer.Write(0);
                                msg.data = writer;
                            }
                        }
                    }
                }
                break;
                case ClientTags.SYNC_AUDIO:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var bytes = reader.ReadBytes();

                                CallsController.SyncAudio(bytes, con.id);

                                writer.Write(0);
                                msg.data = writer;
                            }
                        }
                    }
                }
                break;
                case ClientTags.SYNC_VIDEO:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var bytes = reader.ReadBytes();

                                CallsController.SyncVideo(bytes, con.id);

                                writer.Write(0);
                                msg.data = writer;
                            }
                        }
                    }
                }
                break;
                #endregion

                #region Mapping
                case ClientTags.REQUEST_POS:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                if (!account.IsPremium)
                                {
                                    writer.Write(0);
                                    msg.data = writer;
                                    con.SendReply(ClientTags.PREMIUM_REQUIRED, 0, writer);
                                    return;
                                }
                                var otherAccountId = reader.ReadString();
                                var otherAccount = AccountsController.GetAccountById(otherAccountId);
                                if (otherAccount == null)
                                {
                                    writer.Write(0);
                                    msg.data = writer;
                                }
                                else
                                {
                                    MappingController.GetMapPosition(account, otherAccount);
                                    writer.Write(0);
                                    msg.data = writer;
                                }
                            }
                        }
                    }
                }
                break;
                case ClientTags.SET_REQUEST_POS_RESULT:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var otherAccountId = reader.ReadString();
                                var result = reader.ReadBoolean();

                                MappingController.SetRequestResult(account.Id, otherAccountId, result);

                                writer.Write(0);
                                msg.data = writer;
                            }
                        }
                    }
                }
                break;
                #endregion

                #region Groups
                case ClientTags.GET_MY_GROUPS:
                {
                    msg.DecodeData();
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var groups = GroupsController.GetUserGroups(account.Id, account.Id);
                                if (groups == null)
                                {
                                    writer.Write(0);
                                    msg.data = writer;
                                }
                                else
                                {
                                    writer.Write(ProtoSerializer.Serialize(groups.ToArray()));
                                    msg.data = writer;
                                    con.SendReply(ClientTags.GET_MY_GROUPS, 0, writer);
                                }
                            }
                        }
                    }
                }
                break;
                case ClientTags.GET_USER_GROUPS:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var userId = reader.ReadString();
                                if (string.IsNullOrEmpty(userId))
                                {
                                    writer.Write(0);
                                    msg.data = writer;
                                }
                                else
                                {
                                    var groups = GroupsController.GetUserGroups(account.Id, userId);
                                    if (groups == null)
                                    {
                                        writer.Write(0);
                                        msg.data = writer;
                                    }
                                    else
                                    {
                                        writer.Write(ProtoSerializer.Serialize(groups.ToArray()));
                                        con.SendReply(ClientTags.GET_USER_GROUPS, 0, writer);
                                    }
                                }
                            }
                        }
                    }
                }
                break;
                case ClientTags.CREATE_GROUP:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var groupName = reader.ReadString();
                                if (string.IsNullOrEmpty(groupName))
                                {
                                    writer.Write(0);
                                    msg.data = writer;
                                }
                                else
                                {

                                    var group = GroupsController.CreateGroup(account.Id, groupName);
                                    if (group == null)
                                    {
                                        writer.Write(0);
                                    }
                                    else
                                    {
                                        writer.Write(1);
                                        writer.Write(ProtoSerializer.Serialize(group));
                                    }

                                    msg.data = writer;
                                    con.SendReply(ClientTags.CREATE_GROUP, 0, writer);
                                }
                            }
                        }
                    }
                }
                break;
                case ClientTags.DELETE_GROUP:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var groupId = reader.ReadString();
                                if (string.IsNullOrEmpty(groupId))
                                {
                                    writer.Write(0);
                                    msg.data = writer;
                                }
                                else
                                {
                                    GroupsController.DeleteGroup(account.Id, groupId);
                                    writer.Write(0);
                                    msg.data = writer;
                                }
                            }
                        }
                    }
                }
                break;
                case ClientTags.INVITE_GROUP:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var userId = reader.ReadString();
                                var groupId = reader.ReadString();
                                if (string.IsNullOrEmpty(userId) || string.IsNullOrEmpty(groupId))
                                {
                                    writer.Write(0);
                                    msg.data = writer;
                                }
                                else
                                {
                                    var otherAccount = AccountsController.GetAccountById(userId);
                                    if (otherAccount == null || otherAccount.IsBanned || otherAccount.IsBlocked || otherAccount.IsSuspended)
                                    {
                                        writer.Write(0);
                                        msg.data = writer;
                                    }
                                    else
                                    {
                                        var invitation = GroupsController.InviteUserToGroup(account.Id, otherAccount.Id, groupId);
                                        if (invitation == null)
                                        {
                                            writer.Write(0);
                                            msg.data = writer;
                                            con.SendReply(ClientTags.INVITE_GROUP, 0, writer);
                                        }
                                        else
                                        {
                                            writer.Write(1);
                                            writer.Write(ProtoSerializer.Serialize(invitation));
                                            msg.data = writer;
                                            con.SendReply(ClientTags.INVITE_GROUP, 0, writer);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                break;
                case ClientTags.SET_GROUP_INVITE_RESULT:
                {
                    //invitation id, result
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var invitationId = reader.ReadString();
                                var invitationResult = reader.ReadBoolean();
                                if (string.IsNullOrEmpty(invitationId))
                                {
                                    writer.Write(0);
                                    msg.data = writer;
                                }
                                else
                                {
                                    GroupsController.SetGroupInvitationResult(account.Id, invitationId, invitationResult);
                                    writer.Write(0);
                                    msg.data = writer;
                                }
                            }
                        }
                    }
                }
                break;
                case ClientTags.REQUEST_JOIN_GROUP:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var groupId = reader.ReadString();
                                if (string.IsNullOrEmpty(groupId))
                                {
                                    writer.Write(0);
                                    msg.data = writer;
                                }
                                else
                                {
                                    var request = GroupsController.RequestGroupJoin(account.Id, groupId);
                                    if (request == null)
                                    {
                                        writer.Write(0);
                                        msg.data = writer;
                                        con.SendReply(ClientTags.REQUEST_JOIN_GROUP, 0, writer);
                                    }
                                    else
                                    {
                                        writer.Write(1);
                                        writer.Write(ProtoSerializer.Serialize(request));
                                        msg.data = writer;
                                        con.SendReply(ClientTags.REQUEST_JOIN_GROUP, 0, writer);
                                    }
                                }
                            }
                        }
                    }
                }
                break;
                case ClientTags.CANCEL_REQUEST_GROUP:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var requestId = reader.ReadString();
                                if (string.IsNullOrEmpty(requestId))
                                {
                                    writer.Write(0);
                                    msg.data = writer;
                                }
                                else
                                {
                                    GroupsController.CancelGroupRequest(account.Id, requestId);
                                    writer.Write(0);
                                    msg.data = writer;
                                }
                            }
                        }
                    }
                }
                break;
                case ClientTags.SET_GROUP_REQUEST_RESULT:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var requestId = reader.ReadString();
                                var result = reader.ReadBoolean();
                                if (string.IsNullOrEmpty(requestId))
                                {
                                    writer.Write(0);
                                    msg.data = writer;
                                }
                                else
                                {
                                    GroupsController.SetRequestResult(account.Id, requestId, result);
                                    writer.Write(0);
                                    msg.data = writer;
                                }
                            }
                        }
                    }
                }
                break;
                case ClientTags.SET_MEMBER_ADMINISTRATOR:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var userId = reader.ReadString();
                                var groupId = reader.ReadString();
                                var result = reader.ReadBoolean();
                                if (string.IsNullOrEmpty(userId) || string.IsNullOrEmpty(groupId))
                                {
                                    writer.Write(0);
                                    msg.data = writer;
                                }
                                else
                                {
                                    GroupsController.SetGroupAdministrator(account.Id, userId, groupId, result);
                                    writer.Write(0);
                                    msg.data = writer;
                                }
                            }
                        }
                    }
                }
                break;
                case ClientTags.DELETE_GROUP_MEMBER:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var userId = reader.ReadString();
                                var groupId = reader.ReadString();
                                if (string.IsNullOrEmpty(userId) || string.IsNullOrEmpty(groupId))
                                {
                                    writer.Write(0);
                                    msg.data = writer;
                                }
                                else
                                {
                                    GroupsController.DeleteGroupMember(account.Id, userId, groupId);
                                    writer.Write(0);
                                    msg.data = writer;
                                }
                            }
                        }
                    }
                }
                break;
                case ClientTags.CREATE_GROUP_POST:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var groupId = reader.ReadString();
                                var message = reader.ReadString();
                                var attachmentId = reader.ReadString();
                                if (string.IsNullOrEmpty(groupId) || (string.IsNullOrEmpty(message) || string.IsNullOrEmpty(attachmentId)) || !GroupsController.IsGroupMember(account.Id, groupId))
                                {
                                    writer.Write(0);
                                    msg.data = writer;
                                }
                                else
                                {
                                    var groupPost = GroupsController.CreateGroupPost(account.Id, groupId, message, attachmentId);
                                    if (groupPost == null)
                                    {
                                        writer.Write(0);
                                        msg.data = writer;
                                    }
                                    else
                                    {
                                        writer.Write(ProtoSerializer.Serialize(groupPost));
                                        msg.data = writer;
                                        con.SendReply(ClientTags.CREATE_GROUP_POST, 0, writer);
                                    }
                                }
                            }
                        }
                    }
                }
                break;
                case ClientTags.DELETE_GROUP_POST:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var postId = reader.ReadString();
                                if (string.IsNullOrEmpty(postId))
                                {
                                    writer.Write(0);
                                    msg.data = writer;
                                }
                                else
                                {
                                    GroupsController.RemoveGroupPost(account.Id, postId);

                                    writer.Write(0);
                                    msg.data = writer;
                                }
                            }
                        }
                    }
                }
                break;
                case ClientTags.GET_GROUP_POSTS:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var groupId = reader.ReadString();
                                if (string.IsNullOrEmpty(groupId) || !GroupsController.IsGroupMember(account.Id, groupId))
                                {
                                    writer.Write(0);
                                    msg.data = writer;
                                }
                                else
                                {
                                    var posts = GroupsController.GetGroupPosts(account.Id, groupId);

                                    writer.Write(ProtoSerializer.Serialize(posts.ToArray()));
                                    msg.data = writer;
                                    con.SendReply(ClientTags.GET_GROUP_POSTS, 0, writer);
                                }
                            }
                        }
                    }
                }
                break;
                case ClientTags.COMMENT_GROUP_POST:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var postId = reader.ReadString();
                                var message = reader.ReadString();
                                var attachmentId = reader.ReadString();
                                if (string.IsNullOrEmpty(postId) || (string.IsNullOrEmpty(message) || string.IsNullOrEmpty(attachmentId)))
                                {
                                    writer.Write(0);
                                    msg.data = writer;
                                }
                                else
                                {
                                    var groupPostComment = GroupsController.CreateGroupComment(account.Id, postId, message);
                                    if (groupPostComment == null)
                                    {
                                        writer.Write(0);
                                        msg.data = writer;
                                    }
                                    else
                                    {
                                        writer.Write(ProtoSerializer.Serialize(groupPostComment));
                                        msg.data = writer;
                                        con.SendReply(ClientTags.COMMENT_GROUP_POST, 0, writer);
                                    }
                                }
                            }
                        }
                    }
                }
                break;
                case ClientTags.DELETE_GROUP_COMMENT:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var commentId = reader.ReadString();
                                if (string.IsNullOrEmpty(commentId))
                                {
                                    writer.Write(0);
                                    msg.data = writer;
                                }
                                else
                                {
                                    GroupsController.RemoveGroupComment(account.Id, commentId);

                                    writer.Write(0);
                                    msg.data = writer;
                                }
                            }
                        }
                    }
                }
                break;
                case ClientTags.GET_GROUP_POST_COMMENTS:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var postId = reader.ReadString();
                                if (string.IsNullOrEmpty(postId))
                                {
                                    writer.Write(0);
                                    msg.data = writer;
                                }
                                else
                                {
                                    var comments = GroupsController.GetGroupPostComments(account.Id, postId);

                                    writer.Write(ProtoSerializer.Serialize(comments));
                                    msg.data = writer;
                                    con.SendReply(ClientTags.GET_GROUP_POST_COMMENTS, 0, writer);
                                }
                            }
                        }
                    }
                }
                break;
                case ClientTags.SET_GROUP_PUBLIC:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var groupId = reader.ReadString();
                                var isPublic = reader.ReadBoolean();

                                if (string.IsNullOrEmpty(groupId))
                                {
                                    writer.Write(0);
                                    msg.data = writer;
                                }
                                else
                                {
                                    GroupsController.SetGroupPublic(account.Id, groupId, isPublic);

                                    writer.Write(0);
                                    msg.data = writer;
                                }
                            }
                        }
                    }
                }
                break;
                case ClientTags.GET_GROUP_MEMBERS:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var groupId = reader.ReadString();
                                if (string.IsNullOrEmpty(groupId) || !GroupsController.IsGroupMember(account.Id, groupId))
                                {
                                    writer.Write(0);
                                    msg.data = writer;
                                }
                                else
                                {
                                    var members = GroupsController.GetGroupMembers(account.Id, groupId);

                                    writer.Write(ProtoSerializer.Serialize(members.ToArray()));
                                    msg.data = writer;
                                    con.SendReply(ClientTags.GET_GROUP_MEMBERS, 0, writer);
                                }
                            }
                        }
                    }
                }
                break;
                case ClientTags.SEARCH_GROUP:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var groupSearch = reader.ReadString();
                                if (string.IsNullOrEmpty(groupSearch))
                                {
                                    writer.Write(0);
                                    msg.data = writer;
                                }
                                else
                                {
                                    var groups = GroupsController.SearchGroup(account.Id, groupSearch);

                                    writer.Write(ProtoSerializer.Serialize(groups.ToArray()));
                                    msg.data = writer;
                                    con.SendReply(ClientTags.SEARCH_GROUP, 0, writer);
                                }
                            }
                        }
                    }
                }
                break;
                case ClientTags.GET_PENDING_GROUP_INVITATIONS:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var groupId = reader.ReadString();
                                if (string.IsNullOrEmpty(groupId))
                                {
                                    writer.Write(0);
                                    msg.data = writer;
                                }
                                else
                                {
                                    var groupInvitations = GroupsController.GetPendingGroupInvitations(account.Id, groupId);

                                    writer.Write(ProtoSerializer.Serialize(groupInvitations));
                                    msg.data = writer;
                                    con.SendReply(ClientTags.GET_PENDING_GROUP_INVITATIONS, 0, writer);
                                }
                            }
                        }
                    }
                }
                break;
                case ClientTags.GET_MY_GROUP_INVITATIONS:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.Close();
                            }
                            else
                            {
                                var groupInvitations = GroupsController.GetMyGroupInvitations(account.Id);

                                writer.Write(ProtoSerializer.Serialize(groupInvitations));
                                msg.data = writer;
                                con.SendReply(ClientTags.GET_MY_GROUP_INVITATIONS, 0, writer);
                            }
                        }
                    }
                }
                break;
                case ClientTags.GET_MY_PENDING_GROUP_REQUESTS:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.Close();
                            }
                            else
                            {
                                var groupRequests = GroupsController.GetMyPendingGroupRequests(account.Id);

                                writer.Write(ProtoSerializer.Serialize(groupRequests));
                                msg.data = writer;
                                con.SendReply(ClientTags.GET_MY_PENDING_GROUP_REQUESTS, 0, writer);
                            }
                        }
                    }
                }
                break;
                case ClientTags.GET_GROUP_REQUESTS:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.Close();
                            }
                            else
                            {
                                var groupId = reader.ReadString();
                                if (string.IsNullOrEmpty(groupId))
                                {
                                    writer.Write(0);
                                    msg.data = writer;
                                }
                                else
                                {
                                    var groupRequests = GroupsController.GetGroupRequests(account.Id, groupId);

                                    writer.Write(ProtoSerializer.Serialize(groupRequests));
                                    msg.data = writer;
                                    con.SendReply(ClientTags.GET_PENDING_GROUP_INVITATIONS, 0, writer);
                                }
                            }
                        }
                    }
                }
                break;
                case ClientTags.QUIT_GROUP:
                {
                    msg.DecodeData();
                    using (NetworkReader reader = (NetworkReader)msg.data)
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        var session = SessionsController.GetSessionByConnectionId(con.id);
                        if (session == null)
                        {
                            writer.Write(0);
                            msg.data = writer;
                            con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                            con.Close();
                        }
                        else
                        {
                            var account = AccountsController.GetAccountById(session.AccountId);
                            if (account == null)
                            {
                                writer.Write(0);
                                msg.data = writer;
                                con.SendReply(ClientTags.DISCONNECTED_BY_SERVER, 0, writer);
                                con.Close();
                            }
                            else
                            {
                                var groupId = reader.ReadString();
                                if (string.IsNullOrEmpty(groupId))
                                {
                                    writer.Write(0);
                                    msg.data = writer;
                                }
                                else
                                {
                                    GroupsController.QuitGroup(account.Id, groupId);
                                    writer.Write(0);
                                    msg.data = writer;
                                }
                            }
                        }
                    }
                }
                break;
                #endregion

            }
        }

        public bool IsEmailValid(string email)
        {
            string pattern = @"^(?!\.)(""([^""\r\\]|\\[""\r\\])*""|"
              + @"([-a-z0-9!#$%&'*+/=?^_`{|}~]|(?<!\.)\.)*)(?<!\.)"
              + @"@[a-z0-9][\w\.-]*[a-z0-9]\.[a-z][a-z\.]*[a-z]$";

            Regex regex = new Regex(pattern, RegexOptions.IgnoreCase);
            return regex.Match(email).Success;
        }

        public bool Is18OrOver(int d, int m, int y)
        {
            DateTime BirthDate = DateTime.Parse(y + "-" + m + "-" + d);
            DateTime CurrentDate = DateTime.Today;
            int Age = CurrentDate.Year - BirthDate.Year;
            if (Age >= 18)
            {
                return true;
            }
            return false;
        }

        public int GetAge(int d, int m, int y)//TODO: Make this more accurate...
        {
            DateTime BirthDate = DateTime.Parse(y + "-" + m + "-" + d);
            DateTime CurrentDate = DateTime.Today;
            return CurrentDate.Year - BirthDate.Year;
        }

        //Cross Server Functions
        public void DisconnectAccountId(string accountId)
        {
            //TODO: Send disconnect request to master server, so he can tell other server to disconnect an account!
        }

        public static void NotifiyMessage(MessageData message)
        {
            var session = SessionsController.GetSessionByAccountId(message.ToId);
            if (session == null)
            {
                //TODO: Send it to the master server!
            }
            else
            {
                try
                {
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        writer.Write(1);
                        writer.Write(ProtoSerializer.Serialize(message));

                        var conversationId = MessagesController.GetConversationId(message.FromId, message.ToId);
                        if (string.IsNullOrEmpty(conversationId))
                        {
                        }
                        else
                        {
                            var conversation = MessagesController.GetConversationById(conversationId);
                            if (conversation == null)
                            {
                            }
                            else
                            {
                                var user1 = AccountsController.GetAccountById(conversation.User1Id);
                                if (user1 != null)
                                {
                                    conversation.User1Name = user1.Username;
                                    conversation.User1Avatar = user1.Avatar;
                                    conversation.User1Gender = user1.Gender;
                                }
                                var user2 = AccountsController.GetAccountById(conversation.User2Id);
                                if (user2 != null)
                                {
                                    conversation.User2Name = user2.Username;
                                    conversation.User2Avatar = user2.Avatar;
                                    conversation.User2Gender = user2.Gender;
                                }

                                if (message.FromId == session.AccountId)
                                {
                                    conversation.LastMessageFromMe = true;
                                }
                                else
                                {
                                    conversation.LastMessageFromMe = false;
                                }

                                MessagesController.CacheConversation(conversation);
                                writer.Write(ProtoSerializer.Serialize(conversation));
                            }
                        }
                        var connection = NetworkServer.GetConnectionServiceByID(session.ConnectionId);
                        if (connection != null)
                        {
                            connection.SendReply(ClientTags.RECEIVED_MESSAGE, 0, writer);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Interface.LogError(ex.StackTrace);
                }
            }
        }

        public static void NotifyMessageSeen(MessageData message)
        {
            var session = SessionsController.GetSessionByAccountId(message.ToId);
            if (session == null)
            {
                //TODO: Send it to the master server!
            }
            else
            {
                try
                {
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        writer.Write(ProtoSerializer.Serialize(message));
                        NetworkServer.GetConnectionServiceByID(session.ConnectionId).SendReply(ClientTags.MESSAGE_SEEN, 0, writer);
                    }
                }
                catch (Exception ex)
                {
                    Interface.LogError(ex.StackTrace);
                }
            }
        }

        public static void NotifyNotification(NotificationData notification)
        {
            if (notification == null) return;
            var session = SessionsController.GetSessionByAccountId(notification.ProfileId);
            if (session == null)
            {
                //TODO: Send it to the master server!
            }
            else
            {
                try
                {
                    using (NetworkWriter writer = new NetworkWriter())
                    {
                        writer.Write(ProtoSerializer.Serialize(notification));
                        NetworkServer.GetConnectionServiceByID(session.ConnectionId)?.SendReply(ClientTags.RECEIVED_NOTIFICATION, 0, writer);
                    }
                }
                catch (Exception ex)
                {
                    Interface.LogError(ex.StackTrace);
                }
            }
        }

        public static void NotifyPost(PostData post)
        {
            //TODO
        }

        public static void NotifyPostRemoved(PostData post)
        {
            //TODO
        }

        public static void NotifyComment(CommentData comment)
        {
            //TODO
        }

        public static void NotifyCommentRemoved(CommentData comment)
        {
            //TODO
        }

        public static void NotifyMapRequest(RequestData callRequest)
        {
            //TODO
        }
        public static void NotifyPosition(RequestData request, AccountData otherAccount)
        {
            //TODO
        }
        public static void NotifyMsgRequest(RequestData callRequest)
        {
            //TODO
        }
        public static void NotifyUserOnline(string userId)
        {
            //TODO
        }
        public static void NotifyUserOffline(string userId)
        {
            //TODO
        }

        public static void NotifyGroupPost(GroupPostData groupPost)
        {
            //TODO
        }

        public static void NotifyGroupPostRemoved(GroupPostData groupPost)
        {
            //TODO
        }

        public static void NotifyGroupComment(GroupCommentData groupComment)
        {
            //TODO
        }

        public static void NotifyGroupCommentRemoved(GroupCommentData groupComment)
        {
            //TODO
        }
        public static void NotifyCallRequest(RequestData callRequestData)
        {
            //TODO
        }


    }
}