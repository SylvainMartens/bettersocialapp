﻿using SocialAPP.Shared.Data;
using System;
using System.Collections.Generic;

namespace SocialAPP
{
    public static class EventsController
    {
        public static Action<string> Log;//Implemented

        public static Action OnReturnToHome;//Implemented

        //Connection
        public static Action OnConnected;
        public static Action OnDisconnected;

        //Lobby
        public static Action<string> OnGetVersion;//Implemented

        public static Action<int> OnGetLoginResult;//Implemented
        public static Action<int> OnGetRegisterResult;//Implemented
        public static Action<int> OnGetRecoverResult;//Implemented
        public static Action<string, string, string, int, bool> OnGetAccountInfos;//Implemented

        //Edits
        public static Action<string, string, string, string, string, string, string> OnGetEditAccount;//Implemented
        public static Action<int> OnGetEditAccountResult;//Implemented
        public static Action OnProfileEdited;//Implemented
        public static Action<ProfileData> OnGetEditProfileData;//Implemented
        public static Action<string> OnGetUploadProfilePictureToken;//Implemented

        //Profile
        public static Action<ProfileData> OnGetProfileData;//Implemented
        public static Action<List<PostData>> OnGetProfilePictures;//Implemented
        public static Action<List<FollowData>> OnGetProfileFollowers;//Implemented
        public static Action<List<FollowData>> OnGetProfileFollowings;//Implemented

        //Messages
        public static Action<List<ConversationData>> OnLoadConversationsResult;//Implemented
        public static Action<List<MessageData>> OnLoadMessagesResult;//Implemented
        public static Action<MessageData, ConversationData> OnReceiveMessage;//Implemented
        public static Action<MessageData> OnMessageSeen;//Implemented
        public static Action<ConversationData> OnConversationStarted;//Implemented

        //Search
        public static Action<Dictionary<List<AccountData>, int>> OnGetSearchResults;//Implemented

        //Notifications
        public static Action<List<NotificationData>> OnGetNotifications;//Implemented
        public static Action<NotificationData> OnGetNotification;//Implemented

        //Posts
        public static Action<List<PostData>> OnGetProfilePosts;//Implemented
        public static Action<List<PostData>> OnGetPosts;//Implemented
        public static Action<PostData> OnGetPost;//Implemented
        public static Action<string> OnGetPostDeleted;//Implemented
        public static Action<List<CommentData>> OnGetComments;//Implemented
        public static Action<CommentData> OnGetComment;//Implemented
        public static Action<string> OnGetCommentDeleted;//Implemented

        //Payments
        public static Action<List<TransactionData>> OnGetTransactions;//Implemented
        public static Action<bool> OnGetPaymentResult;//Implemented
        public static Action<bool> OnGetVerifyMembership;//Implemented
        public static Action OnPremiumRequired;//Implemented

        //Blocks
        public static Action<List<BlockData>> OnGetBlockedUsers;//Implemented

        //Calls
        public static Action<RequestData, bool> OnGetRequest;
        public static Action<List<RequestData>> OnGetRequests;
        public static Action<CallData> OnStartCalling;
        public static Action<CallData> OnReceiveCall;
        public static Action OnCallRingTone;
        public static Action<CallData, string, int> OnStartCall;
        public static Action<CallData> OnCallEnded;
        
        //Mapping
        public static Action<double, double> OnGetPosition;

        //Groups
        public static Action<List<GroupData>> OnGetMyGroups;
        public static Action<List<GroupData>> OnGetUserGroups;

        public static Action<int, GroupData> OnGetGroupCreated;
        public static Action<int, GroupInvitationData> OnGetGroupInvite;
        public static Action<int, GroupRequestData> OnGetGroupRequest;
        public static Action<GroupPostData> OnGetGroupPostCreated;
        //Group Post Deleted
        public static Action<List<GroupPostData>> OnGetGroupPosts;
        public static Action<GroupCommentData> OnGetGroupCommentCreated;
        public static Action<List<GroupCommentData>> OnGetGroupPostComments;
        //Group Comment Deleted
        public static Action<List<GroupMemberData>> OnGetGroupMembers;
        public static Action<List<GroupData>> OnGetGroupSearchResults;
        public static Action<List<GroupInvitationData>> OnGetGroupPendingInvitations;
        public static Action<List<GroupInvitationData>> OnGetGroupMyInvitations;
        public static Action<List<GroupRequestData>> OnGetGroupMyRequests;
        public static Action<List<GroupRequestData>> OnGetGroupPendingRequests;
    }
}