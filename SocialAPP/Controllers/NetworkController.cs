﻿using Network;
using SocialAPP.Shared;
using SocialAPP.Shared.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using NetworkReader = Network.NetworkReader;
using NetworkWriter = Network.NetworkWriter;

namespace SocialAPP
{
    public class NetworkController
    {
        #region Variables

        private static NetworkConnection Connection = new NetworkConnection();
//#if DEBUG
//        public static string ServerHostname = "149.56.25.176";
//        public static string WebServerAddress = "http://149.56.25.176/social/";
//#else
        public static string ServerHostname = "149.56.25.176";
        public static string WebServerAddress = "http://curssor.com/";
//#endif
        public static int ServerPort = 4297;
        public const string Version = "1.0.1";
        private static bool shouldBeConnected;
        private static bool isConnecting;
        private static bool changePictureToo = false;
        public static string AccountId = "";
        public static string AccountName = "";
        public static string AccountAvatar = "";
        public static int AccountGender = 0;
        public static bool AccountMembership = false;
        public static bool IsLoggedIn;
        private bool preventSending;
        private static Timer receiveTimer;
        private static volatile int dldnow;
        public static double Latitude;
        public static double Longitude;

        #endregion Variables

        #region Constructors

        public NetworkController()
        {
            Task.Factory.StartNew(() =>
            {
                while (true)
                {
                    Receive(dldnow);
                }
            });
            //receiveTimer = new Timer(new TimerCallback(Receive), dldnow, 100, 100);
            Connection.onData += OnReceiveData;
            Connection.onPlayerDisconnected += onDisconnected;
        }
        
        #endregion Constructors

        #region Functions

        public void Receive(object obj)
        {
            try
            {
                if (IsConnected)
                {
                    Connection.Receive();
                    //EventsController.Log?.Invoke("Receiving data", null);
                }
                else
                {
                    if (shouldBeConnected)
                    {
                        shouldBeConnected = false;
                        Connect();
                    }
                }
            }
            catch (Exception ex)
            {
                EventsController.Log?.Invoke(ex.StackTrace);
                //throw ex;
            }
        }

        public static bool IsConnected { get { return Connection != null && Connection.isConnected; } }

        private void onDisconnected(ushort id)
        {
            IsLoggedIn = false;
            EventsController.OnDisconnected?.Invoke();
        }

        public bool Connect()
        {
            if (IsConnected)
            {
                return true;
            }
            try
            {
                if (isConnecting) return true;
                isConnecting = true;
                Connection.Connect(ServerHostname, ServerPort);
                if (IsConnected)
                {
                    EventsController.OnConnected?.Invoke();
                    GetVersion();
                    isConnecting = false;
                    shouldBeConnected = true;
                    return true;
                }
            }
            catch (Exception ex)
            {
                //Interface.LogError(ex.StackTrace);
            }
            return false;
        }

        public bool Disconnect()
        {
            try
            {
                Connection.Disconnect();
                EventsController.OnDisconnected?.Invoke();
                return true;
            }
            catch (Exception ex)
            {
                //Interface.LogError(ex.StackTrace);
            }
            return false;
        }

        private void OnReceiveData(byte tag, ushort subject, object data)
        {
            switch (tag)
            {
                #region Authentication

                case ClientTags.GET_VERSION:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            var version = reader.ReadString();
                            EventsController.OnGetVersion?.Invoke(version);
                        }
                        catch (Exception ex)
                        {
                            //Interface.LogError(ex.StackTrace);
                        }
                    }
                }
                break;
                case ClientTags.DO_LOGIN:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        int result = reader.ReadInt32();
                        if (result == 0)
                        {
                            IsLoggedIn = true;
                            AccountId = reader.ReadString();
                            AccountName = reader.ReadString();
                            AccountAvatar = reader.ReadString();
                            AccountGender = reader.ReadInt32();
                            AccountMembership = reader.ReadBoolean();

                            var bytes = reader.ReadBytes();
                            var requests = ProtoSerializer.Deserialize<RequestData[]>(bytes);

                            EventsController.OnGetAccountInfos?.Invoke(AccountId, AccountName, AccountAvatar, AccountGender, AccountMembership);
                            EventsController.OnGetRequests(requests.ToList());
                            LoadConversations(1);
                            LoadNotifications();
                            GetRecentPosts();
                        }
                        EventsController.OnGetLoginResult?.Invoke(result);
                        preventSending = false;
                    }
                }
                break;
                case ClientTags.DO_LOGOUT:
                break;
                case ClientTags.DO_REGISTER:
                using (NetworkReader reader = (NetworkReader)data)
                {
                    int result = reader.ReadInt32();

                    EventsController.OnGetRegisterResult?.Invoke(result);
                    preventSending = false;
                }
                break;
                case ClientTags.DO_RECOVER:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        int result = reader.ReadInt32();

                        EventsController.OnGetRecoverResult?.Invoke(result);
                        preventSending = false;
                    }
                }
                break;
                case ClientTags.DISCONNECTED_BY_SERVER:
                try
                {
                    Connection.Disconnect();
                }
                catch (Exception) { }

                onDisconnected(0);
                break;

                #endregion Authentication

                #region Edit Account
                case ClientTags.GETEDIT_ACCOUNT:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        string email = reader.ReadString();
                        string username = reader.ReadString();
                        string firstname = reader.ReadString();
                        string lastname = reader.ReadString();
                        string dob_d = reader.ReadString();
                        string dob_m = reader.ReadString();
                        string dob_y = reader.ReadString();

                        EventsController.OnGetEditAccount?.Invoke(email, username, firstname, lastname, dob_d, dob_m, dob_y);
                    }
                }
                break;
                case ClientTags.DO_SAVEACCOUNT:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        var result = reader.ReadInt32();
                        EventsController.OnGetEditAccountResult?.Invoke(result);
                    }
                }
                break;
                #endregion Edit Account

                case ClientTags.UPDATE_POS:

                break;

                #region View Profile
                case ClientTags.GET_PROFILE_DATA:
                using (NetworkReader reader = (NetworkReader)data)
                {
                    if (reader.ReadInt32() == 0)
                    {
                        EventsController.OnReturnToHome?.Invoke();
                    }
                    else
                    {
                        string id = reader.ReadString();
                        string username = reader.ReadString();
                        string picture2 = reader.ReadString();
                        string city2 = reader.ReadString();
                        string job2 = reader.ReadString();
                        string activities2 = reader.ReadString();
                        string description2 = reader.ReadString();
                        string lookingFor2 = reader.ReadString();
                        string lookingForDescription2 = reader.ReadString();
                        int gender2 = reader.ReadInt32();
                        int age = reader.ReadInt32();
                        bool followed = reader.ReadBoolean();
                        ProfileData profileData = new ProfileData
                        {
                            Id = id,
                            Username = username,
                            Picture = picture2,
                            City = city2,
                            Job = job2,
                            Activities = activities2,
                            Description = description2.Replace("\n", "\\n"),
                            LookingFor = lookingFor2,
                            LookingForDescription = lookingForDescription2.Replace("\n", "\\n"),
                            Gender = gender2,
                            Age = age,
                            Followed = followed
                        };

                        //Followers
                        var followersBytes = reader.ReadBytes();
                        var followers = ProtoSerializer.Deserialize<List<FollowData>>(followersBytes);
                        EventsController.OnGetProfileFollowers?.Invoke(followers);

                        //Followings
                        var followingsBytes = reader.ReadBytes();
                        var followings = ProtoSerializer.Deserialize<List<FollowData>>(followingsBytes);
                        EventsController.OnGetProfileFollowings?.Invoke(followings);

                        //Posts
                        var postsBytes = reader.ReadBytes();
                        var posts = ProtoSerializer.Deserialize<List<PostData>>(postsBytes);
                        EventsController.OnGetProfilePosts(posts);
                        foreach (var post in posts)
                        {
                            GetPostComments(post.Id);
                        }
                        profileData.PhotosCount = reader.ReadInt32();
                        profileData.IsBlocked = reader.ReadBoolean();
                        //Trigger this at the end!
                        EventsController.OnGetProfileData?.Invoke(profileData);
                    }
                }
                break;
                case ClientTags.GETPROFILE_PHOTOS:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        var photosBytes = reader.ReadBytes();

                        var photo = ProtoSerializer.Deserialize<PostData[]>(photosBytes);

                        EventsController.OnGetProfilePictures?.Invoke(photo.ToList());
                    }
                }
                break;
                #endregion View Profile

                #region Edit Profile
                case ClientTags.GET_ACCOUNT_DATA:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        string picture = reader.ReadString();
                        string city = reader.ReadString();
                        string job = reader.ReadString();
                        string activities = reader.ReadString();
                        string description = reader.ReadString();
                        string lookingFor = reader.ReadString();
                        string lookingForDescription = reader.ReadString();
                        int gender = reader.ReadInt32();

                        ProfileData profileData = new ProfileData
                        {
                            Picture = picture,
                            City = city,
                            Job = job,
                            Activities = activities,
                            Description = description.Replace("\n", "\\n"),
                            LookingFor = lookingFor,
                            LookingForDescription = lookingForDescription.Replace("\n", "\\n"),
                            Gender = gender
                        };
                        EventsController.OnGetEditProfileData?.Invoke(profileData);
                    }
                }
                break;
                case ClientTags.EDIT_PROFILE_DATA:
                {
                    if (!changePictureToo)
                    {
                        EventsController.OnProfileEdited?.Invoke();
                    }
                    changePictureToo = false;
                }
                break;
                case ClientTags.EDIT_PROFILE_PICTURE:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        string uploadToken = reader.ReadString();
                        EventsController.OnGetUploadProfilePictureToken?.Invoke(uploadToken);
                    }
                }
                break;
                #endregion Edit Profile

                #region Search
                case ClientTags.SEARCH_PROFILES:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            List<AccountData> key = ProtoSerializer.Deserialize<List<AccountData>>(reader.ReadBytes());
                            int value = reader.ReadInt32();
                            Dictionary<List<AccountData>, int> profiles = new Dictionary<List<AccountData>, int>();
                            profiles.Add(key, value);

                            EventsController.OnGetSearchResults?.Invoke(profiles);
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace);
                        }
                    }
                }
                break;
                #endregion Search

                #region Messages
                case ClientTags.GET_CONVERSATIONS:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            List<ConversationData> conversations = ProtoSerializer.Deserialize<List<ConversationData>>(reader.ReadBytes());
                            EventsController.OnLoadConversationsResult?.Invoke(conversations);
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace);
                        }
                    }
                }
                break;
                case ClientTags.GET_MESSAGES:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            List<MessageData> messages = ProtoSerializer.Deserialize<List<MessageData>>(reader.ReadBytes());
                            EventsController.OnLoadMessagesResult?.Invoke(messages);
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace);
                        }
                    }
                }
                break;
                case ClientTags.RECEIVED_MESSAGE:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            var result = reader.ReadInt32();
                            if (result == 0)
                            {
                                EventsController.OnReturnToHome?.Invoke();
                            }
                            else
                            {
                                MessageData message = ProtoSerializer.Deserialize<MessageData>(reader.ReadBytes());
                                ConversationData conversation = ProtoSerializer.Deserialize<ConversationData>(reader.ReadBytes());
                                EventsController.OnReceiveMessage?.Invoke(message, conversation);
                            }
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace);
                        }
                    }
                }
                break;
                case ClientTags.MESSAGE_SEEN:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            MessageData messages = ProtoSerializer.Deserialize<MessageData>(reader.ReadBytes());
                            EventsController.OnMessageSeen?.Invoke(messages);
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace);
                        }
                    }
                }
                break;
                case ClientTags.START_CONVERSATION:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            var result = reader.ReadInt32();
                            if (result == 0)
                            {
                                EventsController.OnReturnToHome?.Invoke();
                            }
                            else
                            {
                                ConversationData conversation = ProtoSerializer.Deserialize<ConversationData>(reader.ReadBytes());
                                EventsController.OnConversationStarted?.Invoke(conversation);
                            }
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace);
                        }
                    }
                }
                break;
                #endregion Messages

                #region Notifications
                case ClientTags.GET_NOTIFICATIONS:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            List<NotificationData> notifications = ProtoSerializer.Deserialize<List<NotificationData>>(reader.ReadBytes());
                            EventsController.OnGetNotifications?.Invoke(notifications);
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace);
                        }
                    }
                }
                break;
                case ClientTags.RECEIVED_NOTIFICATION:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            NotificationData notification = ProtoSerializer.Deserialize<NotificationData>(reader.ReadBytes());
                            EventsController.OnGetNotification?.Invoke(notification);
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace);
                        }
                    }
                }
                break;
                #endregion Notifications

                #region Posts
                case ClientTags.CREATE_POST:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            var postBytes = reader.ReadBytes();
                            var post = ProtoSerializer.Deserialize<PostData>(postBytes);

                            if (post != null)
                            {
                                //Post created successfully
                                EventsController.OnGetPost?.Invoke(post);
                            }
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace);
                        }
                    }
                }
                break;
                case ClientTags.DELETE_POST:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            var postId = reader.ReadString();
                            var result = reader.ReadBoolean();
                            if (!string.IsNullOrEmpty(postId) && result)
                            {
                                //Post deleted successfully
                                EventsController.OnGetPostDeleted?.Invoke(postId);
                            }
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace);
                        }
                    }
                }
                break;
                case ClientTags.GETUSER_POSTS:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            var bytes = reader.ReadBytes();
                            var posts = ProtoSerializer.Deserialize<PostData[]>(bytes);
                            EventsController.OnGetProfilePosts?.Invoke(posts.ToList());
                            foreach (var post in posts)
                            {
                                GetPostComments(post.Id);
                            }
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace);
                        }
                    }
                }
                break;
                case ClientTags.GETRECENT_POSTS:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            var bytes = reader.ReadBytes();
                            var posts = ProtoSerializer.Deserialize<PostData[]>(bytes).ToList();
                            if (posts != null)
                            {
                                EventsController.OnGetPosts?.Invoke(posts.ToList());
                                foreach (var post in posts)
                                {
                                    GetPostComments(post.Id);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace);
                        }
                    }
                }
                break;
                case ClientTags.COMMENT_POST:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            var trigger = reader.ReadBoolean();
                            if (trigger)
                            {
                                var bytes = reader.ReadBytes();
                                var comment = ProtoSerializer.Deserialize<CommentData>(bytes);

                                EventsController.OnGetComment?.Invoke(comment);
                            }
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace);
                        }
                    }
                }
                break;
                case ClientTags.UNCOMMENT_POST:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            var commentId = reader.ReadString();
                            var result = reader.ReadBoolean();
                            if (result)
                                EventsController.OnGetCommentDeleted?.Invoke(commentId);
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace);
                        }
                    }
                }
                break;
                case ClientTags.GETCOMMENTS_POST:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            var bytes = reader.ReadBytes();
                            var comments = ProtoSerializer.Deserialize<CommentData[]>(bytes);
                            EventsController.OnGetComments?.Invoke(comments.ToList());
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace);
                        }
                    }
                }
                break;
                #endregion Posts

                #region Payments
                case ClientTags.GET_TRANSACTIONS:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            var bytes = reader.ReadBytes();
                            var transactions = ProtoSerializer.Deserialize<TransactionData[]>(bytes);
                            EventsController.OnGetTransactions?.Invoke(transactions.ToList());
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace);
                        }
                    }
                }
                break;
                case ClientTags.VERIFY_MEMBERSHIP:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            var result = reader.ReadBoolean();
                            AccountMembership = result;
                            EventsController.OnGetVerifyMembership?.Invoke(result);
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace);
                        }
                    }
                }
                break;
                case ClientTags.SEND_TRANSACTION:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            var result = reader.ReadBoolean();
                            AccountMembership = result;
                            EventsController.OnGetPaymentResult?.Invoke(result);
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace);
                        }
                    }
                }
                break;
                case ClientTags.PREMIUM_REQUIRED:
                {
                    EventsController.OnPremiumRequired?.Invoke();
                }
                break;
                #endregion Payments

                #region Blocks
                case ClientTags.GET_BLOCKEDUSERS:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            List<BlockData> blockedUsers = ProtoSerializer.Deserialize<List<BlockData>>(reader.ReadBytes());
                            EventsController.OnGetBlockedUsers?.Invoke(blockedUsers);
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace);
                        }
                    }
                }
                break;
                #endregion

                #region Calls
                case ClientTags.REQUEST:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            RequestData request = ProtoSerializer.Deserialize<RequestData>(reader.ReadBytes());
                            EventsController.OnGetRequest?.Invoke(request, true);
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace);
                        }
                    }
                }
                break;
                case ClientTags.DO_CALL:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            CallData call = ProtoSerializer.Deserialize<CallData>(reader.ReadBytes());
                            EventsController.OnStartCalling?.Invoke(call);
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace);
                        }
                    }
                }
                break;
                case ClientTags.DO_CALL_RING:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            EventsController.OnCallRingTone?.Invoke();
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace);
                        }
                    }
                }
                break;
                case ClientTags.RECEIVE_CALL:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            CallData call = ProtoSerializer.Deserialize<CallData>(reader.ReadBytes());
                            EventsController.OnReceiveCall?.Invoke(call);
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace);
                        }
                    }
                }
                break;
                case ClientTags.START_CALL:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            CallData call = ProtoSerializer.Deserialize<CallData>(reader.ReadBytes());
                            var serverHostname = reader.ReadString();
                            var serverPort = reader.ReadInt32();
                            
                            EventsController.OnStartCall?.Invoke(call, serverHostname, serverPort);
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace);
                        }
                    }
                }
                break;
                case ClientTags.END_CALL:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            CallData call = ProtoSerializer.Deserialize<CallData>(reader.ReadBytes());
                            EventsController.OnCallEnded?.Invoke(call);
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace);
                        }
                    }
                }
                break;
                #endregion

                #region Mapping
                case ClientTags.SEND_REQUEST_POS:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            var latitude = reader.ReadDouble();
                            var longitude = reader.ReadDouble();
                            EventsController.OnGetPosition?.Invoke(latitude , longitude);
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace);
                        }
                    }
                }
                break;
                #endregion

                #region Groups
                case ClientTags.CREATE_GROUP:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            var result = reader.ReadInt32();
                            var group = result == 1 ? ProtoSerializer.Deserialize<GroupData>(reader.ReadBytes()) : null;

                            EventsController.OnGetGroupCreated?.Invoke(result, group);
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace);
                        }
                    }
                }
                break;
                case ClientTags.INVITE_GROUP:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            var result = reader.ReadInt32();
                            var invitation = result == 1 ? ProtoSerializer.Deserialize<GroupInvitationData>(reader.ReadBytes()) : null;

                            EventsController.OnGetGroupInvite?.Invoke(result, invitation);
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace);
                        }
                    }
                }
                break;
                case ClientTags.REQUEST_JOIN_GROUP:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            var result = reader.ReadInt32();
                            var request = result == 1 ? ProtoSerializer.Deserialize<GroupRequestData>(reader.ReadBytes()) : null;

                            EventsController.OnGetGroupRequest?.Invoke(result, request);
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace);
                        }
                    }
                }
                break;
                case ClientTags.CREATE_GROUP_POST:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            var post = ProtoSerializer.Deserialize<GroupPostData>(reader.ReadBytes());

                            EventsController.OnGetGroupPostCreated?.Invoke(post);
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace);
                        }
                    }
                }
                break;
                case ClientTags.GET_GROUP_POSTS:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            var post = ProtoSerializer.Deserialize<GroupPostData[]>(reader.ReadBytes());

                            EventsController.OnGetGroupPosts?.Invoke(post.ToList());
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace);
                        }
                    }
                }
                break;
                case ClientTags.COMMENT_GROUP_POST:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            var comment = ProtoSerializer.Deserialize<GroupCommentData>(reader.ReadBytes());

                            EventsController.OnGetGroupCommentCreated?.Invoke(comment);
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace);
                        }
                    }
                }
                break;
                case ClientTags.GET_GROUP_POST_COMMENTS:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            var comments = ProtoSerializer.Deserialize<GroupCommentData[]>(reader.ReadBytes());

                            EventsController.OnGetGroupPostComments?.Invoke(comments.ToList());
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace);
                        }
                    }
                }
                break;
                case ClientTags.GET_GROUP_MEMBERS:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            var members = ProtoSerializer.Deserialize<GroupMemberData[]>(reader.ReadBytes());

                            EventsController.OnGetGroupMembers?.Invoke(members.ToList());
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace);
                        }
                    }
                }
                break;
                case ClientTags.SEARCH_GROUP:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            var groups = ProtoSerializer.Deserialize<GroupData[]>(reader.ReadBytes());

                            EventsController.OnGetGroupSearchResults?.Invoke(groups.ToList());
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace);
                        }
                    }
                }
                break;
                case ClientTags.GET_PENDING_GROUP_INVITATIONS:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            var invitations = ProtoSerializer.Deserialize<GroupInvitationData[]>(reader.ReadBytes());

                            EventsController.OnGetGroupPendingInvitations?.Invoke(invitations.ToList());
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace);
                        }
                    }
                }
                break;
                case ClientTags.GET_MY_GROUP_INVITATIONS:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            var invitations = ProtoSerializer.Deserialize<GroupInvitationData[]>(reader.ReadBytes());

                            EventsController.OnGetGroupMyInvitations?.Invoke(invitations.ToList());
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace);
                        }
                    }
                }
                break;
                case ClientTags.GET_MY_PENDING_GROUP_REQUESTS:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            var requests = ProtoSerializer.Deserialize<GroupRequestData[]>(reader.ReadBytes());

                            EventsController.OnGetGroupMyRequests?.Invoke(requests.ToList());
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace);
                        }
                    }
                }
                break;
                case ClientTags.GET_GROUP_REQUESTS:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            var requests = ProtoSerializer.Deserialize<GroupRequestData[]>(reader.ReadBytes());

                            EventsController.OnGetGroupPendingRequests?.Invoke(requests.ToList());
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace);
                        }
                    }
                }
                break;
                case ClientTags.GET_MY_GROUPS:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            var groups = ProtoSerializer.Deserialize<GroupData[]>(reader.ReadBytes());

                            EventsController.OnGetMyGroups?.Invoke(groups.ToList());
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace);
                        }
                    }
                }
                break;
                case ClientTags.GET_USER_GROUPS:
                {
                    using (NetworkReader reader = (NetworkReader)data)
                    {
                        try
                        {
                            var groups = ProtoSerializer.Deserialize<GroupData[]>(reader.ReadBytes());

                            EventsController.OnGetUserGroups?.Invoke(groups.ToList());
                        }
                        catch (Exception ex)
                        {
                            EventsController.Log?.Invoke(ex.StackTrace);
                        }
                    }
                }
                break;
                #endregion
            }
        }

        #endregion Functions

        #region Triggers

        //Send
        public void GetVersion()
        {
            Connection.SendMessageToServer(ClientTags.GET_VERSION, 0, null);
        }

        #region Account Authentication

        public void SendLogin(string username, string password)
        {
            if (preventSending) return;

            preventSending = true;
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(Version);
                writer.Write(username);
                writer.Write(password);
                Connection.SendMessageToServer(ClientTags.DO_LOGIN, 0, writer);
            }
        }

        public void Logout()
        {
            Connection.SendMessageToServer(ClientTags.DO_LOGOUT, 0, null);
            IsLoggedIn = false;
            shouldBeConnected = false;
            Connection.Disconnect();
        }

        public void Register(string username, string password, string email, string firstname, string lastname, int dob_d, int dob_m, int dob_y, int gender)
        {
            if (preventSending) return;

            preventSending = true;
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(username.ToLower());
                writer.Write(Encryption.SHA1Of(password));
                writer.Write(email);
                writer.Write(firstname);
                writer.Write(lastname);
                writer.Write(dob_d);
                writer.Write(dob_m);
                writer.Write(dob_y);
                writer.Write(gender);
                Connection.SendMessageToServer(ClientTags.DO_REGISTER, 0, writer);
            }
        }

        public void Recover(string userText)
        {
            if (!Connection.isConnected && shouldBeConnected) Connect();
            if (preventSending) return;
            preventSending = true;
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(userText);
                Connection.SendMessageToServer(ClientTags.DO_RECOVER, 0, writer);
            }
        }

        #endregion Account Authentication

        //Geolocation
        public void UpdateGeolocation(double latitude, double longitude)
        {
            Latitude = latitude;
            Longitude = longitude;
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(latitude);
                writer.Write(longitude);
                Connection.SendMessageToServer(ClientTags.UPDATE_POS, 0, writer);
            }
        }

        //Search
        public void SearchProfiles(string username, string gender, string minAge, string maxAge, string distance, int page = 1)
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(username);
                writer.Write(gender);
                writer.Write(minAge);
                writer.Write(maxAge);
                writer.Write(distance);
                writer.Write(page);
                Connection.SendMessageToServer(ClientTags.SEARCH_PROFILES, 0, writer);
            }
        }

        #region Edit Profile

        public void LoadEditAccountInfo()
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(0);
                Connection.SendMessageToServer(ClientTags.GETEDIT_ACCOUNT, 0, writer);
            }
        }

        public void DoSaveAccount(string email, string username, string password, string firstname, string lastname, string dob_d, string dob_m, string dob_y)
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(email);
                writer.Write(username);
                writer.Write(Encryption.SHA1Of(password));
                writer.Write(firstname);
                writer.Write(lastname);
                writer.Write(Convert.ToInt32(dob_d));
                writer.Write(Convert.ToInt32(dob_m));
                writer.Write(Convert.ToInt32(dob_y));
                Connection.SendMessageToServer(ClientTags.DO_SAVEACCOUNT, 0, writer);
            }
        }

        public void EditProfilePicture()
        {
            changePictureToo = true;
            using (NetworkWriter writer = new NetworkWriter())
            {
                Connection.SendMessageToServer(ClientTags.EDIT_PROFILE_PICTURE, 0, writer);
            }
        }

        public void EditProfileInfo(string city, string job, string activities, string description, string lookingFor, string lookingForDescription)
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(city);
                writer.Write(job);
                writer.Write(activities);
                writer.Write(description);
                writer.Write(lookingFor);
                writer.Write(lookingForDescription);
                Connection.SendMessageToServer(ClientTags.EDIT_PROFILE_DATA, 0, writer);
            }
        }

        public void UpdateProfilePicture()
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                Connection.SendMessageToServer(ClientTags.UPDATE_PROFILE_PICTURE, 0, null);
            }
        }

        public void LoadEditProfileInfo()
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(0);
                Connection.SendMessageToServer(ClientTags.GET_ACCOUNT_DATA, 0, writer);
            }
        }

        #endregion Edit Profile

        #region View Profile

        public void LoadProfileData(string profileId)
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(profileId);
                Connection.SendMessageToServer(ClientTags.GET_PROFILE_DATA, 0, writer);
            }
        }

        public void GetProfilePhotos(string profileId, int page = 1)
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(profileId);
                writer.Write(page);
                Connection.SendMessageToServer(ClientTags.GETPROFILE_PHOTOS, 0, writer);
            }
        }

        #endregion View Profile

        #region Messages

        public void LoadConversations(int page = 1)
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(page);
                Connection.SendMessageToServer(ClientTags.GET_CONVERSATIONS, 0, writer);
            }
        }

        public void LoadMessages(string conversationId, int page = 1)
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(conversationId);
                writer.Write(page);
                Connection.SendMessageToServer(ClientTags.GET_MESSAGES, 0, writer);
            }
        }

        public void SendMessage(string toAccountId, string message, string attachmentId = "")
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(toAccountId);
                writer.Write(message);
                writer.Write(attachmentId);

                Connection.SendMessageToServer(ClientTags.SEND_MESSAGE, 0, writer);
            }
        }

        public void SeenMessage(string messageId)
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(messageId);
                Connection.SendMessageToServer(ClientTags.MESSAGE_SEEN, 0, writer);
            }
        }

        public void StartConversation(string profileId)
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(profileId);
                Connection.SendMessageToServer(ClientTags.START_CONVERSATION, 0, writer);
            }
        }

        public void SetMsgRequestResult(string userId, bool result)
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(userId);
                writer.Write(result);
                Connection.SendMessageToServer(ClientTags.SET_MSG_REQUEST_RESULT, 0, writer);
            }
        }
        #endregion Messages

        #region Follows

        public void Follow(string accountId)
        {
            if (string.IsNullOrEmpty(accountId)) return;
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(accountId);
                Connection.SendMessageToServer(ClientTags.FOLLOW, 0, writer);
            }
        }

        public void Unfollow(string accountId)
        {
            if (string.IsNullOrEmpty(accountId)) return;
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(accountId);
                Connection.SendMessageToServer(ClientTags.UNFOLLOW, 0, writer);
            }
        }

        #endregion Follows

        #region Notifications

        public void LoadNotifications()
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(AccountId);
                Connection.SendMessageToServer(ClientTags.GET_NOTIFICATIONS, 0, writer);
            }
        }

        public void Poke(string accountId)
        {
            if (string.IsNullOrEmpty(accountId)) return;
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(accountId);
                Connection.SendMessageToServer(ClientTags.POKE, 0, writer);
            }
        }

        public void DeleteNotification(string notificationId)
        {
            if (string.IsNullOrEmpty(notificationId)) return;
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(notificationId);
                Connection.SendMessageToServer(ClientTags.DELETE_NOTIFICATION, 0, writer);
            }
        }

        #endregion Notifications

        #region Posts

        public void GetRecentPosts()
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                Connection.SendMessageToServer(ClientTags.GETRECENT_POSTS, 0, writer);
            }
        }

        public void CreatePost(string content, string attachmentId)
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(content);
                writer.Write(attachmentId);
                Connection.SendMessageToServer(ClientTags.CREATE_POST, 0, writer);
            }
        }

        public void DeletePost(string postId)
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(postId);
                Connection.SendMessageToServer(ClientTags.DELETE_POST, 0, writer);
            }
        }

        public void CommentPost(string postId, string content, string attachmentId)
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(postId);
                writer.Write(content);
                writer.Write(attachmentId);
                Connection.SendMessageToServer(ClientTags.COMMENT_POST, 0, writer);
            }
        }

        public void DeleteComment(string commentId)
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(commentId);
                Connection.SendMessageToServer(ClientTags.UNCOMMENT_POST, 0, writer);
            }
        }

        public void GetPostComments(string postId)
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(postId);
                Connection.SendMessageToServer(ClientTags.GETCOMMENTS_POST, 0, writer);
            }
        }

        #endregion Posts

        #region Payments

        public void GetTransactions(int page = 1)
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(page);
                Connection.SendMessageToServer(ClientTags.GET_TRANSACTIONS, 0, writer);
            }
        }

        public void VerifyMembership()
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                Connection.SendMessageToServer(ClientTags.VERIFY_MEMBERSHIP, 0, writer);
            }
        }

        public void SendPayment(string tokenId, int plan = 0)
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(tokenId);
                writer.Write(plan);
                Connection.SendMessageToServer(ClientTags.SEND_TRANSACTION, 0, writer);
            }
        }

        #endregion Payments

        #region Blocks
        public void GetBlockedUsers()
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                Connection.SendMessageToServer(ClientTags.GET_BLOCKEDUSERS, 0, writer);
            }
        }
        public void BlockUser(string userId)
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(userId);
                Connection.SendMessageToServer(ClientTags.BLOCK_USER, 0, writer);
            }
        }
        public void UnBlockUser(string userId)
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(userId);
                Connection.SendMessageToServer(ClientTags.UNBLOCK_USER, 0, writer);
            }
        }
        #endregion

        #region Calls
        public void SendCall(string userId)
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(userId);
                Connection.SendMessageToServer(ClientTags.REQUEST, 0, writer);
            }
        }
        public void SetCallRequestResult(string userId, bool result)
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(userId);
                writer.Write(result);
                Connection.SendMessageToServer(ClientTags.CALL_REQUEST_RESULT, 0, writer);
            }
        }
        
        public void SetCallResult(string callId, bool result)
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(callId);
                writer.Write(result);
                Connection.SendMessageToServer(ClientTags.SET_CALL_RESULT, 0, writer);
            }
        }
        public void EndCall(string callId)
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(callId);
                Connection.SendMessageToServer(ClientTags.END_CALL, 0, writer);
            }
        }
        public void SetCallVideoEnabled(string callId, bool enabled)
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(callId);
                writer.Write(enabled);
                Connection.SendMessageToServer(ClientTags.VIDEO_ENABLED, 0, writer);
            }
        }

        #endregion

        #region Mapping
        public void GetMapPosition(string userId)
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(userId);
                Connection.SendMessageToServer(ClientTags.REQUEST_POS, 0, writer);
            }
        }
        public void SetMapRequestResult(string userId, bool result)
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(userId);
                writer.Write(result);
                Connection.SendMessageToServer(ClientTags.SET_REQUEST_POS_RESULT, 0, writer);
            }
        }
        #endregion

        #region Groups
        public void GetMyGroups()
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                Connection.SendMessageToServer(ClientTags.GET_MY_GROUPS, 0, writer);
            }
        }

        public void GetUserGroups(string accountId)
        {
            if (string.IsNullOrEmpty(accountId)) return;
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(accountId);
                Connection.SendMessageToServer(ClientTags.GET_USER_GROUPS, 0, writer);
            }
        }

        public void CreateGroup(string groupName)
        {
            if (string.IsNullOrEmpty(groupName)) return;
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(groupName);
                Connection.SendMessageToServer(ClientTags.CREATE_GROUP, 0, writer);
            }
        }
        public void DeleteGroup(string groupId)
        {
            if (string.IsNullOrEmpty(groupId)) return;
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(groupId);
                Connection.SendMessageToServer(ClientTags.DELETE_GROUP, 0, writer);
            }
        }
        public void InviteGroup(string userId, string groupId)
        {
            if (string.IsNullOrEmpty(userId) || string.IsNullOrEmpty(groupId)) return;
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(userId);
                writer.Write(groupId);
                Connection.SendMessageToServer(ClientTags.INVITE_GROUP, 0, writer);
            }
        }
        public void SetGroupInviteResult(string invitationId, bool result)
        {
            if (string.IsNullOrEmpty(invitationId)) return;
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(invitationId);
                writer.Write(result);
                Connection.SendMessageToServer(ClientTags.SET_GROUP_INVITE_RESULT, 0, writer);
            }
        }
        public void RequestJoinGroup(string groupId)
        {
            if (string.IsNullOrEmpty(groupId)) return;
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(groupId);
                Connection.SendMessageToServer(ClientTags.REQUEST_JOIN_GROUP, 0, writer);
            }
        }
        public void CancelGroupRequest(string requestId)
        {
            if (string.IsNullOrEmpty(requestId)) return;
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(requestId);
                Connection.SendMessageToServer(ClientTags.CANCEL_REQUEST_GROUP, 0, writer);
            }
        }
        public void SetGroupRequestResult(string requestId, bool result)
        {
            if (string.IsNullOrEmpty(requestId)) return;
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(requestId);
                writer.Write(result);
                Connection.SendMessageToServer(ClientTags.SET_GROUP_REQUEST_RESULT, 0, writer);
            }
        }
        public void SetMemberAdministrator(string userId, string groupId, bool isAdmin)
        {
            if (string.IsNullOrEmpty(userId) || string.IsNullOrEmpty(groupId)) return;
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(userId);
                writer.Write(groupId);
                writer.Write(isAdmin);
                Connection.SendMessageToServer(ClientTags.SET_MEMBER_ADMINISTRATOR, 0, writer);
            }
        }
        public void DeleteGroupMember(string userId, string groupId)
        {
            if (string.IsNullOrEmpty(userId) || string.IsNullOrEmpty(groupId)) return;
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(userId);
                writer.Write(groupId);
                Connection.SendMessageToServer(ClientTags.DELETE_GROUP_MEMBER, 0, writer);
            }
        }
        public void CreateGroupPost(string groupId, string message, string attachmentId = "")
        {
            if (string.IsNullOrEmpty(message) || string.IsNullOrEmpty(groupId)) return;
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(groupId);
                writer.Write(message);
                writer.Write(attachmentId);
                Connection.SendMessageToServer(ClientTags.CREATE_GROUP_POST, 0, writer);
            }
        }
        public void DeleteGroupPost(string postId)
        {
            if (string.IsNullOrEmpty(postId)) return;
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(postId);
                Connection.SendMessageToServer(ClientTags.DELETE_GROUP_POST, 0, writer);
            }
        }
        public void GetGroupPosts(string groupId)
        {
            if (string.IsNullOrEmpty(groupId)) return;
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(groupId);
                Connection.SendMessageToServer(ClientTags.GET_GROUP_POSTS, 0, writer);
            }
        }
        public void CommentGroupPost(string postId, string message, string attachmentId = "")
        {
            if (string.IsNullOrEmpty(message) || string.IsNullOrEmpty(postId)) return;
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(postId);
                writer.Write(message);
                writer.Write(attachmentId);
                Connection.SendMessageToServer(ClientTags.COMMENT_GROUP_POST, 0, writer);
            }
        }
        public void DeleteGroupComment(string commentId)
        {
            if (string.IsNullOrEmpty(commentId)) return;
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(commentId);
                Connection.SendMessageToServer(ClientTags.DELETE_GROUP_COMMENT, 0, writer);
            }
        }
        public void GetGroupPostComments(string postId)
        {
            if (string.IsNullOrEmpty(postId)) return;
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(postId);
                Connection.SendMessageToServer(ClientTags.GET_GROUP_POST_COMMENTS, 0, writer);
            }
        }
        public void SetGroupPublic(string groupId, bool isPublic)
        {
            if (string.IsNullOrEmpty(groupId)) return;
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(groupId);
                writer.Write(isPublic);
                Connection.SendMessageToServer(ClientTags.SET_GROUP_PUBLIC, 0, writer);
            }
        }
        public void GetGroupMembers(string groupId)
        {
            if (string.IsNullOrEmpty(groupId)) return;
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(groupId);
                Connection.SendMessageToServer(ClientTags.GET_GROUP_MEMBERS, 0, writer);
            }
        }
        public void SearchGroup(string search)
        {
            if (string.IsNullOrEmpty(search)) return;
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(search);
                Connection.SendMessageToServer(ClientTags.SEARCH_GROUP, 0, writer);
            }
        }
        public void GetPendingGroupInvitations(string groupId)
        {
            if (string.IsNullOrEmpty(groupId)) return;
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(groupId);
                Connection.SendMessageToServer(ClientTags.GET_PENDING_GROUP_INVITATIONS, 0, writer);
            }
        }
        public void GetMyGroupInvitations()
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                Connection.SendMessageToServer(ClientTags.GET_MY_GROUP_INVITATIONS, 0, writer);
            }
        }
        public void GetMyPendingGroupRequests()
        {
            using (NetworkWriter writer = new NetworkWriter())
            {
                Connection.SendMessageToServer(ClientTags.GET_MY_PENDING_GROUP_REQUESTS, 0, writer);
            }
        }
        public void GetGroupRequests(string groupId)
        {
            if (string.IsNullOrEmpty(groupId)) return;
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(groupId);
                Connection.SendMessageToServer(ClientTags.GET_GROUP_REQUESTS, 0, writer);
            }
        }
        public void QuitGroup(string groupId)
        {
            if (string.IsNullOrEmpty(groupId)) return;
            using (NetworkWriter writer = new NetworkWriter())
            {
                writer.Write(groupId);
                Connection.SendMessageToServer(ClientTags.QUIT_GROUP, 0, writer);
            }
        }
        #endregion

        #endregion Triggers
    }
}