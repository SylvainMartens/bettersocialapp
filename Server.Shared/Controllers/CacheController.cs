﻿using Enyim.Caching;
using Enyim.Caching.Configuration;
using Enyim.Caching.Memcached;
using Network;
using SocialAPP.Shared;
using System;
using System.Net;

namespace Server.Shared.Controllers
{
    public static class CacheController
    {
        private static MemcachedClient client;
        private static MemcachedClientConfiguration configs;
        private static object locker = new object();
        private static bool initialized;
        private const string cachePrefix = "soc_";

        private static void Initialize()
        {
            lock (locker)
            {
                if (initialized) return;
                try
                {
                    LogManager.AssignFactory(new DiagnosticsLogFactory("log_cache.log"));
                    configs = new MemcachedClientConfiguration();
                    configs.Servers.Add(new IPEndPoint(IPAddress.Parse("127.0.0.1"), 11211));
                    configs.Protocol = MemcachedProtocol.Binary;
                    //configs.Authentication.Type = typeof(PlainTextAuthenticator);
                    //configs.Authentication.Parameters["userName"] = "Default";
                    //configs.Authentication.Parameters["password"] = "0212Wisa";
                    //configs.Authentication.Parameters["zone"] = "";
                    client = new MemcachedClient(configs);
                    initialized = true;
                }
                catch (Exception ex)
                {
                    Interface.Log(ex.StackTrace);
                }
            }
        }

        public static T Get<T>(string cacheName)
        {
            T returnObj = default(T);
            //lock (locker)
            {
                if (!initialized) Initialize();
                var bytes = client.Get<byte[]>(cachePrefix + cacheName.ToLower());
                if (bytes != null && bytes.Length > 0) returnObj = ProtoSerializer.Deserialize<T>(bytes);
            }
            return returnObj;
        }

        public static void Set<T>(string cacheName, T cacheValue, uint cacheTime)
        {
            //lock (locker)
            {
                if (!initialized) Initialize();
                client.Store(StoreMode.Set, cachePrefix + cacheName.ToLower(), ProtoSerializer.Serialize(cacheValue), DateTime.Now.AddSeconds(cacheTime));
            }
        }

        public static void Remove(string cacheName)
        {
            //lock (locker)
            {
                if (!initialized) Initialize();
                client.Remove(cachePrefix + cacheName.ToLower());
            }
        }
    }
}