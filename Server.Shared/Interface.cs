﻿using System;
using System.IO;

namespace Server.Shared
{
	public class Interface
	{
		static string logFilePath = "";

		static object consoleLock = new object();

		static object logfileLock = new object();

		static Action<string> onLog;

		static Action<string> onWarning;

		static Action<string> onError;

		static Action<Exception> onException;

		static Action<string> onFatal;

		public static void LogTrace(string message)
		{
			LogTrace(message, "Trace");
		}

		internal static void LogTrace(string message, string prefix)
		{
			message.Replace("\n", "\n\t\t\t");
			lock (logfileLock)
			{
				using (StreamWriter streamWriter = new StreamWriter(logFilePath, true))
				{
					streamWriter.WriteLine(string.Concat(new string[]
					{
						DateTime.Now.ToString("d/M/yyyy HH:mm:ss tt"),
						"\t[",
						prefix,
						"]\t",
						message
					}));
				}
			}
		}

		public static void Log(string message)
		{
			lock (consoleLock)
			{
				onLog?.Invoke(message);
			}
			LogTrace(message, "Log");
		}

		public static void LogWarning(string message)
		{
			lock (consoleLock)
			{
				onWarning?.Invoke(message);
			}
			LogTrace(message, "Warning");
		}

		public static void LogError(string message)
		{
			lock (consoleLock)
			{
				onError?.Invoke(message);
			}
			LogTrace(message, "Error");
		}

		public static void LogError(Exception exception)
		{
			lock (consoleLock)
			{
				onException?.Invoke(exception);
			}
			LogTrace(exception.StackTrace, "Error");
		}

		public static void LogFatal(string message)
		{
			lock (consoleLock)
			{
				onFatal?.Invoke(message);
			}
			LogTrace(message, "Fatal");
		}

		public static void Setup(Action<string> onLog, Action<string> onWarning, Action<string> onError, Action<string> onFatal, Action<Exception> onException)
		{
			if (!Directory.Exists(string.Concat(new object[]
			{
				Directory.GetCurrentDirectory(),
				Path.DirectorySeparatorChar,
				"Logs",
				Path.DirectorySeparatorChar,
				DateTime.Now.ToString("d-M-yyyy")
			})))
			{
				Directory.CreateDirectory(string.Concat(new object[]
				{
					Directory.GetCurrentDirectory(),
					Path.DirectorySeparatorChar,
					"Logs",
					Path.DirectorySeparatorChar,
					DateTime.Now.ToString("d-M-yyyy")
				}));
			}
			logFilePath = string.Concat(new object[]
			{
				Directory.GetCurrentDirectory(),
				Path.DirectorySeparatorChar,
				"Logs",
				Path.DirectorySeparatorChar,
				DateTime.Now.ToString("d-M-yyyy"),
				Path.DirectorySeparatorChar,
				DateTime.Now.ToString("HH-mm-ss tt"),
				".txt"
			});
			Interface.onLog = onLog;
			Interface.onWarning = onWarning;
			Interface.onError = onError;
			Interface.onFatal = onFatal;
			Interface.onException = onException;
		}
	}
}