﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;

namespace Server.Shared
{
	public class MySQLHelper
	{
		public static string connectionString = "SERVER=149.56.25.176;DATABASE=social;USERNAME=hesa;PASSWORD=0212securitywisa;Convert Zero Datetime=True;Allow Zero Datetime=True;";

		/// <summary>
		/// 	Execute a query on the server with no return values.
		/// </summary>
		/// <param name="query">The query.</param>
		/// <param name="paramaters">The paramaters to be added to this query.</param>
		public void ExecuteNonQuery(string query, params QueryParameter[] parameters)
		{
			try
			{
				using (MySqlConnection connection = new MySqlConnection(connectionString))
				{
					using (MySqlCommand command = new MySqlCommand(query, connection))
					{
						foreach (QueryParameter parameter in parameters)
							command.Parameters.AddWithValue(parameter.Name, parameter.Value);

						connection.Open();
						command.ExecuteNonQuery();
					}
				}
			}
			catch (MySqlException e)
			{
                Interface.LogError(e);
				throw new DatabaseException(e.Message, e);
			}
		}

		/// <summary>
		/// 	Executes a query on the database with a scalar return.
		/// </summary>
		/// <returns>The object returned from the database.</returns>
		/// <param name="query">The query.</param>
		/// <param name="paramaters">The paramaters to be added to this query.</param>
		public object ExecuteScalar(string query, params QueryParameter[] parameters)
		{
			try
			{
				using (MySqlConnection connection = new MySqlConnection(connectionString))
				{
					using (MySqlCommand command = new MySqlCommand(query, connection))
					{
						foreach (QueryParameter parameter in parameters)
							command.Parameters.AddWithValue(parameter.Name, parameter.Value);

						connection.Open();
						return command.ExecuteScalar();
					}
				}
			}
			catch (MySqlException e)
			{
                Interface.LogError(e);
				throw new DatabaseException(e.Message, e);
			}
		}

		/// <summary>
		/// 	Executes a query on the database returning an array of rows.
		/// </summary>
		/// <returns>The rows of the database selected.</returns>
		/// <param name="query">The query.</param>
		/// <param name="paramaters">The paramaters to be added to this query.</param>
		public DatabaseRow[] ExecuteQuery(string query, params QueryParameter[] parameters)
		{
			try
			{
				using (MySqlConnection connection = new MySqlConnection(connectionString))
				{
					using (MySqlCommand command = new MySqlCommand(query, connection))
					{
						foreach (QueryParameter parameter in parameters) command.Parameters.AddWithValue(parameter.Name, parameter.Value);

						connection.Open();
						using (MySqlDataReader reader = command.ExecuteReader())
						{
							int fieldCount = reader.FieldCount;
							List<DatabaseRow> rows = new List<DatabaseRow>();

							while (reader.Read())
							{
								//For each row create a DatabaseRow
								DatabaseRow row = new DatabaseRow();

								//And add each field to it
								for (int i = 0; i < fieldCount; i++)
								{
									row.Add(
										reader.GetName(i),
										reader.GetValue(i)
									);
								}

								//Add it to the rows
								rows.Add(row);
							}

							return rows.ToArray();
						}
					}
				}
			}
			catch (MySqlException e)
			{
                Interface.LogError(e);
				throw new DatabaseException(e.Message, e);
			}
		}

		/// <summary>
		/// 	Removes any characters that could allow SQL injection.
		/// </summary>
		/// <param name="c">The string to escape</param>
		/// <param name="query">Query.</param>
		public string EscapeString(string s)
		{
			return MySqlHelper.EscapeString(s);
		}

	}

	public class DatabaseRow : Dictionary<string, object>
	{
	}

	public class QueryParameter
	{
		string _name = "";
		object _value;

		public string Name { get { return _name; } }
		public object Value { get { return _value; } }

		public QueryParameter(string name, object value)
		{
			_name = name;
			_value = value;
		}
	}

	public class DatabaseException : Exception
	{
		public DatabaseException()
		{
		}

		public DatabaseException(string message) : base(message)
		{
		}

		public DatabaseException(string message, Exception innerException) : base(message, innerException)
		{
		}
	}
}