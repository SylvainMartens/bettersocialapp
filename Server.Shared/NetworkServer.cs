﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using NetworkCommsDotNet.DPSBase;
using NetworkCommsDotNet;
using NetworkCommsDotNet.Connections.TCP;
using NetworkCommsDotNet.Connections;
using NetworkCommsDotNet.Tools;
using System.Reflection;
using Newtonsoft.Json;
using SocialAPP.Shared;

namespace Server.Shared
{
	/// <summary>
	/// Example which demonstrates the ability to establish SSL encrypted TCP connections
	/// </summary>
	public static class NetworkServer
	{
		public static int Port = 4297;
		public static readonly MySQLHelper database = new MySQLHelper();
		static List<Connection> Connections = new List<Connection>();
		public static int ConnectionsLenght { get { return Connections.Count; } }
		/// <summary>
		/// A suitable certificate to use for the example
		/// </summary>
		static X509Certificate2 certificate;

		/// <summary>
		/// SSLOptions which will be used for incoming connections
		/// </summary>
		static SSLOptions listenerSSLOptions;

		/// <summary>
		/// SSLOptions which will be used for outgoing connections
		/// </summary>
		//static SSLOptions connectionSSLOptions;

		/// <summary>
		/// The SendReceiveOptions used for sending
		/// </summary>
		static SendReceiveOptions sendingSendReceiveOptions;

		public static Action<Connection, byte, byte, byte[]> OnMessageReceived;
		public static Action<Connection> OnClientConnected;
		public static Action<Connection> OnClientDisconnected;

		/// <summary>
		/// Run server
		/// </summary>
		public static void Start()
		{
            try
            {
                NetworkComms.AppendGlobalConnectionEstablishHandler(ClientConnected);
                NetworkComms.AppendGlobalConnectionCloseHandler(ClientDisconnected);

                var assembly = Assembly.GetExecutingAssembly();
                var assemblyName = assembly.FullName.Split(',')[0];

                byte[] bytes;
                using (System.IO.Stream s = assembly.GetManifestResourceStream(assemblyName + ".Resources.curssor_com.pfx"))
                {
                    long length = s.Length;
                    bytes = new byte[length];
                    s.Read(bytes, 0, (int) length);
                }

                certificate = new X509Certificate2(bytes, "Security1QW!");

                //Add a global incoming packet handler for packets of type "Message"
                //This handler will convert the incoming raw bytes into a string (this is what 
                //the <string> bit means) and then write that string to the local console window.
                NetworkComms.AppendGlobalIncomingPacketHandler<NetworkMessage>("SecureNetworkMessage", (packetHeader, connection, networkMessage) =>
                {
                    if (networkMessage == null) return;
                    OnMessageReceived?.Invoke(connection, networkMessage.Id, networkMessage.Tag, networkMessage.Data);
                });

                //Create suitable SSLOptions to use with TCP
                SelectSSLOptions();

                //Get a list of all local endPoints using the default port
                //List<IPEndPoint> desiredlocalEndPoints = (from current in HostInfo.IP.FilteredLocalAddresses() select new IPEndPoint(current, 0)).ToList();
                List<IPEndPoint> desiredlocalEndPoints = new List<IPEndPoint>
	            {
	                new IPEndPoint(IPAddress.Any, Port)
	            };

                //Create a list of matching TCP listeners where we provide the listenerSSLOptions
                List<ConnectionListenerBase> listeners = (from current in desiredlocalEndPoints select (ConnectionListenerBase)(new TCPConnectionListener(sendingSendReceiveOptions, ApplicationLayerProtocolStatus.Enabled, listenerSSLOptions))).ToList();

                //Start listening for incoming TCP connections
                Connection.StartListening(listeners, desiredlocalEndPoints, true);

                //Print out the listening addresses and ports
                Console.WriteLine("\nListening for incoming TCP (SSL) connections on:");


                foreach (IPEndPoint localEndPoint in Connection.ExistingLocalListenEndPoints(ConnectionType.TCP))
                    Console.WriteLine("{0}:{1}", localEndPoint.Address, localEndPoint.Port);
            }
            catch (Exception ex)
            {
                Interface.LogError(ex);
            }
		}

		public static Connection GetConnectionServiceByID(ShortGuid id)
		{
			try
			{
				lock (Connections)
				{
					return Connections.FirstOrDefault(x => x.ConnectionInfo.NetworkIdentifier == id);
				}
			}
			catch (Exception ex)
			{
				Interface.LogError(ex);
			}
			return null;
		}

		/// <summary>
		/// A new Client has connected
		/// </summary>
		static void ClientConnected(Connection connection)
		{
			lock (Connections)
			{
				Connections.Add(connection);
			}
			Console.WriteLine("Client connected - " + connection.ToString());
			OnClientConnected?.Invoke(connection);
		}

		/// <summary>
		/// A Client has connected
		/// </summary>
		static void ClientDisconnected(Connection connection)
		{
			lock (Connections)
			{
				Connections.Remove(connection);
			}
			Console.WriteLine("Client has disconnected - " + connection.ToString());
			OnClientDisconnected?.Invoke(connection);
		}

		/// <summary>
		/// Select the SSL options
		/// </summary>
		private static void SelectSSLOptions()
		{
			//Configure the server options
			//These will be applied for incoming connections
			listenerSSLOptions = new SSLOptions(certificate, false, true);//Only allow client with ssl ( for now allow self signed ssl )

			//Configure the connection options
			//These will be used when establishing outgoing connections
			//connectionSSLOptions = new SSLOptions(certificate, false, true);//Provide certificate for outgoing connection

			//Select if the dataPadder will be enabled
            sendingSendReceiveOptions = new SendReceiveOptions<NetworkCommsDotNet.DPSBase.ProtobufSerializer, DataPadder>();
			DataPadder.AddPaddingOptions(sendingSendReceiveOptions.Options, 4096, DataPadder.DataPaddingType.Random, true);
		}
	}
}