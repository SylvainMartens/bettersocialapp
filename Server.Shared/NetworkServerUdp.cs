﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using NetworkCommsDotNet.DPSBase;
using NetworkCommsDotNet;
using NetworkCommsDotNet.Connections.UDP;
using NetworkCommsDotNet.Connections;
using NetworkCommsDotNet.Tools;
using System.Reflection;
using Newtonsoft.Json;
using SocialAPP.Shared;

namespace Server.Shared
{
	/// <summary>
	/// Example which demonstrates the ability to establish SSL encrypted UDP connections
	/// </summary>
	public static class NetworkServerUdp
	{
		public static int Port = 4298;
		public static readonly MySQLHelper database = new MySQLHelper();
		static List<Connection> Connections = new List<Connection>();
		public static int ConnectionsLenght { get { return Connections.Count; } }

		/// <summary>
		/// The SendReceiveOptions used for sending
		/// </summary>
		static SendReceiveOptions sendingSendReceiveOptions;

		public static Action<Connection, byte, byte, byte[]> OnMessageReceived;
		public static Action<Connection> OnClientConnected;
		public static Action<Connection> OnClientDisconnected;

		/// <summary>
		/// Run server
		/// </summary>
		public static void Start()
		{
            try
            {
                NetworkComms.AppendGlobalConnectionEstablishHandler(ClientConnected);
                NetworkComms.AppendGlobalConnectionCloseHandler(ClientDisconnected);

                //Add a global incoming packet handler for packets of type "Message"
                //This handler will convert the incoming raw bytes into a string (this is what 
                //the <string> bit means) and then write that string to the local console window.
                NetworkComms.AppendGlobalIncomingPacketHandler<NetworkMessage>("NetworkMessage", (packetHeader, connection, networkMessage) =>
                {
                    if (networkMessage == null) return;
                    OnMessageReceived?.Invoke(connection, networkMessage.Id, networkMessage.Tag, networkMessage.Data);
                });

                //Get a list of all local endPoints using the default port
                //List<IPEndPoint> desiredlocalEndPoints = (from current in HostInfo.IP.FilteredLocalAddresses() select new IPEndPoint(current, 0)).ToList();
                List<IPEndPoint> desiredlocalEndPoints = new List<IPEndPoint>
	            {
	                new IPEndPoint(IPAddress.Any, Port)
	            };

				//Select if the dataPadder will be enabled
	            sendingSendReceiveOptions = new SendReceiveOptions<NetworkCommsDotNet.DPSBase.ProtobufSerializer, SharpZipLibCompressor.SharpZipLibGzipCompressor>();
				//DataPadder.AddPaddingOptions(sendingSendReceiveOptions.Options, 4096, DataPadder.DataPaddingType.Random, true);

                //Create a list of matching UDP listeners where we provide the listenerSSLOptions
                List<ConnectionListenerBase> listeners = (from current in desiredlocalEndPoints select (ConnectionListenerBase)(new UDPConnectionListener(sendingSendReceiveOptions, ApplicationLayerProtocolStatus.Enabled, UDPOptions.None))).ToList();

                //Start listening for incoming UDP connections
                Connection.StartListening(listeners, desiredlocalEndPoints, true);

                //Print out the listening addresses and ports
                Console.WriteLine("\nListening for incoming UDP connections on:");


                foreach (IPEndPoint localEndPoint in Connection.ExistingLocalListenEndPoints(ConnectionType.UDP))
                    Console.WriteLine("{0}:{1}", localEndPoint.Address, localEndPoint.Port);
            }
            catch (Exception ex)
            {
                Interface.LogError(ex);
            }
		}

		public static void Stop()
		{
			NetworkComms.Shutdown();
		}

		public static Connection GetConnectionServiceByID(ShortGuid id)
		{
			try
			{
				lock (Connections)
				{
					return Connections.FirstOrDefault(x => x.ConnectionInfo.NetworkIdentifier == id);
				}
			}
			catch (Exception ex)
			{
				Interface.LogError(ex);
			}
			return null;
		}

		/// <summary>
		/// A new Client has connected
		/// </summary>
		static void ClientConnected(Connection connection)
		{
			lock (Connections)
			{
				Connections.Add(connection);
			}
			Console.WriteLine("Client connected - " + connection.ToString());
			OnClientConnected?.Invoke(connection);
		}

		/// <summary>
		/// A Client has connected
		/// </summary>
		static void ClientDisconnected(Connection connection)
		{
			lock (Connections)
			{
				Connections.Remove(connection);
			}
			Console.WriteLine("Client has disconnected - " + connection.ToString());
			OnClientDisconnected?.Invoke(connection);
		}
	}
}