using Android.Media;
using SocialAPP.Shared;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace SocialAPP.Droid
{
    public static class AudioPlayer
    {
        static AudioTrack audioTrack = null;
        static bool useSpeaker;
        static bool playSound = true;
        static bool run;
        static List<byte[]> Queued = new List<byte[]>();

        static int bitrate;
        static int encoding;
        static int bufferSize;

        public static void ToggleMuteSound()
        {
            playSound = !playSound;
        }

        public static void WriteBytes(byte[] bytes)
        {
            var audioPacket = ProtoSerializer.Deserialize<AudioPacket>(bytes);
            if (audioPacket == null) return;
            Play(audioPacket);
        }

        public static void ToggleSpeaker()
        {
            useSpeaker = !useSpeaker;

            audioTrack?.Stop();
            audioTrack?.Release();
            audioTrack?.Dispose();

            audioTrack = new AudioTrack(
                // Stream type
                useSpeaker ? Stream.Music : Stream.VoiceCall,
                // Frequency
                bitrate,
                // Mono or stereo
                ChannelOut.Mono,
                // Audio encoding
                Encoding.Pcm16bit,
                // Length of the audio clip.
                bufferSize, //mBufferSize,
                            // Mode. Stream or static.
                AudioTrackMode.Stream
            );
            audioTrack.SetVolume(1);
            audioTrack.Play();
        }

        private static void Play(AudioPacket audioPacket)
        {
            try
            {
                if (!playSound) return;
                if (audioTrack == null)
                {
                    bitrate = audioPacket.Bitrate;
                    encoding = audioPacket.Encoding;
                    bufferSize = audioPacket.Data.Length;

                    audioTrack = new AudioTrack(
                        // Stream type
                        useSpeaker ? Stream.Music : Stream.VoiceCall,
                        // Frequency
                        bitrate,
                        // Mono or stereo
                        ChannelOut.Mono,
                        // Audio encoding
                        Encoding.Pcm16bit,
                        // Length of the audio clip.
                        bufferSize, //mBufferSize,
                                    // Mode. Stream or static.
                        AudioTrackMode.Stream
                    );
                    audioTrack.SetVolume(1);
                    audioTrack.Play();
                }

                //audioTrack.Write(audioPacket.Data, 0, audioPacket.Data.Length);
                Queued.Add(audioPacket.Data);
                if (!run)
                {
                    run = true;
                    Thread playThread = new Thread(PlayThread);
                    playThread.Start();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
        }

        static void PlayThread()
        {
            while (true)
            {
                if (!run)
                {
                    break;
                }
                try
                {
                    if (Queued.Count != 0)
                    {
                        var bytes = Queued[0];
                        Queued.Remove(bytes);
                        audioTrack?.Write(bytes, 0, bytes.Length);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
            }
        }

        public static void Stop()
        {
            run = false;
            Thread.Sleep(500);
            if (audioTrack != null)
            {
                audioTrack.Stop();
                audioTrack.Release();
                audioTrack.Dispose();
                audioTrack = null;
            }
            Queued.Clear();
            useSpeaker = false;
        }
    }
}