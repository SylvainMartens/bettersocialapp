﻿using NetworkCommsDotNet;
using NetworkCommsDotNet.Connections;
using NetworkCommsDotNet.Connections.UDP;
using NetworkCommsDotNet.DPSBase;
using System;
using System.Net;
using System.Threading.Tasks;
using SocialAPP.Shared;
//using SendReceiveOptions = NetworkCommsDotNet.Connections.UDP.UDPOptions.SendReceiveOptions;

namespace SocialAPP.Droid
{
	public class CallClient
	{
        string _hostname = "127.0.0.1";
        int _port = 4141;

        /// <summary>
        /// The SendReceiveOptions used for sending
        /// </summary>
        static SendReceiveOptions sendingSendReceiveOptions;

        static ConnectionInfo connectionInfo;
        static UDPConnection connection;

        public Action OnConnected;

		public string CallId = "";
        public bool RemoteVideoEnabled;
        public bool MyVideoEnabled;

		public Action<byte[]> OnGetAudio;
        public Action<byte[]> OnGetVideo;
        public Action OnDisconnected;
        bool disconnectedTriggered;

		public int Ping
        {
            get
            {
				return 0;//TODO
                //if (_clientConnection == null) return 0;
                //return (int) _clientConnection.AveragePing;
            }
        }

		public CallClient(string hostname, int port, string callId)
		{
            _hostname = hostname;
            _port = port;
            CallId = callId;

            connectionInfo = new ConnectionInfo(new IPEndPoint(IPAddress.Parse(_hostname), _port), ApplicationLayerProtocolStatus.Enabled);

            SetDebugTimeouts();

            //Add a global incoming packet handler for packets of type "Message"
            //This handler will convert the incoming raw bytes into a string (this is what 
            //the <string> bit means) and then write that string to the local console window.
            NetworkComms.AppendGlobalIncomingPacketHandler<NetworkMessage>("NetworkMessage", (packetHeader, connection, networkMessage) =>
            {
                if (networkMessage == null) return;
				try
				{
					switch(networkMessage.Id)
		            {
		                case CallTags.SYNC_AUDIO:
		                {
		                    OnGetAudio?.Invoke(networkMessage.Data);
		                }
		                break;
		                case CallTags.SYNC_VIDEO:
		                {
		                    OnGetVideo?.Invoke(networkMessage.Data);
		                }
		                break;
		                case CallTags.DISCONNECTED:
		                {
		                    if (!disconnectedTriggered)
		                    {
		                        disconnectedTriggered = true;
		                        OnDisconnected?.Invoke();
		                    }
		                }
		                break;
		            }
				}catch(Exception)
				{
				}
            });

			//Select if the dataPadder will be enabled
            sendingSendReceiveOptions = new SendReceiveOptions<NetworkCommsDotNet.DPSBase.ProtobufSerializer, SharpZipLibCompressor.SharpZipLibGzipCompressor>();
            //DataPadder.AddPaddingOptions(sendingSendReceiveOptions.Options, 40960, DataPadder.DataPaddingType.Zero, true);
		}
        
        private static void SetDebugTimeouts()
        {
            NetworkComms.ConnectionEstablishTimeoutMS = 10000;
            NetworkComms.PacketConfirmationTimeoutMS = 5000;
            NetworkComms.ConnectionAliveTestTimeoutMS = 1000;
        }

		public bool Connect()
        {
            try
            {
                disconnectedTriggered = false;

                // Get a connection to the target server using the connection SSL options we configured earlier
                //connection = UDPConnection.GetConnection(connectionInfo, sendingSendReceiveOptions);
				connection = UDPConnection.GetConnection(connectionInfo, UDPConnection.DefaultUDPOptions, sendingSendReceiveOptions);
                connection.AppendShutdownHandler(ClientDisconnected);
                OnConnected?.Invoke();

				using(NetworkWriter writer = new NetworkWriter())
				{
					writer.Write(CallId);
					Send(CallTags.SET_CALL, 0, writer);
				}

                return true;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
            }
            return false;
        }
        
        public void Disconnect()
        {
			connection?.CloseConnection(false, 0);
            connection?.Dispose();
        }

        void ClientDisconnected(Connection connection)
        {
            if (!disconnectedTriggered)
            {
                disconnectedTriggered = true;
                OnDisconnected?.Invoke();
            }
        }

		private bool Send(byte id, byte tag, NetworkWriter data)
        {
            if (connection == null) return false;
            try
            {
                connection.SendObject("NetworkMessage", new NetworkMessage()
                {
                    Id = id,
                    Tag = tag,
                    Data = data.ToArray()
                });
                return true;
            }catch(Exception ex)
            {
                Console.WriteLine(ex);
            }
            return false;
        }

		private bool Send(byte id, byte tag, byte[] data)
        {
            if (connection == null) return false;
            try
            {
                //Send our message on the encrypted connection
                connection.SendObject("NetworkMessage", new NetworkMessage()
                {
                    Id = id,
                    Tag = tag,
                    Data = data
                });
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return false;
        }

		public void SyncAudio(byte[] audio)
        {
			Send(CallTags.SYNC_AUDIO, 0, audio);
        }

        public void SyncVideo(byte[] video)
        {
			Send(CallTags.SYNC_VIDEO, 0, video);
        }

		public void Destroy()
        {
            disconnectedTriggered = true;
			Disconnect();
            OnGetAudio = null;
            OnGetVideo = null;
            OnDisconnected = null;
        }
	}
}
