package md5ba7f759270b4974060ec518cae28626d;


public class MyJavaScriptInterface
	extends java.lang.Object
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_GetWebServerAddress:()Ljava/lang/String;:__export__\n" +
			"n_Connect:()Z:__export__\n" +
			"n_LoadLoginSettings:()V:__export__\n" +
			"n_Login:(Ljava/lang/String;Ljava/lang/String;Z)V:__export__\n" +
			"n_Logout:()V:__export__\n" +
			"n_Register:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIII)V:__export__\n" +
			"n_Recover:(Ljava/lang/String;)V:__export__\n" +
			"n_SearchProfiles:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V:__export__\n" +
			"n_LoadEditAccountInfo:()V:__export__\n" +
			"n_DoSaveAccount:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V:__export__\n" +
			"n_DoEditProfilePicture:()V:__export__\n" +
			"n_SaveEditProfile:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V:__export__\n" +
			"n_LoadEditProfileInfo:()V:__export__\n" +
			"n_LoadProfileData:(Ljava/lang/String;)V:__export__\n" +
			"n_StartConversation:(Ljava/lang/String;)V:__export__\n" +
			"n_GetProfilePhotos:(Ljava/lang/String;I)V:__export__\n" +
			"n_LoadConversations:(I)V:__export__\n" +
			"n_LoadMessages:(Ljava/lang/String;I)V:__export__\n" +
			"n_SendMessage:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V:__export__\n" +
			"n_DoSelectMessagePicture:()V:__export__\n" +
			"n_SetMessageHasSeen:(Ljava/lang/String;)V:__export__\n" +
			"n_Follow:(Ljava/lang/String;)V:__export__\n" +
			"n_Unfollow:(Ljava/lang/String;)V:__export__\n" +
			"n_Poke:(Ljava/lang/String;)V:__export__\n" +
			"n_DeleteNotification:(Ljava/lang/String;)V:__export__\n" +
			"n_PageChanged:(Ljava/lang/String;)V:__export__\n" +
			"n_CreatePost:(Ljava/lang/String;Ljava/lang/String;)V:__export__\n" +
			"n_DoSelectPostPicture:(Z)V:__export__\n" +
			"n_DeletePost:(Ljava/lang/String;)V:__export__\n" +
			"n_CommentPost:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V:__export__\n" +
			"n_DeleteComment:(Ljava/lang/String;)V:__export__\n" +
			"n_GetTransactions:(I)V:__export__\n" +
			"n_VerifyMembership:()V:__export__\n" +
			"n_SendPayment:(Ljava/lang/String;I)V:__export__\n" +
			"n_GetBlockedUsers:()V:__export__\n" +
			"n_BlockUser:(Ljava/lang/String;)V:__export__\n" +
			"n_UnBlockUser:(Ljava/lang/String;)V:__export__\n" +
			"n_SendCall:(Ljava/lang/String;)V:__export__\n" +
			"n_SetRequestResult:(Ljava/lang/String;II)V:__export__\n" +
			"n_SetCallResult:(Ljava/lang/String;I)V:__export__\n" +
			"n_EndCall:(Ljava/lang/String;)V:__export__\n" +
			"n_SetCallVideoEnabled:(Ljava/lang/String;I)V:__export__\n" +
			"n_ToggleSpeakers:()V:__export__\n" +
			"n_ToggleMuteMic:()V:__export__\n" +
			"n_ToggleMuteSound:()V:__export__\n" +
			"n_ToggleCamera:()V:__export__\n" +
			"n_ToggleSwitchCamera:()V:__export__\n" +
			"n_GetMapPosition:(Ljava/lang/String;)V:__export__\n" +
			"";
		mono.android.Runtime.register ("SocialAPP.Droid.MyJavaScriptInterface, SocialAPP.Droid, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", MyJavaScriptInterface.class, __md_methods);
	}


	public MyJavaScriptInterface () throws java.lang.Throwable
	{
		super ();
		if (getClass () == MyJavaScriptInterface.class)
			mono.android.TypeManager.Activate ("SocialAPP.Droid.MyJavaScriptInterface, SocialAPP.Droid, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}

	public MyJavaScriptInterface (android.content.Context p0) throws java.lang.Throwable
	{
		super ();
		if (getClass () == MyJavaScriptInterface.class)
			mono.android.TypeManager.Activate ("SocialAPP.Droid.MyJavaScriptInterface, SocialAPP.Droid, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "Android.Content.Context, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065", this, new java.lang.Object[] { p0 });
	}

	@android.webkit.JavascriptInterface

	public java.lang.String GetWebServerAddress ()
	{
		return n_GetWebServerAddress ();
	}

	private native java.lang.String n_GetWebServerAddress ();

	@android.webkit.JavascriptInterface

	public boolean Connect ()
	{
		return n_Connect ();
	}

	private native boolean n_Connect ();

	@android.webkit.JavascriptInterface

	public void LoadLoginSettings ()
	{
		n_LoadLoginSettings ();
	}

	private native void n_LoadLoginSettings ();

	@android.webkit.JavascriptInterface

	public void Login (java.lang.String p0, java.lang.String p1, boolean p2)
	{
		n_Login (p0, p1, p2);
	}

	private native void n_Login (java.lang.String p0, java.lang.String p1, boolean p2);

	@android.webkit.JavascriptInterface

	public void Logout ()
	{
		n_Logout ();
	}

	private native void n_Logout ();

	@android.webkit.JavascriptInterface

	public void Register (java.lang.String p0, java.lang.String p1, java.lang.String p2, java.lang.String p3, java.lang.String p4, int p5, int p6, int p7, int p8)
	{
		n_Register (p0, p1, p2, p3, p4, p5, p6, p7, p8);
	}

	private native void n_Register (java.lang.String p0, java.lang.String p1, java.lang.String p2, java.lang.String p3, java.lang.String p4, int p5, int p6, int p7, int p8);

	@android.webkit.JavascriptInterface

	public void Recover (java.lang.String p0)
	{
		n_Recover (p0);
	}

	private native void n_Recover (java.lang.String p0);

	@android.webkit.JavascriptInterface

	public void SearchProfiles (java.lang.String p0, java.lang.String p1, java.lang.String p2, java.lang.String p3, java.lang.String p4, int p5)
	{
		n_SearchProfiles (p0, p1, p2, p3, p4, p5);
	}

	private native void n_SearchProfiles (java.lang.String p0, java.lang.String p1, java.lang.String p2, java.lang.String p3, java.lang.String p4, int p5);

	@android.webkit.JavascriptInterface

	public void LoadEditAccountInfo ()
	{
		n_LoadEditAccountInfo ();
	}

	private native void n_LoadEditAccountInfo ();

	@android.webkit.JavascriptInterface

	public void DoSaveAccount (java.lang.String p0, java.lang.String p1, java.lang.String p2, java.lang.String p3, java.lang.String p4, java.lang.String p5, java.lang.String p6, java.lang.String p7)
	{
		n_DoSaveAccount (p0, p1, p2, p3, p4, p5, p6, p7);
	}

	private native void n_DoSaveAccount (java.lang.String p0, java.lang.String p1, java.lang.String p2, java.lang.String p3, java.lang.String p4, java.lang.String p5, java.lang.String p6, java.lang.String p7);

	@android.webkit.JavascriptInterface

	public void DoEditProfilePicture ()
	{
		n_DoEditProfilePicture ();
	}

	private native void n_DoEditProfilePicture ();

	@android.webkit.JavascriptInterface

	public void SaveEditProfile (java.lang.String p0, java.lang.String p1, java.lang.String p2, java.lang.String p3, java.lang.String p4, java.lang.String p5, java.lang.String p6)
	{
		n_SaveEditProfile (p0, p1, p2, p3, p4, p5, p6);
	}

	private native void n_SaveEditProfile (java.lang.String p0, java.lang.String p1, java.lang.String p2, java.lang.String p3, java.lang.String p4, java.lang.String p5, java.lang.String p6);

	@android.webkit.JavascriptInterface

	public void LoadEditProfileInfo ()
	{
		n_LoadEditProfileInfo ();
	}

	private native void n_LoadEditProfileInfo ();

	@android.webkit.JavascriptInterface

	public void LoadProfileData (java.lang.String p0)
	{
		n_LoadProfileData (p0);
	}

	private native void n_LoadProfileData (java.lang.String p0);

	@android.webkit.JavascriptInterface

	public void StartConversation (java.lang.String p0)
	{
		n_StartConversation (p0);
	}

	private native void n_StartConversation (java.lang.String p0);

	@android.webkit.JavascriptInterface

	public void GetProfilePhotos (java.lang.String p0, int p1)
	{
		n_GetProfilePhotos (p0, p1);
	}

	private native void n_GetProfilePhotos (java.lang.String p0, int p1);

	@android.webkit.JavascriptInterface

	public void LoadConversations (int p0)
	{
		n_LoadConversations (p0);
	}

	private native void n_LoadConversations (int p0);

	@android.webkit.JavascriptInterface

	public void LoadMessages (java.lang.String p0, int p1)
	{
		n_LoadMessages (p0, p1);
	}

	private native void n_LoadMessages (java.lang.String p0, int p1);

	@android.webkit.JavascriptInterface

	public void SendMessage (java.lang.String p0, java.lang.String p1, java.lang.String p2)
	{
		n_SendMessage (p0, p1, p2);
	}

	private native void n_SendMessage (java.lang.String p0, java.lang.String p1, java.lang.String p2);

	@android.webkit.JavascriptInterface

	public void DoSelectMessagePicture ()
	{
		n_DoSelectMessagePicture ();
	}

	private native void n_DoSelectMessagePicture ();

	@android.webkit.JavascriptInterface

	public void SetMessageHasSeen (java.lang.String p0)
	{
		n_SetMessageHasSeen (p0);
	}

	private native void n_SetMessageHasSeen (java.lang.String p0);

	@android.webkit.JavascriptInterface

	public void Follow (java.lang.String p0)
	{
		n_Follow (p0);
	}

	private native void n_Follow (java.lang.String p0);

	@android.webkit.JavascriptInterface

	public void Unfollow (java.lang.String p0)
	{
		n_Unfollow (p0);
	}

	private native void n_Unfollow (java.lang.String p0);

	@android.webkit.JavascriptInterface

	public void Poke (java.lang.String p0)
	{
		n_Poke (p0);
	}

	private native void n_Poke (java.lang.String p0);

	@android.webkit.JavascriptInterface

	public void DeleteNotification (java.lang.String p0)
	{
		n_DeleteNotification (p0);
	}

	private native void n_DeleteNotification (java.lang.String p0);

	@android.webkit.JavascriptInterface

	public void PageChanged (java.lang.String p0)
	{
		n_PageChanged (p0);
	}

	private native void n_PageChanged (java.lang.String p0);

	@android.webkit.JavascriptInterface

	public void CreatePost (java.lang.String p0, java.lang.String p1)
	{
		n_CreatePost (p0, p1);
	}

	private native void n_CreatePost (java.lang.String p0, java.lang.String p1);

	@android.webkit.JavascriptInterface

	public void DoSelectPostPicture (boolean p0)
	{
		n_DoSelectPostPicture (p0);
	}

	private native void n_DoSelectPostPicture (boolean p0);

	@android.webkit.JavascriptInterface

	public void DeletePost (java.lang.String p0)
	{
		n_DeletePost (p0);
	}

	private native void n_DeletePost (java.lang.String p0);

	@android.webkit.JavascriptInterface

	public void CommentPost (java.lang.String p0, java.lang.String p1, java.lang.String p2)
	{
		n_CommentPost (p0, p1, p2);
	}

	private native void n_CommentPost (java.lang.String p0, java.lang.String p1, java.lang.String p2);

	@android.webkit.JavascriptInterface

	public void DeleteComment (java.lang.String p0)
	{
		n_DeleteComment (p0);
	}

	private native void n_DeleteComment (java.lang.String p0);

	@android.webkit.JavascriptInterface

	public void GetTransactions (int p0)
	{
		n_GetTransactions (p0);
	}

	private native void n_GetTransactions (int p0);

	@android.webkit.JavascriptInterface

	public void VerifyMembership ()
	{
		n_VerifyMembership ();
	}

	private native void n_VerifyMembership ();

	@android.webkit.JavascriptInterface

	public void SendPayment (java.lang.String p0, int p1)
	{
		n_SendPayment (p0, p1);
	}

	private native void n_SendPayment (java.lang.String p0, int p1);

	@android.webkit.JavascriptInterface

	public void GetBlockedUsers ()
	{
		n_GetBlockedUsers ();
	}

	private native void n_GetBlockedUsers ();

	@android.webkit.JavascriptInterface

	public void BlockUser (java.lang.String p0)
	{
		n_BlockUser (p0);
	}

	private native void n_BlockUser (java.lang.String p0);

	@android.webkit.JavascriptInterface

	public void UnBlockUser (java.lang.String p0)
	{
		n_UnBlockUser (p0);
	}

	private native void n_UnBlockUser (java.lang.String p0);

	@android.webkit.JavascriptInterface

	public void SendCall (java.lang.String p0)
	{
		n_SendCall (p0);
	}

	private native void n_SendCall (java.lang.String p0);

	@android.webkit.JavascriptInterface

	public void SetRequestResult (java.lang.String p0, int p1, int p2)
	{
		n_SetRequestResult (p0, p1, p2);
	}

	private native void n_SetRequestResult (java.lang.String p0, int p1, int p2);

	@android.webkit.JavascriptInterface

	public void SetCallResult (java.lang.String p0, int p1)
	{
		n_SetCallResult (p0, p1);
	}

	private native void n_SetCallResult (java.lang.String p0, int p1);

	@android.webkit.JavascriptInterface

	public void EndCall (java.lang.String p0)
	{
		n_EndCall (p0);
	}

	private native void n_EndCall (java.lang.String p0);

	@android.webkit.JavascriptInterface

	public void SetCallVideoEnabled (java.lang.String p0, int p1)
	{
		n_SetCallVideoEnabled (p0, p1);
	}

	private native void n_SetCallVideoEnabled (java.lang.String p0, int p1);

	@android.webkit.JavascriptInterface

	public void ToggleSpeakers ()
	{
		n_ToggleSpeakers ();
	}

	private native void n_ToggleSpeakers ();

	@android.webkit.JavascriptInterface

	public void ToggleMuteMic ()
	{
		n_ToggleMuteMic ();
	}

	private native void n_ToggleMuteMic ();

	@android.webkit.JavascriptInterface

	public void ToggleMuteSound ()
	{
		n_ToggleMuteSound ();
	}

	private native void n_ToggleMuteSound ();

	@android.webkit.JavascriptInterface

	public void ToggleCamera ()
	{
		n_ToggleCamera ();
	}

	private native void n_ToggleCamera ();

	@android.webkit.JavascriptInterface

	public void ToggleSwitchCamera ()
	{
		n_ToggleSwitchCamera ();
	}

	private native void n_ToggleSwitchCamera ();

	@android.webkit.JavascriptInterface

	public void GetMapPosition (java.lang.String p0)
	{
		n_GetMapPosition (p0);
	}

	private native void n_GetMapPosition (java.lang.String p0);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
