//#region Variables
//General
var shouldNotShotBootBtns = false;
var connectedToServer = false;
var webServerAddress = "";
var currentPage = "home";
//Account
var AccountId = "0";
var AccountName = "";
var AccountAvatar = "";
var AccountGender = 0;
var AccountUsername = "";
var AccountMembership = false;
//Search
var search_username = "";
var search_gender = "";
var search_minAge = "";
var search_maxAge = "";
var search_distance = "";
var search_page = 0;
//Profile
var selectedProfileId = "";
var selectedProfileFollowed = false;
//Messages
var selectedConversationId = "";
var sendToId = "";
var otherUserAvatar = "";
var otherUserName = "";
var newMessageCount = 0;
//
var profilePictureChanged = false;
//Calls
var numberOfCalls = 0;
var requestsCount = 0;
var map;
//#endregion
$(document).ready(function () {
    webServerAddress = CSharp.GetWebServerAddress();
    $('#ms-menu-trigger').click(function (e) {
        $('.ms-menu').toggleClass('toggled');
    });
});

(function () {
    function isTouchDevice() {
        try {
            document.createEvent("TouchEvent");
            return true;
        } catch (e) {
            return false;
        }
    }
    function touchScroll(id) {
        if (isTouchDevice()) { //if touch events exist...
            var el = document.getElementById(id);
            var scrollStartPos = 0;

            document.getElementById(id).addEventListener("touchstart", function (event) {
                scrollStartPos = this.scrollTop + event.touches[0].pageY;
                //event.preventDefault();
            }, false);
            document.getElementById(id).addEventListener("touchmove", function (event) {
                this.scrollTop = scrollStartPos - event.touches[0].pageY;
                event.preventDefault();
            }, false);
        }
    }
    //On page load
    touchScroll('scrollMe');
    if (typeof CSharp !== 'undefined') {
        connectedToServer = CSharp.Connect();
        if (!connectedToServer) {
            $('.connectionStatus').text("Cannot connect to the server!");
        }
        CSharp.LoadLoginSettings();
    }
})();

//#region Stripe Payment
$(function () {
    var $form = $('#payment-form');
    $form.submit(function (event) {
        // Disable the submit button to prevent repeated clicks:
        $form.find('.submit').prop('disabled', true);

        // Request a token from Stripe:
        Stripe.card.createToken($form, stripeResponseHandler);

        // Prevent the form from being submitted:
        return false;
    });
});
function stripeResponseHandler(status, response) {
    // Grab the form:
    var $form = $('#payment-form');
    if (response.error) { // Problem!
        // Show the errors on the form:
        $form.find('.payment-errors').text(response.error.message);
        $form.find('.submit').prop('disabled', false); // Re-enable submission
    } else { // Token was created!
        // Get the token ID:
        var token = response.id;
        CSharp.SendPayment(token, 2);
    }
};
//#endregion

//#region Functions
var delay = (function () {
    var timer = 0;
    return function (callback, ms) {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
    };
})();
function ClearCurrentPage() {
    $('.homeContainer').hide();
    $('.notificationsContainer').hide();
    $('.messagesContainer').hide();
    $('.requestsContainer').hide();
    $('.searchContainer').hide();
    $('.profileContainer').hide();
    $(".groupsContainer").hide();
    $('.settingsContainer').hide();
    $('.accountContainer').hide();
    $('.editProfileContainer').hide();
    $('.paymentsContainer').hide();

    $('#link_home').removeClass('active');
    $('#link_notifications').removeClass('active');
    $('#link_messages').removeClass('active');
    $('#link_requests').removeClass('active');
    $('#link_search').removeClass('active');
    $('#link_profile').removeClass('active');
    $('#link_groups').removeClass('active');
    $('#link_settings').removeClass('active');
    $('#link_account').removeClass('active');
    $('#link_payments').removeClass('active');
}

function changePage(page) {
    ClearCurrentPage();
    currentPage = page;
    CSharp.PageChanged(currentPage);
    switch (page) {
        default:
        case "home":
            $('.homeContainer').show();
            $('#link_home').addClass('active');
        break;
        case "notifications":
            $('.notificationsContainer').show();
            $('#link_notifications').addClass('active');
        break;
        case "messages":
            $('.messagesContainer').show();
            $('#link_messages').addClass('active');
            if (selectedConversationId != "") {
                ShowConversation(selectedConversationId);
            }
        break;
        case "requests":
            $('.requestsContainer').show();
            $('#link_requests').addClass('active');
        break;
        case "search":
            $('.searchContainer').show();
            $('#link_search').addClass('active');
        break;
        case "profile":
            ViewProfile(AccountId);
            $('#link_profile').addClass('active');
        break;
        case "groups":
            LoadMyGroups();
            LoadMyGroupsInvitation();
            $('.groupsContainer').show();
            $('#link_groups').addClass('active');
        break;
        case "settings":
            $('.settingsContainer').show();
            $('#link_settings').addClass('active');
        break;
        case "account":
            $("#editAccountSuccess").hide();
            $('.accountContainer').show();
            $('#link_account').addClass('active');
            CSharp.LoadEditAccountInfo();
            GetBlockedUsers();
        break;
        case "editProfile":
            $('.editProfileContainer').show();
            $('#link_account').addClass('active');
            CSharp.LoadEditProfileInfo();
        break;
        case "payments":
            CSharp.GetTransactions(1);
            if (AccountMembership) {
                $("#paymentFormContainer").hide();
            } else {
                $("#paymentFormContainer").show();
            }
            $('.paymentsContainer').show();
            $('#link_payments').addClass('active');
        break;
    }
    $('body').click();
}

function changeBootLevel(level) {
    $('.boot').show();

    $('.bootBtns').hide();
    $('.bootLogin').hide();
    $('.bootRegister').hide();
    $('.bootForgot').hide();
    switch (level) {
        case 0:
            $('.connectionStatus').text("");
            $('.bootBtns').show();
            break;
        case 1:
            $('.bootLogin').show();
            break;
        case 2:
            $('.bootRegister').show();
            break;
        case 3:
            $('.connectionStatus').text("");
            $('.bootForgot').show();
            break;
    }
}

function isEmail(email) {
    return /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i.test(email);
}

// Date string (YYY-mm-dd) to Int age (years old)
function date_to_age(date) {
    try {
        var today = new Date();
        var d = new Date(date);

        var year = today.getFullYear() - d.getFullYear();
        var month = today.getMonth() - d.getMonth();
        var day = today.getDate() - d.getDate();
        var carry = 0;

        if (year < 0)
            return 0;
        if (month <= 0 && day <= 0)
            carry -= 1;

        var age = parseInt(year);
        age += carry;

        return Math.abs(age);
    } catch (err) {
        console.log(err.message);
    }
}

function DoSearch() {
    var username = document.getElementById('search_username').value;
    var gender = document.getElementById('search_gender').value;
    var minAge = document.getElementById('search_minAge').value;
    var maxAge = document.getElementById('search_maxAge').value;
    var distance = document.getElementById('search_distance').value;
    SearchProfiles(username, gender, minAge, maxAge, distance, 1);
}

function ChangeSearchPage(page) {
    SearchProfiles(search_username, search_gender, search_minAge, search_maxAge, search_distance, page);
}

function ShowNoSearchResults(show) {
    if (show) {
        $("#no-search-results").show();
    } else {
        $("#no-search-results").hide();
    }
}

function ClearSearchResults() {
    $(".search-results").html('');
}

function CreatePagination(total_count) {
    if (total_count < 1)
        $('.search_pagination').hide();
    else {
        $('.search_pagination').show();
        $('.search_pagination').bootpag({
            total: Math.ceil(total_count / 50),
            page: 1,
            maxVisible: 5,
            leaps: true,
            firstLastUse: true,
            first: '←',
            last: '→',
            wrapClass: 'pagination',
            activeClass: 'active',
            disabledClass: 'disabled',
            nextClass: 'next',
            prevClass: 'prev',
            lastClass: 'last',
            firstClass: 'first'
        }).on("page", function (event, num) {
            ChangeSearchPage(num);
        });
    }
}

function ShowWorkingLoader() {
    $('#workingLoader').fadeIn();
}

function HideWorkingLoader() {
    $('#workingLoader').fadeOut();
}

function UpdateUserAvatarName() {
    $('#messageUserAvatar').attr('src', AccountAvatar);
    $('#messageUserName').html(AccountName);
}

function HidePostImage(fromHome) {
    if(fromHome)
    {
        $('#postPictureBoxHome').hide();
        $('#postImgPathHome').attr("value", "");
    }else{
        $('#postPictureBox').hide();
        $('#postImgPath').attr("value", "");
    }
}

function HideMessageImage() {
    $('#messagePictureBox').hide();
    $('#messageImgInput').attr("value", "");
}

function Block()
{
    BlockUser(selectedProfileId);
    $("#btnBlock").hide();
    $("#btnUnBlock").show();
}
function UnBlock()
{
    UnBlockUser(selectedProfileId);
    $("#btnUnBlock").hide();
    $("#btnBlock").show();
}

function Call()
{
    CallUser(selectedProfileId);
}

//#endregion
//#region Network Triggers
function Login() {
    $('.connectionStatus').text('Please wait...');
    CSharp.Login(document.getElementById('login_username').value, document.getElementById('login_password').value, document.getElementById('login_remember').checked);
}

function Logout() {
    CSharp.Logout();
}

function Register() {
    var username = document.getElementById('register_username').value;
    var password = document.getElementById('register_password').value;
    var password2 = document.getElementById('register_password2').value;
    var email = document.getElementById('register_email').value;
    var fname = document.getElementById('register_fname').value;
    var lname = document.getElementById('register_lname').value;
    var dob_d = parseInt(document.getElementById('register_dob_d').value);
    var dob_m = parseInt(document.getElementById('register_dob_m').value);
    var dob_y = parseInt(document.getElementById('register_dob_y').value);
    var gender = parseInt(document.getElementById('register_gender').value);

    var accept_tos = document.getElementById('register_tos').checked;

    if (username == null || username == "" ||
	password == null || password == "" ||
	password2 == null || password2 == "" ||
	email == null || email == "" ||
	fname == null || fname == "" ||
	lname == null || lname == "" ||
	dob_d == null || dob_d == 0 ||
	dob_m == null || dob_m == 0 ||
	dob_y == null || dob_y == 0) {
        $('.connectionStatus').html("<b class=\"red\">Please fill all registration fields.</b>");
    } else {
        if (username.length < 4 || username.length > 30) {
            $('.connectionStatus').html("<b class=\"red\">Username must be between 4 and 30 characters.</b>");
        } else {
            if (!/^[A-Za-z]{1}[A-Za-z0-9]{4,31}$/.test(username)) {
                $('.connectionStatus').html("<b class=\"red\">Username is invalid (A-z 0-9 only).</b>");
            } else {
                if (password.length < 6 || password.length > 30) {
                    $('.connectionStatus').html("<b class=\"red\">Password must be between 6 and 30 characters.</b>");
                } else {
                    if (password != password2) {
                        $('.connectionStatus').html("<b class=\"red\">Confirmation password does not match.</b>");
                    } else {
                        if (!isEmail(email)) {
                            $('.connectionStatus').html("<b class=\"red\">Email address is invalid.</b>");
                        } else {
                            if (fname.length < 2 || fname.length > 30) {
                                $('.connectionStatus').html("<b class=\"red\">Firstname must be between 2 and 30 characters.</b>");
                            } else {
                                if (lname.length < 2 || lname.length > 30) {
                                    $('.connectionStatus').html("<b class=\"red\">Lastname must be between 2 and 30 characters.</b>");
                                } else {
                                    if (isNaN(dob_d) || dob_d < 1 || dob_d > 31) {
                                        $('.connectionStatus').html("<b class=\"red\">Please select your birth date.</b>");
                                    } else {
                                        if (isNaN(dob_m) || dob_m < 1 || dob_m > 12) {
                                            $('.connectionStatus').html("<b class=\"red\">Please select your birth date.</b>");
                                        } else {
                                            if (isNaN(dob_y) || dob_y < 1940 || dob_y > 2016) {
                                                $('.connectionStatus').html("<b class=\"red\">Please select your birth date.</b>");
                                            } else {
                                                if (date_to_age(dob_y + "-" + dob_m + "-" + dob_d) < 18) {
                                                    $('.connectionStatus').html("<b class=\"red\">Sorry, you must be 18+ years old to use this app.</b>");
                                                } else {
                                                    if (isNaN(gender) || gender < 0 || gender > 1) {
                                                        $('.connectionStatus').html("<b class=\"red\">Please select your gender.</b>");
                                                    } else {
                                                        if (!accept_tos) {
                                                            $('.connectionStatus').html("<b class=\"red\">Sorry, you must read and agree with the Terms and Conditions to use this app.</b>");
                                                        } else {
                                                            $('.connectionStatus').html("<b class=\"white\">Please wait...</b>");
                                                            CSharp.Register(username, password, email, fname, lname, dob_d, dob_m, dob_y, gender);
                                                            document.getElementById('login_username').value = username;
                                                            document.getElementById('login_password').value = "";
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

function Recover() {
    var recoverText = document.getElementById("recover_text").value;
    //document.getElementById("recover_text").value = "";
    CSharp.Recover(recoverText);
}

function SearchProfiles(username, gender, minAge, maxAge, distance, page) {
    if (username == search_username &&
	gender == search_gender &&
	minAge == search_minAge &&
	maxAge == search_maxAge &&
	distance == search_distance &&
	page == search_page) return;//We already did this search!

    search_username = username;
    search_gender = gender;
    search_minAge = minAge;
    search_maxAge = maxAge;
    search_distance = distance;
    search_page = page;

    CSharp.SearchProfiles(search_username, search_gender, search_minAge, search_maxAge, search_distance, search_page);
}

function DoEditProfilePicture() {
    CSharp.DoEditProfilePicture();
}

function DoSaveProfile() {
    var picturePath = document.getElementById('edit_profilePicturePath').value;
    var edit_city = document.getElementById('edit_city').value;
    var edit_job = document.getElementById('edit_job').value;
    var edit_activities = document.getElementById('edit_activities').value;
    var edit_description = document.getElementById('edit_description').value;
    var edit_lookingFor = document.getElementById('edit_lookingFor').value;
    var edit_looingDescription = document.getElementById('edit_looingDescription').value;
    CSharp.SaveEditProfile(picturePath, edit_city, edit_job, edit_activities, edit_description, edit_lookingFor, edit_looingDescription);
}

function DoSaveAccount() {
    $("#editAccountSuccess").hide();
    var email = document.getElementById("edit_email").value;//
    var username = document.getElementById("edit_username").value;//
    var password = document.getElementById("edit_password").value;//
    var cpassword = document.getElementById("edit_cpassword").value;//
    var firstname = document.getElementById("edit_firstname").value;//
    var lastname = document.getElementById("edit_lastname").value;//
    var dob_d = parseInt(document.getElementById("edit_dob_d").value);
    var dob_m = parseInt(document.getElementById("edit_dob_m").value);
    var dob_y = parseInt(document.getElementById("edit_dob_y").value);

    if (username.toLowerCase() != AccountUsername.toLowerCase()) {
        $("#editAccountStatus").text("Username does not match, you are only allowed to change capitalization.");
    }
    else if (!isEmail(email)) {
        $("#editAccountStatus").text("Email address is invalid.");
    }
    else if (password != "" && (password.length < 6 || password.length > 30)) {
        $("#editAccountStatus").text("Password must be between 6 and 30 characters.");
    }
    else if (password != "" && cpassword != password) {
        $("#editAccountStatus").text("Confirmation password does not match.");
    }
    else if (firstname.length < 2 || firstname.length > 30) {
        $("#editAccountStatus").text("Firstname must be between 2 and 30 characters.");
    }
    else if (lastname.length < 2 || lastname.length > 30) {
        $("#editAccountStatus").text("Lastname must be between 2 and 30 characters.");
    }
    else if (isNaN(dob_d) || dob_d < 1 || dob_d > 31) {
        $("#editAccountStatus").text("Please select a valid birth date.");
    }
    else if (isNaN(dob_m) || dob_m < 1 || dob_m > 12) {
        $("#editAccountStatus").text("Please select a valid birth date.");
    }
    else if (isNaN(dob_y) || dob_y < 1940 || dob_y > 2016) {
        $("#editAccountStatus").text("Please select a valid birth date.");
    } else {
        $("#editAccountStatus").text("");
        CSharp.DoSaveAccount(email, username, password, firstname, lastname, dob_d, dob_m, dob_y);
    }
}

function ViewProfile(profileId) {
    ClearCurrentPage();
    $("#followersCount").text("0");
    $("#followingsCount").text("0");
    $("#followersGrid").html("");
    $("#followingsGrid").html("");
    $("#profilePosts").html("");
    $("#btnProfileTimeline").click();
    $(".profileMap").html("");
    if (profileId == AccountId) {
        $("#btnEditProfile").show();
        $("#profileInteraction").hide();
        document.getElementById('edit_profilePicturePath').value = "";
    } else {
        $("#btnEditProfile").hide();
        $("#profileInteraction").show();
    }
    CSharp.LoadProfileData(profileId);
}

function GetProfilePhotos() {
    CSharp.GetProfilePhotos(selectedProfileId, 1);
}

function ChangeProfilePhotosPage(page) {
    CSharp.GetProfilePhotos(selectedProfileId, parseInt(page));
}

function Follow() {
    CSharp.Follow(selectedProfileId);
    $("#btnFollow").hide();
    $("#btnUnfollow").show();
    $('.commentBox_' + selectedProfileId).show();
}

function Unfollow() {
    CSharp.Unfollow(selectedProfileId);
    $("#btnUnfollow").hide();
    $("#btnFollow").show();
    $('.commentBox_' + selectedProfileId).hide();
}

function DeleteNotification(id) {
    $("#notification_" + id).remove();
    CSharp.DeleteNotification(id);
    var count = parseInt($("#notificationsCount").text());
    count--;
    if (count < 0) count = 0;
    $("#notificationsCount").text(count);
    if (count == 0) {
        $("#notificationsCount").removeClass("notification-active");
        $("#notificationsCount").addClass("notification");
    } else {
        $("#notificationsCount").removeClass("notification");
        $("#notificationsCount").addClass("notification-active");
    }
}

function Poke() {
    CSharp.Poke(selectedProfileId);
}

function CreatePost(fromHome) {
    if(fromHome)
    {
        var content = document.getElementById("postTextHome").value;
        document.getElementById("postTextHome").value = "";

        var imgPath = document.getElementById("postImgPathHome").value;
        document.getElementById("postImgPathHome").value = "";
        $('#postImageHome').attr("src", "");
        HidePostImage(fromHome);

        CSharp.CreatePost(content, imgPath);
    }else{
        var content = document.getElementById("postText").value;
        document.getElementById("postText").value = "";

        var imgPath = document.getElementById("postImgPath").value;
        document.getElementById("postImgPath").value = "";
        $('#postImage').attr("src", "");

        HidePostImage(fromHome);

        CSharp.CreatePost(content, imgPath);
    }
}

function DeletePost(postId) {
    CSharp.DeletePost(postId);
}

function CommentPost(postId) {
    var content = document.getElementById("input_comment_" + postId).value;
    document.getElementById("input_comment_" + postId).value = "";

    var imgPath = "";

    CSharp.CommentPost(postId, content, imgPath);
}

function CommentPostText(postId) {
    var content = document.getElementById("profile_input_comment_" + postId).value;
    document.getElementById("profile_input_comment_" + postId).value = "";

    var imgPath = "";

    CSharp.CommentPost(postId, content, imgPath);
}

function DeleteComment(commentId) {
    CSharp.DeleteComment(commentId);
}

function DoSelectPostPicture(fromHome) {
    CSharp.DoSelectPostPicture(fromHome);
}

function DoSelectMessagePicture() {
    CSharp.DoSelectMessagePicture();
}
function GetBlockedUsers()
{
    CSharp.GetBlockedUsers();
}
function BlockUser(userId)
{
    CSharp.BlockUser(userId);
}
function UnBlockUser(userId)
{
    CSharp.UnBlockUser(userId);
    $("#block_" + userId).remove();
}

function CallUser(userId)
{
    CSharp.SendCall(userId);
}
function AcceptRequest(userId, type)
{
    CSharp.SetRequestResult(userId, 1, type);
    $("#request_" + userId + "_" + type + "_1").remove();
    var count = parseInt($("#requestsCount").text());
    if (count == 0) return;
    count--;
    requestsCount--;
    if (count < 0) count = 0;
    $("#requestsCount").text(count);
    if (count == 0) {
        $("#requestsCount").addClass("notification");
        $("#requestsCount").removeClass("notification-active");
        $(".requests-feed").html("<center id=\"no-request\"><b>You have no requests.</b></center>");
    }
}
function DeclineRequest(userId, type)
{
    CSharp.SetRequestResult(userId, 0, type);
    $("#request_" + userId + "_" + type + "_1").remove();
    var count = parseInt($("#requestsCount").text());
    if (count == 0) return;
    count--;
    requestsCount--;
    if (count < 0) count = 0;
    $("#requestsCount").text(count);
    if (count <= 0) {
        $("#requestsCount").addClass("notification");
        $("#requestsCount").removeClass("notification-active");
        $(".requests-feed").html("<center id=\"no-request\"><b>You have no requests.</b></center>");
    }
}
function EndCall(callId)
{
    CSharp.EndCall(callId);
    OnCallEnded(callId);
}
function SetCallResult(callId, result)
{
    CSharp.SetCallResult(callId, result);
}
function GetMapPosition()
{
    CSharp.GetMapPosition(selectedProfileId);
}

function LoadMyGroups()
{
    CSharp.GetMyGroups();
}
function LoadMyGroupsInvitation()
{

}

//#endregion
//#region Network Receivers
//#region Authentication
function isVersionValid(version) {
    if (!version) {
        $('.connectionStatus').text("Application outdated, please update.");
    } else if (!shouldNotShotBootBtns) {
        $('.bootBtns').fadeIn();
    }
}

function loginInfos(username, password, autoLogin) {
    document.getElementById('login_username').value = username;
    document.getElementById('login_password').value = password;
    var checked = (username != "");
    document.getElementById('login_remember').checked = checked;
    if (checked && autoLogin == 1 && connectedToServer) {
        $('.connectionStatus').text('Please wait...');
        shouldNotShotBootBtns = true;
        changeBootLevel(1);
        if (typeof CSharp !== 'undefined')
            CSharp.Login(username, password, checked);
    }
}

function loginResult(result) {
    switch (result) {
        case 0:
            $('.boot').hide();
			$('#mainContainer').fadeIn();
		break;
        case 1:
            $('.connectionStatus').text("Application outdated, please update.");
            break;
        case 2:
            $('.connectionStatus').text("Account informations are incorrect.");
            break;
        case 3:
            $('.connectionStatus').text("Account is suspended.");
            break;
        case 4:
            $('.connectionStatus').text("Account is banned.");
            break;
    }
}

function OnGetAccountInfos(accountId, accountName, accountAvatar, accountGender, accountMembership) {
    AccountId = accountId;
    AccountName = accountName;
    AccountGender = accountGender;
    AccountAvatar = accountAvatar == "" ? ((AccountGender == 0 || AccountGender == "0") ? "img/avatar-male.png" : "img/avatar-female.png") : (webServerAddress + "?avatar=" + accountAvatar);
    AccountMembership = accountMembership;
    UpdateUserAvatarName();
}

function registerResult(result) {
    switch (result) {
        case 0:
            changeBootLevel(1);
            $('.connectionStatus').html("Account successfully created.");
            break;
        case 1:
            $('.connectionStatus').html("<b class=\"red\">Username must be between 4 and 30 characters.</b>");
            break;
        case 2:
            $('.connectionStatus').html("<b class=\"red\">Username is invalid (A-z 0-9 only).</b>");
            break;
        case 3:
            $('.connectionStatus').html("<b class=\"red\">Password must be between 6 and 30 characters.</b>");
            break;
        case 4:
            $('.connectionStatus').html("<b class=\"red\">Email address is invalid.</b>");
            break;
        case 5:
            $('.connectionStatus').html("<b class=\"red\">Firstname must be between 2 and 30 characters.</b>");
            break;
        case 6:
            $('.connectionStatus').html("<b class=\"red\">Lastname must be between 2 and 30 characters.</b>");
            break;
        case 7:
            $('.connectionStatus').html("<b class=\"red\">Please select your birth date.</b>");
            break;
        case 8:
            $('.connectionStatus').html("<b class=\"red\">Sorry, you must be 18+ years old to use this app.</b>");
            break;
        case 9:
            $('.connectionStatus').html("<b class=\"red\">Sorry, this username is already being used.</b>");
            break;
        case 10:
            $('.connectionStatus').html("<b class=\"red\">Sorry, this email is already being used.</b>");
            break;
    }
}

function OnGetRecoverResult(result) {
    if (result == 0) {
        $('.connectionStatus').text("Account password has been reset, please check your email.");
    } else {
        $('.connectionStatus').text("Cannot find any account matching the information given.");
    }
}
//#endregion
//#region Search
function AddProfileToSearchResults(id, username, picture, age, gender) {
    var avatar = picture == "" ? (gender == 0 ? "img/avatar-male.png" : "img/avatar-female.png") : webServerAddress + "?avatar=" + picture;
    var gender = gender == 0 ? "Male" : "Female";
    var html = '<li class="search-item"><table cellpadding="4"><tbody><tr><td><img onclick="ViewProfile(\'' + id + '\');" src="' + avatar + '" width="60" class="img-circle small-circle-img"></td><td width="100%">' + username + ' - ' + gender + ' - ' + age + ' years old.</td><td width="10px"><a href="#" onclick="ViewProfile(\'' + id + '\');" class="btn-default">View</a></td></tr></tbody></table></li>';
    $(".search-results").append(html);
}
//#endregion
//#region Edit Profile
function DisplayEditProfilePicture(path, dataPath) {
    $('#edit_profilePictureImg').attr("src", path);
    $('#edit_profilePicturePath').attr("value", dataPath);
    profilePictureChanged = true;
}

function OnGetEditProfileInfo(picture, city, job, activities, description, lookingFor, lookingForDescription, gender) {
    var avatar = picture == "" ? (gender == 0 ? "img/avatar-male.png" : "img/avatar-female.png") : webServerAddress + "?avatar=" + picture;
    //$('#edit_profilePictureImg').attr("src", "");
    //$('#edit_profilePictureImg').attr("src", avatar);
    $('#edit_profilePictureImg').attr("src", avatar);
    document.getElementById('edit_city').value = city;
    document.getElementById('edit_job').value = job;

    $('#edit_activities').tagsinput('removeAll');
    var a = activities.split(",");
    for (i = 0; i < a.length; i++) {
        $('#edit_activities').tagsinput('add', a[i]);
    }
    //document.getElementById('edit_activities').value = activities;
    document.getElementById('edit_description').value = description;
    document.getElementById('edit_lookingFor').value = lookingFor;
    document.getElementById('edit_looingDescription').value = lookingForDescription;
}
//#endregion
//#region View Profile
function OnGetEditAccount(email, username, firstname, lastname, dob_d, dob_m, dob_y) {
    AccountUsername = username;
    $('#edit_email').attr("value", email);
    $('#edit_username').attr("value", username);
    $('#edit_firstname').attr("value", firstname);
    $('#edit_lastname').attr("value", lastname);
    $('#edit_dob_d').val(dob_d);
    $('#edit_dob_m').val(dob_m);
    $('#edit_dob_y').val(dob_y);
}
function OnGetEditAccountResult(result) {
    switch (result) {
        case 0:
            $("#editAccountStatus").text("");
            $("#editAccountSuccess").show();
            break;
        case 1:
            $("#editAccountStatus").text("Email address is invalid.");
            break;
        case 2:
            $("#editAccountStatus").text("Username does not match, you are only allowed to change capitalization.");
            break;
        case 3:
            $("#editAccountStatus").text("Firstname must be between 2 and 30 characters.");
            break;
        case 4:
            $("#editAccountStatus").text("Lastname must be between 2 and 30 characters.");
            break;
        case 5:
            $("#editAccountStatus").text("Please select a valid birth date.");
            break;
    }
}
function OnGetProfileData(id, username, picture, isBlocked, photosCount, city, job, activities, description, lookingFor, lookingForDescription, gender, age, followed) {
    selectedProfileId = id;
    var avatar = picture == "" ? ((gender == 0 || gender == "0") ? "img/avatar-male.png" : "img/avatar-female.png") : (webServerAddress + "?avatar=" + picture);
    if (selectedProfileId == AccountId)
    {
        $("#profileCreatePostBox").show();
        AccountAvatar = avatar;
    } else {
        $("#profileCreatePostBox").hide();
    }

    $('#profileUsername').text(username);
    //$('#profilePicture').attr("src", "");
    $('#profilePicture').attr("src", avatar);
    $(".avatar_" + id).attr("src", avatar);

    if (city == "") $("#profileCityContainer").hide(); else $("#profileCityContainer").show();
    $('#profileCity').text(city);
    $('#profileAboutCity').text(city);

    if (job == "") $("#profileJobContainer").hide(); else $("#profileJobContainer").show();
    $('#profileJob').text(job);
    $('#profileAboutJob').text(job);

    if (description == "") $("#profileDescriptionContainer").hide(); else $("#profileDescriptionContainer").show();
    $('#profileDescription').text(description);
    $('#profileAboutDescription').text(description);

    var _gender = (gender == 0 || gender == "0") ? "Male" : "Female";
    $('#profileGender').text(_gender);
    $('#profileAboutGender').text(_gender);

    $('#profileAge').text(age);
    $('#profileAboutAge').text(age);

    $('#profileAboutActivities').text(activities);

    switch (parseInt(lookingFor)) {
        case 0:
            $('#profileAboutLookingFor').text("Anything");
            break;
        case 1:
            $('#profileAboutLookingFor').text("Curiosity");
            break;
        case 2:
            $('#profileAboutLookingFor').text("Fun");
            break;
        case 3:
            $('#profileAboutLookingFor').text("Friends");
            break;
        case 4:
            $('#profileAboutLookingFor').text("Relationship");
            break;
    }

    $('#profileAboutLookingForDescription').text(lookingForDescription);

    $('.profileContainer').show();

    $('#photosCount').text(photosCount);

    if (photosCount < 1)
        $('.photos_pagination').hide();
    else {
        $('.photos_pagination').show();
        $('.photos_pagination').bootpag({
            total: Math.ceil(photosCount / 50),
            page: 1,
            maxVisible: 5,
            leaps: true,
            firstLastUse: true,
            first: '←',
            last: '→',
            wrapClass: 'pagination',
            activeClass: 'active',
            disabledClass: 'disabled',
            nextClass: 'next',
            prevClass: 'prev',
            lastClass: 'last',
            firstClass: 'first'
        }).on("page", function (event, num) {
            ChangeProfilePhotosPage(num);
        });
    }

    if (followed == 1) {
        selectedProfileFollowed = true;
        $("#btnFollow").hide();
        $("#btnUnfollow").show();
    } else {
        selectedProfileFollowed = false;
        $("#btnUnfollow").hide();
        $("#btnFollow").show();
    }
    if(isBlocked)
    {
        $("#btnBlock").hide();
        $("#btnUnBlock").show();
    } else {
        $("#btnUnBlock").hide();
        $("#btnBlock").show();
    }
}
function OnGetProfilePhoto(id, attachmentId, message) {
    var attachment = attachmentId == "" ? "" : (webServerAddress + "?attachment=" + attachmentId);
    if (attachment == "") return;
    if (document.getElementById("photo_" + id)) return;

    var photoElement = '<div id="photo_' + id + '" class="mix col-sm-3 margin30"><div class="item-img-wrap "><img src="' + attachment + '" class="img-responsive"><div class="item-img-overlay"><a href="#" class="show-image"><span></span></a></div></div></div>';
    $(photoElement).prependTo("#photoGrid");
}
//#endregion
//#region Messages
function OnGetConversation(conversationId, otherId, otherName, otherAvatar, otherGender, myAvatar, myGender, lastMessage, lastMessageFromMe, lastMessageSeen) {
    //Check if it exist first!
    if ($("#conversation_" + conversationId).length == 0) {
        //doesnt exist
        var seen = lastMessageSeen == 1;
        var fromMe = lastMessageFromMe == 1;
        var avatar = otherAvatar == "" ? ((otherGender == 0 || otherGender == "0") ? "img/avatar-male.png" : "img/avatar-female.png") : (webServerAddress + "?avatar=" + otherAvatar);
        var name = (!fromMe && !seen) ? "<b>" + otherName + "</b>" : otherName;
        var message = (!fromMe && !seen) ? "<b>" + lastMessage + "</b>" : lastMessage

        $(".avatar_" + otherId).attr("src", avatar);

        var myavatar = myAvatar == "" ? ((myGender == 0 || myGender == "0") ? "img/avatar-male.png" : "img/avatar-female.png") : (webServerAddress + "?avatar=" + myAvatar);

        var conversationElement = '<div class="lv-item media" id="conversation_' + conversationId + '" data-newmsgs="" data-newmsgscount="0" data-otherid="' + otherId + '" data-othername="' + name + '" data-otheravatar="' + avatar + '" data-myavatar="' + myavatar + '" onclick="ShowConversation(\'' + conversationId + '\');"><div class="lv-avatar pull-left"><img class="avatar_' + otherId + '" alt="" src="' + avatar + '" id="avatar_' + otherId + '"></div><div class="media-body"><div class="lv-title" id="conversation_' + conversationId + '_name">' + name + '</div><div class="lv-small" id="conversation_' + conversationId + '_msg">' + message + '</div></div></div>';
        $(conversationElement).prependTo("#conversations");

        //Messages container
        var messagesContainer = '<div id="conversation_messages_' + conversationId + '" style="display:none;"></div>';
        $(messagesContainer).prependTo("#conversations_messages");
    } else {
        //Update something !?
    }
}

function OnGetMessage(conversationId, messageId, fromId, attachmentId, message, dateAt, seen, seenAt) {
    //If it is not seen and it has not been sent by us, update the conversation side menu to bold text!
    var otherId = $("#conversation_" + conversationId).attr("data-otherid");
    var fromMe = fromId != otherId;

    var otherAvatar = $("#conversation_" + conversationId).attr("data-otheravatar");

    var myAvatar = $("#conversation_" + conversationId).attr("data-myavatar");

    var avatar = fromMe ? myAvatar : otherAvatar;
    var alligment = fromMe ? ' right' : '';
    var avatarAlligment = fromMe ? 'right' : 'left';
    var seenStyle = seen ? "" : ' style="display:none;"';

    var attachment = attachmentId == "" ? "" : (webServerAddress + "?attachment=" + attachmentId);
    var attachmentElement = attachment == "" ? '' : '<a href="' + attachment + '" data-lightbox="' + messageId + '"><img src="' + attachment + '" width="100%" /></a>';

    var messageElement = '<div class="lv-item media' + alligment + '" id="message_' + messageId + '"><div class="lv-avatar pull-' + avatarAlligment + '"><a href="javascript: ViewProfile(\'' + fromId + '\');"><img class="avatar_' + fromId + ' img-circle small-circle-img" alt="" src="' + avatar + '"></a></div><div class="media-body"><div class="ms-item"><span class="glyphicon glyphicon-triangle-left"style="color:#000000;"></span> <span id="msg_content_' + messageId + '"></span>' + attachmentElement + '</div><small class="ms-date"><span class="fa fa-clock-o"></span>&nbsp; ' + dateAt + '</small><small class="ms-date" id="msg_seen_' + messageId + '"' + seenStyle + '>Seen ' + seenAt + ' <span class="fa fa-clock-o"></span></small></div></div>';

    $("#conversation_messages_" + conversationId).append(messageElement);
    $("#msg_content_" + messageId).text(message.replace("<br>", "\n"));

    if (!seen && (selectedConversationId != conversationId || currentPage != "messages")) {
        if (!fromMe) {
            var newMsgs = $("#conversation_" + conversationId).attr("data-newmsgs");

            if (newMsgs == "") {
                newMsgs = messageId;
            } else {
                newMsgs = newMsgs + "," + messageId;
            }
            $("#conversation_" + conversationId).attr("data-newmsgs", newMsgs);

            var msgCount = parseInt($("#conversation_" + conversationId).attr("data-newmsgscount"));
            msgCount++;
            $("#conversation_" + conversationId).attr("data-newmsgscount", msgCount);

            newMessageCount++;
            UpdateNewMessageCount();
            $("#conversation_" + conversationId + "_msg").html("<b>" + message + "</b>");
        }
    } else if (!fromMe && selectedConversationId == conversationId && currentPage == "messages") {
        OnSeenMessage(conversationId, messageId);
        $("#conversation_" + conversationId + "_msg").html(message);
    }

    var objDiv = document.getElementById("messagesScrollArea");
    objDiv.scrollTop = objDiv.scrollHeight;
}

function OnSeenMessage(conversationId, messageId) {
    var msgCount = parseInt($("#conversation_" + conversationId).attr("data-newmsgscount"));
    newMessageCount -= msgCount;
    $("#conversation_" + conversationId).attr("data-newmsgscount", 0);
    UpdateNewMessageCount();

    var msg = $("#conversation_" + conversationId + "_msg").html();
    msg.replace("<b>", "");
    msg.replace("</b>", "");
    $("#conversation_" + conversationId + "_msg").html(msg);

    CSharp.SetMessageHasSeen(messageId + "");
}

function OnGetMessageSeen(messageId, seenAt) {
    $("#msg_seen_" + messageId).html("Seen " + seenAt + ' <span class="fa fa-clock-o"></span>');
    $("#msg_seen_" + messageId).show();
}

function ShowConversation(conversationId) {
    if (selectedConversationId != "") {
        $("#conversation_" + selectedConversationId).toggleClass('active');
        $("#conversation_messages_" + selectedConversationId).hide();
    }
    selectedConversationId = conversationId;
    $("#conversation_" + conversationId).toggleClass('active');
    $("#conversation_messages_" + conversationId).show();

    sendToId = $("#conversation_" + conversationId).attr("data-otherid");
    otherUserAvatar = $("#conversation_" + conversationId).attr("data-otheravatar");
    otherUserName = $("#conversation_" + conversationId).attr("data-othername");

    $("#conversation_message_name").text(otherUserName);
    $('#conversation_message_avatar').attr("src", otherUserAvatar);

    var newMsgs = $("#conversation_" + conversationId).attr("data-newmsgs").split(',');
    for (var i = 0; i < newMsgs.length; i++) {
        var msgId = newMsgs[i];
        if (msgId != "") OnSeenMessage(conversationId, msgId);
    }
    $("#conversation_" + conversationId).attr("data-newmsgs", "");
}

function StartConversation() {
    CSharp.StartConversation(selectedProfileId);
}

function SendMessage() {
    if (selectedConversationId == "" || selectedConversationId == null) return;

    var otherId = $("#conversation_" + selectedConversationId).attr("data-otherid");
    if (otherId == "") return;

    var messageInput = document.getElementById('messageInput').value;
    if (messageInput == "") return;

    document.getElementById('messageInput').value = "";

    var imgPath = document.getElementById("messageImgInput").value;
    document.getElementById("messageImgInput").value = "";

    HideMessageImage();
    CSharp.SendMessage(otherId, messageInput, imgPath);
}

function UpdateNewMessageCount() {
    if (newMessageCount < 0) newMessageCount = 0;
    if (newMessageCount == 0) {
        $("#notification_menu_messages").removeClass("notification-active");
        $("#notification_menu_messages").addClass("notification");
        $("#NewMessagesCounter").html("0 New Messages");
    } else {
        $("#notification_menu_messages").removeClass("notification");
        $("#notification_menu_messages").addClass("notification-active");
        $("#NewMessagesCounter").html("<b>" + newMessageCount + "</b> New Messages");
    }
    $("#notification_menu_messages").text(newMessageCount);
}

function DisplaySelectMessagePicture(path, dataPath) {
    $('#messageImage').attr("src", path);
    $('#messageImgInput').attr("value", dataPath);
    if (path != "")
        $('#messagePictureBox').show();
    else
        $('#messagePictureBox').hide();
    //profilePictureChanged = true;
}
//#endregion
//#region Follows
function OnGetFollower(id, userId, userName, userAvatar, gender) {
    var avatar = userAvatar == "" ? ((gender == 0 || gender == "0") ? "img/avatar-male.png" : "img/avatar-female.png") : (webServerAddress + "?avatar=" + userAvatar);
    var followerElement = '<div class="mix col-sm-3"><div class="item-img-wrap"><img style="max-height:300px;" src="' + avatar + '" class="img-responsive" alt="' + userName + '"><span style="position: absolute;width: 100%;left: 0px;bottom: 10px;font-weight: bold;font-size: 25px;text-shadow: -1px -1px 0 #FFF, 1px -1px 0 #FFF, -1px 1px 0 #FFF, 1px 1px 0 #FFF;">' + userName + '</span><div class="item-img-overlay"><a href="#" onclick="ViewProfile(\'' + userId + '\');"><span></span></a></div></div></div>';

    $("#followersGrid").append(followerElement);
}

function OnGetFollowersCount(count) {
    $("#followersCount").text(count);
}

function OnGetFollowing(id, userId, userName, userAvatar, gender) {
    var avatar = userAvatar == "" ? ((gender == 0 || gender == "0") ? "img/avatar-male.png" : "img/avatar-female.png") : (webServerAddress + "?avatar=" + userAvatar);
    var followerElement = '<div class="mix col-sm-3"><div class="item-img-wrap"><img style="max-height:300px;" src="' + avatar + '" class="img-responsive" alt="' + userName + '"><span style="position: absolute;width: 100%;left: 0px;bottom: 10px;font-weight: bold;font-size: 25px;text-shadow: -1px -1px 0 #FFF, 1px -1px 0 #FFF, -1px 1px 0 #FFF, 1px 1px 0 #FFF;">' + userName + '</span><div class="item-img-overlay"><a href="#" onclick="ViewProfile(\'' + userId + '\');"><span></span></a></div></div></div>';

    $("#followingsGrid").append(followerElement);
}

function OnGetFollowingsCount(count) {
    $("#followingsCount").text(count);
}
//#endregion

//#region Notifications
function OnGetNotification(id, fromId, fromName, fromAvatar, fromGender, type, when) {
    var count = parseInt($("#notificationsCount").text());
    count++;
    $("#notificationsCount").text(count);
    $("#notificationsCount").removeClass("notification");
    $("#notificationsCount").addClass("notification-active");
    //
    var notificationText = "";
    switch (type) {
        case 0://View
            notificationText = '<a href="#" onclick="ViewProfile(\'' + fromId + '\');">' + fromName + "</a> viewed your profile. - " + when;
        break;
        case 1://Poke
            notificationText = '<a href="#" onclick="ViewProfile(\'' + fromId + '\');">' + fromName + "</a> poked you. - " + when;
        break;
        case 2://Follow
            notificationText = '<a href="#" onclick="ViewProfile(\'' + fromId + '\');">' + fromName + "</a> followed your profile. - " + when;
        break;
        case 3://Unfollow
            notificationText = '<a href="#" onclick="ViewProfile(\'' + fromId + '\');">' + fromName + "</a> unfollowed your profile. - " + when;
        break;
        case 4:
        break;
        case 5://Accepted Call Request
            notificationText = '<a href="#" onclick="ViewProfile(\'' + fromId + '\');">' + fromName + "</a> accepted your call request. - " + when;
        break;
        case 6://Declined Call Request
            notificationText = '<a href="#" onclick="ViewProfile(\'' + fromId + '\');">' + fromName + "</a> declined your call request. - " + when;
        break;
        case 7://Missed Call
            notificationText = '<a href="#" onclick="ViewProfile(\'' + fromId + '\');">' + fromName + "</a> called you but you missed. - " + when;
        break;
        case 9://Accepted Position Request
            notificationText = '<a href="#" onclick="ViewProfile(\'' + fromId + '\');">' + fromName + "</a> accepted your position request. - " + when;
        break;
        case 10://Declined Position Request
            notificationText = '<a href="#" onclick="ViewProfile(\'' + fromId + '\');">' + fromName + "</a> declined your position request. - " + when;
            break;
        case 11://Accepted Message Request
            notificationText = '<a href="#" onclick="ViewProfile(\'' + fromId + '\');">' + fromName + "</a> accepted your message request. - " + when;
        break;
        case 12://Declined Message Request
            notificationText = '<a href="#" onclick="ViewProfile(\'' + fromId + '\');">' + fromName + "</a> declined your message request. - " + when;
        break;
    }
    var avatar = fromAvatar == "" ? ((fromGender == 0 || fromGender == "0") ? "img/avatar-male.png" : "img/avatar-female.png") : (webServerAddress + "?avatar=" + fromAvatar);
    //
    var notificationElement = '<li class="news-item" id="notification_' + id + '"><table cellpadding="4"><tr><td><img src="' + avatar + '" width="60" class="img-circle small-circle-img" onclick="ViewProfile(\'' + fromId + '\');" /></td><td width="100%">' + notificationText + '</td><td width="10px"><a href="#" class="btn-default" onclick="DeleteNotification(\'' + id + '\')">X</a></td></tr></table></li>';
    $(".notifications-feed").append(notificationElement);
}

//#endregion

//#region Posts

function OnGetPost(postId, userId, userName, userAvatar, gender, content, attachmentId, dateAt) {
    if (userId == selectedProfileId) {
        OnGetProfilePost(postId, userId, userName, userAvatar, gender, content, attachmentId, dateAt);
    }
    if (document.getElementById("post_" + postId)) return;

    var avatar = userAvatar == "" ? ((gender == 0 || gender == "0") ? "img/avatar-male.png" : "img/avatar-female.png") : (webServerAddress + "?avatar=" + userAvatar);
    var attachment = attachmentId == "" ? null : (webServerAddress + "?attachment=" + attachmentId);

    $(".avatar_" + userId).attr("src", avatar);

    var attachmentElement = attachment == null ? '' : '<a href="' + attachment + '" data-lightbox="' + postId + '"><img src="' + attachment + '" width="100%" /></a>';
    var deleteButton = userId == AccountId ? '<a href="#" onclick="DeletePost(\'' + postId + '\'); return false;">X</a>' : "";
    //Add to view!
    var postElement = '<div class="col-md-12" id="post_' + postId + '"><div class="listview lv-message"><div class="lv-body"><div class="lv-item media"><div class="lv-avatar pull-left"><img class="avatar_' + userId + '" onclick="ViewProfile(\'' + userId + '\');" alt="" src="' + avatar + '"></div><div class="media-body"><b onclick="ViewProfile(\'' + userId + '\');">' + userName + '</b> <b style="float:right;">' + deleteButton + '</b><br><div class="ms-item" style="max-width: 100%;width:100%; border-radius:5px;"><small class="ms-date"><span class="fa fa-clock-o"></span>&nbsp; ' + dateAt + '</small><span id="post_content_' + postId + '"></span>' + attachmentElement + '<div id="post_' + postId + '_comments" class="box-footer box-comments" style="display: block;"></div><div class="box-footer" style="display: block;"><form onsubmit="event.preventDefault();CommentPost(\'' + postId + '\'); return false;"><img class="avatar_' + userId + ' img-responsive img-circle img-sm" src="' + AccountAvatar + '"><div class="img-push"><input id="input_comment_' + postId + '" autocomplete="off" type="text" class="form-control input-sm" placeholder="Press enter to post comment"></div></form></div></div></div></div></div><div class="clearfix"></div></div></div>';
    $(postElement).prependTo("#homePosts");
    $("#post_content_" + postId).text(content.replace("<br>", "\n"));
}

function OnGetPostDeleted(postId) {
    $("#post_" + postId).remove();
    $("#profile_post_" + postId).remove();
}

function OnGetProfilePost(postId, userId, userName, userAvatar, gender, content, attachmentId, dateAt) {
    if (document.getElementById("profile_post_" + postId)) return;

    if (attachmentId != null && attachmentId != "") {
        var photosCount = parseInt($('#photosCount').text());
        photosCount++;
        $('#photosCount').text(photosCount);
    }
    var avatar = userAvatar == "" ? ((gender == 0 || gender == "0") ? "img/avatar-male.png" : "img/avatar-female.png") : (webServerAddress + "?avatar=" + userAvatar);
    var attachment = attachmentId == "" ? "" : (webServerAddress + "?attachment=" + attachmentId);

    var attachmentElement = attachment == "" ? "" : '<a href="' + attachment + '" data-lightbox="' + postId + '"><img src="' + attachment + '" width="100%" /></a>';
    var deleteButton = userId == AccountId ? '<a href="#" onclick="DeletePost(\'' + postId + '\'); return false;">X</a>' : "";
    //Add to view!
    if (selectedProfileFollowed || userId == AccountId)
    {
        var postElement = '<div class="col-md-12" id="profile_post_' + postId + '"><div class="listview lv-message"><div class="lv-body"><div class="lv-item media"><div class="lv-avatar pull-left"><img class="avatar_' + userId + '" onclick="ViewProfile(\'' + userId + '\');" alt="" src="' + avatar + '"></div><div class="media-body"><b onclick="ViewProfile(\'' + userId + '\');">' + userName + '</b> <b style="float:right;">' + deleteButton + '</b><br><div class="ms-item" style="max-width: 100%;width:100%; border-radius:5px;"><small class="ms-date"><span class="fa fa-clock-o"></span>&nbsp; ' + dateAt + '</small><span id="post_content_p_' + postId + '"></span>' + attachmentElement + '<div id="profile_post_' + postId + '_comments" class="box-footer box-comments" style="display: block;"></div><div class="box-footer commentBox_' + userId + '" style="display: block;"><form onsubmit="event.preventDefault();CommentPostText(\'' + postId + '\'); return false;"><img class="avatar_' + userId + ' img-responsive img-circle img-sm" src="' + AccountAvatar + '"><div class="img-push"><input id="profile_input_comment_' + postId + '" autocomplete="off" type="text" class="form-control input-sm" placeholder="Press enter to post comment"></div></form></div></div></div></div></div><div class="clearfix"></div></div></div>';
    } else {
        var postElement = '<div class="col-md-12" id="profile_post_' + postId + '"><div class="listview lv-message"><div class="lv-body"><div class="lv-item media"><div class="lv-avatar pull-left"><img class="avatar_' + userId + '" onclick="ViewProfile(\'' + userId + '\');" alt="" src="' + avatar + '"></div><div class="media-body"><b onclick="ViewProfile(\'' + userId + '\');">' + userName + '</b> <b style="float:right;">' + deleteButton + '</b><br><div class="ms-item" style="max-width: 100%;width:100%; border-radius:5px;"><small class="ms-date"><span class="fa fa-clock-o"></span>&nbsp; ' + dateAt + '</small><span id="post_content_p_' + postId + '"></span>' + attachmentElement + '<div id="profile_post_' + postId + '_comments" class="box-footer box-comments" style="display: block;"></div><div class="box-footer commentBox_' + userId + '" style="display: block;display:none;"><form onsubmit="event.preventDefault();CommentPostText(\'' + postId + '\'); return false;"><img class="img-responsive img-circle img-sm" src="' + AccountAvatar + '"><div class="img-push"><input id="profile_input_comment_' + postId + '" autocomplete="off" type="text" class="form-control input-sm" placeholder="Press enter to post comment"></div></form></div></div></div></div></div><div class="clearfix"></div></div></div>';
    }

    $(postElement).prependTo("#profilePosts");
    $("#post_content_p_" + postId).text(content.replace("<br>", "\n"));
}

function OnGetComment(commentId, postId, postUserId, userId, userName, userAvatar, gender, content, attachmentId, dateAt) {
    var avatar = userAvatar == "" ? ((gender == 0 || gender == "0") ? "img/avatar-male.png" : "img/avatar-female.png") : (webServerAddress + "?avatar=" + userAvatar);
    var attachment = attachmentId == "" ? "" : (webServerAddress + "?attachment=" + attachmentId);

    var attachmentElement = attachment == "" ? '' : '<a href="' + attachment + '" data-lightbox=""><img src="' + attachment + '" width="100%" /></a>';
    var deleteButton = (userId == AccountId || postUserId == AccountId) ? '<a href="#" onclick="DeleteComment(\'' + commentId + '\'); return false;">X</a>' : "";

    if (!document.getElementById("comment_" + commentId)) {

        $(".avatar_" + userId).attr("src", avatar);

        var commentElement = '<div class="box-comment" id="comment_' + commentId + '"><img onclick="ViewProfile(\'' + userId + '\');" class="avatar_' + userId + ' img-circle img-sm" src="' + avatar + '" alt=""><div class="comment-text"><span class="username"><b onclick="ViewProfile(\'' + userId + '\');">' + userName + '</b><b style="float:right;">' + deleteButton + '</b><br><span class="text-muted">' + dateAt + '</span><br><span id="comment_content_' + commentId + '"></span>' + attachmentElement + '</div></div>';
        $("#post_" + postId + "_comments").append(commentElement);
        $("#comment_content_" + commentId).text(content);
    }

    if (!document.getElementById("profile_comment_" + commentId)) {
        var commentElement = '<div class="box-comment" id="profile_comment_' + commentId + '"><img onclick="ViewProfile(\'' + userId + '\');" class="avatar_' + userId + ' img-circle img-sm" src="' + avatar + '" alt=""><div class="comment-text"><span class="username"><b onclick="ViewProfile(\'' + userId + '\');">' + userName + '</b><b style="float:right;">' + deleteButton + '</b><br><span class="text-muted">' + dateAt + '</span></span><br><span id="comment_content_p_' + commentId + '"></span>' + attachmentElement + '</div></div>';
        $("#profile_post_" + postId + "_comments").append(commentElement);
        $("#comment_content_p_" + commentId).text(content);
    }
}

function OnGetCommentDeleted(commentId) {
    $("#comment_" + commentId).remove();
    $("#profile_comment_" + commentId).remove();
}

function DisplaySelectPostPicture(path, dataPath, fromHome) {
    if(fromHome == 1)
    {
        $('#postImageHome').attr("src", path);
        $('#postImgPathHome').attr("value", dataPath);
        if (path != "")
            $('#postPictureBoxHome').show();
        else
            $('#postPictureBoxHome').hide();
    }else{
        $('#postImage').attr("src", path);
        $('#postImgPath').attr("value", dataPath);
        if (path != "")
            $('#postPictureBox').show();
        else
            $('#postPictureBox').hide();
    }
    //profilePictureChanged = true;
}

//#endregion

//#region Payments
function OnGetTransaction(id, tokenId, amount, currency, dateAt, expireAt) {
    if (!document.getElementById("transaction_" + id)) {
        var plan = "";
        switch (parseInt(amount)) {
            case 99:
                plan = "1 Day";
                break;
            case 299:
                plan = "7 Days";
                break;
            case 999:
                plan = "1 Month";
                break;
        }
        var transactionElement = '<tr id="transaction_' + id + '"><td>' + id + '</td><td>' + plan + '</td><td>' + (parseInt(amount) / 100) + '</td><td>' + currency + '</td><td>' + dateAt + '</td><td>' + expireAt + '</td></tr>';
        $("#transactions_table").append(transactionElement);
    }
}
function OnGetPaymentResult(result) {
    AccountMembership = result;
    if (result) {
        changePage('payments');
    } else {
        //Show Payment was declined.
        var $form = $('#payment-form');
        $form.find('.payment-errors').text("Payment was declined.");
        $form.find('.submit').prop('disabled', false); // Re-enable submission
    }
}
function OnGetVerifyMembership(result) {
    AccountMembership = result;
    if (result) {
        $("#paymentFormContainer").hide();
    } else {
        $("#paymentFormContainer").show();
    }
}
function OnPremiumRequired() {
    $(".premiumRequired").show();
}
//#endregion
//#region Blocks
function OnGetBlockedUser(id, username, picture, gender, dateAt)
{
    if (document.getElementById("block_" + id)) return;
    var avatar = picture == "" ? ((gender == 0 || gender == "0") ? "img/avatar-male.png" : "img/avatar-female.png") : webServerAddress + "?avatar=" + picture;

    var html = '<li class="blocked-user" id="block_' + id +'"><table cellpadding="4"><tbody><tr><td><img onclick="ViewProfile(\'' + id + '\');" src="' + avatar + '" width="60" class="img-circle small-circle-img"></td><td width="100%">' + username + ' - ' + dateAt + '</td><td width="10px"><a href="#" onclick="UnBlockUser(\'' + id + '\');" class="btn-default">UnBlock</a></td></tr></tbody></table></li>';
    $(".blocked-users").append(html);
}
//#endregion
//#region Calls
function OnStartCalling(id, userId, userName, userAvatar, userGender)
{
    numberOfCalls++;
    var avatar = userAvatar == "" ? ((userGender == 0 || userGender == "0") ? "img/avatar-male.png" : "img/avatar-female.png") : (webServerAddress + "?avatar=" + userAvatar);
    
    var callElement = '<div class="content" id="call_' + id + '">' +
            '<div id="otherInfos" class="otherInfos">' +
                '<center>' +
                    '<img src="' + avatar + '" id="callOtherAvatar" /><br /><br />' +
                    '<span id="callOtherName">' + userName + '</span><br />' +
                    '<span id="callStatus">Calling...</span>' +
                '</center>' +
            '</div>' +
            '<div id="otherVideo"></div>' +
            '<div id="myVideo"></div>' +
            '<div id="callActions">' +
                '<center>' +
                    '<a href="#" id="buttonDeclineCall" onclick="EndCall(\'' + id + '\');"><i class="fa fa-minus-circle"></i></a>' +
                '</center>' +
            '</div>' +
            '<div id="overlay">' +
                '<center>' +
                    '<a href="#" id="buttonToggleSpeakers" onclick="ToggleSpeakers(\'' + id + '\');"><i class="fa fa-volume-control-phone"></i></a>' +
                    '<a href="#" id="buttonToggleMuteMic" onclick="ToggleMuteMic(\'' + id + '\');"><i class="fa fa-microphone"></i></a>' +
                    '<a href="#" id="buttonToggleMuteSound" onclick="ToggleMuteSound(\'' + id + '\');"><i class="fa fa-volume-off"></i></a>' +
                    '<a href="#" id="buttonToggleCamera" onclick="ToggleCamera(\'' + id + '\');"><i class="fa fa-video-camera"></i></a>' +
                    '<a href="#" id="buttonToggleSwitchCamera" onclick="ToggleSwitchCamera(\'' + id + '\');" style="display:none;"><i class="fa fa-refresh"></i></a>' +
                '</center>' +
            '</div>' +
        '</div>';
    $(".callContainer").append(callElement);

    $(".callContainer").show();
}
function OnReceiveCall(id, userId, userName, userAvatar, userGender)
{
    var ms = 0;
    if ($("body").hasClass("canvas-slid")) {
        $('.container').click();
        ms = 1000;
    }
    delay(function(){
        numberOfCalls++;
        var avatar = userAvatar == "" ? ((userGender == 0 || userGender == "0") ? "img/avatar-male.png" : "img/avatar-female.png") : (webServerAddress + "?avatar=" + userAvatar);
    
        var callElement = '<div class="content" id="call_' + id + '">' +
                '<div id="otherInfos" class="otherInfos">' +
                    '<center>' +
                        '<img src="' + avatar + '" id="callOtherAvatar" /><br /><br />' +
                        '<span id="callOtherName">' + userName + '</span><br />' +
                        '<span id="callStatus">Receiving Call...</span>' +
                    '</center>' +
                '</div>' +
                '<div id="otherVideo"></div>' +
                '<div id="myVideo"></div>' +
                '<div id="callActions">' +
                    '<center>' +
                        '<a href="#" id="buttonAnswerCall" onclick="SetCallResult(\'' + id + '\', 1);"><i class="fa fa-phone"></i></a>' +
                        '<a href="#" id="buttonDeclineCall" onclick="SetCallResult(\'' + id + '\', 0);"><i class="fa fa-minus-circle"></i></a>' +
                    '</center>' +
                '</div>' +
                '<div id="overlay">' +
                    '<center>' +
                        '<a href="#" id="buttonToggleSpeakers" onclick="ToggleSpeakers(\'' + id + '\');"><i class="fa fa-volume-control-phone"></i></a>' +
                        '<a href="#" id="buttonToggleMuteMic" onclick="ToggleMuteMic(\'' + id + '\');"><i class="fa fa-microphone"></i></a>' +
                        '<a href="#" id="buttonToggleMuteSound" onclick="ToggleMuteSound(\'' + id + '\');"><i class="fa fa-volume-off"></i></a>' +
                        '<a href="#" id="buttonToggleCamera" onclick="ToggleCamera(\'' + id + '\');"><i class="fa fa-video-camera"></i></a>' +
                        '<a href="#" id="buttonToggleSwitchCamera" onclick="ToggleSwitchCamera(\'' + id + '\');" style="display:none;"><i class="fa fa-refresh"></i></a>' +
                    '</center>' +
                '</div>' +
            '</div>';
        $(".callContainer").append(callElement);
        $(".callContainer").show();
    }, ms); // end delay
}

function OnCallEnded(callId)
{
    VideoEnabled(false);
    if (document.getElementById("call_" + callId))
    {
        $("#call_" + callId).remove();
    }
    numberOfCalls--;
    if (numberOfCalls <= 0)
        $(".callContainer").hide();
    if (numberOfCalls < 0) numberOfCalls = 0;
    //$("#scrollMe").show();
}

function OnStartCall(callId)
{
    //$("#call_" + callId + " > #callStatus").text("Connecting...");
    $("#call_" + callId + " #callStatus").text("Connecting...");

    //$("#call_" + callId + " > #buttonAnswerCall").hide();
    $("#call_" + callId + " #buttonAnswerCall").hide();
}
function ToggleSpeakers(callId)
{
    $("#call_" + callId + " #buttonToggleSpeakers").toggleClass("active");
    $("#call_" + callId + " #buttonToggleSpeakers i").toggleClass("fa-phone");
    $("#call_" + callId + " #buttonToggleSpeakers i").toggleClass("fa-volume-control-phone");
    CSharp.ToggleSpeakers();
}
function ToggleMuteMic(callId)
{
    $("#call_" + callId + " #buttonToggleMuteMic").toggleClass("active");
    $("#call_" + callId + " #buttonToggleMuteMic i").toggleClass("fa-microphone");
    $("#call_" + callId + " #buttonToggleMuteMic i").toggleClass("fa-microphone-slash");
    CSharp.ToggleMuteMic();
}
function ToggleMuteSound(callId) {
    $("#call_" + callId + " #buttonToggleMuteSound").toggleClass("active");
    $("#call_" + callId + " #buttonToggleMuteSound i").toggleClass("fa-volume-off");
    $("#call_" + callId + " #buttonToggleMuteSound i").toggleClass("fa-volume-up");
    CSharp.ToggleMuteSound();
}
function ToggleCamera(callId) {
    $("#call_" + callId + " #buttonToggleCamera").toggleClass("active");
    $("#call_" + callId + " #buttonToggleCamera i").toggleClass("fa-video-camera");
    $("#call_" + callId + " #buttonToggleCamera i").toggleClass("fa-low-vision");
    $("#call_" + callId + " #buttonToggleSwitchCamera").toggle();
    CSharp.ToggleCamera();
    //$('html').css('background-color', 'transparent');
    //$('body').css('background-color', 'transparent');
    //$('.callContainer').css('background-color', 'transparent');
}
function ToggleSwitchCamera(callId) {
    $("#call_" + callId + " #buttonToggleSwitchCamera").toggleClass("active");
    CSharp.ToggleSwitchCamera();
}
function OnCallConnected(callId)
{
    //$("#call_" + callId + " > #callStatus").text("Connected");
    $("#call_" + callId + " #callStatus").text("Connected");
    //$("#call_" + callId + " > #overlay").show();
    $("#call_" + callId + " #overlay").show();
}
function OnGetCallVideoFrame(videoFrame)
{
    //$("#otherVideo").html('<img src="data:image/png;base64,' + videoFrame + '" width="100%" height="100%" />');
}
function VideoEnabled(enabled)
{
    if(enabled)
    {
        $('html, body, .callContainer').css("background", "transparent");
        $('.navmenu, .navbar, .container').hide();
        $('.otherInfos').hide();
    } else {
        $('.navmenu, .navbar, .container').show();
        $('html, body').removeAttr("style");
        $('.callContainer').css("background", "#5f5aa7");
        $('.callContainer').hide();
        $('.callContainer').show();
        $('.otherInfos').show();
    }
}
//#endregion
//#region Mapping
function OnGetPosition(latitude, longitude, myLat, myLong)
{
    $(".profileMap").html('<div id="map-canvas"></div>');

    var bounds = new google.maps.LatLngBounds();

    var myPosition = new google.maps.LatLng(myLat, myLong);
    var otherPosition = new google.maps.LatLng(latitude, longitude);

    bounds.extend(myPosition);
    bounds.extend(otherPosition);

    var mapOptions = {
        mapTypeId: 'roadmap'
        //zoom: 16,
        //center: otherPosition
    }
    var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
    //Markers
    var marker = new google.maps.Marker({
        position: otherPosition,
        map: map,
        draggable: false,
        animation: google.maps.Animation.DROP,
        title: "Destination"
    });

    var marker2 = new google.maps.Marker({
        position: myPosition,
        map: map,
        draggable: false,
        animation: google.maps.Animation.DROP,
        title: "My Position"
    });
    marker2.setIcon('https://maps.google.com/mapfiles/ms/icons/blue-dot.png');
    map.fitBounds(bounds);

    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function (event) {
        this.setZoom(14);
        google.maps.event.removeListener(boundsListener);
    });

    // To add the marker to the map, call setMap();
    //marker.setMap(map);
    //marker2.setMap(map);
    mapShow();
}
//#endregion

//#region Requests
function OnGetRequest(id, username, picture, gender, dateAt, fromMe, type, accepted) {
    requestsCount++;
    if (document.getElementById("no-request")) {
        $("#no-request").remove();
    }

    username = '<a href="#" onclick="ViewProfile(\'' + id + '\');">' + username + '</a>';

    if (!fromMe) {
        if (!accepted) {
            var count = parseInt($("#requestsCount").text());
            count++;
            $("#requestsCount").text(count);
            $("#requestsCount").removeClass("notification");
            $("#requestsCount").addClass("notification-active");
        }

        var message = "";
        switch (type) {
            case 0:
                message = accepted ? "You accepted a call request from " + username + "." : username + " sent you a call request.";
            break;
            case 1:
                message = accepted ? "You accepted a position request from " + username + "." : username + " sent you a position request.";
            break;
            case 2:
                message = accepted ? "You accepted a message request from " + username + "." : username + " sent you a message request.";
            break;
        }

        var actionButtons = accepted ? "" : '<a href="#" class="btn-default" onclick="AcceptRequest(\'' + id + '\', ' + type + ');">Accept</a> <a href="#" class="btn-default" onclick="DeclineRequest(\'' + id + '\', ' + type + ');">Decline</a>';

        var avatar = picture == "" ? ((gender == 0 || gender == "0") ? "img/avatar-male.png" : "img/avatar-female.png") : (webServerAddress + "?avatar=" + picture);
        var requestElement = '<li class="news-item" id="request_' + id + "_" + type + "_1" + '"><table cellpadding="4"><tr><td><img src="' + avatar + '" width="60" class="img-circle small-circle-img" onclick="ViewProfile(\'' + id + '\');" /></td><td width="100%">' + message + ' - ' + dateAt + '</td><td width="10px">' + actionButtons + '</td></tr></table></li>';
        $(".requests-feed").append(requestElement);
    } else {
        var avatar = picture == "" ? ((gender == 0 || gender == "0") ? "img/avatar-male.png" : "img/avatar-female.png") : (webServerAddress + "?avatar=" + picture);

        var message = "";
        switch (type) {
            case 0:
                message = accepted ? username + " accepted your call request." : username + " has your call request pending.";
            break;
            case 1:
                message = accepted ? username + " accepted your position request." : username + " has your position request pending.";
            break;
            case 2:
                message = accepted ? username + " accepted your message request." : username + " has your message request pending.";
            break;
        }

        var requestElement = '<li class="news-item" id="request_' + id + "_" + type + "_0" + '"><table cellpadding="4"><tr><td><img src="' + avatar + '" width="60" class="img-circle small-circle-img" onclick="ViewProfile(\'' + id + '\');" /></td><td width="100%">' + message + ' - ' + dateAt + '</td></tr></table></li>';
        $(".requests-feed").append(requestElement);
    }
}
function OnDeleteMyRequest(id, type) {
    $("#request_" + id + "_" + type + "_0").remove();
    requestsCount--;
    if (requestsCount < 0) requestsCount = 0;
    if (requestsCount <= 0) {
        $(".requests-feed").html("<center id=\"no-request\"><b>You have no requests.</b></center>");
    }
}
//#endregion

//#region Groups



//#endregion
//#endregion