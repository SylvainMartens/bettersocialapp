using Android.Content;
using Android.Graphics;
using Android.Webkit;
using Java.Interop;
using Java.IO;
using SocialAPP.Shared;
using System;
using System.Net;
using System.Text;

namespace SocialAPP.Droid
{
    public class MyJavaScriptInterface : Java.Lang.Object
    {
        public static MyJavaScriptInterface Instance;
        public static MainActivity mainActivity;
        private Context context;

        public MyJavaScriptInterface(Context context)
        {
            this.context = context;
            Instance = this;
        }

        #region Events

        [JavascriptInterface, Export]
        public string GetWebServerAddress()
        {
            return NetworkController.WebServerAddress;
        }

        #region Account Authentication

        [JavascriptInterface, Export]
        public bool Connect()
        {
            if (NetworkController.IsConnected) return true;
            return MainActivity.network.Connect();
        }

        [JavascriptInterface, Export]
        public void LoadLoginSettings()
        {
            MainActivity.LoadLoginInfo();
        }

        [JavascriptInterface, Export]
        public void Login(string username, string password, bool remember)
        {
            System.Console.WriteLine("Sending login request for user {0} password {1}", username, password);
            if (remember)
            {
                MainActivity.Username = username;
                MainActivity.Password = password;
                MainActivity.AutoLogin = true;
            }
            else
            {
                MainActivity.Username = "";
                MainActivity.Password = "";
            }
            MainActivity.SaveLoginInfo();
            password = Encryption.SHA1Of(password);
            MainActivity.network.SendLogin(username, password);
        }

        [JavascriptInterface, Export]
        public void Logout()
        {
            MainActivity.network.Logout();
            MainActivity.Password = "";
            MainActivity.AutoLogin = false;
            MainActivity.SaveLoginInfo();
            MainActivity.webView.LoadUrl("file:///android_asset/index.html");
        }

        [JavascriptInterface, Export]
        public void Register(string username, string password, string email, string firstname, string lastname, int dob_d, int dob_m, int dob_y, int gender)
        {
            MainActivity.network.Register(username, password, email, firstname, lastname, dob_d, dob_m, dob_y, gender);
        }

        [JavascriptInterface, Export]
        public void Recover(string userText)
        {
            mainActivity.ShowWorkingLoader();
            MainActivity.network.Recover(userText);
        }

        #endregion Account Authentication

        #region Search

        [JavascriptInterface, Export]
        public void SearchProfiles(string username, string gender, string minAge, string maxAge, string distance, int page = 1)
        {
            mainActivity.ShowWorkingLoader();
            MainActivity.network.SearchProfiles(username, gender, minAge, maxAge, distance, page);
        }

        #endregion Search

        #region Edit Profile

        [JavascriptInterface, Export]
        public void LoadEditAccountInfo()
        {
            mainActivity.ShowWorkingLoader();
            MainActivity.network.LoadEditAccountInfo();
        }

        [JavascriptInterface, Export]
        public void DoSaveAccount(string email, string username, string password, string firstname, string lastname, string dob_d, string dob_m, string dob_y)
        {
            mainActivity.ShowWorkingLoader();
            MainActivity.network.DoSaveAccount(email, username, password, firstname, lastname, dob_d, dob_m, dob_y);
        }

        [JavascriptInterface, Export]
        public void DoEditProfilePicture()
        {
            mainActivity.SelectProfilePicture();
        }

        public static Bitmap scaleBitmap(Bitmap bitmapToScale, float newWidth, float newHeight)
        {
            if (bitmapToScale == null)
            {
                return null;
            }
            int width = bitmapToScale.Width;
            int height = bitmapToScale.Height;
            Matrix matrix = new Matrix();
            matrix.PostScale(newWidth / (float)width, newHeight / (float)height);
            return Bitmap.CreateBitmap(bitmapToScale, 0, 0, bitmapToScale.Width, bitmapToScale.Height, matrix, true);
        }

        [JavascriptInterface, Export]
        public void SaveEditProfile(string picturePath, string city, string job, string activities, string description, string lookingFor, string lookingForDescription)
        {
            mainActivity.ShowWorkingLoader();
            if (!string.IsNullOrEmpty(picturePath) && new File(picturePath).Exists())
            {
                MainActivity.PictureToUpload = picturePath;
                MainActivity.network.EditProfilePicture();
            }
            MainActivity.network.EditProfileInfo(city, job, activities, description, lookingFor, lookingForDescription);
        }

        [JavascriptInterface, Export]
        public void LoadEditProfileInfo()
        {
            mainActivity.ShowWorkingLoader();
            MainActivity.network.LoadEditProfileInfo();
        }

        #endregion

        #region View Profile

        [JavascriptInterface, Export]
        public void LoadProfileData(string profileId)
        {
            mainActivity.ShowWorkingLoader();
            MainActivity.network.LoadProfileData(profileId);
        }

        [JavascriptInterface, Export]
        public void StartConversation(string profileId)
        {
            mainActivity.ShowWorkingLoader();
            MainActivity.network.StartConversation(profileId);
        }

        [JavascriptInterface, Export]
        public void GetProfilePhotos(string profileId, int page = 1)
        {
            mainActivity.ShowWorkingLoader();
            MainActivity.network.GetProfilePhotos(profileId, page);
        }

        #endregion View Profile

        #region Messages

        [JavascriptInterface, Export]
        public void LoadConversations(int page = 1)
        {
            MainActivity.network.LoadConversations(page);
        }

        [JavascriptInterface, Export]
        public void LoadMessages(string conversationId, int page = 1)
        {
            MainActivity.network.LoadMessages(conversationId, page);
        }

        [JavascriptInterface, Export]
        public void SendMessage(string toAccountId, string message, string attachmentPath = "")
        {
            if (string.IsNullOrEmpty(toAccountId) || string.IsNullOrEmpty(message)) return;

            var attachmentId = "";
            mainActivity.ShowWorkingLoader();
            try
            {
                if (!string.IsNullOrEmpty(attachmentPath) && new File(attachmentPath).Exists())
                {
                    WebClient webClient = new WebClient();
                    webClient.Credentials = CredentialCache.DefaultCredentials;
                    webClient.Headers.Add("Content-Type", "binary/octet-stream");
                    byte[] array = webClient.UploadFile(NetworkController.WebServerAddress + "uploadAttachment", "POST", attachmentPath);
                    string @string = Encoding.UTF8.GetString(array, 0, array.Length);
                    webClient.Dispose();
                    if (!string.IsNullOrEmpty(@string) && @string != "0")
                        attachmentId = @string;
                }
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.StackTrace);
            }

            MainActivity.network.SendMessage(toAccountId, message, attachmentId);
            mainActivity.HideWorkingLoader();
        }

        [JavascriptInterface, Export]
        public void DoSelectMessagePicture()
        {
            mainActivity.SelectMessagePicture();
        }

        [JavascriptInterface, Export]
        public void SetMessageHasSeen(string messageId)
        {
            if (string.IsNullOrEmpty(messageId)) return;
            MainActivity.network.SeenMessage(messageId);
        }

        #endregion Messages

        #region Follows

        [JavascriptInterface, Export]
        public void Follow(string accountId)
        {
            MainActivity.network.Follow(accountId);
        }

        [JavascriptInterface, Export]
        public void Unfollow(string accountId)
        {
            MainActivity.network.Unfollow(accountId);
        }

        #endregion Follows

        #region Notifications

        [JavascriptInterface, Export]
        public void Poke(string accountId)
        {
            if (string.IsNullOrEmpty(accountId)) return;
            MainActivity.network.Poke(accountId);
        }

        [JavascriptInterface, Export]
        public void DeleteNotification(string notificationId)
        {
            if (string.IsNullOrEmpty(notificationId)) return;
            MainActivity.network.DeleteNotification(notificationId);
        }

        #endregion Notifications

        [JavascriptInterface, Export]
        public void PageChanged(string page)
        {
            MainActivity.CurrentPage = page;
        }

        #region Posts

        [JavascriptInterface, Export]
        public void CreatePost(string content, string picturePath = null)
        {
            mainActivity.ShowWorkingLoader();
            var attachmentId = "";
            try
            {
                if (!string.IsNullOrEmpty(picturePath) && new File(picturePath).Exists())
                {
                    WebClient webClient = new WebClient();
                    webClient.Credentials = CredentialCache.DefaultCredentials;
                    webClient.Headers.Add("Content-Type", "binary/octet-stream");
                    byte[] array = webClient.UploadFile(NetworkController.WebServerAddress + "uploadAttachment", "POST", picturePath);
                    string @string = Encoding.UTF8.GetString(array, 0, array.Length);
                    webClient.Dispose();
                    if (!string.IsNullOrEmpty(@string) && @string != "0")
                        attachmentId = @string;
                }
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.StackTrace);
            }
            MainActivity.network.CreatePost(content, attachmentId);
            mainActivity.HideWorkingLoader();
        }

        [JavascriptInterface, Export]
        public void DoSelectPostPicture(bool fromHome)
        {
            mainActivity.SelectPostPicture(fromHome);
        }

        [JavascriptInterface, Export]
        public void DeletePost(string postId)
        {
            MainActivity.network.DeletePost(postId);
        }

        [JavascriptInterface, Export]
        public void CommentPost(string postId, string content, string attachmentId = null)
        {
            MainActivity.network.CommentPost(postId, content, attachmentId);
        }

        [JavascriptInterface, Export]
        public void DeleteComment(string commentId)
        {
            MainActivity.network.DeleteComment(commentId);
        }

        #endregion Posts

        #region Payments

        [JavascriptInterface, Export]
        public void GetTransactions(int page = 1)
        {
            mainActivity.ShowWorkingLoader();
            MainActivity.network.GetTransactions(page);
        }

        [JavascriptInterface, Export]
        public void VerifyMembership()
        {
            MainActivity.network.VerifyMembership();
        }

        [JavascriptInterface, Export]
        public void SendPayment(string tokenId, int plan = 0)
        {
            mainActivity.ShowWorkingLoader();
            MainActivity.network.SendPayment(tokenId, plan);
        }

        #endregion Payments

        #region Blocks
        [JavascriptInterface, Export]
        public void GetBlockedUsers()
        {
            mainActivity.ShowWorkingLoader();
            MainActivity.network.GetBlockedUsers();
        }
        [JavascriptInterface, Export]
        public void BlockUser(string userId)
        {
            MainActivity.network.BlockUser(userId);
        }
        [JavascriptInterface, Export]
        public void UnBlockUser(string userId)
        {
            MainActivity.network.UnBlockUser(userId);
        }
        #endregion

        #region Calls
        [JavascriptInterface, Export]
        public void SendCall(string userId)
        {
            MainActivity.network.SendCall(userId);
        }
        [JavascriptInterface, Export]
        public void SetRequestResult(string userId, int result, int type)
        {
            switch(type)
            {
                case 0:
                {
                    MainActivity.network.SetCallRequestResult(userId, result == 1);
                }
                break;
                case 1:
                {
                    MainActivity.network.SetMapRequestResult(userId, result == 1);
                }
                break;
                case 2:
                {
                    MainActivity.network.SetMsgRequestResult(userId, result == 1);
                }
                break;
            }
        }
        [JavascriptInterface, Export]
        public void SetCallResult(string callId, int result)
        {
            MainActivity.network.SetCallResult(callId, result == 1);
        }
        [JavascriptInterface, Export]
        public void EndCall(string callId)
        {
            MainActivity.network.EndCall(callId);
            if(MainActivity.callClient != null) MainActivity.callClient.Destroy();
        }
        [JavascriptInterface, Export]
        public void SetCallVideoEnabled(string callId, int enabled)
        {
            MainActivity.network.SetCallVideoEnabled(callId, enabled == 1);
        }
        [JavascriptInterface, Export]
        public void ToggleSpeakers()
        {
            AudioPlayer.ToggleSpeaker();
        }
        [JavascriptInterface, Export]
        public void ToggleMuteMic()
        {
            MainActivity.MicCapture?.ToggleMuteMic();
        }
        [JavascriptInterface, Export]
        public void ToggleMuteSound()
        {
            AudioPlayer.ToggleMuteSound();
        }
        [JavascriptInterface, Export]
        public void ToggleCamera()
        {
            if (MainActivity.CamCapture == null || MainActivity.callClient == null) return;
            MainActivity.CamCapture.ToggleCamera();
            MainActivity.callClient.MyVideoEnabled = MainActivity.CamCapture.IsRecording;
            mainActivity.VideoEnabled();
            MainActivity.network.SetCallVideoEnabled(MainActivity.callClient.CallId, MainActivity.CamCapture.IsRecording);
        }
        [JavascriptInterface, Export]
        public void ToggleSwitchCamera()
        {
            if (MainActivity.CamCapture == null || MainActivity.callClient == null) return;
            MainActivity.CamCapture.SwitchCamera();
        }
        #endregion

        #region Mapping
        [JavascriptInterface, Export]
        public void GetMapPosition(string userId)
        {
            MainActivity.network.GetMapPosition(userId);
        }
        #endregion

        #region Groups
        public void GetMyGroups()
        {
            MainActivity.network.GetMyGroups();
        }
        public void GetUserGroups(string userId)
        {
            if (string.IsNullOrEmpty(userId)) return;
            MainActivity.network.GetUserGroups(userId);
        }
        public void CreateGroup(string groupName)
        {
            if (string.IsNullOrEmpty(groupName)) return;
            MainActivity.network.CreateGroup(groupName);
        }
        public void DeleteGroup(string groupId)
        {
            if (string.IsNullOrEmpty(groupId)) return;
            MainActivity.network.DeleteGroup(groupId);
        }
        public void InviteGroup(string userId, string groupId)
        {
            if (string.IsNullOrEmpty(userId) || string.IsNullOrEmpty(groupId)) return;
            MainActivity.network.InviteGroup(userId, groupId);
        }
        public void SetGroupInviteResult(string invitationId, bool result)
        {
            if (string.IsNullOrEmpty(invitationId)) return;
            MainActivity.network.SetGroupInviteResult(invitationId, result);
        }
        public void RequestJoinGroup(string groupId)
        {
            if (string.IsNullOrEmpty(groupId)) return;
            MainActivity.network.RequestJoinGroup(groupId);
        }
        public void CancelGroupRequest(string requestId)
        {
            if (string.IsNullOrEmpty(requestId)) return;
            MainActivity.network.CancelGroupRequest(requestId);
        }
        public void SetGroupRequestResult(string requestId, bool result)
        {
            if (string.IsNullOrEmpty(requestId)) return;
            MainActivity.network.SetGroupRequestResult(requestId, result);
        }
        public void SetMemberAdministrator(string userId, string groupId, bool isAdmin)
        {
            if (string.IsNullOrEmpty(userId) || string.IsNullOrEmpty(groupId)) return;
            MainActivity.network.SetMemberAdministrator(userId, groupId, isAdmin);
        }
        public void DeleteGroupMember(string userId, string groupId)
        {
            if (string.IsNullOrEmpty(userId) || string.IsNullOrEmpty(groupId)) return;
            MainActivity.network.DeleteGroupMember(userId, groupId);
        }
        public void CreateGroupPost(string groupId, string message, string attachmentId = "")
        {
            if (string.IsNullOrEmpty(message) || string.IsNullOrEmpty(groupId)) return;
            MainActivity.network.CreateGroupPost(groupId, message, attachmentId);
        }
        public void DeleteGroupPost(string postId)
        {
            if (string.IsNullOrEmpty(postId)) return;
            MainActivity.network.DeleteGroupPost(postId);
        }
        public void GetGroupPosts(string groupId)
        {
            if (string.IsNullOrEmpty(groupId)) return;
            MainActivity.network.GetGroupPosts(groupId);
        }
        public void CommentGroupPost(string postId, string message, string attachmentId = "")
        {
            if (string.IsNullOrEmpty(message) || string.IsNullOrEmpty(postId)) return;
            MainActivity.network.CommentGroupPost(postId, message, attachmentId);
        }
        public void DeleteGroupComment(string commentId)
        {
            if (string.IsNullOrEmpty(commentId)) return;
            MainActivity.network.DeleteGroupComment(commentId);
        }
        public void GetGroupPostComments(string postId)
        {
            if (string.IsNullOrEmpty(postId)) return;
            MainActivity.network.GetGroupPostComments(postId);
        }
        public void SetGroupPublic(string groupId, bool isPublic)
        {
            if (string.IsNullOrEmpty(groupId)) return;
            MainActivity.network.SetGroupPublic(groupId, isPublic);
        }
        public void GetGroupMembers(string groupId)
        {
            if (string.IsNullOrEmpty(groupId)) return;
            MainActivity.network.GetGroupMembers(groupId);
        }
        public void SearchGroup(string search)
        {
            if (string.IsNullOrEmpty(search)) return;
            MainActivity.network.SearchGroup(search);
        }
        public void GetPendingGroupInvitations(string groupId)
        {
            if (string.IsNullOrEmpty(groupId)) return;
            MainActivity.network.GetPendingGroupInvitations(groupId);
        }
        public void GetMyGroupInvitations()
        {
            MainActivity.network.GetMyGroupInvitations();
        }
        public void GetMyPendingGroupRequests()
        {
            MainActivity.network.GetMyPendingGroupRequests();
        }
        public void GetGroupRequests(string groupId)
        {
            if (string.IsNullOrEmpty(groupId)) return;
            MainActivity.network.GetGroupRequests(groupId);
        }
        public void QuitGroup(string groupId)
        {
            if (string.IsNullOrEmpty(groupId)) return;
            MainActivity.network.QuitGroup(groupId);
        }
        #endregion

        #endregion Events
    }
}