﻿using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Database;
using Android.Graphics;
using Android.Locations;
using Android.Media;
using Android.OS;
using Android.Provider;
using Android.Runtime;
using Android.Support.V4.App;
using Android.Views;
using Android.Webkit;
using Java.Net;
using SocialAPP.Shared;
using SocialAPP.Shared.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using TaskStackBuilder = Android.Support.V4.App.TaskStackBuilder;
using Android.Widget;
using Android.Hardware;
using static Android.Views.View;

namespace SocialAPP.Droid
{
    [Activity(Label = "Social APP", MainLauncher = true, Icon = "@drawable/icon", Theme = "@android:style/Theme.NoTitleBar", ConfigurationChanges = (ConfigChanges.Orientation | ConfigChanges.ScreenSize), HardwareAccelerated = true, LaunchMode = LaunchMode.SingleInstance)]
    public class MainActivity : FragmentActivity, ILocationListener, IJavaObject, IDisposable, TextureView.ISurfaceTextureListener, ISensorEventListener, IOnTouchListener
    {
        #region Variables
        public static Context currentContext;

        public static NetworkController network = new NetworkController();

        public static WebView webView;

        public static string Username = "";

        public static string Password = "";

        public static bool AutoLogin = false;

        public static string PictureToUpload = "";

        private static bool _workingLoaderDisplayed = false;

        private static Location _currentLocation;

        private static LocationManager _locationManager;

        private static string _locationProvider;

        private static bool msgBoxShown;

        private static double prevLat;

        private static double prevLon;

        public static string CurrentPage = "home";
        public static int notificationId = 0;

        public static MediaPlayer RingTonePlayer;

        public static CallClient callClient;
        public static AudioCapture MicCapture;
        public static bool PostFromHome;
        ImageView imageView;
        TextureView videoView;
        
        //Sensor
        static readonly object _syncLock = new object();
        SensorManager _sensorManager;

        public static CameraCapture CamCapture;
        #endregion Variables

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            #region WebView

            if (webView == null)
            {
                currentContext = this;
                //CallServer.OnReceiveAudioPacket += OnSyncAudioFromClient;
                #region Events

                #region Authentication
                EventsController.Log += OnLog;
                EventsController.OnGetVersion += OnGetVersionFromServer;
                EventsController.OnGetLoginResult += OnGetLoginResult;
                EventsController.OnGetRegisterResult += OnGetRegisterResult;
                EventsController.OnGetRecoverResult += OnGetRecoverResult;
                EventsController.OnGetAccountInfos += OnGetAccountInfos;
                EventsController.OnGetEditAccount += OnGetEditAccount;
                EventsController.OnGetEditAccountResult += OnGetEditAccountResult;

                #endregion Authentication

                EventsController.OnGetSearchResults += OnGetSearchResults;

                #region Edit Profile

                EventsController.OnProfileEdited += OnProfileEdited;
                EventsController.OnGetUploadProfilePictureToken += OnGetUploadProfilePictureToken;
                EventsController.OnGetEditProfileData += OnGetEditProfileData;

                #endregion Edit Profile

                EventsController.OnGetProfileData += OnGetProfileData;
                EventsController.OnGetProfilePictures += OnGetProfilePictures;

                EventsController.OnReturnToHome += OnReturnToHome;

                #region Messages Events

                EventsController.OnLoadConversationsResult += OnGetConversations;
                EventsController.OnLoadMessagesResult += OnGetMessages;
                EventsController.OnReceiveMessage += OnMessageReceived;
                EventsController.OnMessageSeen += OnMessageSeen;
                EventsController.OnConversationStarted += OnConversationStarted;

                #endregion Messages Events

                #region Follows Events

                EventsController.OnGetProfileFollowers += OnGetProfileFollowers;
                EventsController.OnGetProfileFollowings += OnGetProfileFollowings;

                #endregion Follows Events

                #region Notifications Events

                EventsController.OnGetNotifications += OnGetNotifications;
                EventsController.OnGetNotification += OnGetNotification;

                #endregion Notifications Events

                #region Posts Events

                EventsController.OnGetPost += OnGetPost;
                EventsController.OnGetPostDeleted += OnGetPostDeleted;
                EventsController.OnGetPosts += OnGetPosts;
                EventsController.OnGetProfilePosts += OnGetProfilePosts;
                EventsController.OnGetComments += OnGetComments;
                EventsController.OnGetComment += OnGetComment;
                EventsController.OnGetCommentDeleted += OnGetCommentDeleted;

                #endregion Posts Events

                #region Payments Events

                EventsController.OnGetTransactions += OnGetTransactions;
                EventsController.OnGetPaymentResult += OnGetPaymentResult;
                EventsController.OnGetVerifyMembership += OnGetVerifyMembership;
                EventsController.OnPremiumRequired += OnPremiumRequired;

                #endregion Payments Events

                #region Blocks
                EventsController.OnGetBlockedUsers += OnGetBlockedUsers;
                #endregion

                #region Calls
                EventsController.OnGetRequest += OnGetRequest;
                EventsController.OnGetRequests += OnGetRequests;
                EventsController.OnStartCalling += OnStartCalling;
                EventsController.OnReceiveCall += OnReceiveCall;
                EventsController.OnCallRingTone += OnCallRingTone;
                EventsController.OnStartCall += OnStartCall;
                EventsController.OnCallEnded += OnCallEnded;
                #endregion

                #region Mapping
                EventsController.OnGetPosition += OnGetPosition;
                #endregion

                #region Groups
                EventsController.OnGetMyGroups += OnGetMyGroups;
                EventsController.OnGetUserGroups += OnGetUserGroups;
                EventsController.OnGetGroupCreated += OnGetGroupCreated;
                EventsController.OnGetGroupInvite += OnGetGroupInvite;
                EventsController.OnGetGroupRequest += OnGetGroupRequest;
                EventsController.OnGetGroupPostCreated += OnGetGroupPostCreated;
                EventsController.OnGetGroupPosts += OnGetGroupPosts;
                EventsController.OnGetGroupCommentCreated += OnGetGroupCommentCreated;
                EventsController.OnGetGroupPostComments += OnGetGroupPostComments;
                EventsController.OnGetGroupMembers += OnGetGroupMembers;
                EventsController.OnGetGroupSearchResults += OnGetGroupSearchResults;
                EventsController.OnGetGroupPendingInvitations += OnGetGroupPendingInvitations;
                EventsController.OnGetGroupMyInvitations += OnGetGroupMyInvitations;
                EventsController.OnGetGroupMyRequests += OnGetGroupMyRequests;
                EventsController.OnGetGroupPendingRequests += OnGetGroupPendingRequests;
                #endregion

                #endregion Events

                #region Creator

                SetContentView(Resource.Layout.Main);
                _sensorManager = (SensorManager)GetSystemService(SensorService);
                _sensorManager.RegisterListener(this, _sensorManager.GetDefaultSensor(SensorType.Accelerometer), SensorDelay.Ui);


                imageView = FindViewById<ImageView>(Resource.Id.myImageView);

                videoView = FindViewById<TextureView>(Resource.Id.myVideoView);
                videoView.SurfaceTextureListener = this;
                videoView.SetBackgroundColor(Color.Transparent);

                webView = FindViewById<WebView>(Resource.Id.myWebView);
                webView.SetOnTouchListener(this);
                webView.Settings.JavaScriptEnabled = true;
                webView.Settings.UseWideViewPort = true;
                webView.VerticalScrollBarEnabled = false;
                webView.HorizontalScrollBarEnabled = false;
                webView.AddJavascriptInterface(new MyJavaScriptInterface(this), "CSharp");
                webView.LoadUrl("file:///android_asset/index.html");
                webView.SetBackgroundColor(Color.Transparent);

                AutoLogin = true;
                InitializeLocationManager();
                Task.Factory.StartNew(delegate
                {
                    UpdatePosition();
                });
                MyJavaScriptInterface.mainActivity = this;
                CamCapture = new CameraCapture(videoView);
                CamCapture.OnFrameReady += OnVideoReadyToBeSent;
                #endregion
            }
            else
            {
                var viewGroup = ((ViewGroup)webView.Parent.Parent.Parent.Parent.Parent);
                viewGroup.RemoveView((ViewGroup)webView.Parent.Parent.Parent.Parent);
                SetContentView((ViewGroup)webView.Parent.Parent.Parent.Parent);
            }
            #endregion WebView

            if(Intent != null && Intent.Extras != null)
            {
                var conversationId = Intent.Extras.GetString("ConversationId");
                if (!string.IsNullOrEmpty(conversationId))
                {
                    ShowMessage(conversationId);
                }
                var _notificationId = Intent.Extras.GetString("NotificationId");
                if (!string.IsNullOrEmpty(_notificationId))
                {
                    ShowPage("notifications");
                }
                var requestId = Intent.Extras.GetString("RequestId");
                if (!string.IsNullOrEmpty(requestId))
                {
                    ShowPage("requests");
                }
            }
        }
        protected override void OnDestroy()
        {
            if(callClient != null)
            {
                network.EndCall(callClient.CallId);
                callClient.Destroy();
            }
            base.OnDestroy();
        }
        protected override void OnStop()
        {
            if (callClient != null)
            {
                network.EndCall(callClient.CallId);
                callClient.Destroy();
            }
            base.OnStop();
        }
        #region Functions

        //Functions
        public void OnLog(string log)
        {
            Console.WriteLine(log);
        }
        public void ShowWorkingLoader()
        {
            if (_workingLoaderDisplayed)
            {
                return;
            }
            _workingLoaderDisplayed = true;
            ExecuteJavascript("ShowWorkingLoader();");
        }

        public void HideWorkingLoader()
        {
            if (!_workingLoaderDisplayed)
            {
                return;
            }
            _workingLoaderDisplayed = false;
            ExecuteJavascript("HideWorkingLoader();");
        }

        public static void SaveLoginInfo()
        {
            ISharedPreferencesEditor preferences = Application.Context.GetSharedPreferences("SocialApp", FileCreationMode.Private).Edit();
            preferences.PutString("Username", Username);
            preferences.PutString("Password", Password);
            AutoLogin = true;
            preferences.Commit();
        }

        public static void LoadLoginInfo()
        {
            ISharedPreferences preferences = Application.Context.GetSharedPreferences("SocialApp", FileCreationMode.Private);
            Username = preferences.GetString("Username", "");
            Password = preferences.GetString("Password", "");

            if (string.IsNullOrEmpty(Username) || string.IsNullOrEmpty(Password))
            {
                AutoLogin = false;
            }
            ExecuteJavascript(string.Format("loginInfos('{0}', '{1}', {2});", Username, Password, AutoLogin ? 1 : 0));
        }

        public void SelectProfilePicture()
        {
            Intent intent = new Intent();
            intent.SetType("image/*");
            intent.SetAction("android.intent.action.GET_CONTENT");
            StartActivityForResult(Intent.CreateChooser(intent, "Select profile picture"), 0);
        }

        public void SelectPostPicture(bool fromHome)
        {
            PostFromHome = fromHome;
            Intent intent = new Intent();
            intent.SetType("image/*");
            intent.SetAction("android.intent.action.GET_CONTENT");
            StartActivityForResult(Intent.CreateChooser(intent, "Select picture to post"), 1);
        }

        public void SelectMessagePicture()
        {
            Intent intent = new Intent();
            intent.SetType("image/*");
            intent.SetAction("android.intent.action.GET_CONTENT");
            StartActivityForResult(Intent.CreateChooser(intent, "Select picture to send"), 2);
        }

        private string GetPathToImage(Android.Net.Uri uri)
        {
            ICursor cursor = ContentResolver.Query(uri, null, null, null, null);
            cursor.MoveToFirst();
            string text = cursor.GetString(0);
            if (text.Contains(":"))
            {
                text = text.Split(':')[1];
            }
            cursor.Close();
            cursor = this.ContentResolver.Query(MediaStore.Images.Media.ExternalContentUri, null, "_id = ? ", new string[]
            {
                text
            }, null);
            cursor.MoveToFirst();
            string @string = cursor.GetString(cursor.GetColumnIndex("_data"));
            cursor.Close();
            return @string;
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            try
            {
                if (resultCode == Result.Ok)
                {
                    switch (requestCode)
                    {
                        case 0://Edit Profile Picture
                        {
                            string dataString = data.DataString;
                            string pathToImage = GetPathToImage(data.Data);
                            Console.WriteLine("IMG PATH: " + dataString);
                            ExecuteJavascript(string.Format("DisplayEditProfilePicture('{0}', '{1}');", dataString, pathToImage));
                        }
                        break;

                        case 1://Select Post Picture
                        {
                            string dataString = data.DataString;
                            string pathToImage = GetPathToImage(data.Data);
                            Console.WriteLine("IMG PATH: " + dataString);
                            ExecuteJavascript(string.Format("DisplaySelectPostPicture('{0}', '{1}', {2});", dataString, pathToImage, PostFromHome ? 1 : 0));
                        }
                        break;

                        case 2://Message Picture
                        {
                            string dataString = data.DataString;
                            string pathToImage = GetPathToImage(data.Data);
                            Console.WriteLine("IMG PATH: " + dataString);
                            ExecuteJavascript(string.Format("DisplaySelectMessagePicture('{0}', '{1}');", dataString, pathToImage));
                        }
                        break;

                        case 3://Comment Picture
                        {
                        }
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
        }

        public int GetAge(DateTime dateOfBirth)
        {
            DateTime today = DateTime.Today;
            int arg_42_0 = (today.Year * 100 + today.Month) * 100 + today.Day;
            int num = (dateOfBirth.Year * 100 + dateOfBirth.Month) * 100 + dateOfBirth.Day;
            return (arg_42_0 - num) / 10000;
        }

        public static void ExecuteJavascript(string javascript)
        {
            using (var h = new Handler(Looper.MainLooper))
            {
                h.Post(() => {
                    webView.LoadUrl("javascript:" + javascript);
                });
            }
        }

        protected override void OnResume()
        {
            base.OnResume();
            //_sensorManager.RegisterListener(this, _sensorManager.GetDefaultSensor(SensorType.Accelerometer), SensorDelay.Ui);

            if (_locationManager == null || string.IsNullOrEmpty(_locationProvider))
            {
                return;
            }
            _locationManager.RequestLocationUpdates(_locationProvider, 0L, 0f, this);
        }

        protected override void OnPause()
        {
            base.OnPause();

            //_sensorManager.UnregisterListener(this);

            if (_locationManager == null)
            {
                return;
            }
            _locationManager.RemoveUpdates(this);
        }

        public void OnLocationChanged(Location location)
        {
            if (location == null)
            {
                return;
            }
            _currentLocation = location;
        }

        private void InitializeLocationManager()
        {
            if (_locationManager == null)
            {
                _locationManager = (LocationManager)GetSystemService("location");
            }
            Criteria criteria = new Criteria
            {
                Accuracy = Accuracy.Fine
            };
            IList<string> providers = _locationManager.GetProviders(criteria, true);
            if (providers.Any())
            {
                _locationProvider = providers.First();
            }
            else
            {
                _locationProvider = string.Empty;
            }
            if (!msgBoxShown && string.IsNullOrEmpty(_locationProvider))
            {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
                dialogBuilder.SetTitle("Location Services Not Active");
                dialogBuilder.SetMessage("Please enable Location Services and GPS");
                dialogBuilder.SetPositiveButton("OK", delegate (object senderAlert, DialogClickEventArgs args)
                {
                    Intent intent = new Intent("android.settings.LOCATION_SOURCE_SETTINGS");
                    StartActivity(intent);
                    msgBoxShown = false;
                });
                AlertDialog dialog = dialogBuilder.Create();
                dialog.SetCancelable(false);
                dialog.SetCanceledOnTouchOutside(false);
                dialog.Show();
                msgBoxShown = true;
            }
            Console.WriteLine("Using " + _locationProvider + ".");
        }

        public async void UpdatePosition()
        {
            while (true)
            {
                if (NetworkController.IsConnected && NetworkController.IsLoggedIn && _currentLocation != null && _currentLocation.Latitude != prevLat && _currentLocation.Longitude != prevLon)
                {
                    prevLat = _currentLocation.Latitude;
                    prevLon = _currentLocation.Longitude;
                    network.UpdateGeolocation(_currentLocation.Latitude, _currentLocation.Longitude);
                }
                await Task.Delay(1000 * 30);//Every 30 secs.
            }
        }

        public void OnProviderDisabled(string provider)
        {
            if (_locationProvider == provider)
            {
                msgBoxShown = false;
                _locationProvider = string.Empty;
            }
        }

        public void OnProviderEnabled(string provider)
        {
        }

        public void OnStatusChanged(string provider, Availability status, Bundle extras)
        {
        }

        public static void ShowMessage(string conversationId)
        {
            ExecuteJavascript("ShowConversation('" + conversationId + "');");
            ExecuteJavascript("changePage('messages');");
        }

        public static void ShowPage(string page)
        {
            ExecuteJavascript("changePage('" + page + "');");
        }

        public Bitmap getBitmapFromURL(string strURL)
        {
            try
            {
                URL url = new URL(strURL);
                HttpURLConnection connection = (HttpURLConnection)url.OpenConnection();
                connection.DoInput = true;
                connection.Connect();
                System.IO.Stream input = connection.InputStream;
                Bitmap myBitmap = BitmapFactory.DecodeStream(input);
                return myBitmap;
            }
            catch (IOException e)
            {
                Console.WriteLine(e.StackTrace);
                return null;
            }
        }

        public void VideoEnabled()
        {
            var enabled = callClient.MyVideoEnabled || callClient.RemoteVideoEnabled; 
            ExecuteJavascript(string.Format("VideoEnabled({0});", enabled ? "true" : "false"));
        }

        #endregion Functions

        #region Events Receivers

        #region Account Authentication

        public void OnGetVersionFromServer(string version)
        {
            if (version == NetworkController.Version)
            {
                ExecuteJavascript("isVersionValid(true);");
                return;
            }
            ExecuteJavascript("isVersionValid(false);");
        }

        public void OnGetLoginResult(int result)
        {
            ExecuteJavascript(string.Format("loginResult({0});", result));
        }

        public void OnGetAccountInfos(string accountId, string accountName, string accountAvatar, int accountGender, bool accountMembership)
        {
            ExecuteJavascript(string.Format("OnGetAccountInfos('{0}', '{1}', '{2}', {3}, {4});", accountId, accountName, accountAvatar, accountGender, accountMembership ? "true" : "false"));
        }

        public void OnGetEditAccount(string email, string username, string firstname, string lastname, string dob_d, string dob_m, string dob_y)
        {
            ExecuteJavascript(string.Format("OnGetEditAccount('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}');", email, username, firstname, lastname, dob_d, dob_m, dob_y));
            HideWorkingLoader();
        }

        public void OnGetEditAccountResult(int result)
        {
            ExecuteJavascript(string.Format("OnGetEditAccountResult({0});", result));
            HideWorkingLoader();
        }

        public void OnGetRegisterResult(int result)
        {
            ExecuteJavascript(string.Format("registerResult({0});", result));
        }

        public void OnGetRecoverResult(int result)
        {
            ExecuteJavascript(string.Format("OnGetRecoverResult({0});", result));
            HideWorkingLoader();
        }

        #endregion Account Authentication

        #region Search

        public void OnGetSearchResults(Dictionary<List<AccountData>, int> results)
        {
            List<AccountData> key = results.First().Key;
            int value = results.First().Value;
            ExecuteJavascript("ClearSearchResults();");
            ExecuteJavascript("CreatePagination(" + value + ");");
            if (key.Count == 0)
            {
                ExecuteJavascript("ShowNoSearchResults(true);");
            }
            else
            {
                ExecuteJavascript("ShowNoSearchResults(false);");
                foreach (AccountData current in key)
                {
                    int age = GetAge(DateTime.Parse(string.Concat(new string[]
                    {
                        current.Dob_y,
                        "-",
                        current.Dob_m,
                        "-",
                        current.Dob_d
                    })));
                    ExecuteJavascript(string.Concat(new object[]
                    {
                        "AddProfileToSearchResults('",
                        current.Id,
                        "','",
                        current.Username,
                        "','",
                        current.Avatar,
                        "','",
                        age,
                        "', '",
                        current.Gender,
                        "');"
                    }));
                }
            }
            HideWorkingLoader();
        }

        #endregion Search

        #region Edit Profile

        public void OnProfileEdited()
        {
            ExecuteJavascript("changePage('profile');");
        }

        public bool ResizeBitmap(string path, int height, int width)
        {
            try
            {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.InPreferredConfig = Bitmap.Config.Argb8888;
                Bitmap bitmap = BitmapFactory.DecodeFile(path, options);

                var bitmapScalled = Bitmap.CreateScaledBitmap(bitmap, width, height, true);
                bitmap.Recycle();
                using (var stream = new MemoryStream())
                {
                    bitmapScalled.Compress(Bitmap.CompressFormat.Jpeg, 50, stream);
                    File.WriteAllBytes(path + ".tmp", stream.ToArray());
                }
                return true;
            }catch(Exception)
            {
                
            }
            return false;
        }

        public void OnGetUploadProfilePictureToken(string text)
        {
            if (!string.IsNullOrEmpty(text))
            {
                try
                {
                    var resized = ResizeBitmap(PictureToUpload, 500, 500);

                    WebClient webClient = new WebClient();
                    webClient.Credentials = CredentialCache.DefaultCredentials;
                    webClient.Headers.Add("Content-Type", "binary/octet-stream");
                    byte[] array = webClient.UploadFile(NetworkController.WebServerAddress + "uploadAvatar?token=" + text, "POST", resized ? PictureToUpload + ".tmp" : PictureToUpload);
                    string @string = System.Text.Encoding.UTF8.GetString(array, 0, array.Length);
                    webClient.Dispose();
                    if (@string == "success")
                    {
                        network.UpdateProfilePicture();
                        ExecuteJavascript("changePage('profile');");
                    }
                    else
                    {
                        ExecuteJavascript("changePage('profile');");
                    }

                    if (resized)
                    {
                        File.Delete(PictureToUpload + ".tmp");
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                    ExecuteJavascript("changePage('profile');");
                }
            }
        }

        public void OnGetEditProfileData(ProfileData profileData)
        {
            ExecuteJavascript(string.Concat(new object[]
            {
                "OnGetEditProfileInfo('",
                profileData.Picture,
                "', '",
                profileData.City.Replace("\n", "<br>").Replace("'", "\\\'"),
                "', '",
                profileData.Job.Replace("\n", "<br>").Replace("'", "\\\'"),
                "', '",
                profileData.Activities.Replace("\n", "<br>").Replace("'", "\\\'"),
                "', '",
                profileData.Description.Replace("\n", "<br>").Replace("'", "\\\'"),
                "','",
                profileData.LookingFor,
                "', '",
                profileData.LookingForDescription.Replace("\n", "<br>").Replace("'", "\\\'"),
                "', ",
                profileData.Gender,
                ");"
            }));
            HideWorkingLoader();
        }

        #endregion Edit Profile

        #region View Profile

        public void OnGetProfileData(ProfileData profileData)
        {
            var js = @"OnGetProfileData('" + profileData.Id + "', '" + profileData.Username + "', '" + profileData.Picture + "', " + (profileData.IsBlocked ? "true" : "false") +", '" + profileData.PhotosCount + "', '" + profileData.City.Replace("\n", "<br>").Replace("'", "\\\'") + "', '" + profileData.Job.Replace("\n", "<br>").Replace("'", "\\\'") + "', '" + profileData.Activities.Replace("\n", "<br>").Replace("'", "\\\'") + "', '" + profileData.Description.Replace("\n", "<br>").Replace("'", "\\\'") + "', '" + profileData.LookingFor + "', '" + profileData.LookingForDescription.Replace("\n", "<br>").Replace("'", "\\\'") + "', " + profileData.Gender + ", " + profileData.Age + ", " + (profileData.Followed ? 1 : 0) + ");";
            ExecuteJavascript(js);
            HideWorkingLoader();
        }

        public void OnGetProfilePictures(List<PostData> photos)
        {
            if (photos != null)
            {
                foreach (var photo in photos)
                {
                    if (!string.IsNullOrEmpty(photo.AttachmentId))
                    {
                        ExecuteJavascript(string.Format("OnGetProfilePhoto('{0}', '{1}', '{2}');", photo.Id, photo.AttachmentId, photo.Content));
                    }
                }
            }
            HideWorkingLoader();
        }

        #endregion View Profile

        public void OnReturnToHome()
        {
            ExecuteJavascript("changePage('home');");
            HideWorkingLoader();
        }

        #region Messages

        public void OnGetConversations(List<ConversationData> conversations)
        {
            if (conversations == null)
            {
                //Do something ?
            }
            else
            {
                foreach (var conversation in conversations)
                {
                    //Append to view!
                    var otherId = "";
                    var otherAvatar = "";
                    var otherName = "";
                    var myAvatar = "";
                    var otherGender = 0;
                    var myGender = 0;

                    if (conversation.User1Id == NetworkController.AccountId)
                    {
                        otherId = conversation.User2Id;
                        otherAvatar = conversation.User2Avatar;
                        otherName = conversation.User2Name;
                        otherGender = conversation.User2Gender;
                        myAvatar = conversation.User1Avatar;
                        myGender = conversation.User1Gender;
                    }
                    else
                    {
                        otherId = conversation.User1Id;
                        otherAvatar = conversation.User1Avatar;
                        otherName = conversation.User1Name;
                        otherGender = conversation.User1Gender;
                        myAvatar = conversation.User2Avatar;
                        myGender = conversation.User2Gender;
                    }
                    ExecuteJavascript("OnGetConversation('" + conversation.Id + "', '" + otherId + "', '" + otherName + "', '" + otherAvatar + "', " + otherGender + ", '" + myAvatar + "', " + myGender + ", '" + (!string.IsNullOrEmpty(conversation.LastMessage) ? conversation.LastMessage.Replace("\n", "<br>").Replace("'", "\\\'") : "") + "', " + (conversation.LastMessageFromMe ? 1 : 0) + ", " + (conversation.LastMessageSeen ? 1 : 0) + ");");
                    network.LoadMessages(conversation.Id, 1);
                }
            }
        }

        public void OnGetMessages(List<MessageData> messages)
        {
            if (messages == null)
            {
                //Do something ?
            }
            else
            {
                foreach (var message in messages)
                {
                    //Append to view!
                    ExecuteJavascript("OnGetMessage('" + message.ConversationId + "', '" + message.Id + "', '" + message.FromId + "', '" + message.AttachmentId + "', '" + message.Message.Replace("\n", "<br>").Replace("'", "\\\'") + "', '" + message.When.ToString() + "', " + ((message.SeenAt == null || !message.SeenAt.HasValue || message.SeenAt.Value.Year == 1) ? 0 : 1) + ", '" + ((message.SeenAt != null && message.SeenAt.HasValue) ? message.SeenAt.Value.ToString() : "") + "');");
                }
            }
        }

        public void OnMessageReceived(MessageData message, ConversationData conversation)
        {
            if (message != null)
            {
                //Append to view!
                var otherId = "";
                var otherAvatar = "";
                var otherName = "";
                var myAvatar = "";
                var otherGender = 0;
                var myGender = 0;

                if (conversation.User1Id == NetworkController.AccountId)
                {
                    otherId = conversation.User2Id;
                    otherAvatar = conversation.User2Avatar;
                    otherName = conversation.User2Name;
                    otherGender = conversation.User2Gender;
                    myAvatar = conversation.User1Avatar;
                    myGender = conversation.User1Gender;
                }
                else
                {
                    otherId = conversation.User1Id;
                    otherAvatar = conversation.User1Avatar;
                    otherName = conversation.User1Name;
                    otherGender = conversation.User1Gender;
                    myAvatar = conversation.User2Avatar;
                    myGender = conversation.User2Gender;
                }

                if (message.FromId != NetworkController.AccountId && (!HasWindowFocus || CurrentPage != "messages"))
                {
                    Bundle valuesForActivity = new Bundle();
                    valuesForActivity.PutString("ConversationId", message.ConversationId);

                    //Show Notification in android
                    Intent resultIntent = new Intent(this, typeof(MainActivity));
                    resultIntent.SetFlags(ActivityFlags.ClearTop | ActivityFlags.SingleTop);
                    // Pass some values to SecondActivity:
                    resultIntent.PutExtras(valuesForActivity);

                    // Construct a back stack for cross-task navigation:
                    TaskStackBuilder stackBuilder = TaskStackBuilder.Create(this);
                    stackBuilder.AddParentStack(Java.Lang.Class.FromType(typeof(MainActivity)));
                    stackBuilder.AddNextIntent(resultIntent);

                    // Create the PendingIntent with the back stack:
                    PendingIntent resultPendingIntent =
                        stackBuilder.GetPendingIntent(0, (int)PendingIntentFlags.UpdateCurrent);

                    // Build the notification:
                    NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                        .SetAutoCancel(true)                    // Dismiss from the notif. area when clicked
                        .SetContentIntent(resultPendingIntent)  // Start 2nd activity when the intent is clicked.
                        .SetContentTitle(otherName)      // Set its title
                        .SetSmallIcon(Resource.Drawable.Icon)  // Display this icon
                        .SetDefaults(-1)//Enable sound, vibration, light
                        .SetContentText(message.Message); // The message to display.

                    // Finally, publish the notification:
                    NotificationManager notificationManager = (NotificationManager)GetSystemService(NotificationService);
                    notificationManager.Notify(notificationId, builder.Build());
                    notificationId++;
                }

                ExecuteJavascript("OnGetConversation('" + conversation.Id + "', '" + otherId + "', '" + otherName + "', '" + otherAvatar + "', " + otherGender + ", '" + myAvatar + "', " + myGender + ", '" + (!string.IsNullOrEmpty(conversation.LastMessage) ? conversation.LastMessage.Replace("\n", "<br>").Replace("'", "\\\'") : "") + "', " + (conversation.LastMessageFromMe ? 1 : 0) + ", " + (conversation.LastMessageSeen ? 1 : 0) + ");");
                //Append message to view!
                ExecuteJavascript("OnGetMessage('" + message.ConversationId + "', '" + message.Id + "', '" + message.FromId + "', '" + message.AttachmentId + "', '" + message.Message.Replace("\n", "<br>").Replace("'", "\\\'") + "', '" + message.When.ToString() + "', " + ((message.SeenAt == null || !message.SeenAt.HasValue || message.SeenAt.Value.Year == 1) ? 0 : 1) + ", '" + ((message.SeenAt != null && message.SeenAt.HasValue) ? message.SeenAt.Value.ToString() : "") + "');");
            }
        }

        public void OnMessageSeen(MessageData message)
        {
            if (message != null && message.SeenAt.HasValue)
            {
                //set message to view!
                ExecuteJavascript("OnGetMessageSeen('" + message.FromId + "', '" + message.SeenAt.Value.ToString() + "');");
            }
        }

        public void OnConversationStarted(ConversationData conversation)
        {
            //Append to view!
            var otherId = "";
            var otherAvatar = "";
            var otherName = "";
            var myAvatar = "";
            var otherGender = 0;
            var myGender = 0;

            if (conversation.User1Id == NetworkController.AccountId)
            {
                otherId = conversation.User2Id;
                otherAvatar = conversation.User2Avatar;
                otherName = conversation.User2Name;
                otherGender = conversation.User2Gender;
                myAvatar = conversation.User1Avatar;
                myGender = conversation.User1Gender;
            }
            else
            {
                otherId = conversation.User1Id;
                otherAvatar = conversation.User1Avatar;
                otherName = conversation.User1Name;
                otherGender = conversation.User1Gender;
                myAvatar = conversation.User2Avatar;
                myGender = conversation.User2Gender;
            }
            ExecuteJavascript("OnGetConversation('" + conversation.Id + "', '" + otherId + "', '" + otherName + "', '" + otherAvatar + "', " + otherGender + ", '" + myAvatar + "', " + myGender + ", '" + (!string.IsNullOrEmpty(conversation.LastMessage) ? conversation.LastMessage : "") + "', " + (conversation.LastMessageFromMe ? 1 : 0) + ", " + (conversation.LastMessageSeen ? 1 : 0) + ");");
            ExecuteJavascript("ShowConversation('" + conversation.Id + "');");
            ExecuteJavascript("changePage('messages');");
            HideWorkingLoader();
        }

        #endregion Messages

        #region Follows

        public void OnGetProfileFollowers(List<FollowData> followers)
        {
            ExecuteJavascript("OnGetFollowersCount(" + followers.Count + ");");
            foreach (var follower in followers)
            {
                ExecuteJavascript("OnGetFollower('" + follower.Id + "', '" + follower.UserId + "', '" + follower.UserName + "', '" + follower.UserAvatar + "', " + follower.UserGender + ");");
            }
        }

        public void OnGetProfileFollowings(List<FollowData> followings)
        {
            ExecuteJavascript("OnGetFollowingsCount(" + followings.Count + ");");
            foreach (var following in followings)
            {
                ExecuteJavascript("OnGetFollowing('" + following.Id + "', '" + following.ProfileId + "', '" + following.ProfileName + "', '" + following.ProfileAvatar + "', " + following.ProfileGender + ");");
            }
        }

        #endregion Follows

        #region Notifications

        public void OnGetNotifications(List<NotificationData> notifications)
        {
            if (notifications != null)
            {
                foreach (var notification in notifications)
                {
                    //Append it to the view!
                    ExecuteJavascript("OnGetNotification('" + notification.Id + "', '" + notification.FromId + "', '" + notification.FromName + "', '" + notification.FromAvatar + "', " + notification.FromGender + ", " + ((int)notification.Type) + ",'" + notification.When.ToString() + "');");
                }
            }
        }

        public void OnGetNotification(NotificationData notification)
        {
            if (notification != null)
            {
                //Trigger phone notification here!
                if (CurrentPage != "notifications" && notification.Type != NotificationTypes.View)
                {
                    try
                    {
                        Bundle valuesForActivity = new Bundle();
                        valuesForActivity.PutString("NotificationId", notification.Id);
                        valuesForActivity.PutInt("NotificationType", (int)notification.Type);

                        //Show Notification in android
                        Intent resultIntent = new Intent(currentContext, typeof(MainActivity));
                        resultIntent.SetFlags(ActivityFlags.ClearTop | ActivityFlags.SingleTop);
                        // Pass some values to SecondActivity:
                        resultIntent.PutExtras(valuesForActivity);

                        // Construct a back stack for cross-task navigation:
                        TaskStackBuilder stackBuilder = TaskStackBuilder.Create(this);
                        stackBuilder.AddParentStack(Java.Lang.Class.FromType(typeof(MainActivity)));
                        stackBuilder.AddNextIntent(resultIntent);

                        // Create the PendingIntent with the back stack:
                        PendingIntent resultPendingIntent =
                            stackBuilder.GetPendingIntent(0, (int)PendingIntentFlags.UpdateCurrent);
                        resultIntent.SetFlags(ActivityFlags.ClearTop | ActivityFlags.SingleTop);
                        var message = "";
                        switch (notification.Type)
                        {
                            case NotificationTypes.View:
                                message = "Viewed your profile.";
                            break;

                            case NotificationTypes.Poke:
                                message = "Poked you.";
                            break;

                            case NotificationTypes.Follow:
                                message = "Followed your profile.";
                            break;

                            case NotificationTypes.Unfollow:
                                message = "Unfollowed your profile.";
                            break;
                            case NotificationTypes.CallRequest:
                                ExecuteJavascript(string.Format("OnDeleteMyRequest('{0}', 0);", notification.FromId));
                                message = "Sent you a call request.";
                            break;
                            case NotificationTypes.CallRequestAccepted:
                                ExecuteJavascript(string.Format("OnDeleteMyRequest('{0}', 0);", notification.FromId));
                                message = "Accepted your call request.";
                            break;
                            case NotificationTypes.CallRequestDeclined:
                                ExecuteJavascript(string.Format("OnDeleteMyRequest('{0}', 0);", notification.FromId));
                                message = "Declined your call request.";
                            break;
                            case NotificationTypes.CallMissed:
                                message = "You missed a call.";
                            break;
                            case NotificationTypes.CallEnded:
                                message = "Call terminated.";
                            break;
                            case NotificationTypes.MapRequest:
                            {
                                ExecuteJavascript(string.Format("OnDeleteMyRequest('{0}', 1);", notification.FromId));
                                message = "Sent you a position request.";
                            }
                            break;
                            case NotificationTypes.MapRequestAccepted:
                            {
                                ExecuteJavascript(string.Format("OnDeleteMyRequest('{0}', 1);", notification.FromId));
                                message = "Accepted your position request.";
                            }
                            break;
                            case NotificationTypes.MapRequestDeclined:
                            {
                                ExecuteJavascript(string.Format("OnDeleteMyRequest('{0}', 1);", notification.FromId));
                                message = "Declined your position request.";
                            }
                            break;
                            case NotificationTypes.MsgRequestAccepted:
                            {
                                ExecuteJavascript(string.Format("OnDeleteMyRequest('{0}', 2);", notification.FromId));
                                message = "Accepted your message request.";
                            }
                            break;
                            case NotificationTypes.MsgRequestDeclined:
                            {
                                ExecuteJavascript(string.Format("OnDeleteMyRequest('{0}', 2);", notification.FromId));
                                message = "Declined your message request.";
                            }
                            break;
                        }

                        Bitmap icon;

                        if (string.IsNullOrEmpty(notification.FromAvatar))
                        {
                            if (notification.FromGender == 0)
                                icon = BitmapFactory.DecodeResource(Resources, Resource.Drawable.avatarmale);
                            else
                                icon = BitmapFactory.DecodeResource(Resources, Resource.Drawable.avatarfemale);
                        }
                        else
                        {
                            icon = getBitmapFromURL(NetworkController.WebServerAddress + "?avatar=" + notification.FromAvatar);
                            if (icon == null)
                            {
                                if (notification.FromGender == 0)
                                    icon = BitmapFactory.DecodeResource(Resources, Resource.Drawable.avatarmale);
                                else
                                    icon = BitmapFactory.DecodeResource(Resources, Resource.Drawable.avatarfemale);
                            }
                        }

                        // Build the notification:
                        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                            .SetAutoCancel(true)                    // Dismiss from the notif. area when clicked
                            .SetContentIntent(resultPendingIntent)  // Start 2nd activity when the intent is clicked.
                            .SetContentTitle(notification.FromName)      // Set its title
                            .SetLargeIcon(icon)  // Display this icon
                            .SetSmallIcon(Resource.Drawable.Icon)
                            .SetDefaults(-1)//Enable sound, vibration, light
                            .SetContentText(message); // The message to display.

                        // Finally, publish the notification:
                        NotificationManager notificationManager = (NotificationManager)GetSystemService(NotificationService);
                        notificationManager.Notify(notificationId, builder.Build());
                        notificationId++;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.StackTrace);
                    }
                }
                //Append it to the view!
                ExecuteJavascript("OnGetNotification('" + notification.Id + "', '" + notification.FromId + "', '" + notification.FromName + "', '" + notification.FromAvatar + "', " + notification.FromGender + ", " + ((int)notification.Type) + ",'" + notification.When.ToString() + "');");
            }
        }

        #endregion Notifications

        #region Posts

        public void OnGetPost(PostData post)
        {
            if (post == null) return;
            
            ExecuteJavascript("OnGetPost('" + post.Id + "','" + post.UserId + "','" + post.UserName + "','" + post.UserAvatar + "', " + post.Gender + ",'" + post.Content.Replace("\n", "<br>").Replace("'", "\\\'") + "','" + post.AttachmentId + "','" + post.DateAt.ToString() + "')");
        }

        public void OnGetPostDeleted(string postId)
        {
            if (string.IsNullOrEmpty(postId)) return;

            ExecuteJavascript("OnGetPostDeleted('" + postId + "');");
        }

        public void OnGetPosts(List<PostData> posts)
        {
            if (posts == null) return;

            foreach (var post in posts)
            {
                //if (post.UserId == NetworkController.AccountId) post.UserId = "0";

                ExecuteJavascript("OnGetPost('" + post.Id + "','" + post.UserId + "','" + post.UserName + "','" + post.UserAvatar + "', " + post.Gender + ",'" + post.Content.Replace("\n", "<br>").Replace("'", "\\\'") + "','" + post.AttachmentId + "','" + post.DateAt.ToString() + "')");
            }
        }

        public void OnGetProfilePosts(List<PostData> posts)
        {
            if (posts == null) return;

            foreach (var post in posts)
            {
                //if (post.UserId == NetworkController.AccountId) post.UserId = "0";

                ExecuteJavascript("OnGetProfilePost('" + post.Id + "','" + post.UserId + "','" + post.UserName + "','" + post.UserAvatar + "', " + post.Gender + ",'" + post.Content.Replace("\n", "<br>").Replace("'", "\\\'") + "','" + post.AttachmentId + "','" + post.DateAt.ToString() + "')");
            }
        }

        public void OnGetComments(List<CommentData> comments)
        {
            if (comments == null) return;

            foreach (var comment in comments)
            {
                ExecuteJavascript("OnGetComment('" + comment.Id + "','" + comment.PostId + "','" + comment.PostUserId + "','" + comment.UserId + "','" + comment.UserName + "','" + comment.UserAvatar + "', " + comment.Gender + ",'" + comment.Content.Replace("\n", "<br>").Replace("'", "\\\'") + "','" + comment.AttachmentId + "','" + comment.DateAt.ToString() + "');");
            }
        }

        public void OnGetComment(CommentData comment)
        {
            if (comment == null) return;

            ExecuteJavascript("OnGetComment('" + comment.Id + "','" + comment.PostId + "','" + comment.PostUserId + "','" + comment.UserId + "','" + comment.UserName + "','" + comment.UserAvatar + "', " + comment.Gender + ",'" + comment.Content.Replace("\n", "<br>").Replace("'", "\\\'") + "','" + comment.AttachmentId + "','" + comment.DateAt.ToString() + "');");
        }

        public void OnGetCommentDeleted(string commentId)
        {
            if (string.IsNullOrEmpty(commentId)) return;

            ExecuteJavascript("OnGetCommentDeleted('" + commentId + "');");
        }

        #endregion Posts

        #region Payments

        public void OnGetTransactions(List<TransactionData> transactions)
        {
            if (transactions == null) return;
            foreach (var transaction in transactions)
            {
                ExecuteJavascript(string.Format("OnGetTransaction('{0}', '{1}', '{2}', '{3}', '{4}', '{5}')", transaction.Id, transaction.TokenId, transaction.Amount, transaction.Currency, transaction.DateAt.ToString(), transaction.ExpireAt.ToString()));
            }
            HideWorkingLoader();
        }

        public void OnGetPaymentResult(bool result)
        {
            ExecuteJavascript(string.Format("OnGetPaymentResult({0})", result ? "true" : "false"));
            HideWorkingLoader();
        }

        public void OnGetVerifyMembership(bool result)
        {
            ExecuteJavascript(string.Format("OnGetVerifyMembership({0})", result ? "true" : "false"));
            HideWorkingLoader();
        }

        public void OnPremiumRequired()
        {
            ExecuteJavascript("OnPremiumRequired();");
            HideWorkingLoader();
        }

        #endregion Payments

        #region Blocks
        public void OnGetBlockedUsers(List<BlockData> blockedUsers)
        {
            if(blockedUsers.Count == 0)
            {
                ExecuteJavascript("$('#no-blocked-users').show();");
            }else
            {
                ExecuteJavascript("$('#no-blocked-users').hide();");
            }
            ExecuteJavascript("$('.blocked-users').html('');");
            foreach(var blockedUser in blockedUsers)
            {
                ExecuteJavascript(string.Format("OnGetBlockedUser('{0}', '{1}', '{2}', '{3}', '{4}');", blockedUser.ProfileId, blockedUser.ProfileUserName, blockedUser.ProfileAvatar, blockedUser.ProfileGender, blockedUser.DateAt.ToString()));
            }
            HideWorkingLoader();
        }
        #endregion

        #region Calls
        public void OnCallVideoChanged(bool enabled)
        {
            if(enabled)
            {
                callClient.RemoteVideoEnabled = true;
                VideoEnabled();
            }
            else
            {
                callClient.RemoteVideoEnabled = false;
                VideoEnabled();
                using (var h = new Handler(Looper.MainLooper))
                {
                    h.Post(() => {
                        imageView.SetImageDrawable(null);
                        imageView.RefreshDrawableState();
                        imageView.ClearFocus();
                        //webView.ClearFocus();
                    });
                }
            }
        }
        public void OnGetRequests(List<RequestData> requests)
        {
            if (requests == null) return;
            foreach(var request in requests)
            {
                OnGetRequest(request, false);
            }
            if(requests.Count == 0)
            {
                ExecuteJavascript("$('.requests-feed').append('<center id=\"no-request\"><b>You have no requests.</b></center>');");
            }
        }
        public void OnGetRequest(RequestData callRequest, bool showRequest = false)
        {
            if (callRequest == null) return;

            if(callRequest.UserId == NetworkController.AccountId)
            {
                //I Sent the request
                ExecuteJavascript(string.Format("OnGetRequest('{0}', '{1}', '{2}', '{3}', '{4}', true, {5}, {6});", callRequest.ProfileId, callRequest.ProfileName, callRequest.ProfileAvatar, callRequest.ProfileGender, callRequest.DateAt, callRequest.RequestType, callRequest.Accepted ? "true" : "false"));
                if(showRequest) ShowPage("requests");
            }
            else
            {
                //I received the request
                if (!callRequest.Accepted)
                {
                    try
                    {
                        Bundle valuesForActivity = new Bundle();
                        valuesForActivity.PutString("RequestId", callRequest.Id);

                        //Show Notification in android
                        Intent resultIntent = new Intent(currentContext, typeof(MainActivity));

                        // Pass some values to SecondActivity:
                        resultIntent.PutExtras(valuesForActivity);

                        // Construct a back stack for cross-task navigation:
                        TaskStackBuilder stackBuilder = TaskStackBuilder.Create(this);
                        stackBuilder.AddParentStack(Java.Lang.Class.FromType(typeof(MainActivity)));
                        stackBuilder.AddNextIntent(resultIntent);

                        // Create the PendingIntent with the back stack:
                        PendingIntent resultPendingIntent =
                            stackBuilder.GetPendingIntent(0, (int)PendingIntentFlags.UpdateCurrent);

                        var message = "";
                        switch (callRequest.RequestType)
                        {
                            case 0://Call Request
                            {
                                message = "Sent you a call request.";
                            }
                            break;
                            case 1://Mapping Request
                            {
                                message = "Sent you a position request.";
                            }
                            break;
                            case 2://Message Request
                            {
                                message = "Sent you a private message request.";
                            }
                            break;
                        }

                        Bitmap icon;

                        if (string.IsNullOrEmpty(callRequest.UserAvatar))
                        {
                            if (callRequest.UserGender == 0)
                                icon = BitmapFactory.DecodeResource(Resources, Resource.Drawable.avatarmale);
                            else
                                icon = BitmapFactory.DecodeResource(Resources, Resource.Drawable.avatarfemale);
                        }
                        else
                        {
                            icon = getBitmapFromURL(NetworkController.WebServerAddress + "?avatar=" + callRequest.UserAvatar);
                            if (icon == null)
                            {
                                if (callRequest.UserGender == 0)
                                    icon = BitmapFactory.DecodeResource(Resources, Resource.Drawable.avatarmale);
                                else
                                    icon = BitmapFactory.DecodeResource(Resources, Resource.Drawable.avatarfemale);
                            }
                        }

                        // Build the notification:
                        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                            .SetAutoCancel(true)                    // Dismiss from the notif. area when clicked
                            .SetContentIntent(resultPendingIntent)  // Start 2nd activity when the intent is clicked.
                            .SetContentTitle(callRequest.UserName)      // Set its title
                            .SetLargeIcon(icon)  // Display this icon
                            .SetSmallIcon(Resource.Drawable.Icon)
                            .SetDefaults(-1)//Enable sound, vibration, light
                            .SetContentText(message); // The message to display.

                        // Finally, publish the notification:
                        NotificationManager notificationManager = (NotificationManager)GetSystemService(NotificationService);
                        notificationManager.Notify(notificationId, builder.Build());
                        notificationId++;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.StackTrace);
                    }
                }
                ExecuteJavascript(string.Format("OnGetRequest('{0}', '{1}', '{2}', '{3}', '{4}', false, {5}, {6});", callRequest.UserId, callRequest.UserName, callRequest.UserAvatar, callRequest.UserGender, callRequest.DateAt, callRequest.RequestType, callRequest.Accepted ? "true" : "false"));
            }
            HideWorkingLoader();
        }
        public void OnStartCalling(CallData callData)
        {
            if (callData == null) return;

            ExecuteJavascript(string.Format("OnStartCalling('{0}', '{1}', '{2}', '{3}', '{4}');", callData.Id, callData.ProfileId, callData.ProfileName, callData.ProfileAvatar, callData.ProfileGender));
            RingTonePlayer = MediaPlayer.Create(this, Resource.Raw.RingTone);
            //RingTonePlayer.SetAudioStreamType(Android.Media.Stream.System);
            //RingTonePlayer.Prepare();
        }
        public void OnReceiveCall(CallData callData)
        {
            if (callData == null) return;

            ExecuteJavascript(string.Format("OnReceiveCall('{0}', '{1}', '{2}', '{3}', '{4}');", callData.Id, callData.UserId, callData.UserName, callData.UserAvatar, callData.UserGender));
            RingTonePlayer = MediaPlayer.Create(this, Resource.Raw.RingTone);
            //RingTonePlayer.SetAudioStreamType(Android.Media.Stream.System);
            //RingTonePlayer.Prepare();
        }
        public void OnCallRingTone()
        {
            if (!RingTonePlayer.IsPlaying)
            {
                RingTonePlayer.Start();
            }
        }
        public void OnStartCall(CallData callData, string hostName, int port)
        {
            if(callClient != null)
            {
                callClient.Destroy();
                callClient = null;
            }

            if(MicCapture != null)
            {
                MicCapture.Stop();
                MicCapture = null;
            }
            if(RingTonePlayer != null)
            {
                RingTonePlayer.Stop();
                RingTonePlayer.Release();
                RingTonePlayer = null;
            }

            ExecuteJavascript(string.Format("OnStartCall('{0}');", callData.Id));
            bool connected = false;
            try
            {
                #if DEBUG
                //hostName = "192.168.1.106";
                #endif
                callClient = new CallClient(hostName, port, callData.Id);
                callClient.OnGetAudio += OnGetSyncAudio;
                callClient.OnGetVideo += OnGetSyncVideo;
                callClient.OnDisconnected += OnGetCallDisconnected;
                callClient.OnVideoEnabled += OnCallVideoChanged;
                connected = callClient.Connect();
                ExecuteJavascript("OnCallConnected('" + callData.Id + "');");
                SetAppFullscreen(true);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
            if (connected)
            {
                MicCapture = new AudioCapture();
                MicCapture.OnAudioReady += OnAudioReadyToBeSent;
                MicCapture.Start();
            }
            else
                network.EndCall(callData.Id);
        }
        public void OnCallEnded(CallData callData)
        {
            if (callData == null) return;

            if(RingTonePlayer != null)
            {
                RingTonePlayer.Stop();
                RingTonePlayer.Release();
                RingTonePlayer = null;
            }

            CamCapture?.Stop();

            if (callClient != null)
            {
                callClient.Destroy();
                callClient = null;
            }

            if (MicCapture != null)
            {
                MicCapture.Stop();
                MicCapture = null;
            }
            AudioPlayer.Stop();
            ExecuteJavascript(string.Format("OnCallEnded('{0}');", callData.Id));
            SetAppFullscreen(false);
        }
        
        public void OnAudioReadyToBeSent(byte[] bytes)
        {
            callClient?.SyncAudio(bytes);
        }

        public void OnVideoReadyToBeSent(byte[] bytes)
        {
            callClient?.SyncVideo(bytes);
        }
        
        public void OnGetSyncAudio(byte[] bytes)
        {
            AudioPlayer.WriteBytes(bytes);
        }

        public void OnGetSyncVideo(byte[] bytes)
        {
            if (bytes == null) return;
            if (callClient.RemoteVideoEnabled == false) return;

            Bitmap bmp = BitmapFactory.DecodeByteArray(bytes, 0, bytes.Length);

            using (var h = new Handler(Looper.MainLooper))
            {
                h.Post(() => { imageView.SetImageBitmap(bmp); });
            }

            //var encoded = Base64.EncodeToString(bytes, Base64Flags.Default);
            //var encoded = "";
            //ExecuteJavascript(string.Format("OnGetCallVideoFrame('{0}')", encoded));
        }

        public void OnGetCallDisconnected()
        {
            network.EndCall(callClient.CallId);
        }
        #endregion

        #region Mapping
        public void OnGetPosition(double latitude, double longitude)
        {
            ExecuteJavascript(string.Format("OnGetPosition({0}, {1}, {2}, {3});", latitude.ToString().Replace(',', '.'), longitude.ToString().Replace(',', '.'), NetworkController.Latitude.ToString().Replace(',', '.'), NetworkController.Longitude.ToString().Replace(',', '.')));
        }
        #endregion

        #region Groups
        public void OnGetMyGroups(List<GroupData> groups)
        {

        }
        public void OnGetUserGroups(List<GroupData> groups)
        {

        }
        public void OnGetGroupCreated(int result, GroupData group)
        {
            if(result == 0 || group == null)
            {
                //Name Error!
            }else
            {
                //Group Created
            }
        }
        public void OnGetGroupInvite(int result, GroupInvitationData invitation)
        {
            if (result == 0 || invitation == null)
            {
                //Name Error!
            }
            else
            {
                //Group Invitation Created
            }
        }
        public void OnGetGroupRequest(int result, GroupRequestData request)
        {
            if (result == 0 || request == null)
            {
                //Name Error!
            }
            else
            {
                //Group Request Created
            }
        }
        public void OnGetGroupPostCreated(GroupPostData group)
        {

        }
        public void OnGetGroupPosts(List<GroupPostData> posts)
        {

        }
        public void OnGetGroupCommentCreated(GroupCommentData comment)
        {

        }
        public void OnGetGroupPostComments(List<GroupCommentData> comments)
        {

        }
        public void OnGetGroupMembers(List<GroupMemberData> members)
        {

        }
        public void OnGetGroupSearchResults(List<GroupData> groups)
        {

        }
        public void OnGetGroupPendingInvitations(List<GroupInvitationData> invitations)
        {

        }
        public void OnGetGroupMyInvitations(List<GroupInvitationData> invitations)
        {

        }
        public void OnGetGroupMyRequests(List<GroupRequestData> request)
        {

        }
        public void OnGetGroupPendingRequests(List<GroupRequestData> request)
        {

        }

        #endregion

        #endregion Events Receivers

        #region Camera Surface
        public void OnSurfaceTextureAvailable(SurfaceTexture surface, int w, int h)
        {
            videoView.LayoutParameters = new FrameLayout.LayoutParams(w, h);
            CamCapture.UpdateSurface(surface);
        }
        public bool OnSurfaceTextureDestroyed(SurfaceTexture surface)
        {
            CamCapture.UpdateSurface(surface);
            return true;
        }

        public void OnSurfaceTextureSizeChanged(SurfaceTexture surface, int w, int h)
        {
            videoView.LayoutParameters = new FrameLayout.LayoutParams(w, h);
            CamCapture.UpdateSurface(surface);
        }

        public void OnSurfaceTextureUpdated(SurfaceTexture surface)
        {
            
        }

        public void OnAccuracyChanged(Sensor sensor, [GeneratedEnum] SensorStatus accuracy)
        {
            // We don't want to do anything here.
        }
        bool ignoreTouch = false;
        public void OnSensorChanged(SensorEvent e)
        {
            lock (_syncLock)
            {
                var x = e.Values[0];
                //var y = e.Values[1];
                //var z = e.Values[2];
                //var axis =  string.Format("x={0:f}, y={1:f}, y={2:f}", x, y, z);
                //Console.WriteLine(axis);

                if(x < 9 && x > 4)
                {
                    //Lock
                    ignoreTouch = true;
                }
                else if(x < -9 && x > -4)
                {
                    //Lock
                    ignoreTouch = true;
                }
                else
                {
                    //Unlock
                    ignoreTouch = false;
                }
            }
        }
        
        public bool OnTouch(View v, MotionEvent e)
        {
            return ignoreTouch && callClient != null;
        }

        public void SetAppFullscreen(bool fullscreen)
        {
            if(fullscreen)
            {
                Window.AddFlags(WindowManagerFlags.Fullscreen);
            }
            else
            {
                Window.ClearFlags(WindowManagerFlags.Fullscreen);
            }
        }

        #endregion
    }
}