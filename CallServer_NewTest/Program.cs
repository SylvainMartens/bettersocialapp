﻿using System;
using System.Threading;
using Server.Shared;
using NetworkCommsDotNet;
using NetworkCommsDotNet.Tools;

namespace CallServer
{
	public class Program
	{
        static int Port = 4298;
        private static object consoleLock = new object();

        static void Main(string[] args)
        {
            try
            {
                Console.SetBufferSize(120, 200);
                Console.SetWindowSize(120, 25);
            }
            catch (NotImplementedException) { }

            AppDomain.CurrentDomain.ProcessExit += delegate (object sender, EventArgs e)
            {
				NetworkServerUdp.Stop();
            };
            
			Thread.CurrentThread.Name = "MainThread";

            Console.WriteLine("Initiating Call Server Server.\n");

			Interface.Setup(OnLog, OnWarning, OnError, OnFatal, OnException);

            SetDebugTimeouts();

			bool started = false;
            try
            {
				NetworkServerUdp.Port = Port;
                NetworkServerUdp.Start();
                started = true;
            }
            catch(Exception ex)
            {
                //If an error was uncaught by the examples we can log the exception to a file here
                LogTools.LogException(ex, "NetworkServerError");
                NetworkComms.Shutdown();
                Console.WriteLine(ex.ToString());
            }

			if(started)
            {
                CallServer.Initialize();
            }

            while(true)
            {
                Console.ReadKey(true);
            }
        }

        private static void SetDebugTimeouts()
        {
            NetworkComms.ConnectionEstablishTimeoutMS = 10000;
            NetworkComms.PacketConfirmationTimeoutMS = 5000;
            NetworkComms.ConnectionAliveTestTimeoutMS = 1000;
        }

        private static void OnLog(string message)
        {
            lock (consoleLock)
            {
                Console.WriteLine(message);
            }
        }

        private static void OnWarning(string message)
        {
            lock (consoleLock)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("[Warning] " + message);
                Console.ResetColor();
            }
        }

        private static void OnError(string message)
        {
            lock (consoleLock)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("[Error] " + message);
                Console.ResetColor();
            }
        }

        private static void OnException(Exception message)
        {
            lock (consoleLock)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("[Exception] " + message);
                Console.ResetColor();
            }
        }

        private static void OnFatal(string message)
        {
            lock (consoleLock)
            {
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.BackgroundColor = ConsoleColor.Gray;
                Console.WriteLine("[Fatal] " + message);
                Console.ResetColor();
            }
        }
	}
}
