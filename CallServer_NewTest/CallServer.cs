﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using NetworkCommsDotNet.Connections;
using SocialAPP.Shared;
using Server.Shared;

namespace CallServer
{
	public static class CallServer
	{
        static List<Call> Calls = new List<Call>();
		static object callsLock = new object();

        public static void Initialize()
        {
            NetworkServerUdp.OnClientConnected += OnClientConnected;
            NetworkServerUdp.OnClientDisconnected += OnClientDisconnected;
            NetworkServerUdp.OnMessageReceived += OnMessageReceived;
            Task.Factory.StartNew(async () =>
            {
                while (true)
                {
                    var userCount = NetworkServerUdp.ConnectionsLenght;
                    Console.WriteLine("Users connected = " + userCount);

                    await Task.Delay(1000 * 30);
                }
            });
        }

        public static void OnClientConnected(Connection connection)
        {
        }

        public static void OnClientDisconnected(Connection connection)
        {
            if (connection == null) return;
			lock(callsLock)
			{
				var call = Calls.FirstOrDefault(x => x.Connection1.ConnectionInfo.RemoteEndPoint.ToString() == connection.ConnectionInfo.RemoteEndPoint.ToString() || x.Connection2.ConnectionInfo.RemoteEndPoint.ToString() == connection.ConnectionInfo.RemoteEndPoint.ToString());
	            if (call != null)
	            {
	                try
	                {
	                    call.Connection1?.Close();
	                }
	                catch (Exception ex)
	                {
	                    Interface.LogError(ex.StackTrace);
	                }
	                try
	                {
	                    call.Connection2?.Close();
	                }
	                catch (Exception ex)
	                {
	                    Interface.LogError(ex.StackTrace);
	                }
	                Calls.Remove(call);
	                Interface.Log("Calls removed.");
	            }
			}
        }

		public static void OnMessageReceived(Connection connection, byte id, byte tag, byte[] data)
        {
            try
            {
                switch (id)
                {
					case CallTags.SET_CALL:
	                {
	                    Interface.Log("Received [SET_CALL] from " + connection.ConnectionInfo.RemoteEndPoint.ToString() + ".");
	                    
						using(NetworkReader reader = new NetworkReader(data))
						{
							var callId = reader.ReadString();
		                    lock (callsLock)
		                    {
		                        var call = Calls.FirstOrDefault(x => x.Id == callId);
		                        if (call == null)
		                        {
		                            call = new Call()
		                            {
		                                Id = callId,
		                                Connection1 = connection
		                            };
		                            Calls.Add(call);
		                        }
		                        else
		                        {
		                            call.Connection2 = connection;
		                        }
		                    }
						}
	                }
	                break;
	                case CallTags.SYNC_AUDIO:
	                {
	                    //Interface.Log("Received [SYNC_AUDIO] from " + connection.ConnectionInfo.RemoteEndPoint.ToString() + ".");
						//lock(callsLock)
						{
		                    var call = Calls.FirstOrDefault(x => x.Connection1 == connection || x.Connection2 == connection);
		                    if (call == null) return;
		                    if(call.Connection1 == connection)
		                    {
								call.Connection2?.SendReply(id, tag, data, false);
		                    }else if(call.Connection2 == connection)
		                    {
								call.Connection1?.SendReply(id, tag, data, false);
		                    }
						}
	                }
	                break;
	                case CallTags.SYNC_VIDEO:
	                {
	                    //Interface.Log("Received [SYNC_VIDEO] from " + connection.ConnectionInfo.RemoteEndPoint.ToString() + ".");
						//lock(callsLock)
						{
		                    var call = Calls.FirstOrDefault(x => x.Connection1 == connection || x.Connection2 == connection);
		                    if (call == null) return;
		                    if (call.Connection1 == connection)
		                    {
								call.Connection2?.SendReply(id, tag, data, false);
		                    }
		                    else if (call.Connection2 == connection)
		                    {
								call.Connection1?.SendReply(id, tag, data, false);
		                    }
						}
	                }
	                break;
	                case CallTags.DISCONNECTED:
	                {
	                    Interface.Log("Received [DISCONNECTED] from " + connection.ConnectionInfo.RemoteEndPoint.ToString() + ".");
						lock(callsLock)
						{
		                    var call = Calls.FirstOrDefault(x => x.Connection1 == connection || x.Connection2 == connection);
		                    if (call != null) Calls.Remove(call);
						}
	                    connection.Close();
	                }
	                break;
				}
			}catch(Exception ex)
			{
				Interface.LogError(ex);
			}
		}
	}
}