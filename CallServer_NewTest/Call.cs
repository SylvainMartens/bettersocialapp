﻿using NetworkCommsDotNet.Connections;

namespace CallServer
{
    public class Call
    {
        public string Id { get; set; }
        public Connection Connection1 { get; set; }
        public Connection Connection2 { get; set; }
    }
}